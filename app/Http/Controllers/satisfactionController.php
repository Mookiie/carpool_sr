<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class satisfactionController extends Controller
{
  public function satisfaction(Request $req)
  {
        return view('dashboard.satisfaction');
  }

  public function satisfactionDB(Request $req)
  {
    $bk_id = $req->input("bk_id");
    $car = $req->input("car");
    $driver = $req->input("driver");
    $Suggestion = $req->input("Suggestion");

    $sql_insert = DB::table("tb_booking")
                          ->where("bk_id","=",$bk_id)
                          ->update([ 'satisfaction_car'=>$car,
                                     'satisfaction_driver'=>$driver,
                                     'satisfaction_ suggestion'=>$Suggestion
                                  ]);
  }
}
