<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class ReportController extends Controller
{
    public function ReportBookingShow()
    {
      if (session()->has('user'))
      {
        $username = session()->get('user');
        $users = DB::table('tb_employee')
                ->join("tb_department",function($join){
                        $join->on('tb_employee.dep_id', '=', 'tb_department.dep_id')
                             ->on("tb_employee.com_id","=","tb_department.com_id");
                              })
                ->join('tb_employee_login', 'tb_employee.emp_id', '=', 'tb_employee_login.emp_id')
                ->where('tb_employee.emp_email','=',$username)->get();
        return view('Report.reportbooking',['users'=>$users]);
      }
      else
      {
        exit("<script>window.location='/';</script>");
      }
    }

    public function ReportBookingSearch(Request $req)
    {
          $type = $req->input('type');
          $typeSearch = $req->input('typeSearch');
          $txtSearch = $req->input('txtSearch');
          $bk_status = $req->input('bk_status');
          $date1 = $req->input('date1');
          $date2 = $req->input('date2');
          if ($req->input('month')<10) {
            $month = '0'.$req->input('month');
          }
          else {
            $month = $req->input('month');
          }
          $date3 = $req->input('year1').'-'.$month;
          $date4 = $req->input('year2');
          $com_id = $req->input('com_id');
          $dep_id = $req->input('bk_dep');
          return view('Models.Report.reportbooking_tb',['type'=>$type,'typeSearch'=>$typeSearch,
                      'txtSearch'=>$txtSearch,'bk_status'=>$bk_status,'date1'=>$date1,'date2'=>$date2,
                      'date3'=>$date3,'date4'=>$date4,'com_id'=>$com_id,'dep_id'=>$dep_id]);
    }

    public function ReportBookingSearchDep(Request $req)
    {
      $dep_id = $req->input('dep_id');
      $com_id = $req->input('com_id');
      if ($dep_id == 'All') {
        $sql_emp = DB::table('tb_employee')->where('com_id','=',$com_id)->get();
      }
      else {
        $sql_emp = DB::table('tb_employee')->where('com_id','=',$com_id)->where('job_id','=',$dep_id)->get();
      }
      echo "<option value='All' selected >ทุกคน</option>";
      foreach ($sql_emp as $emp) {
      echo "<option value='".$emp->emp_id."'>".$emp->emp_fname." ".$emp->emp_lname."</option>";
      }

    }

    public function ReportCarShow(Request $req)
    {
      if (session()->has('user'))
      {
        $username = session()->get('user');
        $users = DB::table('tb_employee')
                ->join("tb_department",function($join){
                        $join->on('tb_employee.dep_id', '=', 'tb_department.dep_id')
                             ->on("tb_employee.com_id","=","tb_department.com_id");
                              })
                ->join('tb_employee_login', 'tb_employee.emp_id', '=', 'tb_employee_login.emp_id')
                ->where('tb_employee.emp_email','=',$username)->get();
        return view('Report.reportcar',['users'=>$users,'depcar'=>$req->input('w')]);
      }
      else
      {
        exit("<script>window.location='/';</script>");
      }
    }

    public function ReportCarSearch(Request $req)
    {
          return view('Models.Report.reportcar_tb',['date'=>$req->input('date'),'dep_car'=>$req->input('dep_car'),'com_id'=>$req->input('com_id')]);
    }

    public function ReportCarShowOT(Request $req)
    {
      if (session()->has('user'))
      {
        $username = session()->get('user');
        $users = DB::table('tb_employee')
                ->join("tb_department",function($join){
                        $join->on('tb_employee.dep_id', '=', 'tb_department.dep_id')
                             ->on("tb_employee.com_id","=","tb_department.com_id");
                              })
                ->join('tb_employee_login', 'tb_employee.emp_id', '=', 'tb_employee_login.emp_id')
                ->where('tb_employee.emp_email','=',$username)->get();
        return view('Report.reportcarOT',['users'=>$users,'depcar'=>$req->input('w')]);
      }
      else
      {
        exit("<script>window.location='/';</script>");
      }
    }

    public function ReportCarSearchOT(Request $req)
    {
          return view('Models.Report.reportcar_tbOT',['date'=>$req->input('date'),'dep_car'=>$req->input('dep_car'),'com_id'=>$req->input('com_id')]);
    }

}
