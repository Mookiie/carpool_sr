<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mail\SendMail;
use Mail;
use DB;

class mailController extends Controller
{

    public function sendBooking(Request $req)
    {
      $id = $req->input('id');
      $sqlBooking = DB::table('tb_booking')->join('tb_employee', 'tb_employee.emp_id', '=', 'tb_booking.emp_id')->where('bk_id','=',$id)->select('tb_employee.emp_email')->get();
      foreach ($sqlBooking as $bk):
      Mail::send('Mail.mailbooking',['user' => 'Carpool Siamraj Service'], function ($m) use ($bk) {
           $m->from('sys.carpool@siamraj.com', 'Carpool Siamraj Service');
           $m->to($bk->emp_email)->subject('Carpool Siamraj Service : SUCCESS Booking ');
       });
       endforeach;
       $msg = array("success"=>true,"bk_id"=>$id);
      return Response(json_encode($msg));
    }

    public function sendEditBooking(Request $req)
    {
      $id = $req->input('id');
      $sqlBooking = DB::table('tb_booking')->join('tb_employee', 'tb_employee.emp_id', '=', 'tb_booking.emp_id')->where('bk_id','=',$id)->select('tb_employee.emp_email')->get();
      foreach ($sqlBooking as $bk):
      Mail::send('Mail.maileditbooking',['user' => 'Carpool Siamraj Service'], function ($m) use ($bk) {
           $m->from('sys.carpool@siamraj.com', 'Carpool Siamraj Service');
           $m->to($bk->emp_email)->subject('Carpool Siamraj Service : SUCCESS Edit Booking ');
       });
       endforeach;
       $msg = array("success"=>true,"bk_id"=>$id);
      return Response(json_encode($msg));
    }

    public function sendBookingApprove(Request $req)
    {
      $id = $req->input('bk_id');
      $sqlBooking = DB::table('tb_booking')->join('tb_employee', 'tb_employee.emp_id', '=', 'tb_booking.emp_id')->join('tb_employee_login', 'tb_employee_login.emp_id', '=', 'tb_booking.emp_id')->where('bk_id','=',$id)->get();
      foreach ($sqlBooking as $bk) {
        $dep_id = $bk->dep_id;
        $com_id = $bk->com_id;
        $lv = $bk->emp_level;
      }
      $mails = array();
        if ($lv == 0 || $lv == 2 || $lv == 99 || $lv == 999) {
          // $sql_login = DB::table('tb_approver')
          //             ->join('tb_employee_login','tb_employee_login.emp_id','=','tb_approver.app_id')
          //             ->where('tb_approver.dep_id','=',$dep_id)
          //             ->where('tb_approver.com_id','=',$com_id)
          //             ->where('tb_employee_login.emp_level','=','1')
          //             ->get();
          // foreach ($sql_login as $lv):
          //   array_push($mails,$lv->username);
          // endforeach;
           $sql_login = DB::table('tb_approver')
                      ->join('tb_employee_login','tb_employee_login.emp_id','=','tb_approver.app_id')
                      ->where('tb_approver.dep_id','=',$dep_id)
                      ->where('tb_approver.com_id','=',$com_id)
                      ->where('tb_employee_login.emp_level','=','1')
                      ->get();
          if (count($sql_login)==0) {
            $sql_login_1 = DB::table('tb_approver')
                        ->join('tb_employee_login','tb_employee_login.emp_id','=','tb_approver.app_id')
                        ->where('tb_approver.dep_id','=',$dep_id)
                        ->where('tb_approver.com_id','=',$com_id)
                        ->where('tb_employee_login.emp_level','=','11')
                        ->get();
              if (count($sql_login_1)==0) {
                $sql_login_11 = DB::table('tb_approver')
                            ->join('tb_employee_login','tb_employee_login.emp_id','=','tb_approver.app_id')
                            ->where('tb_approver.dep_id','=',$dep_id)
                            ->where('tb_approver.com_id','=',$com_id)
                            ->where('tb_employee_login.emp_level','=','111')
                            ->get();
                            foreach ($sql_login_11 as $lv):
                              array_push($mails,$lv->username);
                            endforeach;
              }else {
                foreach ($sql_login_1 as $lv):
                  array_push($mails,$lv->username);
                endforeach;
              }
          }else {
            foreach ($sql_login as $lv):
              array_push($mails,$lv->username);
            endforeach;
          }//----------
        }
        elseif ($lv == 1) {
          $sql_login = DB::table('tb_approver')
                      ->join('tb_employee_login','tb_employee_login.emp_id','=','tb_approver.app_id')
                      ->where('tb_approver.dep_id','=',$dep_id)
                      ->where('tb_approver.com_id','=',$com_id)
                      ->where('tb_employee_login.emp_level','=','11')
                      ->get();
          foreach ($sql_login as $lv):
            array_push($mails,$lv->username);
          endforeach;
        }
        elseif ($lv == 11) {
          $sql_login = DB::table('tb_approver')
                      ->join('tb_employee_login','tb_employee_login.emp_id','=','tb_approver.app_id')
                      ->where('tb_approver.dep_id','=',$dep_id)
                      ->where('tb_approver.com_id','=',$com_id)
                      ->where('tb_employee_login.emp_level','=','111')
                      ->get();
          foreach ($sql_login as $lv):
            array_push($mails,$lv->username);
          endforeach;
        }

        Mail::send('Mail.mailbookingApprove',['user' => 'Carpool Siamraj Service'],
         function ($m) use ($mails) {
           $m->from('sys.carpool@siamraj.com', 'Carpool Siamraj Service');
           $m->to($mails)->subject('Carpool Siamraj Service : Wait Approve ');
          }
        );
       $msg = array("success"=>true,"bk_id"=>$id,"lv"=>$lv,"email"=>$mails);
      return Response(json_encode($msg));
    }

    public function sendApproveSetcar(Request $req)
    {
      $id = $req->input('bk_id');
      // $id = 'BK1801220005';
      $sqlBooking = DB::table('tb_booking')->join('tb_employee', 'tb_employee.emp_id', '=', 'tb_booking.emp_id')->where('bk_id','=',$id)->get();
      foreach ($sqlBooking as $bk) {
        $dep_id = $bk->dep_car;
        $com_id = $bk->com_id;
      }
      $mails = array();
        $sql_login = DB::table('tb_organizer')->join('tb_employee','tb_employee.emp_id','=','tb_organizer.or_id')
        ->where('tb_organizer.dep_id','=',$dep_id)->where('tb_organizer.com_id','=',$com_id)->get();
        foreach ($sql_login as $lv):
          array_push($mails,$lv->emp_email);
        endforeach;
        Mail::send('Mail.mailApproveSetcar',['user' => 'Carpool Siamraj Service'],
         function ($m) use ($mails) {
           $m->from('sys.carpool@siamraj.com', 'Carpool Siamraj Service');
           $m->to($mails)->subject('Carpool Siamraj Service : Wait Organizer Car ');
          }
        );
       $msg = array("success"=>true,"bk_id"=>$id);
      return Response(json_encode($msg));
    }


    public function sendEmp(Request $req)
    {
      $id = $req->input('id');
      $pass = $req->input('pass');
      session()->put('new_id', $id);
      session()->put('new_pass', $pass);

      $sqlEmp = DB::table('tb_employee')->join('tb_employee_login','tb_employee.emp_id','=','tb_employee_login.emp_id')->where('tb_employee_login.emp_id','=',$id)->get();


      foreach ($sqlEmp as $emp):
      Mail::send('Mail.mailempadd',['user' => 'Carpool Siamraj Service'], function ($m) use ($emp) {
           $m->from('sys.carpool@siamraj.com', 'Carpool Siamraj Service');
           $m->to($emp->emp_email)->subject('Carpool Siamraj Service : Welcome');
       });


      endforeach;

      $msg = array("success"=>true);


      return Response(json_encode($msg));

    }

    public function sendApprove(Request $req)
    {
      $id = $req->input('id');
      session()->forget('bk_id');
      session()->put('bk_id', $id);
      $sqlBooking = DB::table('tb_booking')->join('tb_employee', 'tb_employee.emp_id', '=', 'tb_booking.emp_id')->where('bk_id','=',$id)->select('tb_employee.emp_email')->get();
      foreach ($sqlBooking as $bk):

      Mail::send('Mail.mailapprove',['user' => 'Carpool Siamraj Service'], function ($m) use ($bk) {
           $m->from('sys.carpool@siamraj.com', 'Carpool Siamraj Service');
           $m->to($bk->emp_email)->subject('Carpool Siamraj Service : Approve Booking');
       });
       endforeach;
      $msg = array("success"=>true);
      return Response(json_encode($msg));
    }

    public function sendSetCar(Request $req)
    {
      $id = $req->input('id');
      session()->forget('bk_id');
      session()->put('bk_id', $id);
      $sqlBooking = DB::table('tb_booking')->join('tb_employee', 'tb_employee.emp_id', '=', 'tb_booking.emp_id')->where('bk_id','=',$id)->select('tb_employee.emp_email')->get();
      foreach ($sqlBooking as $bk):
        $email = $bk->emp_email;
      Mail::send('Mail.mailsetcar',['user' => 'Carpool Siamraj Service'], function ($m) use ($bk) {
           $m->from('sys.carpool@siamraj.com', 'Carpool Siamraj Service');
           $m->to($bk->emp_email)->subject('Carpool Siamraj Service : SUCCESS');
           // $m->to($bk->emp_email)->subject('ขออนุญาตทดสอบการส่งเมลยืนยันระบบCarpool ขออภัยหากรบกวน');
       });
       endforeach;
      $msg = array("success"=>true,"email"=>"");
      return Response(json_encode($msg));
    }

    public function sendSetCarOT(Request $req)
    {
      $id = $req->input('id');
      // $sqlBookingid = DB::table('tb_booking')->join('tb_employee', 'tb_employee.emp_id', '=', 'tb_booking.emp_id')->where('bk_id','=',$id)->select('tb_booking.bk_merge')->get();
      //   foreach ($sqlBookingid as $mbk) {
      //     $merge = $mbk->$bk_merge;
          if(strpos($id,';')!==FALSE){
               $str = explode(";",$id);
              }else{
                $str = array($id);
              }
            print_r($str);
        for ($i=0; $i < count($str); $i++) {
          session()->forget('bk_id');
          session()->put('bk_id', $str[$i]);
          $sqlBooking = DB::table('tb_booking')->join('tb_employee', 'tb_employee.emp_id', '=', 'tb_booking.emp_id')->where('bk_id','=',$str[$i])->select('tb_employee.emp_email')->get();
          foreach ($sqlBooking as $bk){
           echo $email = $bk->emp_email;
          Mail::send('Mail.mailOvertime',['user' => 'Carpool Siamraj Service'], function ($m) use ($bk) {
               $m->from('sys.carpool@siamraj.com', 'Carpool Siamraj Service');
               $m->to($bk->emp_email)->subject('Carpool Siamraj Service : SUCCESS');
               // $m->to($bk->emp_email)->subject('ขออนุญาตทดสอบการส่งเมลยืนยันระบบCarpool ขออภัยหากรบกวน');
           });
         }//bk
          echo $i;
        }
      // }//mbk

      // $msg = array("success"=>true,"email"=>'');
      // return Response(json_encode($msg));
    }

    public function sendEject(Request $req)
    {
      $id = $req->input('id');
      session()->put('bk_id', $id);
      $sqlBooking = DB::table('tb_booking')
                    ->join('tb_employee', 'tb_employee.emp_id', '=', 'tb_booking.emp_id')
                    ->where('bk_id','=',$id)->select('tb_employee.emp_email')->get();
      foreach ($sqlBooking as $bk):
      Mail::send('Mail.maileject',['user' => 'Carpool Siamraj Service'], function ($m) use ($bk) {
           $m->from('sys.carpool@siamraj.com', 'Carpool Siamraj Service');
           $m->to($bk->emp_email)->subject('Carpool Siamraj Service : Eject Booking');
       });
       endforeach;
      $msg = array("success"=>true);
      return Response(json_encode($msg));
    }

    public function sendNocar(Request $req)
    {
      $id = $req->input('data');
      session()->put('bk_id', $id);
      $sqlBooking = DB::table('tb_booking')->join('tb_employee', 'tb_employee.emp_id', '=', 'tb_booking.emp_id')->where('bk_id','=',$id)->select('tb_employee.emp_email')->get();
      foreach ($sqlBooking as $bk):

      Mail::send('Mail.mailnonecar',['user' => 'Carpool Siamraj Service'], function ($m) use ($bk) {
           $m->from('sys.carpool@siamraj.com', 'Carpool Siamraj Service');
           $m->to($bk->emp_email)->subject('Carpool Siamraj Service : NoneCar');
       });
       endforeach;
      $msg = array("success"=>true);
      return Response(json_encode($msg));
    }

    public function sendPass(Request $req)
    {
      $token = $req->input('token');
      session()->put('tokenmail', $token);
      $sqlEmp = DB::table('tb_employee')->join('tb_employee_login','tb_employee.emp_id','=','tb_employee_login.username')->where('tb_employee_login.token','=','0271f432022e94b8f1212a7d37e604f1')->get();

      foreach ($sqlEmp as $emp):
      Mail::send('Mail.mailchangepass',['user' => 'Carpool Siamraj Service'], function ($m) use ($emp) {
           $m->from('sys.carpool@siamraj.com', 'Carpool Siamraj Service');
           $m->to($emp->emp_email)->subject('Carpool Siamraj Service : Change Password');
       });
      endforeach;
      $msg = array("success"=>true);

      return Response(json_encode($msg));
    }

    public function sendForget(Request $req)
    {
      $email = $req->input('email');
      $token =$req->input('token');
      $sqlEmp = DB::table('tb_employee')->join('tb_employee_login','tb_employee.emp_id','=','tb_employee_login.emp_id')->where('emp_email','=',$email)->get();
      if (count($sqlEmp)==0) {
      $msg = array("type"=>"email","success"=>false,"msg"=>"Please enter your email.","data"=>"");

      }
      else{

      foreach ($sqlEmp as $emp):
      session()->put('token', $token);
      $sqlUPDATE = DB::table('tb_employee_login')
                   ->where('username', '=' ,$emp->emp_email)
                   ->update(['token' => $token]);

      Mail::send('Mail.mailforget',['user' => 'Carpool Siamraj Service'], function ($m) use ($emp) {
           $m->from('sys.carpool@siamraj.com', 'Carpool Siamraj Service');
           $m->to($emp->emp_email)->subject('Carpool Siamraj Service : Forget Password');
       });
      endforeach;

      $msg = array("success"=>true);

      }
      return Response(json_encode($msg));

    }

    public function sendInvite(Request $req)
    {
       $mail = $req->input('mail');
       session()->put('imail', $mail);
       Mail::send('Mail.mailinvite',['user' => 'Carpool Siamraj Service'], function ($massage) use ($mail) {
            $massage->from('sys.carpool@siamraj.com', 'Carpool Siamraj Service');
            $massage->to($mail)->subject('Carpool Siamraj Service : Invite');
        });
      $msg = array("success"=>true);
      return Response(json_encode($msg));

    }

    public function sendalertstart(Request $req)
    {
      $data = explode(";",$req->input("data"));
      for ($i=0; $i < count($data); $i++) {
        $sql_booking = DB::table("tb_booking")->join('tb_employee', 'tb_employee.emp_id', '=', 'tb_booking.emp_id')->where("bk_id","=",$data[$i])->get();
          foreach ($sql_booking as $bk) {
            session()->forget('bk_id');
            session()->put('bk_id', $bk->bk_id);
            Mail::send('Mail.mailalertbooking',['user' => 'Siamraj Carpool Service'], function ($m) use ($bk) {
                 $m->from('sys.carpool@siamraj.com', 'Siamraj Carpool Service');
                 $m->to($bk->emp_email)->subject('Siamraj Carpool Service : Booking Scheduled For Tomorrow ');
             });
            $sql = DB::table("tb_booking")
                        ->where("bk_id","=",$bk->bk_id)
                        ->update(["send_alert"=>"1"]);
          }

      }
    }

    public function sendalertsatisfaction(Request $req)
    {
      $data = explode(";",$req->input("data"));
      for ($i=0; $i < count($data); $i++) {
        $sql_booking = DB::table("tb_booking")->join('tb_employee', 'tb_employee.emp_id', '=', 'tb_booking.emp_id')->where("bk_id","=",$data[$i])->get();
          foreach ($sql_booking as $bk) {
            session()->forget('bk_id');
            session()->put('bk_id', $bk->bk_id);
            Mail::send('Mail.mailsatisfaction',['user' => 'Siamraj Carpool Service'], function ($m) use ($bk) {
                 $m->from('sys.carpool@siamraj.com', 'Siamraj Carpool Service');
                 $m->to($bk->emp_email)->subject('Siamraj Carpool Service : Satisfaction');
             });
            $sql = DB::table("tb_booking")
                        ->where("bk_id","=",$bk->bk_id)
                        ->update(["send_satisfaction"=>"1"]);
          }

      }
    }



}
