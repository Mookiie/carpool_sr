<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use DateTime;

class CalendarController extends Controller
{
    //
    public function Calendar()
    {
      if (session()->has('user'))
      {
        $username = session()->get('user');
        $users = DB::table('tb_employee')
                ->join("tb_department",function($join){
                        $join->on('tb_employee.dep_id', '=', 'tb_department.dep_id')
                             ->on("tb_employee.com_id","=","tb_department.com_id");
                 })
                ->join('tb_employee_login', 'tb_employee.emp_id', '=', 'tb_employee_login.emp_id')
                ->where('tb_employee.emp_email','=',$username)->get();
        return view('calendar.calendar',['users'=>$users,'valuepage'=>'approve']);
      }
      else
      {
        exit("<script>window.location='/';</script>");
      }
    }

    public function CalendarDep()
    {
      if (session()->has('user'))
      {
        $username = session()->get('user');
        $users = DB::table('tb_employee')
                ->join("tb_department",function($join){
                        $join->on('tb_employee.dep_id', '=', 'tb_department.dep_id')
                             ->on("tb_employee.com_id","=","tb_department.com_id");
        })
                ->join('tb_employee_login', 'tb_employee.emp_id', '=', 'tb_employee_login.emp_id')
                ->where('tb_employee.emp_email','=',$username)->get();
        return view('calendar.calendardep',['users'=>$users,'valuepage'=>'approve']);
      }
      else
      {
        exit("<script>window.location='/';</script>");
      }
    }

    public function AllCarEvent(Request $req)
    {
      // $car_id =  $req->input('data');
      $car_id = '';
      $com_id =  $req->input('data');
      $job_id =  $req->input('job_id');
      $str = "";
      $strUse = "";
      if($car_id != ""){
        // $sqlbooking = DB::table('tb_booking')
        // ->join("tb_car_type",function($join){
        //       $join->on("tb_booking.ctype_id","=","tb_car_type.ctype_id")
        //            ->on("tb_booking.com_id","=","tb_car_type.com_id");
        //   })
        // ->join('tb_car',function($join){
        //   $join->on('tb_booking.car_id','=','tb_car.car_id')
        //        ->on("tb_booking.com_id","=","tb_car.com_id");
        // })
        // ->join("tb_employee",function($join){
        //        $join->on('tb_booking.emp_id', '=', 'tb_employee.emp_id')
        //             ->on("tb_booking.com_id","=","tb_employee.com_id");
        //   })
        // ->where("tb_booking.com_id",'=',$com_id)
        // ->where('bk_status','=','success')
        // ->where('tb_booking.car_id','=',"$car_id")->get();
      }
      else {
        $sqlbooking = DB::table('tb_booking')
        // ->join("tb_car_type",function($join){
        //       $join->on("tb_booking.ctype_id","=","tb_car_type.ctype_id")
        //            ->on("tb_booking.com_id","=","tb_car_type.com_id");
        //   })
        // ->join('tb_car',function($join){
        //   $join->on('tb_booking.car_id','=','tb_car.car_id')
        //        ->on("tb_booking.com_id","=","tb_car.com_id");
        // })
        // ->join("tb_employee",function($join){
        //        $join->on('tb_booking.emp_id', '=', 'tb_employee.emp_id')
        //             ->on("tb_booking.com_id","=","tb_employee.com_id");
        // })
        // ->join("tb_job",function($join){
        //         $join->on('tb_job.job_id', '=', 'tb_booking.dep_car')
        //              ->on("tb_job.com_id","=","tb_booking.com_id");
        //   })
        ->where("tb_booking.com_id",'=',$com_id)
        ->where("tb_booking.dep_car",'=',$job_id)
        ->where('bk_status','=','success')
        ->orderBy('bk_id')
        ->get();
      }

      $json_data = array();
      foreach ($sqlbooking as $bk):

        $sql_car = DB::table('tb_car')->where('car_id','=',$bk->car_id)->get();
        foreach ($sql_car as $car) {
          $car_number = $car->car_number;
        }
        // $car_dep = $bk->dep_car;
        $dep_car = DB::table('tb_job')->where('job_id','=',$bk->dep_car)->where('com_id','=',$com_id)->get();
        foreach ($dep_car as $dc) {
          $dc_car = $dc->job_name;
        }
        $date1 = substr($bk->bk_start_start,0,10);
        $date2 = substr($bk->bk_end_start,0,10);
        $d1= new DateTime($date1);
        $d2 = new DateTime($date2);
        $diff = $d1->diff($d2)->format("%a");
        // $name = $bk->emp_fname;
        // $arr = array();
        // if($diff >= 5){
        //     $arr="#f06292";
        //   }else if($diff >= 3){
        //     $arr="#F39C12";
        //   }else if($diff >= 2){
        //     $arr="#3498DB";
        //   }else{
        //     $arr="#2ECC71";
        //   }
        if ($diff >0) {
          $arr="#F39C12";
        }else{
          $arr="#2ECC71";
        }

        if ($diff>0) {
          // $date2 = date('Y-m-d',strtotime(str_replace('-', '/', $date2) . "+1 days"));
          $date2 = date('Y-m-d',strtotime(str_replace('-', '/', $date2)));
          if (!$bk->bk_use) {

            $text =
            " ออกเวลา ".substr($bk->bk_start_start,11,2).".".substr($bk->bk_start_start,14,2).
            " กลับวันที่ ".substr($date2,8,2)." เวลา ".substr($bk->bk_end_start,11,2).".".substr($bk->bk_end_start,14,2)
            ;

          }
          else {
            $text =
            " ช่วงเวลา ".
            substr($bk->bk_start_start,11,2).".".substr($bk->bk_start_start,14,2).
            "-".substr($bk->bk_end_start,11,2).".".substr($bk->bk_end_start,14,2)." ของทุกวัน";
            $arr = "#3498DB";
          }
        }
        else {

          $text=  " (".substr($bk->bk_start_start,11,2).".".substr($bk->bk_start_start,14,2)."-".substr($bk->bk_end_start,11,2).".".substr($bk->bk_end_start,14,2).")";
          if (substr($bk->bk_end_start,11,2)-(substr($bk->bk_start_start,11,2))<7) {
            $arr = "#3498DB";
          }
        }
        $json_data[]=array(
             "id"=>$bk->bk_id,
             // "title"=>$bk->bk_id,
             // "title"=>$bk->car_number." ".$bk->car_model,
             "title"=>$car_number." ".$text,
             // "title" =22"[".$bk->job_name."] ".$bk->car_number." ".$text,
             // "start"=>$bk->bk_start_start,
             // "end"=>$bk->bk_end_start,
             "start"=>$date1,
             "end"=>date('Y-m-d',strtotime(str_replace('-', '/', $date2) . "+1 days")),
             "textColor"=>"#fff",
             "borderColor"=>"#fff",
             "color"=>$arr,
             "allDay"=>(true)
             // กำหนด event object property อื่นๆ ที่ต้องการ
         );
         $json= json_encode($json_data);
      endforeach;

      if(isset($_GET['callback']) && $_GET['callback']!=""){
      echo $_GET['callback']."(".$json.");";
      }else{
      echo $json;
      }
    }

    public function BkEvent(Request $req)
    {
      $bk_id =  $req->input('data');
      $search =  $req->input('search');
      $str = "";
      $strUse = "";
      $username = session()->get('user');
      $SqlLv=DB::table('tb_employee_login')->where()->get();
      if($bk_id != ""){
        $sqlbooking = DB::table('tb_booking')
                      ->join('tb_car_type','tb_booking.ctype_id','=','tb_car_type.ctype_id')
                      ->join('tb_employee','tb_employee.emp_id','=','tb_booking.emp_id')
                      ->where('tb_employee.emp_email','=',"$username")
                      ->where('tb_booking.bk_id','=',"$bk_id")->get();
        }
        elseif ($search != "") {
        $sqlbooking = DB::table('tb_booking')
                      ->join('tb_car_type','tb_booking.ctype_id','=','tb_car_type.ctype_id')
                      ->join('tb_employee','tb_employee.emp_id','=','tb_booking.emp_id')
                      ->where('tb_employee.emp_email','=',"$username")
                      ->where('tb_booking.bk_status','=',"$search")->get();
        }
        else {
        $sqlbooking = DB::table('tb_booking')
                        ->join('tb_car_type','tb_booking.ctype_id','=','tb_car_type.ctype_id')
                        ->join('tb_employee','tb_employee.emp_id','=','tb_booking.emp_id')
                        ->where('tb_employee.emp_email','=',"$username")->get();
        }


      $json_data = array();
      foreach ($sqlbooking as $bk):
        $arr = array();
              if($bk->bk_status == "wait"){
                  array_push($arr,"รอการอนุมัติ","warning","#F39C12");
                }else if($bk->bk_status == "approve"){
                  array_push($arr,"รอการจัดรถ","info","#3498DB");
                }else if($bk->bk_status == "success"){
                  array_push($arr,"สำเร็จ","success","#2ECC71");
                }else if($bk->bk_status == "eject"){
                  array_push($arr,"ยกเลิกการจอง","danger","#E74C3C");
                }else if($bk->bk_status == "nonecar"){
                  array_push($arr,"ไม่มีรถ","danger","#E74C3C");
                }

        $json_data[]=array(
         "id"=>$bk->bk_id,
         "title"=>$bk->bk_id,
         "start"=>$bk->bk_start_start,
         "end"=>$bk->bk_end_start,
         "textColor"=>"#fff",
         "color"=>$arr[2],
         "allDay"=>(true)
             // กำหนด event object property อื่นๆ ที่ต้องการ
         );
 $json= json_encode($json_data);
      endforeach;

      if(isset($_GET['callback']) && $_GET['callback']!=""){
      echo $_GET['callback']."(".$json.");";
      }else{
      echo $json;
      }
    }

}
