<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class ColorManageController extends Controller
{
    //
    public function AddShowcolor(Request $req)
    {
      $emp_id =$req->input("id");
      return view('Models.Color.addcolor',['emp_id'=>$emp_id]);
    }

    public function AddcolorDB(Request $req)
    {
      date_default_timezone_set("Asia/Bangkok");
      $emp_id = strtoupper($req->input("emp_id")) ;
      $color_id=  $req->input("color_id");
      $color_name = $req->input("color_name");
      $date =date("Y-m-d H:i:s");
      $msg = array();

               $sql = DB::table('tb_color')
                            ->insert(['color_id' => $color_id,
                                      'color_name' => $color_name,
                                      'update_by' =>$emp_id,
                                      'update_date'=>$date]);
              if(!$sql){
                    $msg = array("type"=>"","success"=>false,"msg"=>"");

              }else{
                  $msg = array("type"=>"","success"=>true,"msg"=>"");
              }

              return Response(json_encode($msg));
      }

      public function EditColor(Request $req)
      {
        $emp_id =$req->input("emp_id");
        $color_id = $req->input("color");
        return view('Models.Color.editcolor',['emp_id'=>$emp_id,'color_id'=>$color_id]);
      }

      public function UpdateColorDB(Request $req)
      {
        date_default_timezone_set("Asia/Bangkok");
        $emp_id = strtoupper($req->input("emp_id")) ;
        $color_id=  $req->input("color_id");
        $color_name = $req->input("color_name");
        $date =date("Y-m-d H:i:s");
        $msg = array();

        $sqlUPDATE = DB::table('tb_color')
                      ->where('color_id', '=' ,$color_id)
                      ->update(['color_name' => $color_name]);

                    $msg = array("type"=>"","success"=>true,"msg"=>"");

                return Response(json_encode($msg));
        }

}
