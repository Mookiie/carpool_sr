<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class TestController extends Controller
{
    //
    public function ViweTest()
    {
      return view('Test');
    }
    public function LocateDB(Request $req)
    {
      $area = array( );
      $id = $req->input('id');
      $sql = DB::table('tb_booking_location')->where("bk_id","=","$id")->get();
      foreach ($sql as $bk):
       $location_id = $bk->location_id;
       $location_name = $bk->location_name;
       $array_locate =  array('id' => $location_id ,'name' => $location_name );
      //  return Response(json_encode($array_locate));
       array_push($area,$array_locate);
      endforeach;
      // echo json_encode($area);
      return Response(json_encode($area));
    }

      public function showmodel()
      {?>
        <div class="modal fade" id="modalBk">
          <div class="col-md-12">
            <div class="card card_bk">
              <div class="modal-header">
                <span class="	fa fa-map-marker"></span>  สถานที่
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <div class="card-block" id="map" style="height:500px;"></div>
                <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAaXMuANTFVYidZ3gQaPaUVQU4zeEmk33U&callback=initMap"></script>
              </div>
              </div>
            </div>
          </div>
        </div>
        <script type="text/javascript">


        $(document).ready(function() {
            $('#modalBk').on('shown', function () {
                google.maps.event.trigger(map, "resize");
            });
        });


        function initMap(address) {

          var obj = [];

          <?php
          $sqlbooking_locate = DB::table('tb_booking_location')->where('bk_id', '=', 'BK1704100001')->get();
          foreach ($sqlbooking_locate as $locate):
            $id = $locate->bk_id;
            $name = $locate->location_name;
            $address =  $locate->location_id;
            $latitude = $locate->location_lat;
            $longitude = $locate->location_lng;?>
            var location = ["<?php echo $address ?>",<?php echo $latitude ?>,<?php echo $longitude ?>,"<?php echo $name ?>"];
            obj.push(location);
          <?php endforeach; ?>
          // console.log(obj);
          // console.log(name);

            var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 10,
            center: new google.maps.LatLng(obj[0][1],obj[0][2]),
            mapTypeId: google.maps.MapTypeId.ROADMAP
            });

            var infowindow = new google.maps.InfoWindow();

            var marker, i;

            for (i = 0; i < obj.length; i++) {
            marker = new google.maps.Marker({
              position: new google.maps.LatLng(obj[i][1], obj[i][2]),
              map: map
               });

            google.maps.event.addListener(marker, 'click', (function(marker, i) {
                  return function() {
                    var name = "สถานที่ : "+obj[i][0]+"<br> ตำแหน่ง : "+obj[i][3]
                    infowindow.setContent(name);
                    infowindow.open(map, marker);
                  }
                }
              )

             (marker, i));
            }

            var circle = new google.maps.Circle({
              map: map,
              radius: 30000,    // 30km to metres
              fillColor: '#B2E5B2'
            });
            circle.bindTo('center', marker, 'position');

        }
      </script>

    <?php  }
}
