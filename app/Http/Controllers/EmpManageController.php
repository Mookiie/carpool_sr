<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class EmpManageController extends Controller
{
  public function EmpCreate()
  {
    if (session()->has('user'))
    {
      $username = session()->get('user');
      $users = DB::table('tb_employee')
              ->join("tb_department",function($join){
                        $join->on('tb_employee.dep_id', '=', 'tb_department.dep_id')
                             ->on("tb_employee.com_id","=","tb_department.com_id");
                              })
              ->join('tb_employee_login', 'tb_employee.emp_id', '=', 'tb_employee_login.emp_id')
              ->where('tb_employee.emp_email','=',$username)->get();
      return view('employee.addemp',['users'=>$users]);
    }
    else
    {
      exit("<script>window.location='/';</script>");
    }
  }

  public function EmpEdit(Request $req)
  {
    $emp = $req->input('emp');
    if (session()->has('user'))
    {
      $username = session()->get('user');

      $users = DB::table('tb_employee')
              ->join("tb_department",function($join){
                        $join->on('tb_employee.dep_id', '=', 'tb_department.dep_id')
                             ->on("tb_employee.com_id","=","tb_department.com_id");
                              })
              ->join('tb_employee_login', 'tb_employee.emp_id', '=', 'tb_employee_login.emp_id')
              ->where('tb_employee.emp_email','=',$username)->get();
      return view('employee.editemp',['users'=>$users,'emp'=>$emp]);
    }
    else
    {
      exit("<script>window.location='/';</script>");
    }
  }

  public function ShowEmp()
  {
    if (session()->has('user'))
    {
      $username = session()->get('user');
      $users = DB::table('tb_employee')
              ->join("tb_department",function($join){
                        $join->on('tb_employee.dep_id', '=', 'tb_department.dep_id')
                             ->on("tb_employee.com_id","=","tb_department.com_id");
                              })
              ->join('tb_employee_login', 'tb_employee.emp_id', '=', 'tb_employee_login.emp_id')
              ->where('tb_employee.emp_email','=',$username)->get();
      return view('employee.emp',['users'=>$users]);
    }
    else
    {
      exit("<script>window.location='/';</script>");
    }
  }

  public function ComToDep(Request $req)
  {
    $com_id = $req->input('com_id');
    $sqldep = DB::table('tb_department')->where('com_id','=',$com_id)->get();
      echo  "<option value='DEP999'>กรุณาเลือกแผนก</option>";
    foreach ($sqldep as $dep) {
      echo  "<option value='$dep->dep_id'>$dep->dep_name</option>";
    }
  }

  public function DepToJob(Request $req)
  {
    $com_id = $req->input('com_id');
    $dep_id = $req->input('dep_id');
    $sqljob = DB::table('tb_job')->where('com_id','=',$com_id)->where('dep_id','=',$dep_id)->get();
    echo  "<option value='0'>กรุณาเลือกตำแหน่ง</option>";
    foreach ($sqljob as $job) {
      echo  "<option value='$job->job_id'>$job->job_name</option>";
    }
  }

  public function InsertEmp(Request $req)
  {
    date_default_timezone_set("Asia/Bangkok");
       $emp_id = strtoupper($req->input('emp_id'));
       $emp_pass=md5($req->input('emp_pass'));
       $emp_fname = trim($req->input('emp_fname'));
       $emp_lname = trim($req->input('emp_lname'));
       $emp_email = trim($req->input('emp_email'));
       $com_id = $req->input('com_id');
       $emp_job = $req->input('job_id');
       $emp_sex = $req->input('emp_sex');
       $emp_tel = $req->input('mtel');
       $dep_id = $req->input('dep_id');
       $emp_level = $req->input('emp_level');
      //  $emp_img = $req->input('img');


       $date =date("Y-m-d H:i:s");
       $id = "";
       $y = date("y")+43;
       $sql = DB::table('tb_employee')->where('com_id','=',$com_id)->select('emp_id')->orderBy('emp_id','desc')->limit(1)->get();
     foreach ($sql as $emp):
       $last_id = $emp->emp_id;
     endforeach;
     $sql_com = DB::table('tb_company')->select('com_code')->where('com_id','=',$com_id)->get();
     foreach ($sql_com as $com):
       $code = $com->com_code;
     endforeach;
       $old = explode($code,$last_id);
       $old_y = substr($old[1],0,2);
       $old_dep = substr($old[1],2,3);
       $old_id = substr($old[1],5,4);

       $id = $code.$y.sprintf("%07d",$old_id+1);

      $chk_passlogin = DB::table('tb_employee_login')->where(['emp_id'=>$emp_id , 'emp_status'=>'0' , 'password'=>$emp_pass])->get();
      if (count($chk_passlogin)<1) {
        $msg = array("type"=>"emp_pass","success"=>false,"msg"=>"Incorrect password.","data"=>"");
      }
      else {
       $msg = array();
       $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
       $pass = substr( str_shuffle( $chars ), 0, 8);
       $password=md5($pass);
       $email_count = DB::table('tb_employee')->where(['emp_email'=>$emp_email])->count();
       if( !preg_match( "(^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$)i", $emp_email))
        {
          $msg = array("type"=>"emp_email","success"=>false,"msg"=>"Please enter your Email.","data"=>"");
        }
        elseif ($email_count!=0) {
          $msg = array("type"=>"emp_email","success"=>false,"msg"=>"Please enter your Email. ","data"=>"");
        }
        elseif (!preg_match("/^\d{10}$/", $emp_tel)) {
          $msg = array("type"=>"mtel","success"=>false,"msg"=>"Please enter your Phone Number.","data"=>"");
        }
        elseif ($com_id =='') {
          $msg = array("type"=>"com_id","success"=>false,"msg"=>"Please select company.","data"=>"");
        }
        else {
          $sqlemp =  DB::table('tb_employee')->insert(
                      [  'emp_id' => "$id",
                         'com_id' => "$com_id",
                         'emp_fname' => "$emp_fname",
                         'emp_lname' => "$emp_lname",
                         'emp_sex' => "$emp_sex",
                         'job_id' => "$emp_job",
                         'emp_email' => "$emp_email",
                         'emp_tel' => "$emp_tel",
                         'emp_img' => "user.png",
                         'dep_id' => "$dep_id",
                         'create_by' => "$emp_id",
                         'create_date' => "$date"
                      ]);

       $sqllogin =  DB::table('tb_employee_login')->insert(
                     [ 'username' => "$emp_email",
                       'emp_id' => "$id",
                       'com_id' => "$com_id",
                       'password' => "$password",
                       'emp_level' => "$emp_level",
                       'emp_status' => "0",
                     ]);
          $msg = array("type"=>"","success"=>true,"msg"=>"","id"=>$id,"pass"=>$pass);
        }
     }

       return Response(json_encode($msg));

  }

  public function TableEmp(Request $req)
  {
    $com_id = $req->input('com_id');
    $lv_user = $req->input('lv_user');
    $typeSearch = $req->input('typeSearch');
    $txtSearch = $req->input('txtSearch');
    $emp_job = $req->input('emp_job');
    $emp_lv = $req->input('emp_lv');
    $emp_oc = $req->input('emp_oc');
    $emp_status = $req->input('emp_status');
    return view('Models.Employee.tableemp',['com_id'=>$com_id,'lv_user'=>$lv_user,'typeSearch'=>$typeSearch,'txtSearch'=>$txtSearch,
    'emp_job'=>$emp_job,'emp_lv'=>$emp_lv,'emp_oc'=>$emp_oc,'emp_status'=>$emp_status]);
  }

  public function DetailEmp(Request $req)
  {
      $id = $req->input('id');
      return view('Models.Employee.detailemp',['id'=>$id]);
  }

  public function DisableLevel(Request $req)
  {
    $id = $req->input('emp');
    $sqlUPDATE = DB::table('tb_employee_login')
                  ->where('username', '=' ,$id)
                  ->update(['emp_status' => "1",]);
    if (!$sqlUPDATE) {
        echo"2";
    }
    else {
        echo "1";
    }
  }

  public function EmpEditDB(Request $req)
  {
          date_default_timezone_set("Asia/Bangkok");
          $emp_update = strtoupper($req->input('emp_update'));
          $emp_id = strtoupper($req->input('emp_id'));
          $emp_pass=$req->input('emp_pass');
          $emp_fname = trim($req->input('emp_fname'));
          $emp_lname = trim($req->input('emp_lname'));
          $emp_email = trim($req->input('emp_email'));
          $emp_job = $req->input('job_id');
          $emp_sex = $req->input('emp_sex');
          $emp_tel = $req->input('mtel');
          $dep_id = $req->input('dep_id');
          $date =date("Y-m-d H:i:s");

          $chk_passlogin = DB::table('tb_employee_login')->where(['username'=>$emp_update , 'emp_status'=>'0' , 'password'=>md5($emp_pass)])->get();
          if (count($chk_passlogin)<1) {
            $msg = array("type"=>"emp_pass","success"=>false,"msg"=>"Incorrect password.","data"=>"");
          }
          else {
           $msg = array();
           $email_count = DB::table('tb_employee')->where(['emp_email'=>$emp_email])->count();
           if( !preg_match( "(^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$)i", $emp_email))
            {
              $msg = array("type"=>"emp_email","success"=>false,"msg"=>"Please enter your Email.","data"=>"$email_count");
            }
            elseif ($email_count>2) {
              $msg = array("type"=>"emp_email","success"=>false,"msg"=>"Please enter your Email. ","data"=>"$email_count");
            }
            elseif (!preg_match("/^\d{10}$/", $emp_tel)) {
              $msg = array("type"=>"mtel","success"=>false,"msg"=>"Please enter your Phone Number.","data"=>"$emp_tel");
            }
            else {
              $update = DB::table('tb_employee_login')->where('username','=',$emp_update)->get();
              foreach ($update as $key) {
                $update_by = $key->emp_id;
              }

              $sqlemp = DB::table('tb_employee')
                            ->where('emp_id', '=' ,$emp_id)
                            ->update(['emp_fname' => "$emp_fname",
                                      'emp_lname' => "$emp_lname",
                                      'emp_sex' => "$emp_sex",
                                      'job_id' => "$emp_job",
                                      'emp_email' => "$emp_email",
                                      'emp_tel' => "$emp_tel",
                                      'dep_id' => "$dep_id",
                                      'update_by' => "$update_by",
                                      'update_date' => "$date"]);

              $msg = array("type"=>"","success"=>true,"msg"=>"");

            }
         }

           return Response(json_encode($msg));

  }

  public function RegisterEmp()
  {
    return view('Models.Employee.register');
  }

  public function InviteEmp()
  {
    if (session()->has('user'))
    {
      $username = session()->get('user');
      $users = DB::table('tb_employee')
              ->join("tb_department",function($join){
                        $join->on('tb_employee.dep_id', '=', 'tb_department.dep_id')
                             ->on("tb_employee.com_id","=","tb_department.com_id");
                              })
              ->join('tb_employee_login', 'tb_employee.emp_id', '=', 'tb_employee_login.emp_id')
              ->where('tb_employee.emp_email','=',$username)->get();
      return view('employee.invite',['users'=>$users]);
    }
    else
    {
      exit("<script>window.location='/';</script>");
    }

  }

  public function InviteEmpRegister()
  {
    return view('employee.addempinvite');
  }


  public function CheckInviteEmp(Request $req)
  {
    $emp_pass = $req->input('emp_pass');
    $emp_id = $req->input('emp_id');
    $chk_passlogin = DB::table('tb_employee_login')->where(['emp_id'=>$emp_id , 'emp_status'=>'0' , 'password'=>md5($emp_pass)])->get();
    if (count($chk_passlogin)==1) {
      $num = $req->input('num');
      $emp_level = $req->input('emp_level');
      $com_id = $req->input('com_id');
      $email = array();
      session()->put('com_id', $com_id);
      session()->put('emp_level', $emp_level);

      for ($i=1; $i <= $num; $i++) {
        if ($req->input('emp_email'.$i) <> '') {
          array_push($email,$req->input('emp_email'.$i));
          $msg = array("type"=>"","success"=>true,"msg"=>$email,"level"=>$emp_level);
        }//if null
      }//for
    }//if pass
    else {
      $msg = array("type"=>"emp_pass","success"=>false,"msg"=>"Incorrect password.","data"=>"");
    }//else pass

    return Response(json_encode($msg));
  }//end CheckInviteEmp

  public function InviteEmpRegisterDB(Request $req)
  {
    date_default_timezone_set("Asia/Bangkok");
    $emp_fname = $req->input('emp_fname');
    $emp_lname = $req->input('emp_lname');
    $dep_id = $req->input('dep_id');
    $job_id = $req->input('job_id');
    $emp_tel = $req->input('mtel');
    $emp_sex = $req->input('emp_sex');
    $emp_level = $req->input('emp_level');
    $com_id = $req->input('com_id');
    $emp_email = $req->input('emp_email');

    if ($emp_fname == '') {
      $msg = array("type"=>"emp_fname","success"=>false,"msg"=>"Please input your Firstname.","data"=>"");
    }
    else {
      if ($emp_lname == '') {
        $msg = array("type"=>"emp_fname","success"=>false,"msg"=>"Please input your Lastname.","data"=>"");
      }
      else {
        if ($dep_id == 'DEP999') {
          $msg = array("type"=>"emp_fname","success"=>false,"msg"=>"Please select your Department.","data"=>"");
        }
        else {
          if ($job_id == '0') {
            $msg = array("type"=>"emp_fname","success"=>false,"msg"=>"Please select your Job.","data"=>"");
          }
          else {
            $date =date("Y-m-d H:i:s");
            $id = "";
            $y = date("y")+43;
            $dep = explode("DEP",$dep_id);
            $sql = DB::table('tb_employee')->where('com_id','=',$com_id)->select('emp_id')->orderBy('emp_id','desc')->limit(1)->get();
          foreach ($sql as $emp):
            $last_id = $emp->emp_id;
          endforeach;
          $sql_com = DB::table('tb_company')->select('com_code')->where('com_id','=',$com_id)->get();
          foreach ($sql_com as $com):
            $code = $com->com_code;
          endforeach;
            $old = explode($code,$last_id);
            $old_y = substr($old[1],0,2);
            $old_dep = substr($old[1],2,3);
            $old_id = substr($old[1],5,4);

            $id = $code.$y.$dep[1].sprintf("%04d",$old_id+1);

            $msg = array();
            $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            $pass = substr( str_shuffle( $chars ), 0, 8);
            $password=md5($pass);

            $sqlemp =  DB::table('tb_employee')->insert(
                        [  'emp_id' => "$id",
                           'com_id' => "$com_id",
                           'emp_fname' => "$emp_fname",
                           'emp_lname' => "$emp_lname",
                           'emp_sex' => "$emp_sex",
                           'job_id' => "$job_id",
                           'emp_email' => "$emp_email",
                           'emp_tel' => "$emp_tel",
                           'emp_img' => "user.png",
                           'dep_id' => "$dep_id",
                           'create_by' => "$id",
                           'create_date' => "$date"
                        ]);

         $sqllogin =  DB::table('tb_employee_login')->insert(
                       [ 'username' => "$emp_email",
                         'emp_id' => "$id",
                         'com_id' => "$com_id",
                         'password' => "$password",
                         'emp_level' => "$emp_level",
                         'emp_status' => "0",
                       ]);
            $msg = array("type"=>"","success"=>true,"msg"=>"","id"=>$id,"pass"=>$pass);

          }//else JOb
        }//else Dep
      }//else lastname
    }//else firstname
    return Response(json_encode($msg));

  }

  public function modalEmpUp(Request $req)
  {
    $emp_id = strtoupper($req->input("emp_id"));
    $com_id = $req->input("com_id") ;
    return view('Models.Employee.updateEmp',['emp_id'=>$emp_id,'com_id'=>$com_id]);
  }

  public function updateEmp(Request $req)
  {

    date_default_timezone_set("Asia/Bangkok");
    // $emp_id = strtoupper($req->input("emp_id"));
    // $com_id = $req->input("com_id") ;
    // $ldap_username = $req->input("username")."@sr.com";
    // $ldap_password = $req->input("password");
    $emp_id = 'SR600000000';
    $com_id= 'C0001';
    $ldap_username = 'syscarpool@sr.com';
    $ldap_password='Siam@2562';
    $ldap_connection = ldap_connect('172.16.0.4');
    $msg = array();
    $date =date("Y-m-d H:i:s");
    if ($ldap_username == ''||$ldap_password =='') {
      $msg = array("type"=>"error","success"=>false,"msg"=>"กรุณากรอกชื่อผู้ใช้หรือรหัสผ่าน");
    }
    else {
    if (FALSE === $ldap_connection){
        // Uh-oh, something is wrong...
    }
    // We have to set this option for the version of Active Directory we are using.
    ldap_set_option($ldap_connection, LDAP_OPT_PROTOCOL_VERSION, 3) or die('Unable to set LDAP protocol version');
    ldap_set_option($ldap_connection, LDAP_OPT_REFERRALS, 0); // We need this for doing an LDAP search.
    $bind = @ldap_bind($ldap_connection, $ldap_username, $ldap_password);

    if ($bind){
        $ldap_base_dn = 'DC=sr,DC=com';
        $search_filter = '(&(objectCategory=person)(samaccountname=*))';
        $attributes = array();
        $attributes[] = 'givenname';
        $attributes[] = 'mail';
        $attributes[] = 'samaccountname';
        $attributes[] = 'sn';
        $attributes[] = 'description';

        $result = ldap_search($ldap_connection, $ldap_base_dn, $search_filter, $attributes);
        if (FALSE !== $result){
            $entries = ldap_get_entries($ldap_connection, $result);
            for ($x=0; $x<$entries['count']; $x++){
                if (!empty($entries[$x]['givenname'][0]) &&
                     // !empty($entries[$x]['mail'][0]) &&
                     !empty($entries[$x]['samaccountname'][0]) &&
                     !empty($entries[$x]['sn'][0])&&
                     !empty($entries[$x]['description'][0])
                    )
                {

//dep_id
            $sql_chk = DB::table('tb_department')
                          ->where('dep_name','=',strtoupper(trim($entries[$x]['description'][0])))
                          ->get();
              foreach ($sql_chk as $dep):
                $up_dep = $dep->dep_id;
              endforeach;
              $sql_job = DB::table('tb_job')
                            ->where('dep_id','=',$up_dep)
                            ->get();
              foreach ($sql_job as $job):
                $job_id = $job->job_id;
              endforeach;

//emp_id
              $date =date("Y-m-d H:i:s");
              $id = "";
              $y = date("y")+43;
              $sql = DB::table('tb_employee')->where('com_id','=',$com_id)->select('emp_id')->orderBy('emp_id','desc')->limit(1)->get();
            foreach ($sql as $emp):
              $last_id = $emp->emp_id;
            endforeach;
            $sql_com = DB::table('tb_company')->select('com_code')->where('com_id','=',$com_id)->get();
            foreach ($sql_com as $com):
              $code = $com->com_code;
            endforeach;
              $old = explode($code,$last_id);
              $old_y = substr($old[1],0,2);
              $old_id = substr($old[1],5,4);
              $id = $code.$y.sprintf("%07d",$old_id+1);

              // $sqldep = DB::table('tb_department')->where('com_id','=',$com_id)->orderBy('dep_id','decs')->limit(1)->get();
              if (empty($entries[$x]['mail'][0])) {
                  $email = $entries[$x]['samaccountname'][0].'@siamraj.com';
              }
              else {
                  $email = strtolower(trim($entries[$x]['mail'][0]));
                }
              if (strpos(trim($entries[$x]['givenname'][0]),".")) {
                  $name = explode(".",trim($entries[$x]['givenname'][0]));
                  $first_name = $name[0];
                  // $first_name = trim($entries[$x]['givenname'][0]);
              }
              else {
                  $first_name = trim($entries[$x]['givenname'][0]);
              }

              $sql_name = DB::table('tb_employee')->where('emp_fname','=',$first_name)->where('emp_lname','=',trim($entries[$x]['sn'][0]))->get();

              $pass = md5('Si@mraj');

                  if (count($sql_name) > 0) {
                    $sqlemp =  DB::table('tb_employee')
                                ->where("emp_fname","=","$first_name")
                                ->where('emp_lname',"=",trim($entries[$x]['sn'][0]))
                                ->update([
                                          "dep_id"=>$up_dep,
                                          "job_id"=>$job_id
                                        ]);
                  }//if $sql_chk > 0
                  else {
                    $sqlemp =  DB::table('tb_employee')->insert(
                                [  'emp_id' => "$id",
                                   'com_id' => "$com_id",
                                   'emp_fname' => "$first_name",
                                   'emp_lname' => trim($entries[$x]['sn'][0]),
                                   'emp_email' => "",
                                   'emp_img' => "user.png",
                                   'dep_id' => "$up_dep",
                                   'job_id'=>"$job_id",
                                   'create_by' => "$emp_id",
                                   'create_date' => "$date"
                                ]);

                 $sqllogin =  DB::table('tb_employee_login')->insert(
                               [ 'user_auth' => trim($entries[$x]['samaccountname'][0]),
                                 'emp_id' => "$id",
                                 'com_id' => "$com_id",
                                 'username' => "",
                                 'password' => $pass,
                                 'emp_level' => "0",
                                 'emp_status' => "0",
                               ]);
                      }//else $sql_chk > 0

                  }//if  !empty($entries[$x]['givenname'][0]
                }// for
            }//if FALSE !== $result
            ldap_unbind($ldap_connection); // Clean up after ourselves.
          $msg = array("type"=>"","success"=>true,"msg"=>"อัพเดทข้อมูลสำเร็จแล้ว");
          // echo '<pre>';
          // print_r($ad_users);
          // echo '</pre>';
        }//if $bind
        else {
          $msg = array("type"=>"error","success"=>false,"msg"=>"ชื่อผู้ใช้หรือรหัสผ่านไม่ถูกต้อง");
        }//else $bind

      }//has value username password

        return Response(json_encode($msg));
  }

  public function SetPermission()
  {
    if (session()->has('user'))
    {
      $username = session()->get('user');
      $users = DB::table('tb_employee')
              ->join("tb_department",function($join){
                        $join->on('tb_employee.dep_id', '=', 'tb_department.dep_id')
                             ->on("tb_employee.com_id","=","tb_department.com_id");
                              })
              ->join('tb_employee_login', 'tb_employee.emp_id', '=', 'tb_employee_login.emp_id')
              ->where('tb_employee.emp_email','=',$username)->get();
      return view('employee.SetPermission',['users'=>$users]);
    }
    else
    {
      exit("<script>window.location='/';</script>");
    }
  }

    public function resetModals(Request $req)
    {
      $id = $req->input('id');
      return view('Models.Employee.RePassEmp',['id'=>$id]);
    }

    public function ResetPassword(Request $req)
    {
      $emp_id = $req->input('emp_id');
      $username = $req->input('username');
      $password = md5($req->input('password'));
      $chk_statuslogin = DB::table('tb_employee_login')->where(['username'=>$username ,'password'=>$password, 'emp_status'=>'0'])->get();
      if (count($chk_statuslogin)<1) {
        $msg = array("type"=>"","success"=>false,"msg"=>"Incorrect password.");
      }
      else {
        $pass = md5('Si@mraj');
        $sql_login = DB::table('tb_employee_login')
                      ->where('emp_id', '=' ,$emp_id)
                      ->update([
                                'password' => $pass,
                                'update_date' =>null
                              ]);
        $msg = array("type"=>"","success"=>true,"msg"=>"");
      }
      return Response(json_encode($msg));
    }

    public function Permission(Request $req)
    {
        $id = $req->input('id');
        return view('Models.Employee.permission',['id'=>$id]);
    }

    public function SetPermissionDB(Request $req)
    {
      $emp_id = $req->input('emp_id');
      $emp_update = $req->input('emp_update');
      $sql_emp = DB::table('tb_employee')->join('tb_employee_login','tb_employee.emp_id','=','tb_employee_login.emp_id')->where('tb_employee_login.emp_id','=',$emp_update)->get();
      foreach ($sql_emp as $emp) {
        $com_id = $emp->com_id;
        $dep_id = $emp->dep_id;
        $lv_emp = $emp->emp_level;
        }
      $lv = $req->input('lv');
      $sqlUpdate = DB::table('tb_employee_login')
                    ->where('emp_id', '=' ,$emp_update)
                    ->update([
                              'emp_level'=>$lv
                            ]);
      if ($lv == '1'||$lv == '11'||$lv == '111') {
        if ($lv_emp <> '1'||$lv_emp <> '11'||$lv_emp <> '111') {
          $sql = DB::table('tb_approver')
                  ->insert([ 'app_id' => $emp_update,
                             'app_count'=>1,
                             'com_id' =>$com_id,
                             'dep_id'=>$dep_id
                          ]);
        }

        if ($lv_emp == '2') {
          $sql_chk_A = DB::table('tb_organizer')->where('or_id','=',$emp_update)->count();
            if ($sql_chk_A>0) {
                                $sql = DB::table('tb_organizer')
                                        ->where('or_id','=',$emp_update)
                                        ->delete();
                              }
        }

      }
      elseif ($lv == '2') {
        $sql = DB::table('tb_organizer')
                ->insert([ 'or_id' => $emp_update,
                           'or_count'=>1,
                           'com_id' =>$com_id,
                           'dep_id'=>$dep_id
                        ]);

        if ($lv_emp == '1'||$lv_emp == '11') {
          $sql_chk_B = DB::table('tb_approver')->where('app_id','=',$emp_update)->count();
            if ($sql_chk_B>0) {
                                $sql = DB::table('tb_approver')
                                        ->where('app_id','=',$emp_update)
                                        ->delete();
                              }
        }
      }

      elseif ($lv == '0') {
        $sql_chk_A = DB::table('tb_approver')->where('app_id','=',$emp_update)->count();
        if ($sql_chk_A>0) {
                            $sql = DB::table('tb_approver')
                                    ->where('app_id','=',$emp_update)
                                    ->delete();
                          }

        if ($lv_emp == '2') {
          $sql_chk_B = DB::table('tb_organizer')->where('or_id','=',$emp_update)->count();
            if ($sql_chk_B>0) {
                                $sql = DB::table('tb_organizer')
                                        ->where('or_id','=',$emp_update)
                                        ->delete();
                              }
        }

        if ($lv_emp == '1'||$lv_emp == '11') {
          $sql_chk_B = DB::table('tb_approver')->where('app_id','=',$emp_update)->count();
            if ($sql_chk_B>0) {
                                $sql = DB::table('tb_approver')
                                        ->where('app_id','=',$emp_update)
                                        ->delete();
                              }
        }

      }

        if (!$sqlUpdate){
          $msg = array("type"=>"error","success"=>false,"msg"=>"Error");
          }
        else {
          $msg = array("type"=>"","success"=>true,"msg"=>"");
        }
    return Response(json_encode($msg));

    }

    public function SetApprove()
    {
      if (session()->has('user'))
      {
        $username = session()->get('user');
        $users = DB::table('tb_employee')
                ->join("tb_department",function($join){
                        $join->on('tb_employee.dep_id', '=', 'tb_department.dep_id')
                             ->on("tb_employee.com_id","=","tb_department.com_id");
                              })
                ->join('tb_employee_login', 'tb_employee.emp_id', '=', 'tb_employee_login.emp_id')
                ->where('tb_employee.emp_email','=',$username)->get();
        return view('employee.SetApprover',['users'=>$users]);
      }
      else
      {
        exit("<script>window.location='/';</script>");
      }
    }

    public function SetOrganize()
    {
      if (session()->has('user'))
      {
        $username = session()->get('user');
        $users = DB::table('tb_employee')
                ->join("tb_department",function($join){
                        $join->on('tb_employee.dep_id', '=', 'tb_department.dep_id')
                             ->on("tb_employee.com_id","=","tb_department.com_id");
                              })
                ->join('tb_employee_login', 'tb_employee.emp_id', '=', 'tb_employee_login.emp_id')
                ->where('tb_employee.emp_email','=',$username)->get();
        return view('employee.SetOrganize',['users'=>$users]);
      }
      else
      {
        exit("<script>window.location='/';</script>");
      }
    }

    public function Approver(Request $req)
    {
        $id = $req->input('id');
        return view('Models.Employee.Approve',['id'=>$id]);
    }

    public function ApproveShow(Request $req)
    {
      $emp_id = $req->input('emp_id');
      $com_id = $req->input('com_id');

      $sql=DB::table('tb_approver')
                ->join("tb_department",function($join){
                  $join->on('tb_department.dep_id', '=', 'tb_approver.dep_id')
                       ->on("tb_department.com_id","=","tb_approver.com_id");
                  })
                ->where('tb_approver.app_id','=',$emp_id)->where('tb_approver.com_id','=',$com_id)->get();
      foreach ($sql as $dep):
        echo $dep->dep_name." <i class='fa fa-trash deleteApprove' data-id='".$dep->dep_id."'></i><br />";
      endforeach;

    }

    public function SetApproveDep(Request $req)
    {
      $emp_id = $req->input('emp_id');
      $com_id = $req->input('com_id');
      $sql_dep = DB::table('tb_department')->where('com_id','=',$com_id)->get();
      foreach ($sql_dep as $dep):
      $sql=DB::table('tb_approver')
                ->join("tb_department",function($join){
                  $join->on('tb_department.dep_id', '=', 'tb_approver.dep_id')
                       ->on("tb_department.com_id","=","tb_approver.com_id");
                  })
                ->where('tb_approver.dep_id','=',$dep->dep_id)
                ->where('tb_approver.app_id','=',$emp_id)
                ->count();

          if ($sql > 0) {
          }
          else {
            echo "<option value='".$dep->dep_id."'>".$dep->dep_name."</option>";
          }

        endforeach;
    }


    public function SetApproveDB(Request $req)
    {
      $emp_id = $req->input('emp_id');
      $com_id = $req->input('com_id');
      $dep_id = $req->input('dep_id');

      $sql = DB::table('tb_approver')->where('app_id','=',$emp_id)->get();
            foreach ($sql as $app) {
              $app_count = $app->app_count;
            }
      if (count($sql)==0) {
            $app_count = 0;
      }

      $sqlINSERT = DB::table('tb_approver')
                    ->insert(['app_id'=>$emp_id,
                              'app_count'=>$app_count+1,
                              'com_id'=>$com_id,
                              'dep_id'=>$dep_id
                            ]);
        if (!$sqlINSERT){
          $msg = array("type"=>"error","success"=>false,"msg"=>"Error");
          }
        else {
          $msg = array("type"=>"","success"=>true,"msg"=>"");
        }
    return Response(json_encode($msg));

    }

    public function SetApproveDelete(Request $req)
    {
      $emp_id = $req->input('emp_id');
      $com_id = $req->input('com_id');
      $dep_id = $req->input('dep_id');

      $sqlDELETE = DB::table('tb_approver')
                    ->where(['app_id'=>$emp_id,'com_id'=>$com_id,
                    'dep_id'=>$dep_id])->delete();

      if (!$sqlDELETE){
        $msg = array("type"=>"error","success"=>false,"msg"=>"Error");
        }
      else {
        $msg = array("type"=>"","success"=>true,"msg"=>"");
      }
      return Response(json_encode($msg));

    }

    public function updateemail(Request $req)
    {
      $emp_id = $req->input('emp_id');
      $email = $req->input('email');
      $sqlemp =  DB::table('tb_employee')
                  ->where("emp_id","=",$emp_id)
                  ->update([
                            "emp_email"=>$email
                          ]);
      $sqlemp_login =  DB::table('tb_employee_login')
                  ->where("emp_id","=",$emp_id)
                  ->update([
                            "username"=>$email
                          ]);
      if (!$sqlemp_login && !$sqlemp){
          $msg = array("type"=>"error","success"=>false,"msg"=>"Error");
                      }
      else {
            $msg = array("type"=>"","success"=>true,"msg"=>"");
            session()->put('user', $email);
            $chk_login = DB::table('tb_employee_login')->where(['username'=>$email , 'emp_status'=>'0'])->get();
                  foreach ($chk_login as $r):
                     $emp_id = $r->emp_id;
                     $userlv = $r->emp_level;
                  endforeach;
                  if ($userlv <> 999) {
                      $sqluser_login =  DB::table('tb_user_login')->insert(
                                        [
                                         'user_id' => $emp_id,
                                         'log_date' => date("Y-m-d H:i:s"),
                                         'log_type' => "login",
                                        ]);
                         }
           }
      return Response(json_encode($msg));
    }



}
