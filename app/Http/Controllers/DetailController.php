<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class DetailController extends Controller
{
   public function DetailBooking(Request $req)
   {
       $bk_id = $req->input('bk');
       session()->put('bk_id', $bk_id);
       return view('Models.booking.detailbooking',['bk_id'=>$bk_id]);
   }


   public function DetailMap(Request $req)
   {
     $id = $req->input('data');
     return view('Models.booking.detailmap',['id'=>$id]);
   }



}
