<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class CompanyController extends Controller
{
    //
    public function AddCompany(Request $req)
    {
      $emp_id =$req->input("id");
      return view('Models.Company.addcom',['emp_id'=>$emp_id]);

    }

    public function AddDB(Request $req)
    {
      date_default_timezone_set("Asia/Bangkok");
      $emp_id = strtoupper($req->input("emp_id")) ;
      $com_id=  $req->input("com_id");
      $com_name = $req->input("com_name");
      $com_code = $req->input("com_code");
      $msg = array();
              $date =date("Y-m-d H:i:s");
              if ($com_name =='') {
                $msg = array("type"=>"com_name","success"=>false,"msg"=>"Please enter your Company Name.","data"=>"");
              }
              else {

                if ($com_code =='') {
                  $msg = array("type"=>"com_code","success"=>false,"msg"=>"Please enter your Company Code.","data"=>"");
                }
                else {
               $sqlUPDATE = DB::table('tb_company')
                            ->insert(['com_id' => $com_id,
                                      'com_name' => $com_name,
                                      'com_code' => $com_code,
                                      'com_status' => '0',
                                      'create_by' =>$emp_id,
                                      'create_date'=>$date]);
                                      $msg = array("type"=>"","success"=>true,"msg"=>"");
                }
             }
              return Response(json_encode($msg));
      }
}
