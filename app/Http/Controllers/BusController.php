<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class BusController extends Controller
{
    //

  public function BusView()
  {
    if (session()->has('user'))
    {
      $username = session()->get('user');
      $users = DB::table('tb_employee')
              ->join("tb_department",function($join){
                        $join->on('tb_employee.dep_id', '=', 'tb_department.dep_id')
                             ->on("tb_employee.com_id","=","tb_department.com_id");
                    })
              ->join('tb_employee_login', 'tb_employee.emp_id', '=', 'tb_employee_login.emp_id')
              ->where('tb_employee.emp_email','=',$username)->get();
      return view('booking.bus',['users'=>$users]);
    }
    else
    {
      exit("<script>window.location='/';</script>");
    }
  }

  public function SelectCar(Request $req)
  {
    $com_id = $req->input('com_id');
    $job_id = $req->input('job_id');
    $round = $req->input('time').$req->input('round');
    $time = $req->input('time');
    return view('Models.Bus.bus_select',['com_id'=>$com_id,'job_id'=>$job_id,'round'=>$round,'time'=>$time]);
  }

  public function InsertBus(Request $req)
  {
    date_default_timezone_set("Asia/Bangkok");
    $emp_id = $req->input('emp_id');
    $com_id = $req->input('com_id');
    $job_id = $req->input('job_id');
    $cdep = $req->input('job_id');
    $dep_id = $req->input('dep_id');
    $person = '0';
    $mtel = $req->input('mtel');
    $bk_date =date("Y-m-d H:i:s");
    $date = str_replace("/","-",$req->input('bk_start_date'));
    $status = "success";
    $req->input('round_am');
    $req->input('round_pm');

    // insert
    if ($req->input('round_am') != "") {
      //round_am
      for ($i=1; $i <= $req->input('round_am'); $i++) {
        // echo "am".$i;
        // gen_id
          $id = "";
          $ymd = date("ymd");
          $sql_booking = DB::table('tb_booking')->select('bk_id')->orderBy('bk_id','desc')->limit(1)->get();
        foreach ($sql_booking as $bk):
          $last_id = $bk->bk_id;
        endforeach;
          $old = explode("BK",$last_id);
          $old_ymd = substr($old[1],0,6);
          $old_id = substr($old[1],6,4);
          if($old_ymd == $ymd){
            $id = $old_id+1;
          }else{
            $id = 1;
          }
        $bk_id = "BK".$ymd.sprintf("%04d",$id);
        // end gen_id

        $bk_start_start_am = $date." ".$req->input("time_am");
        $bk_end_start_am = $date." ".substr($req->input("time_am"),0,2).":59";
        $obj = 'รับส่งพนักงานรอบเช้า คันที่'.$i;

        $car_am = $req->input("caram".$i);
        $sql_car_am = DB::table('tb_car')->where('car_id','=',$car_am)->get();
        foreach ($sql_car_am as $car) {
          $ctype = $car->ctype_id;
        }
        $driver_am = $req->input("driveram".$i);

        $sqlbooking =  DB::table('tb_booking')->insert(
                    [ 'bk_id' => "$bk_id",
                      'com_id' => "$com_id",
                      'bk_date' => "$bk_date",
                      'bk_start_start' => "$bk_start_start_am",
                      'bk_end_start' => "$bk_end_start_am",
                      'bk_percon' => "$person",
                      'bk_obj' => "$obj",
                      'dep_id' => "$dep_id",
                      'job_id' => "$job_id",
                      'dep_car'=> "$cdep",
                      'bk_mtel'=>"$mtel",
                      'emp_id' => "$emp_id",
                      'bk_status' => "$status",
                      'ctype_id' => "$ctype",
                      'approve_by' =>"SR600000000",
                      'approve_date'=>"$bk_date",
                      'setcar_by' =>"SR600000000",
                      'setcar_date'=>"$bk_date",
                      'car_id'=>"$car_am",
                      'drive_id'=>"$driver_am"
                    ]);
        // add location
        // for($i=0;$i<2;$i++){
        //    echo $where = $req->input('bk_where'.($i+1));
              $sqlbooking_locate1 =DB::table('tb_booking_location')->insert(
                ['bk_id' => "$bk_id",
                'com_id' => "$com_id",
                'location_id' => 1,
                'location_name' => $req->input('bk_where1')
                ]);
              $sqlbooking_locate2 =DB::table('tb_booking_location')->insert(
                  ['bk_id' => "$bk_id",
                  'com_id' => "$com_id",
                  'location_id' => 2,
                  'location_name' => $req->input('bk_where2')
                  ]);
          // }
        // add log
        $Sh = substr($req->input("time_am"),0,2);
        $Eh = substr($req->input("time_am"),0,2);

        //=======================================Car=======================================
        $carlogid = DB::table('tb_log_car')->where('com_id','=',$com_id)->orderBy('log_id','desc')->limit(1)->get();
        if (count($carlogid)>0) {
          foreach ($carlogid as $id) {
              $logcar = $id->log_id;
          }
        }
        else {
          $logcar = 0;
        }

        $countcar = DB::table('tb_log_car')->where('com_id','=',$com_id)->where('car_id','=',$car_am)->where('log_date', '=' ,substr($bk_start_start_am,0,10))->count();
        if ($countcar==1) {
          for ($j=$Sh; $j <= $Eh ; $j++) {
            $sqlCarUPDATE = DB::table('tb_log_car')
                            ->where('car_id', '=' ,$car_am)
                            ->where('dep_car','=',$cdep)
                            ->where('com_id','=',$com_id)
                            ->where('log_date', '=' ,substr($bk_start_start_am,0,10))
                            ->update([
                                         'log_'.(int)$j => $bk_id
                                    ]);
          }//for
        }//have log
        else {
           $sqllogcar = DB::table('tb_log_car')
                      ->insert([
                                'log_id'=>$logcar+1,
                                'dep_car'=>$cdep,
                                'com_id'=>$com_id,
                                'log_date'=>substr($bk_start_start_am,0,10),
                                'car_id'=>$car_am
                              ]);

          for ($j=$Sh; $j <= $Eh ; $j++) {
            $sqlCarUPDATE = DB::table('tb_log_car')
                          ->where('car_id', '=' ,$car_am)
                          ->where('com_id','=',$com_id)
                          ->where('dep_car','=',$cdep)
                          ->where('log_date', '=' ,substr($bk_start_start_am,0,10))
                          ->update([
                                    'log_'.(int)$j => $bk_id
                            ]);
          }//for
        }//no log
        //=======================================Endcar=======================================
        //=======================================Driver=======================================
          $countdriver = DB::table('tb_log_driver')->where('com_id','=',$com_id)->where('drive_id','=',$driver_am)->where('log_date', '=' ,substr($bk_start_start_am,0,10))->count();
          if ($countdriver==1) {
            for ($j=$Sh; $j <= $Eh ; $j++) {
              $sqlDriveUPDATE = DB::table('tb_log_driver')
                                ->where('drive_id', '=' ,$driver_am)
                                ->where('dep_drive','=',$cdep)
                                ->where('com_id','=',$com_id)
                                ->where('log_date', '=' ,substr($bk_start_start_am,0,10))
                                ->update([
                                          'log_'.(int)$j => $bk_id
                                        ]);
            }//for
          }//have log
          else {
            $drivelogid =DB::table('tb_log_driver')->where('com_id','=',$com_id)->orderBy('log_id','desc')->limit(1)->get();
            if (count($drivelogid)>0) {
              foreach ($drivelogid as $id) {
                  $logdriver = $id->log_id;
              }
            }
            else {
              $logdriver = 0;
            }
             $sqllogdriver = DB::table('tb_log_driver')
                            ->insert([
                                  'log_id'=>$logdriver+1,
                                  'dep_drive'=>$cdep,
                                  'com_id'=>$com_id,
                                  'log_date'=>substr($bk_start_start_am,0,10),
                                  'drive_id'=>$driver_am
                                ]);
             for ($j=$Sh; $j <= $Eh ; $j++) {
                $sqlDriveUPDATE = DB::table('tb_log_driver')
                                ->where('drive_id', '=' ,$driver_am)
                                ->where('com_id','=',$com_id)
                                ->where('dep_drive','=',$cdep)
                                ->where('log_date', '=' ,substr($bk_start_start_am,0,10))
                                ->update([
                                          'log_'.(int)$j => $bk_id
                                        ]);
                }//for
            }//no log
        //=======================================EndDriver=======================================
      }//for round_am
    }//if am

    if ($req->input('round_pm') != "") {
      //round_pm
      for ($i=1; $i <= $req->input('round_pm'); $i++) {
        // echo "pm".$i;

        // gen_id
          $id = "";
          $ymd = date("ymd");
          $sql_booking = DB::table('tb_booking')->select('bk_id')->orderBy('bk_id','desc')->limit(1)->get();
        foreach ($sql_booking as $bk):
          $last_id = $bk->bk_id;
        endforeach;
          $old = explode("BK",$last_id);
          $old_ymd = substr($old[1],0,6);
          $old_id = substr($old[1],6,4);
          if($old_ymd == $ymd){
            $id = $old_id+1;
          }else{
            $id = 1;
          }
        $bk_id = "BK".$ymd.sprintf("%04d",$id);
        // end gen_id

        $bk_start_start_pm = $date." ".$req->input("time_pm");
        $bk_end_start_pm = $date." ".substr($req->input("time_pm"),0,2).":59";
        $obj = 'รับส่งพนักงานรอบเย็น คันที่'.$i;

        $car_pm = $req->input("carpm".$i);
        $sql_car_pm = DB::table('tb_car')->where('car_id','=',$car_pm)->get();
        foreach ($sql_car_pm as $car) {
          $ctype = $car->ctype_id;
        }
        $driver_pm = $req->input("driverpm".$i);

        $sqlbooking =  DB::table('tb_booking')->insert(
                    [ 'bk_id' => "$bk_id",
                      'com_id' => "$com_id",
                      'bk_date' => "$bk_date",
                      'bk_start_start' => "$bk_start_start_pm",
                      'bk_end_start' => "$bk_end_start_pm",
                      'bk_percon' => "$person",
                      'bk_obj' => "$obj",
                      'dep_id' => "$dep_id",
                      'job_id' => "$job_id",
                      'dep_car'=> "$cdep",
                      'bk_mtel'=>"$mtel",
                      'emp_id' => "$emp_id",
                      'bk_status' => "$status",
                      'ctype_id' => "$ctype",
                      'approve_by' =>"SR600000000",
                      'approve_date'=>"$bk_date",
                      'setcar_by' =>"SR600000000",
                      'setcar_date'=>"$bk_date",
                      'car_id'=>"$car_pm",
                      'drive_id'=>"$driver_pm"
                    ]);

        // add location
        // for($i=0;$i<2;$i++){
        //    echo $where = $req->input('bk_where'.($i+1));
              $sqlbooking_locate1 =DB::table('tb_booking_location')->insert(
                ['bk_id' => "$bk_id",
                'com_id' => "$com_id",
                'location_id' => 1,
                'location_name' => $req->input('bk_where1')
                ]);
              $sqlbooking_locate2 =DB::table('tb_booking_location')->insert(
                  ['bk_id' => "$bk_id",
                  'com_id' => "$com_id",
                  'location_id' => 2,
                  'location_name' => $req->input('bk_where2')
                  ]);
          // }
        // add log
         $Sh = substr($req->input("time_pm"),0,2);
         $Eh = substr($req->input("time_pm"),0,2);

        //=======================================Car=======================================
        $carlogid = DB::table('tb_log_car')->where('com_id','=',$com_id)->orderBy('log_id','desc')->limit(1)->get();
        if (count($carlogid)>0) {
          foreach ($carlogid as $id) {
              $logcar = $id->log_id;
          }
        }
        else {
          $logcar = 0;
        }

        $countcar = DB::table('tb_log_car')->where('com_id','=',$com_id)->where('car_id','=',$car_pm)->where('log_date', '=' ,substr($bk_start_start_pm,0,10))->count();
        if ($countcar==1) {
          for ($j=$Sh; $j <= $Eh ; $j++) {

            $sqlCarUPDATE = DB::table('tb_log_car')
                            ->where('car_id', '=' ,$car_pm)
                            ->where('dep_car','=',$cdep)
                            ->where('com_id','=',$com_id)
                            ->where('log_date', '=' ,substr($bk_start_start_pm,0,10))
                            ->update([
                                         'log_'.(int)$j => $bk_id
                                     ]);
          }//for
        }//have log
        else {
           $sqllogcar = DB::table('tb_log_car')
                      ->insert([
                                'log_id'=>$logcar+1,
                                'dep_car'=>$cdep,
                                'com_id'=>$com_id,
                                'log_date'=>substr($bk_start_start_pm,0,10),
                                'car_id'=>$car_pm
                              ]);

          for ($j=$Sh; $j <= $Eh ; $j++) {
            $sqlCarUPDATE = DB::table('tb_log_car')
                          ->where('car_id', '=' ,$car_pm)
                          ->where('com_id','=',$com_id)
                          ->where('dep_car','=',$cdep)
                          ->where('log_date', '=' ,substr($bk_start_start_pm,0,10))
                          ->update([
                                    'log_'.(int)$j => $bk_id
                                  ]);
          }//for
        }//no log
        //=======================================Endcar=======================================
        //=======================================Driver=======================================
          $countdriver = DB::table('tb_log_driver')->where('com_id','=',$com_id)->where('drive_id','=',$driver_pm)->where('log_date', '=' ,substr($bk_start_start_pm,0,10))->count();
          if ($countdriver==1) {
            for ($j=$Sh; $j <= $Eh ; $j++) {
              $sqlDriveUPDATE = DB::table('tb_log_driver')
                                ->where('drive_id', '=' ,$driver_pm)
                                ->where('dep_drive','=',$cdep)
                                ->where('com_id','=',$com_id)
                                ->where('log_date', '=' ,substr($bk_start_start_pm,0,10))
                                ->update([
                                          'log_'.(int)$j => $bk_id
                                        ]);
            }//for
          }//have log
          else {
            $drivelogid =DB::table('tb_log_driver')->where('com_id','=',$com_id)->orderBy('log_id','desc')->limit(1)->get();
            if (count($drivelogid)>0) {
              foreach ($drivelogid as $id) {
                  $logdriver = $id->log_id;
              }
            }
            else {
              $logdriver = 0;
            }
             $sqllogdriver = DB::table('tb_log_driver')
                            ->insert([
                                  'log_id'=>$logdriver+1,
                                  'dep_drive'=>$cdep,
                                  'com_id'=>$com_id,
                                  'log_date'=>substr($bk_start_start_pm,0,10),
                                  'drive_id'=>$driver_am
                                ]);
             for ($j=$Sh; $j <= $Eh ; $j++) {
                $sqlDriveUPDATE = DB::table('tb_log_driver')
                                ->where('drive_id', '=' ,$driver_am)
                                ->where('com_id','=',$com_id)
                                ->where('dep_drive','=',$cdep)
                                ->where('log_date', '=' ,substr($bk_start_start_pm,0,10))
                                ->update([
                                          'log_'.(int)$j => $bk_id
                                        ]);
              }//for
          }//no log
      //=======================================EndDriver=======================================
      }//for round_pm
    }//if pm
      if(!$sqlbooking and !$sqlCarUPDATE and !$sqlDriveUPDATE ){
        $msg = array("success"=>false,"msg"=>"");//2
      }else{
        $msg = array("success"=>true,"msg"=>"");//1
      }
      return Response(json_encode($msg));
  }
}
