<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class OtherController extends Controller
{
    //
    public function addAll()
    {
      if (session()->has('user'))
      {
        $username = session()->get('user');
        $users = DB::table('tb_employee')
                ->join("tb_department",function($join){
                        $join->on('tb_employee.dep_id', '=', 'tb_department.dep_id')
                             ->on("tb_employee.com_id","=","tb_department.com_id");
                              })
                ->join('tb_employee_login', 'tb_employee.emp_id', '=', 'tb_employee_login.emp_id')
                ->where('tb_employee.emp_email','=',$username)->get();
        return view('other.otheradd',['users'=>$users]);
      }
      else
      {
        exit("<script>window.location='/';</script>");
      }
    }

    public function EditShow()
    {
      if (session()->has('user'))
      {
        $username = session()->get('user');
        $users = DB::table('tb_employee')
                ->join("tb_department",function($join){
                        $join->on('tb_employee.dep_id', '=', 'tb_department.dep_id')
                             ->on("tb_employee.com_id","=","tb_department.com_id");
                              })
                ->join('tb_employee_login', 'tb_employee.emp_id', '=', 'tb_employee_login.emp_id')
                ->where('tb_employee.emp_email','=',$username)->get();
        return view('other.otheredit',['users'=>$users]);
      }
      else
      {
        exit("<script>window.location='/';</script>");
      }
    }

    public function EditDep()
    {
      if (session()->has('user'))
      {
        $username = session()->get('user');
        $users = DB::table('tb_employee')
                ->join("tb_department",function($join){
                        $join->on('tb_employee.dep_id', '=', 'tb_department.dep_id')
                             ->on("tb_employee.com_id","=","tb_department.com_id");
                              })
                ->join('tb_employee_login', 'tb_employee.emp_id', '=', 'tb_employee_login.emp_id')
                ->where('tb_employee.emp_email','=',$username)->get();
        return view('other.editdep',['users'=>$users]);
      }
      else
      {
        exit("<script>window.location='/';</script>");
      }
    }

    public function EditJob()
    {
      if (session()->has('user'))
      {
        $username = session()->get('user');
        $users = DB::table('tb_employee')
                ->join("tb_department",function($join){
                        $join->on('tb_employee.dep_id', '=', 'tb_department.dep_id')
                             ->on("tb_employee.com_id","=","tb_department.com_id");
                              })
                ->join('tb_employee_login', 'tb_employee.emp_id', '=', 'tb_employee_login.emp_id')
                ->where('tb_employee.emp_email','=',$username)->get();
        return view('other.editjob',['users'=>$users]);
      }
      else
      {
        exit("<script>window.location='/';</script>");
      }
    }

    public function EditcType()
    {
      if (session()->has('user'))
      {
        $username = session()->get('user');
        $users = DB::table('tb_employee')
                ->join("tb_department",function($join){
                        $join->on('tb_employee.dep_id', '=', 'tb_department.dep_id')
                             ->on("tb_employee.com_id","=","tb_department.com_id");
                              })
                ->join('tb_employee_login', 'tb_employee.emp_id', '=', 'tb_employee_login.emp_id')
                ->where('tb_employee.emp_email','=',$username)->get();
        return view('other.editctype',['users'=>$users]);
      }
      else
      {
        exit("<script>window.location='/';</script>");
      }
    }

    public function EditColor()
    {
      if (session()->has('user'))
      {
        $username = session()->get('user');
        $users = DB::table('tb_employee')
                ->join("tb_department",function($join){
                        $join->on('tb_employee.dep_id', '=', 'tb_department.dep_id')
                             ->on("tb_employee.com_id","=","tb_department.com_id");
                              })
                ->join('tb_employee_login', 'tb_employee.emp_id', '=', 'tb_employee_login.emp_id')
                ->where('tb_employee.emp_email','=',$username)->get();
        return view('other.editcolor',['users'=>$users]);
      }
      else
      {
        exit("<script>window.location='/';</script>");
      }
    }

    public function EditFuel()
    {
      if (session()->has('user'))
      {
        $username = session()->get('user');
        $users = DB::table('tb_employee')
                ->join("tb_department",function($join){
                        $join->on('tb_employee.dep_id', '=', 'tb_department.dep_id')
                             ->on("tb_employee.com_id","=","tb_department.com_id");
                              })
                ->join('tb_employee_login', 'tb_employee.emp_id', '=', 'tb_employee_login.emp_id')
                ->where('tb_employee.emp_email','=',$username)->get();
        return view('other.editfuel',['users'=>$users]);
      }
      else
      {
        exit("<script>window.location='/';</script>");
      }
    }

    public function EditBrand()
    {
      if (session()->has('user'))
      {
        $username = session()->get('user');
        $users = DB::table('tb_employee')
                ->join("tb_department",function($join){
                        $join->on('tb_employee.dep_id', '=', 'tb_department.dep_id')
                             ->on("tb_employee.com_id","=","tb_department.com_id");
                              })
                ->join('tb_employee_login', 'tb_employee.emp_id', '=', 'tb_employee_login.emp_id')
                ->where('tb_employee.emp_email','=',$username)->get();
        return view('other.editbrand',['users'=>$users]);
      }
      else
      {
        exit("<script>window.location='/';</script>");
      }
    }
}
