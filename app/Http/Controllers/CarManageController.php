<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use DateTime;

class CarManageController extends Controller
{
    //
  public function CarView()
    {
      if (session()->has('user'))
      {
        $username = session()->get('user');
        $users = DB::table('tb_employee')
                ->join("tb_department",function($join){
                        $join->on('tb_employee.dep_id', '=', 'tb_department.dep_id')
                             ->on("tb_employee.com_id","=","tb_department.com_id");
                              })
                ->join('tb_employee_login', 'tb_employee.emp_id', '=', 'tb_employee_login.emp_id')
                ->where('tb_employee.emp_email','=',$username)->get();
        return view('booking.Car',['users'=>$users,'valuepage'=>'approve']);
      }
      else
      {
        exit("<script>window.location='/';</script>");
      }
    }

public function TableCar(Request $req)
    {
      $com_id= $req->input('com_id');
      $typeSearch = $req->input('typeSearch');
      $txtSearch = $req->input('txtSearch');
      $carType = $req->input('carType');
      $carBrand = $req->input('carBrand');
      $lv_user = $req->input('lv_user');
      $dep_id = $req->input('dep_id');
      return view('Models.Car.tablecar',['com_id'=>$com_id,'dep_id'=>$dep_id,'typeSearch'=>$typeSearch,'txtSearch'=>$txtSearch,'carType'=>$carType,'carBrand'=>$carBrand,'lv_user'=>$lv_user]);
    }

 public function TbSetCar(Request $req)
    {
      $ctype= $req->input('ctype');
      $com_id= $req->input('com_id');
      $bk_id = $req->input('bk');
      return view('Models.Car.tablesetcar',['com_id'=>$com_id,'ctype'=>$ctype,'bk_id'=>$bk_id]);
    }


 public function DetailCar(Request $req)
    {
      $car_id = $req->input('data');
      return view('Models.Car.detailcar',['car_id'=>$car_id]);
    }

 public function AddCar(Request $req)
    {
        $emp_id =$req->input("id");
        $com_id =$req->input("com_id");
        $dep_id = $req->input("dep_id");
        return view('Models.Car.addcar',['emp_id'=>$emp_id,'com_id'=>$com_id,'dep_id'=>$dep_id]);
    }

  public function ChangeCar(Request $req)
      {
        $bk_id =$req->input("id");
        $com_id =$req->input("com_id");
        $dep_id = $req->input("dep_id");
        if (session()->has('user'))
        {
          $username = session()->get('user');
          $users = DB::table('tb_employee')
                  ->join("tb_department",function($join){
                        $join->on('tb_employee.dep_id', '=', 'tb_department.dep_id')
                             ->on("tb_employee.com_id","=","tb_department.com_id");
                              })
                  ->join('tb_employee_login', 'tb_employee.emp_id', '=', 'tb_employee_login.emp_id')
                  ->where('tb_employee.emp_email','=',$username)->get();
          return view('Models.Car.changecar',['users'=>$users,'bk_id'=>$bk_id,'com_id'=>$com_id,'dep_id'=>$dep_id]);
        }
        else
        {
          exit("<script>window.location='/';</script>");
        }
     }

      public function AddCarDB(Request $req)
     {
          date_default_timezone_set("Asia/Bangkok");
           $emp_id =  $req->input("emp_id");
           $com_id = $req->input("com_id");
           $dep_id = $req->input("dep_id");
           $car_id = $req->input("car_id");
           $brand_id = $req->input("brand_id");
           $car_model = $req->input("car_model");
           $car_number = $req->input("car_number");
           $ctype = $req->input("ctype");
           $fuel_id = $req->input("fuel_id");
           $color_id = $req->input("color_id");
           $car_status = $req->input("car_status");
           $car_img_front = "no_img.png";
           $car_img_left = "no_img.png";
           $car_img_right = "no_img.png";
           $car_img_back = "no_img.png";
           $date =date("Y-m-d H:i:s");
           if ($req->hasFile('car_img_front')) {
             //upload file
               $filename = "IMG_".time()."F";
               $type = $req->car_img_front->extension();
               if ( $type <> "jpeg" && $type <> "jpg" && $type <> "png") {
                 $msg = array("success"=>false,"msg"=>"type");
                 return Response(json_encode($msg));
               }
               $car_img_front = $filename.'.'.$type;
               $file = $req->car_img_front->storeAs('public/image/car',$car_img_front);
            }
            if ($req->hasFile('car_img_left')) {
              //upload file
                $filename = "IMG_".time()."L";
                $type = $req->car_img_left->extension();
                if ( $type <> "jpeg" && $type <> "jpg" && $type <> "png") {
                  $msg = array("success"=>false,"msg"=>"type");
                  return Response(json_encode($msg));
                }
                $car_img_left = $filename.'.'.$type;
                $file = $req->car_img_left->storeAs('public/image/car',$car_img_left);
            }
            if ($req->hasFile('car_img_right')) {
              //upload file
                $filename = "IMG_".time()."R";
                $type = $req->car_img_right->extension();
                if ( $type <> "jpeg" && $type <> "jpg" && $type <> "png") {
                  $msg = array("success"=>false,"msg"=>"type");
                  return Response(json_encode($msg));
                }
                $car_img_right = $filename.'.'.$type;
                $file = $req->car_img_right->storeAs('public/image/car',$car_img_right);
            }
            if ($req->hasFile('car_img_back')) {
              //upload file
                $filename = "IMG_".time()."B";
                $type = $req->car_img_back->extension();
                if ( $type <> "jpeg" && $type <> "jpg" && $type <> "png") {
                  $msg = array("success"=>false,"msg"=>"type");
                  return Response(json_encode($msg));
                }
                $car_img_back = $filename.'.'.$type;
                $file = $req->car_img_back->storeAs('public/image/car',$car_img_back);
            }

                    $sql= DB::table('tb_car')
                                 ->insert(['car_id'=>$car_id,
                                 'com_id'=>$com_id,
                                 'dep_id'=>$dep_id,
                                 'dep_car'=>$dep_id,
                                 'car_model'=>$car_model,
                                 'car_number'=>$car_number,
                                 'brand_id'=>$brand_id,
                                 'fuel_id'=>$fuel_id,
                                 'color_id'=>$color_id,
                                 'car_status'=>$car_status,
                                 'ctype_id'=>$ctype,
                                 'create_by'=>$emp_id,
                                 'create_date'=>$date,
                                 'car_img_front'=>$car_img_front,
                                 'car_img_left'=>$car_img_left,
                                 'car_img_right'=>$car_img_right,
                                 'car_img_back'=>$car_img_back]);

            $msg ="";
              if (!$sql) {
                $msg = array("success"=>false,"msg"=>"");
              }
              else {
                $msg = array("success"=>true,"msg"=>"");
              }
          return Response(json_encode($msg));

         }

         public function SetCar(Request $req)
         {
           $bk_id = $req->input("bk");
           return view('Models.Car.setcar',['bk_id'=>$bk_id]);

         }

         public function SetCarDB(Request $req)
         {
           date_default_timezone_set("Asia/Bangkok");
           $car = $req->input("car") ;
           $drive =  $req->input("drive");
           $dataid = $req->input("dataid");
           $username = session()->get('user');
           $sqlemp = DB::table('tb_employee')->where('emp_email','=',$username)->get();
           foreach ($sqlemp as $emp) {
             $emp_id = $emp->emp_id;
           }
           $date = date("Y-m-d H:i:s");
           $msg = array();
           $sql_bk = DB::table('tb_booking')->where('bk_id','=',$dataid)->get();
           foreach ($sql_bk as $bk) {
             $bk_start = $bk->bk_start_start;
             $bk_end = $bk->bk_end_start;
             $date1 = substr($bk->bk_start_start,0,10);
             $date2 = substr($bk->bk_end_start,0,10);
             $com_id = $bk->com_id;
             $dep_car = $bk->dep_car;
             $bk_use = $bk->bk_use;
             $Sh = substr($bk->bk_start_start,11,2);
             $End_h = substr($bk->bk_end_start,11,2);
           }
           // $date1 = substr($bk_start,0,10);
           // $date2 = substr($bk_end,0,10);
           $d1= new DateTime($date1);
           $d2 = new DateTime($date2);
           $diff = $d1->diff($d2)->format("%a");
           if ($End_h == '00') {
             $Eh = 23;
           }
           else {
             $Eh = $End_h;
           }
            //gen_bkjob
              $id = "";
              $ymd = date("ymd");
              $cardep = explode("JOB",$dep_car);
              $sql_bkjob = DB::table('tb_booking')->select('bk_job')->where('bk_job','like','SJ'.$ymd.$cardep[1].'%')->orderBy('bk_job','desc')->limit(1)->get();

              if (count($sql_bkjob)<=0) {
                 $id = 1;
              }else
              {
                foreach ($sql_bkjob as $sj):
                  $last_id = $sj->bk_job;
                endforeach;
                  $old = explode("SJ",$last_id);
                  $old_ymd = substr($old[1],0,6);
                  $old_dep = substr($old[1],6,3);
                  $old_id = substr($old[1],10,4);
                  if($old_ymd.$old_dep == $ymd.$cardep[1]){
                    $id = $old_id+1;
                  }else{
                    $id = 1;
                  }
                // end gen_bkjob
              }
                  $bk_job = "SJ".$ymd.$cardep[1].sprintf("%04d",$id);

                  $sqlbookingUPDATE = DB::table('tb_booking')
                              ->where('bk_id', '=' ,$dataid)
                              ->update(['bk_status' => 'success',
                                           'bk_job'=>$bk_job,
                                           'car_id' =>$car,
                                           'drive_id' => $drive,
                                           'setcar_by' => $emp_id,
                                           'setcar_date' => $date]);
                 // booking ใช้ภายในวันเดียวกัน
                  if ($diff==0) {
                    //=======================================Car=======================================
                    $carlogid = DB::table('tb_log_car')->where('com_id','=',$com_id)->orderBy('log_id','desc')->limit(1)->get();
                    if (count($carlogid)>0) {
                      foreach ($carlogid as $id) {
                          $logcar = $id->log_id;
                      }
                    }
                    else {
                      $logcar = 0;
                    }

                    $countcar = DB::table('tb_log_car')->where('com_id','=',$com_id)->where('car_id','=',$car)->where('log_date', '=' ,$date1)->count();
                    if ($countcar==1) {
                      for ($i=$Sh; $i <= $Eh ; $i++) {

                        $sqlCarUPDATE = DB::table('tb_log_car')
                                        ->where('car_id', '=' ,$car)
                                        ->where('dep_car','=',$dep_car)
                                        ->where('com_id','=',$com_id)
                                        ->where('log_date', '=' ,$date1)
                                        ->update([
                                                     'log_'.(int)$i => $dataid
                                                ]);
                      }//for
                    }//have log
                    else {
                      $msg = array("success"=>true,"msg"=>"","bk_id"=>$dataid);
                       $sqllogcar = DB::table('tb_log_car')
                                  ->insert([
                                            'log_id'=>$logcar+1,
                                            'dep_car'=>$dep_car,
                                            'com_id'=>$com_id,
                                            'dep_car'=>$dep_car,
                                            'log_date'=>$date1,
                                            'car_id'=>$car
                                          ]);

                      for ($i=$Sh; $i <= $Eh ; $i++) {
                        $sqlCarUPDATE = DB::table('tb_log_car')
                                      ->where('car_id', '=' ,$car)
                                      ->where('com_id','=',$com_id)
                                      ->where('dep_car','=',$dep_car)
                                      ->where('log_date', '=' ,$date1)
                                      ->update([
                                                'log_'.(int)$i => $dataid
                                              ]);
                      }//for
                    }//no log
                    //=======================================Endcar=======================================
                    //=======================================Driver=======================================
                    if ($drive <> 'D0000000000') {
                      $countdriver = DB::table('tb_log_driver')->where('com_id','=',$com_id)->where('drive_id','=',$drive)->where('log_date', '=' ,$date1)->count();
                      if ($countdriver==1) {
                        $msg = array("success"=>true,"msg"=>"","bk_id"=>$dataid);
                        for ($i=$Sh; $i <= $Eh ; $i++) {
                          $sqlDriveUPDATE = DB::table('tb_log_driver')
                                            ->where('drive_id', '=' ,$drive)
                                            ->where('dep_drive','=',$dep_car)
                                            ->where('com_id','=',$com_id)
                                            ->where('log_date', '=' ,$date1)
                                            ->update([
                                                      'log_'.(int)$i => $dataid
                                                    ]);
                        }//for
                      }//have log
                      else {
                        $drivelogid =DB::table('tb_log_driver')->where('com_id','=',$com_id)->orderBy('log_id','desc')->limit(1)->get();
                        if (count($drivelogid)>0) {
                          foreach ($drivelogid as $id) {
                              $logdriver = $id->log_id;
                          }
                        }
                        else {
                          $logdriver = 0;
                        }
                        $msg = array("success"=>true,"msg"=>"","bk_id"=>$dataid);
                         $sqllogdriver = DB::table('tb_log_driver')
                                        ->insert([
                                              'log_id'=>$logdriver+1,
                                              'dep_drive'=>$dep_car,
                                              'com_id'=>$com_id,
                                              'log_date'=>$date1,
                                              'drive_id'=>$drive
                                            ]);
                         for ($i=$Sh; $i <= $Eh ; $i++) {
                            $sqlDriveUPDATE = DB::table('tb_log_driver')
                                            ->where('drive_id', '=' ,$drive)
                                            ->where('com_id','=',$com_id)
                                            ->where('dep_drive','=',$dep_car)
                                            ->where('log_date', '=' ,$date1)
                                            ->update([
                                                      'log_'.(int)$i => $dataid
                                                    ]);
                          }//for
                      }//no log
                    }//$drive
                  //=======================================EndDriver=======================================
                  }
                  // booking ใช้ข้ามวัน
                  else {
                  //=======================================Car=======================================

                      if ($bk_use == null) {
                        //first
                        $CarStart = DB::table('tb_log_car')->where('com_id','=',$com_id)->where('car_id','=',$car)->where('log_date', '=' ,$date1)->count();
                        if ($CarStart==1) {
                          for ($i=$Sh; $i <= 23 ; $i++) {
                            $sqlCarUPDATE = DB::table('tb_log_car')
                                            ->where('car_id', '=' ,$car)
                                            ->where('dep_car','=',$dep_car)
                                            ->where('com_id','=',$com_id)
                                            ->where('log_date', '=' ,$date1)
                                            ->update([
                                                         'log_'.(int)$i => $dataid
                                                    ]);
                          }//for
                        }//have log
                        else {
                          $carlogid = DB::table('tb_log_car')->where('com_id','=',$com_id)->orderBy('log_id','desc')->limit(1)->get();
                          if (count($carlogid)>0) {
                            foreach ($carlogid as $id) {
                                $logcar = $id->log_id;
                            }
                          }
                          else {
                            $logcar = 0;
                          }
                           //
                           $sqllogcar = DB::table('tb_log_car')
                                      ->insert([
                                                'log_id'=>$logcar+1,
                                                'dep_car'=>$dep_car,
                                                'com_id'=>$com_id,
                                                'dep_car'=>$dep_car,
                                                'log_date'=>$date1,
                                                'car_id'=>$car
                                              ]);

                          for ($i=$Sh; $i <= 23 ; $i++) {
                            $sqlCarUPDATE = DB::table('tb_log_car')
                                          ->where('car_id', '=' ,$car)
                                          ->where('com_id','=',$com_id)
                                          ->where('dep_car','=',$dep_car)
                                          ->where('log_date', '=' ,$date1)
                                          ->update([
                                                    'log_'.(int)$i => $dataid
                                                  ]);
                          }//for
                        }//no log

                        //between
                        for ($i=1; $i <= $diff-1; $i++) {
                          $tomorrow = date('Y-m-d',strtotime(str_replace('-', '/', $date1) . "+".$i." days"));
                          $logidcar =DB::table('tb_log_car')->where('com_id','=',$com_id)->where('log_date','=',$tomorrow)->orderBy('log_id','desc')->get();
                          if (count($logidcar)==0) {
                            $carlogid = DB::table('tb_log_car')->where('com_id','=',$com_id)->orderBy('log_id','desc')->limit(1)->get();
                            if (count($carlogid)>0) {
                              foreach ($carlogid as $id) {
                                  $logcar = $id->log_id;
                              }
                            }
                            else {
                              $logcar = 0;
                            }
                          $sqllogcar = DB::table('tb_log_car')
                                     ->insert([
                                               'log_id'=>$logcar+1,
                                               'dep_car'=>$dep_car,
                                               'com_id'=>$com_id,
                                               'dep_car'=>$dep_car,
                                               'log_date'=>$tomorrow,
                                               'car_id'=>$car
                                             ]);
                          }

                          for ($j=0; $j <= 23 ; $j++) {
                           $sqlCarUPDATE = DB::table('tb_log_car')
                                         ->where('car_id', '=' ,$car)
                                         ->where('com_id','=',$com_id)
                                         ->where('dep_car','=',$dep_car)
                                         ->where('log_date', '=' ,$tomorrow)
                                         ->update([
                                                   'log_'.(int)$j => $dataid
                                                 ]);
                          }//for
                        }//for diffdate

                        //last
                        $CarEnd = DB::table('tb_log_car')->where('com_id','=',$com_id)->where('car_id','=',$car)->where('log_date', '=' ,$date2)->count();
                        if ($CarEnd==1) {
                          for ($i=0 ; $i <= $Eh ; $i++) {
                            $sqlCarUPDATE = DB::table('tb_log_car')
                                            ->where('car_id', '=' ,$car)
                                            ->where('dep_car','=',$dep_car)
                                            ->where('com_id','=',$com_id)
                                            ->where('log_date', '=' ,$date2)
                                            ->update([
                                                         'log_'.(int)$i => $dataid
                                                    ]);
                          }//for
                        }//have log
                        else {
                          $carlogid = DB::table('tb_log_car')->where('com_id','=',$com_id)->orderBy('log_id','desc')->limit(1)->get();
                          if (count($carlogid)>0) {
                            foreach ($carlogid as $id) {
                                $logcar = $id->log_id;
                            }
                          }
                          else {
                            $logcar = 0;
                          }
                           $sqllogcar = DB::table('tb_log_car')
                                      ->insert([
                                                'log_id'=>$logcar+1,
                                                'dep_car'=>$dep_car,
                                                'com_id'=>$com_id,
                                                'dep_car'=>$dep_car,
                                                'log_date'=>$date2,
                                                'car_id'=>$car
                                              ]);

                          for ($i=0; $i <= $Eh ; $i++) {
                            $sqlCarUPDATE = DB::table('tb_log_car')
                                          ->where('car_id', '=' ,$car)
                                          ->where('com_id','=',$com_id)
                                          ->where('dep_car','=',$dep_car)
                                          ->where('log_date', '=' ,$date2)
                                          ->update([
                                                    'log_'.(int)$i => $dataid
                                                  ]);
                          }//for
                        }//no log

                      }//allday

                      else {
                        //first
                        $CarStart = DB::table('tb_log_car')->where('com_id','=',$com_id)->where('car_id','=',$car)->where('log_date', '=' ,$date1)->count();
                        if ($CarStart==1) {
                          for ($i=$Sh; $i <= $Eh ; $i++) {
                            $sqlCarUPDATE = DB::table('tb_log_car')
                                            ->where('car_id', '=' ,$car)
                                            ->where('dep_car','=',$dep_car)
                                            ->where('com_id','=',$com_id)
                                            ->where('log_date', '=' ,$date1)
                                            ->update([
                                                         'log_'.(int)$i => $dataid
                                                    ]);
                          }//for
                        }//have log
                        else {
                          $carlogid = DB::table('tb_log_car')->where('com_id','=',$com_id)->orderBy('log_id','desc')->limit(1)->get();
                          if (count($carlogid)>0) {
                            foreach ($carlogid as $id) {
                                $logcar = $id->log_id;
                            }
                          }
                          else {
                            $logcar = 0;
                          }
                           //
                           $sqllogcar = DB::table('tb_log_car')
                                      ->insert([
                                                'log_id'=>$logcar+1,
                                                'dep_car'=>$dep_car,
                                                'com_id'=>$com_id,
                                                'dep_car'=>$dep_car,
                                                'log_date'=>$date1,
                                                'car_id'=>$car
                                              ]);

                          for ($i=$Sh; $i <= $Eh ; $i++) {
                            $sqlCarUPDATE = DB::table('tb_log_car')
                                          ->where('car_id', '=' ,$car)
                                          ->where('com_id','=',$com_id)
                                          ->where('dep_car','=',$dep_car)
                                          ->where('log_date', '=' ,$date1)
                                          ->update([
                                                    'log_'.(int)$i => $dataid
                                                  ]);
                          }//for
                        }//no log

                        //between
                        for ($i=1; $i <= $diff-1; $i++) {
                          $carlogid = DB::table('tb_log_car')->where('com_id','=',$com_id)->orderBy('log_id','desc')->limit(1)->get();
                          if (count($carlogid)>0) {
                            foreach ($carlogid as $id) {
                                $logcar = $id->log_id;
                            }
                          }
                          else {
                            $logcar = 0;
                          }
                          $tomorrow = date('Y-m-d',strtotime(str_replace('-', '/', $date1) . "+".$i." days"));
                          $sqllogcar = DB::table('tb_log_car')
                                     ->insert([
                                               'log_id'=>$logcar+1,
                                               'dep_car'=>$dep_car,
                                               'com_id'=>$com_id,
                                               'dep_car'=>$dep_car,
                                               'log_date'=>$tomorrow,
                                               'car_id'=>$car
                                             ]);

                          for ($j=$Sh; $j<=$Eh ; $j++) {
                           $sqlCarUPDATE = DB::table('tb_log_car')
                                         ->where('car_id', '=' ,$car)
                                         ->where('com_id','=',$com_id)
                                         ->where('dep_car','=',$dep_car)
                                         ->where('log_date', '=' ,$tomorrow)
                                         ->update([
                                                   'log_'.(int)$j => $dataid
                                                 ]);
                          }//for
                        }//for diffdate

                        //last
                        $CarEnd = DB::table('tb_log_car')->where('com_id','=',$com_id)->where('car_id','=',$car)->where('log_date', '=' ,$date2)->count();
                        if ($CarEnd==1) {
                          for ($i=$Sh; $i<=$Eh; $i++) {
                            $sqlCarUPDATE = DB::table('tb_log_car')
                                            ->where('car_id', '=' ,$car)
                                            ->where('dep_car','=',$dep_car)
                                            ->where('com_id','=',$com_id)
                                            ->where('log_date', '=' ,$date2)
                                            ->update([
                                                         'log_'.(int)$i => $dataid
                                                    ]);
                          }//for
                        }//have log
                        else {
                          $carlogid = DB::table('tb_log_car')->where('com_id','=',$com_id)->orderBy('log_id','desc')->limit(1)->get();
                          if (count($carlogid)>0) {
                            foreach ($carlogid as $id) {
                                $logcar = $id->log_id;
                            }
                          }
                          else {
                            $logcar = 0;
                          }
                           $sqllogcar = DB::table('tb_log_car')
                                      ->insert([
                                                'log_id'=>$logcar+1,
                                                'dep_car'=>$dep_car,
                                                'com_id'=>$com_id,
                                                'dep_car'=>$dep_car,
                                                'log_date'=>$date2,
                                                'car_id'=>$car
                                              ]);

                          for ($i=$Sh; $i<=$Eh ; $i++) {
                            $sqlCarUPDATE = DB::table('tb_log_car')
                                          ->where('car_id', '=' ,$car)
                                          ->where('com_id','=',$com_id)
                                          ->where('dep_car','=',$dep_car)
                                          ->where('log_date', '=' ,$date2)
                                          ->update([
                                                    'log_'.(int)$i => $dataid
                                                  ]);
                          }//for
                        }//no log

                      }//sometime
                  //=======================================EndCar=======================================
                  //=======================================Driver=======================================
                      if ($drive <> 'D0000000000') {
                        if ($bk_use == null) {
                          //first
                          $DriverStart = DB::table('tb_log_driver')->where('com_id','=',$com_id)->where('drive_id','=',$drive)->where('log_date', '=' ,$date1)->count();
                          if ($DriverStart>0) {
                            for ($i=$Sh; $i <= 23 ; $i++) {
                              $sqlDriveUPDATE = DB::table('tb_log_driver')
                                                ->where('drive_id', '=' ,$drive)
                                                ->where('dep_drive','=',$dep_car)
                                                ->where('com_id','=',$com_id)
                                                ->where('log_date', '=' ,$date1)
                                                ->update([
                                                          'log_'.(int)$i => $dataid
                                                        ]);
                            }//for
                          }//have log
                          else {
                            $drivelogid =DB::table('tb_log_driver')->where('com_id','=',$com_id)->orderBy('log_id','desc')->limit(1)->get();
                            if (count($drivelogid)>0) {
                              foreach ($drivelogid as $id) {
                                  $logdriver = $id->log_id;
                              }
                            }
                            else {
                              $logdriver = 0;
                            }

                             $sqllogdriver = DB::table('tb_log_driver')
                                            ->insert([
                                                  'log_id'=>$logdriver+1,
                                                  'dep_drive'=>$dep_car,
                                                  'com_id'=>$com_id,
                                                  'log_date'=>$date1,
                                                  'drive_id'=>$drive
                                                ]);

                             for ($i=$Sh; $i <= 23 ; $i++) {
                                $sqlDriveUPDATE = DB::table('tb_log_driver')
                                                ->where('drive_id', '=' ,$drive)
                                                ->where('com_id','=',$com_id)
                                                ->where('dep_drive','=',$dep_car)
                                                ->where('log_date', '=' ,$date1)
                                                ->update([
                                                          'log_'.(int)$i => $dataid
                                                        ]);
                              }//for
                          }//no log

                          //between
                          for ($i=1; $i <= $diff-1; $i++) {

                            $tomorrow = date('Y-m-d',strtotime(str_replace('-', '/', $date1) . "+".$i." days"));
                            $logiddrive =DB::table('tb_log_driver')->where('com_id','=',$com_id)->where('log_date','=',$tomorrow)->orderBy('log_id','desc')->get();
                            if (count($logiddrive)==0) {
                              $drivelogid =DB::table('tb_log_driver')->where('com_id','=',$com_id)->orderBy('log_id','desc')->limit(1)->get();
                              if (count($drivelogid)>0) {
                                foreach ($drivelogid as $id) {
                                    $logdriver = $id->log_id;
                                }
                              }
                              else {
                                $logdriver = 0;
                              }
                              $sqllogdriver = DB::table('tb_log_driver')
                                           ->insert([
                                                 'log_id'=>$logdriver+1,
                                                 'dep_drive'=>$dep_car,
                                                 'com_id'=>$com_id,
                                                 'log_date'=>$tomorrow,
                                                 'drive_id'=>$drive
                                               ]);
                            }
                            for ($j=0; $j <= 23 ; $j++) {
                               $sqlDriveUPDATE = DB::table('tb_log_driver')
                                               ->where('drive_id', '=' ,$drive)
                                               ->where('com_id','=',$com_id)
                                               ->where('dep_drive','=',$dep_car)
                                               ->where('log_date', '=' ,$tomorrow)
                                               ->update([
                                                         'log_'.(int)$j => $dataid
                                                       ]);
                                }//for
                            }//for diffdate

                          //last
                          $DriverEnd = DB::table('tb_log_driver')->where('com_id','=',$com_id)->where('drive_id','=',$drive)->where('log_date', '=' ,$date2)->count();
                          if ($DriverEnd==1) {
                            for ($i=0; $i <= $Eh ; $i++) {
                              $sqlDriveUPDATE = DB::table('tb_log_driver')
                                                ->where('drive_id', '=' ,$drive)
                                                ->where('dep_drive','=',$dep_car)
                                                ->where('com_id','=',$com_id)
                                                ->where('log_date', '=' ,$date2)
                                                ->update([
                                                          'log_'.(int)$i => $dataid
                                                        ]);
                            }//for
                          }//have log
                          else {
                            $drivelogid =DB::table('tb_log_driver')->where('com_id','=',$com_id)->orderBy('log_id','desc')->limit(1)->get();
                            if (count($drivelogid)>0) {
                              foreach ($drivelogid as $id) {
                                  $logdriver = $id->log_id;
                              }
                            }
                            else {
                              $logdriver = 0;
                            }

                             $sqllogdriver = DB::table('tb_log_driver')
                                            ->insert([
                                                  'log_id'=>$logdriver+1,
                                                  'dep_drive'=>$dep_car,
                                                  'com_id'=>$com_id,
                                                  'log_date'=>$date2,
                                                  'drive_id'=>$drive
                                                ]);
                             for ($i=0; $i <= $Eh ; $i++) {
                                $sqlDriveUPDATE = DB::table('tb_log_driver')
                                                ->where('drive_id', '=' ,$drive)
                                                ->where('com_id','=',$com_id)
                                                ->where('dep_drive','=',$dep_car)
                                                ->where('log_date', '=' ,$date2)
                                                ->update([
                                                          'log_'.(int)$i => $dataid
                                                        ]);
                              }//for
                          }//no log

                        }//allday
                        else {
                          //first
                          $DriverStart = DB::table('tb_log_driver')->where('com_id','=',$com_id)->where('drive_id','=',$drive)->where('log_date', '=' ,$date1)->count();
                          if ($DriverStart>0) {
                            for ($i=$Sh; $i<=$Eh; $i++) {
                              $sqlDriveUPDATE = DB::table('tb_log_driver')
                                                ->where('drive_id', '=' ,$drive)
                                                ->where('dep_drive','=',$dep_car)
                                                ->where('com_id','=',$com_id)
                                                ->where('log_date', '=' ,$date1)
                                                ->update([
                                                          'log_'.(int)$i => $dataid
                                                        ]);
                            }//for
                          }//have log
                          else {

                            $drivelogid =DB::table('tb_log_driver')->where('com_id','=',$com_id)->orderBy('log_id','desc')->limit(1)->get();
                            if (count($drivelogid)>0) {
                              foreach ($drivelogid as $id) {
                                  $logdriver = $id->log_id;
                              }
                            }
                            else {
                              $logdriver = 0;
                            }

                             $sqllogdriver = DB::table('tb_log_driver')
                                            ->insert([
                                                  'log_id'=>$logdriver+1,
                                                  'dep_drive'=>$dep_car,
                                                  'com_id'=>$com_id,
                                                  'log_date'=>$date1,
                                                  'drive_id'=>$drive
                                                ]);

                             for ($i=$Sh; $i<=$Eh; $i++) {
                                $sqlDriveUPDATE = DB::table('tb_log_driver')
                                                ->where('drive_id', '=' ,$drive)
                                                ->where('com_id','=',$com_id)
                                                ->where('dep_drive','=',$dep_car)
                                                ->where('log_date', '=' ,$date1)
                                                ->update([
                                                          'log_'.(int)$i => $dataid
                                                        ]);
                              }//for
                          }//no log

                          //between
                          for ($i=1; $i <= $diff-1; $i++) {
                            $drivelogid =DB::table('tb_log_driver')->where('com_id','=',$com_id)->orderBy('log_id','desc')->limit(1)->get();
                            if (count($drivelogid)>0) {
                              foreach ($drivelogid as $id) {
                                  $logdriver = $id->log_id;
                              }
                            }
                            else {
                              $logdriver = 0;
                            }

                            $tomorrow = date('Y-m-d',strtotime(str_replace('-', '/', $date1) . "+".$i." days"));
                            $sqllogdriver = DB::table('tb_log_driver')
                                           ->insert([
                                                 'log_id'=>$logdriver+1,
                                                 'dep_drive'=>$dep_car,
                                                 'com_id'=>$com_id,
                                                 'log_date'=>$tomorrow,
                                                 'drive_id'=>$drive
                                               ]);
                            for ($j=$Sh; $j<=$Eh; $j++) {
                               $sqlDriveUPDATE = DB::table('tb_log_driver')
                                               ->where('drive_id', '=' ,$drive)
                                               ->where('com_id','=',$com_id)
                                               ->where('dep_drive','=',$dep_car)
                                               ->where('log_date', '=' ,$tomorrow)
                                               ->update([
                                                         'log_'.(int)$j => $dataid
                                                       ]);
                                }//for
                              }//for diffdate

                          //last
                          $DriverEnd = DB::table('tb_log_driver')->where('com_id','=',$com_id)->where('drive_id','=',$drive)->where('log_date', '=' ,$date2)->count();
                          if ($DriverEnd==1) {
                            for ($i=$Sh; $i<=$Eh; $i++) {
                              $sqlDriveUPDATE = DB::table('tb_log_driver')
                                                ->where('drive_id', '=' ,$drive)
                                                ->where('dep_drive','=',$dep_car)
                                                ->where('com_id','=',$com_id)
                                                ->where('log_date', '=' ,$date2)
                                                ->update([
                                                          'log_'.(int)$i => $dataid
                                                        ]);
                            }//for
                          }//have log
                          else {
                            $drivelogid =DB::table('tb_log_driver')->where('com_id','=',$com_id)->orderBy('log_id','desc')->limit(1)->get();
                            if (count($drivelogid)>0) {
                              foreach ($drivelogid as $id) {
                                  $logdriver = $id->log_id;
                              }
                            }
                            else {
                              $logdriver = 0;
                            }

                             $sqllogdriver = DB::table('tb_log_driver')
                                            ->insert([
                                                  'log_id'=>$logdriver+1,
                                                  'dep_drive'=>$dep_car,
                                                  'com_id'=>$com_id,
                                                  'log_date'=>$date2,
                                                  'drive_id'=>$drive
                                                ]);
                             for ($i=$Sh; $i<=$Eh; $i++) {
                                $sqlDriveUPDATE = DB::table('tb_log_driver')
                                                ->where('drive_id', '=' ,$drive)
                                                ->where('com_id','=',$com_id)
                                                ->where('dep_drive','=',$dep_car)
                                                ->where('log_date', '=' ,$date2)
                                                ->update([
                                                          'log_'.(int)$i => $dataid
                                                        ]);
                              }//for
                          }//no log
                        }//sometime
                      }//driver
                  //=======================================EndDriver=======================================

                  }// booking ใช้ข้ามวัน

                    if(!$sqlbookingUPDATE and !$sqlCarUPDATE ){
                      $msg = array("success"=>false,"msg"=>"");//2
                    }else{
                      $msg = array("success"=>true,"msg"=>"","bk_id"=>$dataid);//1
                      session()->put('bk_id', $dataid);
                    }
                 return Response(json_encode($msg));
         }


         public function ReSetcar(Request $req)
         {
           $bk_id = $req->input("data");
           $car = $req->input("car_id") ;
           $drive =  $req->input("drive_id");

           $sql_bk = DB::table('tb_booking')->where('bk_id','=',$bk_id)->get();
           foreach ($sql_bk as $bk) {
             $bk_start = $bk->bk_start_start;
             $bk_end = $bk->bk_end_start;
             $com_id = $bk->com_id;
             $dep_car = $bk->dep_car;
             $bk_use = $bk->bk_use;
             $Sh = substr($bk->bk_start_start,11,2);
             $End_h = substr($bk->bk_end_start,11,2);
           }
           if ($End_h == '00') {
             $Eh = 23;
           }
           else {
             $Eh = $End_h;
           }
           $date1 = substr($bk_start,0,10);
           $date2 = substr($bk_end,0,10);
           $d1= new DateTime($date1);
           $d2 = new DateTime($date2);
           $diff = $d1->diff($d2)->format("%a");

           if ($diff==0) {
             for ($i=$Sh; $i <= $Eh ; $i++) {
               $sqlCarUPDATE = DB::table('tb_log_car')
                               ->where('car_id', '=' ,$car)
                               ->where('dep_car','=',$dep_car)
                               ->where('com_id','=',$com_id)
                               ->where('log_date', '=' ,$date1)
                               ->update([
                                            'log_'.(int)$i => null
                                       ]);
              if ($drive <> 'D0000000000') {
                $sqlDriveUPDATE = DB::table('tb_log_driver')
                                  ->where('drive_id', '=' ,$drive)
                                  ->where('dep_drive','=',$dep_car)
                                  ->where('com_id','=',$com_id)
                                  ->where('log_date', '=' ,$date1)
                                  ->update([
                                                'log_'.(int)$i => null
                                              ]);
              }
             }//for
           }// วันเดียวกัน
           else {
                  if ($bk_use == null){
                    //first
                    for ($i=$Sh; $i < 24 ; $i++) {
                      $sqlCarUPDATE = DB::table('tb_log_car')
                                      ->where('car_id', '=' ,$car)
                                      ->where('dep_car','=',$dep_car)
                                      ->where('com_id','=',$com_id)
                                      ->where('log_date', '=' ,$date1)
                                      ->update([
                                                   'log_'.(int)$i => null
                                              ]);
                     if ($drive <> 'D0000000000') {
                       $sqlDriveUPDATE = DB::table('tb_log_driver')
                                         ->where('drive_id', '=' ,$drive)
                                         ->where('dep_drive','=',$dep_car)
                                         ->where('com_id','=',$com_id)
                                         ->where('log_date', '=' ,$date1)
                                         ->update([
                                                       'log_'.(int)$i => null
                                                     ]);
                     }//if
                    }//for

                    //between
                    for ($i=1; $i < $diff; $i++) {
                      $tomorrow = date('Y-m-d',strtotime(str_replace('-', '/', $date1) . "+".$i." days"));
                      for ($j=0; $j<24; $j++) {
                        $sqlCarUPDATE = DB::table('tb_log_car')
                                        ->where('car_id', '=' ,$car)
                                        ->where('dep_car','=',$dep_car)
                                        ->where('com_id','=',$com_id)
                                        ->where('log_date', '=' ,$tomorrow)
                                        ->update([
                                                     'log_'.(int)$j => null
                                                ]);
                       if ($drive <> 'D0000000000') {
                         $sqlDriveUPDATE = DB::table('tb_log_driver')
                                           ->where('drive_id', '=' ,$drive)
                                           ->where('dep_drive','=',$dep_car)
                                           ->where('com_id','=',$com_id)
                                           ->where('log_date', '=' ,$tomorrow)
                                           ->update([
                                                         'log_'.(int)$j => null
                                                       ]);
                           }//if
                        }//for
                    }//for diffdate

                    //last
                    for ($i=0 ; $i <= $Eh ; $i++) {

                      $sqlCarUPDATE = DB::table('tb_log_car')
                                      ->where('car_id', '=' ,$car)
                                      ->where('dep_car','=',$dep_car)
                                      ->where('com_id','=',$com_id)
                                      ->where('log_date', '=' ,$date2)
                                      ->update([
                                                   'log_'.(int)$i => null
                                              ]);

                      if ($drive <> 'D0000000000') {
                        $sqlDriveUPDATE = DB::table('tb_log_driver')
                                          ->where('drive_id', '=' ,$drive)
                                          ->where('dep_drive','=',$dep_car)
                                          ->where('com_id','=',$com_id)
                                          ->where('log_date', '=' ,$date2)
                                          ->update([
                                                        'log_'.(int)$i => null
                                                      ]);
                          }//if



                    }//for

                  }//allday
                  else {
                    //first
                    for ($i=$Sh; $i <= $Eh ; $i++) {
                      $sqlCarUPDATE = DB::table('tb_log_car')
                                      ->where('car_id', '=' ,$car)
                                      ->where('dep_car','=',$dep_car)
                                      ->where('com_id','=',$com_id)
                                      ->where('log_date', '=' ,$date1)
                                      ->update([
                                                   'log_'.(int)$i => null
                                              ]);
                     if ($drive <> 'D0000000000') {
                       $sqlDriveUPDATE = DB::table('tb_log_driver')
                                         ->where('drive_id', '=' ,$drive)
                                         ->where('dep_drive','=',$dep_car)
                                         ->where('com_id','=',$com_id)
                                         ->where('log_date', '=' ,$date1)
                                         ->update([
                                                       'log_'.(int)$i => null
                                                     ]);
                     }//if
                    }//for

                    //between-last
                    for ($i=1; $i <= $diff; $i++) {
                      $tomorrow = date('Y-m-d',strtotime(str_replace('-', '/', $date1) . "+".$i." days"));
                      for ($j=$Sh; $j<=$Eh; $j++) {
                        $sqlCarUPDATE = DB::table('tb_log_car')
                                        ->where('car_id', '=' ,$car)
                                        ->where('dep_car','=',$dep_car)
                                        ->where('com_id','=',$com_id)
                                        ->where('log_date', '=' ,$tomorrow)
                                        ->update([
                                                     'log_'.(int)$j => null
                                                ]);
                       if ($drive <> 'D0000000000') {
                         $sqlDriveUPDATE = DB::table('tb_log_driver')
                                           ->where('drive_id', '=' ,$drive)
                                           ->where('dep_drive','=',$dep_car)
                                           ->where('com_id','=',$com_id)
                                           ->where('log_date', '=' ,$tomorrow)
                                           ->update([
                                                         'log_'.(int)$j => null
                                                       ]);
                           }//if
                          }//for
                        }//for diffdate

                  }//sometime
           }// คนละวัน


           if(!$sqlCarUPDATE ){
             $msg = array("success"=>false,"msg"=>"");//2
           }else{
             $msg = array("success"=>true,"msg"=>"","bk_id"=>$bk_id);//1
           }
           return Response(json_encode($msg));
         }

         public function ReUseCar(Request $req)
         {
           date_default_timezone_set("Asia/Bangkok");
            $bk_id = $req->input("data");
            $car = $req->input("car_id");
            $drive =  $req->input("drive_id");
            $reasons = $req->input("reasons");
            $username = session()->get('user');
            $date = date("Y-m-d H:i:s");

            $sqlemp = DB::table('tb_employee')->where('emp_email','=',$username)->get();
            foreach ($sqlemp as $emp) {
              $emp_id = $emp->emp_id;
            }

           $sql_bk = DB::table('tb_booking')->where('bk_id','=',$bk_id)->get();
           foreach ($sql_bk as $bk) {
             $bk_start = $bk->bk_start_start;
             $bk_end = $bk->bk_end_start;
             $com_id = $bk->com_id;
             $dep_car = $bk->dep_car;
             $bk_use = $bk->bk_use;
             // $Sh = substr($bk->bk_start_start,11,2);
             $End_h = substr($bk->bk_end_start,11,2);
           }
           if ($End_h == '00') {
             $Eh = 23;
           }
           else {
             $Eh = $End_h;
           }
           $Sh = substr($date,11,2);
           $datenow = substr($date,0,10);
           $date1 = substr($bk_start,0,10);
           $date2 = substr($bk_end,0,10);
           $dn = new DateTime($datenow);
           $d2 = new DateTime($date2);
           $diff = $dn->diff($d2)->format("%a");

           $sqlbookingUPDATE = DB::table('tb_booking')
                       ->where('bk_id', '=' ,$bk_id)
                       ->update(['bk_end_start'=>$date,
                                 'success_by' => $emp_id,
                                 'success_date' =>$date,
                                 'success_reasons' => $reasons
                                 ]);
          //
          if ($diff==0) {
            for ($i=$Sh; $i <= $Eh ; $i++) {
              $sqlCarUPDATE = DB::table('tb_log_car')
                              ->where('car_id', '=' ,$car)
                              ->where('dep_car','=',$dep_car)
                              ->where('com_id','=',$com_id)
                              ->where('log_date', '=' ,$datenow)
                              ->update([
                                           'log_'.(int)$i => null
                                      ]);
             if ($drive <> 'D0000000000') {
               $sqlDriveUPDATE = DB::table('tb_log_driver')
                                 ->where('drive_id', '=' ,$drive)
                                 ->where('dep_drive','=',$dep_car)
                                 ->where('com_id','=',$com_id)
                                 ->where('log_date', '=' ,$datenow)
                                 ->update([
                                               'log_'.(int)$i => null
                                             ]);
             }
            }//for
          }// วันเดียวกัน
          else {
                 if ($bk_use == null){
                   //first
                   for ($i=$Sh; $i <= 23 ; $i++) {
                     $sqlCarUPDATE = DB::table('tb_log_car')
                                     ->where('car_id', '=' ,$car)
                                     ->where('dep_car','=',$dep_car)
                                     ->where('com_id','=',$com_id)
                                     ->where('log_date', '=' ,$datenow)
                                     ->update([
                                                  'log_'.(int)$i => null
                                             ]);
                    if ($drive <> 'D0000000000') {
                      $sqlDriveUPDATE = DB::table('tb_log_driver')
                                        ->where('drive_id', '=' ,$drive)
                                        ->where('dep_drive','=',$dep_car)
                                        ->where('com_id','=',$com_id)
                                        ->where('log_date', '=' ,$datenow)
                                        ->update([
                                                      'log_'.(int)$i => null
                                                    ]);
                    }//if
                   }//for

                   //between
                   for ($i=1; $i < $diff; $i++) {
                     $tomorrow = date('Y-m-d',strtotime(str_replace('-', '/', $datenow) . "+".$i." days"));
                     for ($j=0; $j <= 23; $j++) {
                       $sqlCarUPDATE = DB::table('tb_log_car')
                                       ->where('car_id', '=' ,$car)
                                       ->where('dep_car','=',$dep_car)
                                       ->where('com_id','=',$com_id)
                                       ->where('log_date', '=' ,$tomorrow)
                                       ->update([
                                                    'log_'.(int)$j => null
                                               ]);
                      if ($drive <> 'D0000000000') {
                        $sqlDriveUPDATE = DB::table('tb_log_driver')
                                          ->where('drive_id', '=' ,$drive)
                                          ->where('dep_drive','=',$dep_car)
                                          ->where('com_id','=',$com_id)
                                          ->where('log_date', '=' ,$tomorrow)
                                          ->update([
                                                        'log_'.(int)$j => null
                                                      ]);
                          }//if
                         }//for
                       }//for diffdate

                   //last
                   for ($i=0 ; $i <= $Eh ; $i++) {
                     $sqlCarUPDATE = DB::table('tb_log_car')
                                     ->where('car_id', '=' ,$car)
                                     ->where('dep_car','=',$dep_car)
                                     ->where('com_id','=',$com_id)
                                     ->where('log_date', '=' ,$date2)
                                     ->update([
                                                  'log_'.(int)$i => null
                                             ]);

                    if ($drive <> 'D0000000000') {
                      $sqlDriveUPDATE = DB::table('tb_log_driver')
                                        ->where('drive_id', '=' ,$drive)
                                        ->where('dep_drive','=',$dep_car)
                                        ->where('com_id','=',$com_id)
                                        ->where('log_date', '=' ,$tomorrow)
                                        ->update([
                                                      'log_'.(int)$j => null
                                                    ]);
                        }//if

                   }//for

                 }//allday
                 else {
                   //first
                   for ($i=$Sh; $i <= $Eh ; $i++) {
                     $sqlCarUPDATE = DB::table('tb_log_car')
                                     ->where('car_id', '=' ,$car)
                                     ->where('dep_car','=',$dep_car)
                                     ->where('com_id','=',$com_id)
                                     ->where('log_date', '=' ,$datenow)
                                     ->update([
                                                  'log_'.(int)$i => null
                                             ]);
                    if ($drive <> 'D0000000000') {
                      $sqlDriveUPDATE = DB::table('tb_log_driver')
                                        ->where('drive_id', '=' ,$drive)
                                        ->where('dep_drive','=',$dep_car)
                                        ->where('com_id','=',$com_id)
                                        ->where('log_date', '=' ,$datenow)
                                        ->update([
                                                      'log_'.(int)$i => null
                                                    ]);
                    }//if
                   }//for

                   //between-last
                   for ($i=1; $i <= $diff; $i++) {
                     $tomorrow = date('Y-m-d',strtotime(str_replace('-', '/', $date1) . "+".$i." days"));
                     for ($j=$Sh; $j<=$Eh; $j++) {
                       $sqlCarUPDATE = DB::table('tb_log_car')
                                       ->where('car_id', '=' ,$car)
                                       ->where('dep_car','=',$dep_car)
                                       ->where('com_id','=',$com_id)
                                       ->where('log_date', '=' ,$tomorrow)
                                       ->update([
                                                    'log_'.(int)$j => null
                                               ]);
                      if ($drive <> 'D0000000000') {
                        $sqlDriveUPDATE = DB::table('tb_log_driver')
                                          ->where('drive_id', '=' ,$drive)
                                          ->where('dep_drive','=',$dep_car)
                                          ->where('com_id','=',$com_id)
                                          ->where('log_date', '=' ,$tomorrow)
                                          ->update([
                                                        'log_'.(int)$j => null
                                                      ]);
                          }//if
                         }//for
                       }//for diffdate

                 }//sometime
          }// คนละวัน


          if(!$sqlCarUPDATE ){
            $msg = array("success"=>false,"msg"=>"");//2
          }else{
            $msg = array("success"=>true,"msg"=>"","bk_id"=>$bk_id);//1
          }
          return Response(json_encode($msg));
      }


 public function ChangeCarDB(Request $req)
 {
   date_default_timezone_set("Asia/Bangkok");
    $emp_id =$req->input("emp_id");
    $bk_id =$req->input("bk_id");
    $com_id =$req->input("com_id");
    $newcar =$req->input("newcar");//id
    $oldcar = $req->input("oldcar");//num
    $olddriveid = $req->input("olddriveid");//id
    $olddrive = $req->input("olddrive");//name
    $newdrive =$req->input("newdrive");//id

    $datechange= $req->input("date");

    $date = date("Y-m-d H:i:s");
    $detail="";
    if ($newcar != "" && $newdrive == "") {
        $sql_bk = DB::table("tb_booking")->where("bk_id",'=',$bk_id)->where("com_id",'=',$com_id)->get();
        foreach ($sql_bk as $bk) {
          $sql_car = DB::table("tb_car")->where("car_id",'=',$bk->car_id)->where("com_id",'=',$com_id)->get();
          foreach ($sql_car as $car) {
            $car_number = $car->car_number;
          }
          if ($bk->change_detail==null) {
            $detail.= $bk->change_detail;
          }
          else {
            $detail.= $bk->change_detail.";";
          }

        }

          $detail.="วันที่ ".$datechange."เดิมใช้ ".$car_number." เพราะ".$req->input("detail");

       $sql = DB::table("tb_booking")
              ->where("bk_id",'=',$bk_id)
              ->where("com_id",'=',$com_id)
              ->update([
                          "car_id"=>$newcar,
                          "change_detail"=>$detail,
                          "change_by"=>$emp_id
              ]);

    }//if changecar
    elseif ($newcar == "" && $newdrive != "") {
      $sql_bk = DB::table("tb_booking")->where("bk_id",'=',$bk_id)->where("com_id",'=',$com_id)->get();
      foreach ($sql_bk as $bk) {
      $sql_driver = DB::table("tb_driver")->where("drive_id",'=',$bk->drive_id)->get();
        foreach ($sql_driver as $drive) {
          $drive_fname = $drive->drive_fname;
          $drive_lname = $drive->drive_lname;
        }

        if ($bk->change_detail==null) {
          $detail.= $bk->change_detail;
        }
        else {
          $detail.= $bk->change_detail.";";
        }
      }

      $detail.="เดิม".$drive_fname." ".$drive_lname."เป็นคนขับรถเปลี่ยนเพราะ".$req->input("detail");

      $sql = DB::table("tb_booking")
                ->where("bk_id",'=',$bk_id)
                ->where("com_id",'=',$com_id)
                ->update([
                            "drive_id"=>$newdrive,
                            "change_detail"=>$detail,
                            "change_by"=>$emp_id
                ]);


    }//if changedriver
    elseif ($newcar != "" && $newdrive != "") {
      $sql_bk = DB::table("tb_booking")->where("bk_id",'=',$bk_id)->where("com_id",'=',$com_id)->get();
      foreach ($sql_bk as $bk) {

        $sql_car = DB::table("tb_car")->where("car_id",'=',$bk->car_id)->where("com_id",'=',$com_id)->get();
          foreach ($sql_car as $car) {
            $car_number = $car->car_number;
          }
        $sql_driver = DB::table("tb_driver")->where("drive_id",'=',$bk->drive_id)->get();
          foreach ($sql_driver as $drive) {
            $drive_fname = $drive->drive_fname;
            $drive_lname = $drive->drive_lname;
          }

        if ($bk->change_detail==null) {
          $detail.= $bk->change_detail;
        }
        else {
          $detail.= $bk->change_detail.";";
        }

      }

      $detail.="วันที่ ".$datechange."เดิมใช้ ".$car_number."และเดิม ".$drive_fname." ".$drive_lname." เป็นคนขับรถเปลี่ยนเพราะ".$req->input("detail");

      $sql = DB::table("tb_booking")
             ->where("bk_id",'=',$bk_id)
             ->where("com_id",'=',$com_id)
             ->update([
                         "car_id"=>$newcar,
                         "drive_id"=>$newdrive,
                         "change_detail"=>$detail,
                         "change_by"=>$emp_id
             ]);



    }//if changecar changedriver
    else {
      $sql = true;
    }//don't change


   if(!$sql){
     $msg = array("success"=>false,"msg"=>"$detail");//2
   }else{
     $msg = array("success"=>true,"msg"=>"$detail");//1
   }
   return Response(json_encode($msg));

 }

 public function StatusCar(Request $req)
 {
   $car_id =$req->input("id");
   $status =$req->input("status");
   $sql_update = DB::table("tb_car")
                  ->where("car_id",'=',$car_id)
                  ->update([
                          "car_status"=>$status
                  ]);
   if(!$sql_update){
     $msg = array("success"=>false,"msg"=>"");//2
   }else{
     $msg = array("success"=>true,"msg"=>"");//1
   }
   return Response(json_encode($msg));
 }


public function LoanDue(Request $req)
{
  $ss=$req->input("ss");
  $com_id=$req->input("com_id");
  $dep_id=$req->input("dep_id");
  $car_id=$req->input("id");

  if ($ss == "L") {
    return view('Models.Car.CarLoan',['car_id'=>$car_id,'com_id'=>$com_id,"dep_id"=>$dep_id]);
  }
  else {
    $sql = DB::table("tb_car")->where("car_id",'=',$car_id)->where("com_id",'=',$com_id)->get();
    foreach ($sql as $c) {
      $sql_update = DB::table("tb_car")
                     ->where("car_id",'=',$car_id)
                     ->where("com_id",'=',$com_id)
                     ->update([
                             "dep_id"=>$c->dep_car
                     ]);
      echo 1;

    }

  }
}

public function LoanCar(Request $req)
{
  $com_id=$req->input("com_id");
  $dep_id=$req->input("job_id");
  $car_id=$req->input("car_id");

  $sql_update = DB::table("tb_car")
                 ->where("car_id",'=',$car_id)
                 ->where("com_id",'=',$com_id)
                 ->update([
                         "dep_id"=>$dep_id
                 ]);

      if(!$sql_update){
        $msg = array("success"=>false,"msg"=>"".$dep_id."n".$car_id."n".$com_id);//2
      }else{
        $msg = array("success"=>true,"msg"=>"".$dep_id);//1
      }
      return Response(json_encode($msg));

}

public function MergeCar(Request $req)
{
  if (session()->has('user'))
  {
    $username = session()->get('user');
    $bk_id =$req->input("bk");
    $users = DB::table('tb_employee')
            ->join("tb_department",function($join){
                        $join->on('tb_employee.dep_id', '=', 'tb_department.dep_id')
                             ->on("tb_employee.com_id","=","tb_department.com_id");
                              })
            ->join('tb_employee_login', 'tb_employee.emp_id', '=', 'tb_employee_login.emp_id')
            ->where('tb_employee.emp_email','=',$username)->get();
    return view('Models.Car.mergecar',['users'=>$users,'bk_id'=>$bk_id]);
  }
  else
  {
    exit("<script>window.location='/';</script>");
  }
}

public function MergeCarOT(Request $req)
{
  if (session()->has('user'))
  {
    $username = session()->get('user');
    $bk_id =$req->input("bk");
    $users = DB::table('tb_employee')
            ->join("tb_department",function($join){
                        $join->on('tb_employee.dep_id', '=', 'tb_department.dep_id')
                             ->on("tb_employee.com_id","=","tb_department.com_id");
                              })
            ->join('tb_employee_login', 'tb_employee.emp_id', '=', 'tb_employee_login.emp_id')
            ->where('tb_employee.emp_email','=',$username)->get();
    return view('Models.Car.mergecarOT',['users'=>$users,'bk_id'=>$bk_id]);
  }
  else
  {
    exit("<script>window.location='/';</script>");
  }
}

public function MergeCarDB(Request $req)
{
  $bk_main = $req->input("bk_main");
  $bk_merge = $req->input("bk_merge");
  $emp_id = $req->input("emp_id");
  $date = date("Y-m-d H:i:s");
    $sql = DB::table("tb_booking")->where("bk_id","=",$bk_main)->get();
    foreach ($sql as $bk) {
      if ($bk->bk_merge != "") {
        $bk_merge_id = $bk->bk_merge.";";
      }else {
        $bk_merge_id = $bk->bk_merge;
      }


      $sql_update = DB::table("tb_booking")
                     ->where("bk_id",'=',$bk_merge)
                     ->update([
                                "bk_status"=>"merge",
                                "car_id"=>$bk->car_id,
                                "drive_id"=>$bk->drive_id,
                                "setcar_by"=>$emp_id,
                                "setcar_date"=>$date,
                                "bk_merge"=>$bk_main
                              ]);
     $sql_update_main = DB::table("tb_booking")
                    ->where("bk_id",'=',$bk_main)
                    ->update([
                               "bk_merge"=>$bk_merge_id.$bk_merge
                             ]);
    }

    if(!$sql_update && $sql_update_main){
      $msg = array("success"=>false,"msg"=>"");//2
    }else{
      $msg = array("success"=>true,"msg"=>"","bk_id"=>$bk_merge);//1
      session()->put('bk_id', $bk_merge);
    }
    return Response(json_encode($msg));

}

public function MileView()
  {
    if (session()->has('user'))
    {
      $username = session()->get('user');
      $users = DB::table('tb_employee')
              ->join("tb_department",function($join){
                        $join->on('tb_employee.dep_id', '=', 'tb_department.dep_id')
                             ->on("tb_employee.com_id","=","tb_department.com_id");
                              })
              ->join('tb_employee_login', 'tb_employee.emp_id', '=', 'tb_employee_login.emp_id')
              ->where('tb_employee.emp_email','=',$username)->get();
      return view('booking.mile',['users'=>$users,'valuepage'=>'approve']);
    }
    else
    {
      exit("<script>window.location='/';</script>");
    }
  }

  public function setmile(Request $req)
  {
    $emp_id =$req->input("emp_id");
    $bk_id = $req->input("bk");
    return view('Models.Car.mile',['emp_id'=>$emp_id,'bk_id'=>$bk_id]);
  }

  public function setmileDB(Request $req)
  {
    print_r($_POST);
    $emp_id =$req->input("emp_id");
    $mile_start =$req->input("mile_start");
    $mile_end =$req->input("mile_end");
  }




}
