<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class LoginController extends Controller
{
    public function loginView()
      {
        return view('login.login');
      }
    public function SetEmail()
      {
        if (session()->has('user'))
        {
          $emp_id = session()->get('user');
          return view('login.setemail',['users'=>$emp_id]);
        }else{
          exit("<script>window.location='/';</script>");
        }
      }

    public function loginCheck(Request $req)
      {
        date_default_timezone_set("Asia/Bangkok");
        $username = $req->input('username');
        $password = $req->input('password');
        // $username = 'monchanok.p';
        // $password = 'Mook0601;';

        if (strpos($username,"@")) {
          $pass = md5($password);
          $msg = array();
          //$chk = DB::table('tb_employee_login')->where(['username'=>$username])->tosql();

          if ($username=="")
            {
              $msg = array("type"=>"username","success"=>false,"msg"=>"Please enter your Username.","data"=>"");
            }
          else
            {
              $chk_userlogin = DB::table('tb_employee_login')->where(['username'=>$username])->get();
                  if (count($chk_userlogin)<1)
                    {
                      $msg = array("type"=>"username","success"=>false,"msg"=>"Sorry, system not found this username.","data"=>"");
                    }
                  else
                    {
                      $chk_statuslogin = DB::table('tb_employee_login')->where(['username'=>$username , 'emp_status'=>'0'])->get();
                          if (count($chk_statuslogin)<1)
                            {

                              $msg = array("type"=>"username","success"=>false,"msg"=>"You can't log on this system.","data"=>"");
                            }
                          else
                            {
                              if ($password=="")
                                {

                                  $msg = array("type"=>"password","success"=>false,"msg"=>"Please enter your password.","data"=>"");
                                }
                              else
                                {

                                  $chk_passlogin = DB::table('tb_employee_login')->where(['username'=>$username , 'emp_status'=>'0' , 'password'=>$pass])->get();
                                  foreach ($chk_passlogin as $r):
                                         $update = $r->update_date;
                                  endforeach;

                                    if (count($chk_passlogin)<1)
                                      {
                                        $msg = array("type"=>"password","success"=>false,"msg"=>"Incorrect password.","data"=>"");
                                      }
                                    else
                                      {
                                        session()->put('user', $username);
                                        $chk_passlogin = DB::table('tb_employee_login')->where(['username'=>$username , 'emp_status'=>'0' , 'password'=>$pass])->get();

                                              foreach ($chk_passlogin as $r):
                                                     $emp_id = $r->emp_id;
                                                     $userlv = $r->emp_level;
                                              endforeach;
                                        if ($userlv <> 999) {
                                                $sqluser_login =  DB::table('tb_user_login')->insert(
                                                                    [
                                                                      'user_id' => $emp_id,
                                                                      'log_date' => date("Y-m-d H:i:s"),
                                                                      'log_type' => "login",
                                                                    ]);
                                         }
                                        $page="1";
                                          if ($update !="0000-00-00 00:00:00" and $update !="") {
                                            $page="2";
                                          }
                                          $msg = array("type"=>"","success"=>true,"msg"=>"","data"=>$page);

                                      }
                                }
                            }
                    }
            }

        }
        else {

              $adServer = "172.16.0.4";
              $ldap = ldap_connect($adServer);
              $ldaprdn = 'sr' . "\\" . $username;

              ldap_set_option($ldap, LDAP_OPT_PROTOCOL_VERSION, 3);
              ldap_set_option($ldap, LDAP_OPT_REFERRALS, 0);

              $bind = @ldap_bind($ldap, $ldaprdn, $password);


              if ($bind) {
                $filter="(&(sAMAccountName=$username)(samaccountname=*))";
                $result = ldap_search($ldap,"dc=SR,dc=COM",$filter);
                ldap_sort($ldap,$result,"sn");
                $info = ldap_get_entries($ldap, $result);
                  for ($i=0; $i<$info["count"]; $i++)
                  {
                      if($info["count"] > 1)
                          break;
                          //email
                          // if (empty($info[$i]['mail'][0])) {
                          //     $email = $info[$i]['samaccountname'][0].'@siamraj.com';
                          // }
                          // else {
                          //     $email = strtolower(trim($info[$i]['mail'][0]));
                          //   }
                          //firstname
                          if (strpos(trim($info[$i]['givenname'][0]),".")) {
                              $name = explode(".",trim($info[$i]['givenname'][0]));
                              $first_name = $name[0];
                              // $first_name = trim($entries[$x]['givenname'][0]);
                          }
                          else {
                              $first_name = trim($info[$i]['givenname'][0]);
                          }
                          //count username by emp_fname,emp_lname
                          $sql= DB::table('tb_employee')->where('emp_fname','=',$first_name)->where('emp_lname','=',trim($info[$i]['sn'][0]))->get();
                          if (count($sql)>0 && $sql[0]->emp_email != "") {
                            session()->put('user', $sql[0]->emp_email);
                            $chk_login = DB::table('tb_employee_login')->where(['username'=>$sql[0]->emp_email , 'emp_status'=>'0'])->get();
                                  foreach ($chk_login as $r):
                                     $emp_id = $r->emp_id;
                                     $userlv = $r->emp_level;
                                  endforeach;
                                  if ($userlv <> 999) {
                                      $sqluser_login =  DB::table('tb_user_login')->insert(
                                                        [
                                                         'user_id' => $emp_id,
                                                         'log_date' => date("Y-m-d H:i:s"),
                                                         'log_type' => "login",
                                                        ]);
                                         }
                          }
                          else {
                            $msg = array("type"=>"","success"=>true,"msg"=>"","data"=>'3');
                            session()->put('user', $sql[0]->emp_id);
                            return Response(json_encode($msg));
                          }
                  }
                  $msg = array("type"=>"","success"=>true,"msg"=>"","data"=>'2');


              } else {
                $msg = array("type"=>"error","success"=>false,"msg"=>"Please enter your password.","data"=>"");
              }
        }

        return Response(json_encode($msg));

      }

      public function Logout(Request $req)
        {
          // $emp_id = $req->input('id');
          date_default_timezone_set("Asia/Bangkok");
          $username = session()->get('user');
          $sql = DB::table('tb_employee')->where('emp_email','=',$username)
                ->join('tb_employee_login', 'tb_employee.emp_email', '=', 'tb_employee_login.username')->get();
          foreach ($sql as $emp) {
            $emp_id = $emp->emp_id;
            $userlv = $emp->emp_level;
          }
          $sqlUpdate =DB::table('tb_employee_login')
                              ->where('emp_id', '=' ,$emp_id)
                              ->update(['lat' => "",
                                      'lng' => ""]);
          if ($userlv <> 999) {
          $sqluser_login=  DB::table('tb_user_login')->insert(
                                   [
                                    'user_id' => $emp_id,
                                    'log_date' => date("Y-m-d H:i:s"),
                                    'log_type' => "logout",
                                   ]);
          }
          session()->forget('user');
          exit("<script>window.location='/';</script>");
        }

        public function changePass()
        {
          if (session()->has('user'))
          {
            $username = session()->get('user');
            $users = DB::table('tb_employee')
                    ->join("tb_department",function($join){
                        $join->on('tb_employee.dep_id', '=', 'tb_department.dep_id')
                             ->on("tb_employee.com_id","=","tb_department.com_id");
                              })
                    ->join('tb_employee_login', 'tb_employee.emp_email', '=', 'tb_employee_login.username')
                    ->where('tb_employee.emp_email','=',$username)->get();
            return view('login.changePass',['users'=>$users,'valuepage'=>'']);
          }
          else
          {
            exit("<script>window.location='/';</script>");
          }
        }

        public function passDB(Request $req)
        {
          date_default_timezone_set("Asia/Bangkok");
          $username=$req->input('emp_id');
          $old = md5($req->input('old_pass'));
          $new = $req->input('new_pass');
          $con = $req->input('con_pass');
          $valnew = $req->input('valnew');
          $valcon = $req->input('valcon');
          $date =date("Y-m-d H:i:s");
          $pass = md5($new);
          $msg = array();

          // echo "old".$old."new".$new."con".$con."vnew".$valnew."vcon".$valcon;
          $chk_pass = DB::table('tb_employee_login')->where(['username'=>$username ,'emp_status'=>'0', 'password'=>$old])->get();
          if (count($chk_pass)==1) {
            if ($new == $con)
            {

              if ($valnew=="false" or $valcon=="false") {
                $msg = array("type"=>"new_pass","success"=>false,"msg"=>"Passwords must contain at least eight characters, including uppercase, lowercase letters and numbers.");
              }
              else {
                $sqlUpdate =DB::table('tb_employee_login')
                                    ->where('emp_id', '=' ,$username)
                                    ->update(['password'=>$pass,'update_date'=>$date]);
                $msg = array("type"=>"","success"=>true,"msg"=>"");
              }

            }
            else
            {
              $msg = array("type"=>"con_pass","success"=>false,"msg"=>"Passwords do no match.");
            }

          }
          else {
            $msg = array("type"=>"old_pass","success"=>false,"msg"=>"Incorrect password.");
          }

          return Response(json_encode($msg));

        }

        public function FirstLogin()
        {
          if (session()->has('user'))
          {
            $username = session()->get('user');
            $users = DB::table('tb_employee_login')
                            ->where('username','=',$username)->get();
            return view('login.firstlogin',['users'=>$users,'valuepage'=>'']);
          }
          else
          {
            exit("<script>window.location='/';</script>");
          }
        }

        public function FirstLoginpassDB(Request $req)
        {
          date_default_timezone_set("Asia/Bangkok");
          $username=$req->input('emp_id');
          $new = $req->input('new_pass');
          $con = $req->input('con_pass');
          $valnew = $req->input('valnew');
          $valcon = $req->input('valcon');
          $date =date("Y-m-d H:i:s");
          $pass = md5($new);
          $msg = array();

          // echo "old".$old."new".$new."con".$con."vnew".$valnew."vcon".$valcon;
            if ($new == $con)
            {

              if ($valnew=="false" or $valcon=="false") {
                $msg = array("type"=>"new_pass","success"=>false,"msg"=>"Passwords must contain at least eight characters, including uppercase, lowercase letters and numbers.");
              }
              else {
                $sqlUpdate =DB::table('tb_employee_login')
                                    ->where('emp_id', '=' ,$username)
                                    ->update(['password'=>$pass,'update_date'=>$date]);
                $msg = array("type"=>"","success"=>true,"msg"=>"");
              }

            }
            else
            {
              $msg = array("type"=>"con_pass","success"=>false,"msg"=>"Passwords do no match.");
            }



          return Response(json_encode($msg));

        }

        public function ForgetPass()
        {
          return view('login.forget');
        }

        public function ReSetPass(Request $req)
        {
          $id = $req->input('id');
          return view('login.reset',['id'=>$id]);
        }

        public function ReSetPassDB(Request $req)
        {
          date_default_timezone_set("Asia/Bangkok");
          $token=$req->input('token');
          $new = $req->input('new_pass');
          $con = $req->input('con_pass');
          $valnew = $req->input('valnew');
          $valcon = $req->input('valcon');
          $pass = md5($new);
          $date =date("Y-m-d H:i:s");

          if ($new == $con)
          {

            if ($valnew=="false" or $valcon=="false") {
              $msg = array("type"=>"new_pass","success"=>false,"msg"=>"Passwords must contain at least eight characters, including uppercase, lowercase letters and numbers.");
            }
            else {
              $msg = array("type"=>"","success"=>true,"msg"=>"");
              $sqlUpdate =DB::table('tb_employee_login')
                                  ->where('token', '=' ,$token)
                                  ->update(['token' => "",
                                          'password' => $pass,
                                          'update_date' => $date]);
            }

          }
          else
          {
            $msg = array("type"=>"con_pass","success"=>false,"msg"=>"Passwords do no match.");
          }
          return Response(json_encode($msg));
        }

        public function LocateLogin(Request $req)
        {
           $username=$req->input('user');
           $sql = DB::table('tb_employee_login')->where('username', '=' ,$username)->count();
           if ($sql>0) {
             $lat=$req->input('lat');
             $lng = $req->input('lng');
             $sqlUpdate =DB::table('tb_employee_login')
                                ->where('username', '=' ,$username)
                                ->update(['lat' => $lat,
                                          'lng' => $lng]);
           }
           else {
             $lat=$req->input('lat');
             $lng = $req->input('lng');
             $sqlUpdate =DB::table('tb_employee_login')
                                ->where('user_auth', '=' ,$username)
                                ->update(['lat' => $lat,
                                          'lng' => $lng]);
           }
          if (!$sqlUpdate)
          {echo"2";}
          else {echo "1";}

        }

}
