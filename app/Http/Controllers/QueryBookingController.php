<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Storage;

class QueryBookingController extends Controller
{
    public function QueryView()
    {
      if (session()->has('user'))
      {
        $username = session()->get('user');
        $users = DB::table('tb_employee')
                ->join("tb_department",function($join){
                        $join->on('tb_employee.dep_id', '=', 'tb_department.dep_id')
                             ->on("tb_employee.com_id","=","tb_department.com_id");
                              })
                ->join('tb_employee_login', 'tb_employee.emp_id', '=', 'tb_employee_login.emp_id')
                ->where('tb_employee.emp_email','=',$username)->get();
        return view('booking.query',['users'=>$users,'valuepage'=>'']);
      }
      else
      {
        exit("<script>window.location='/';</script>");
      }

    }

    public function QueryWait()
    {
      if (session()->has('user'))
      {
        $username = session()->get('user');
        $users = DB::table('tb_employee')
                ->join("tb_department",function($join){
                        $join->on('tb_employee.dep_id', '=', 'tb_department.dep_id')
                             ->on("tb_employee.com_id","=","tb_department.com_id");
                              })
                ->join('tb_employee_login', 'tb_employee.emp_id', '=', 'tb_employee_login.emp_id')
                ->where('tb_employee.emp_email','=',$username)->get();
        return view('booking.query',['users'=>$users,'valuepage'=>'wait']);
      }
      else
      {
        exit("<script>window.location='/';</script>");
      }
    }

    public function QueryApprove()
    {
      if (session()->has('user'))
      {
        $username = session()->get('user');
        $users = DB::table('tb_employee')
                ->join("tb_department",function($join){
                        $join->on('tb_employee.dep_id', '=', 'tb_department.dep_id')
                             ->on("tb_employee.com_id","=","tb_department.com_id");
                              })
                ->join('tb_employee_login', 'tb_employee.emp_id', '=', 'tb_employee_login.emp_id')
                ->where('tb_employee.emp_email','=',$username)->get();
        return view('booking.query',['users'=>$users,'valuepage'=>'approve']);
      }
      else
      {
        exit("<script>window.location='/';</script>");
      }
    }

    public function QueryEject()
    {
      if (session()->has('user'))
      {
        $username = session()->get('user');
        $users = DB::table('tb_employee')
                ->join("tb_department",function($join){
                        $join->on('tb_employee.dep_id', '=', 'tb_department.dep_id')
                             ->on("tb_employee.com_id","=","tb_department.com_id");
                              })
                ->join('tb_employee_login', 'tb_employee.emp_id', '=', 'tb_employee_login.emp_id')
                ->where('tb_employee.emp_email','=',$username)->get();
        return view('booking.query',['users'=>$users,'valuepage'=>'eject']);
      }
      else
      {
        exit("<script>window.location='/';</script>");
      }
    }

    public function QueryNonecar()
    {
      if (session()->has('user'))
      {
        $username = session()->get('user');
        $users = DB::table('tb_employee')
                ->join("tb_department",function($join){
                        $join->on('tb_employee.dep_id', '=', 'tb_department.dep_id')
                             ->on("tb_employee.com_id","=","tb_department.com_id");
                              })
                ->join('tb_employee_login', 'tb_employee.emp_id', '=', 'tb_employee_login.emp_id')
                ->where('tb_employee.emp_email','=',$username)->get();
        return view('booking.query',['users'=>$users,'valuepage'=>'nonecar']);
      }
      else
      {
        exit("<script>window.location='/';</script>");
      }
    }

    public function QuerySuccess()
    {
      if (session()->has('user'))
      {
        $username = session()->get('user');
        $users = DB::table('tb_employee')
                ->join("tb_department",function($join){
                        $join->on('tb_employee.dep_id', '=', 'tb_department.dep_id')
                             ->on("tb_employee.com_id","=","tb_department.com_id");
                              })
                ->join('tb_employee_login', 'tb_employee.emp_id', '=', 'tb_employee_login.emp_id')
                ->where('tb_employee.emp_email','=',$username)->get();
        return view('booking.query',['users'=>$users,'valuepage'=>'success']);
      }
      else
      {
        exit("<script>window.location='/';</script>");
      }
    }

      public function QueryCar()
      {
        if (session()->has('user'))
        {
          $username = session()->get('user');
          $users = DB::table('tb_employee')
                  ->join("tb_department",function($join){
                        $join->on('tb_employee.dep_id', '=', 'tb_department.dep_id')
                             ->on("tb_employee.com_id","=","tb_department.com_id");
                              })
                  ->join('tb_employee_login', 'tb_employee.emp_id', '=', 'tb_employee_login.emp_id')
                  ->where('tb_employee.emp_email','=',$username)->get();
          return view('booking.queryCar',['users'=>$users,'valuepage'=>'']);
        }
        else
        {
          exit("<script>window.location='/';</script>");
        }

      }

      public function QueryCarOT()
      {
        if (session()->has('user'))
        {
          $username = session()->get('user');
          $users = DB::table('tb_employee')
                  ->join("tb_department",function($join){
                        $join->on('tb_employee.dep_id', '=', 'tb_department.dep_id')
                             ->on("tb_employee.com_id","=","tb_department.com_id");
                              })
                  ->join('tb_employee_login', 'tb_employee.emp_id', '=', 'tb_employee_login.emp_id')
                  ->where('tb_employee.emp_email','=',$username)->get();
          return view('booking.queryCarOT',['users'=>$users,'valuepage'=>'']);
        }
        else
        {
          exit("<script>window.location='/';</script>");
        }

      }


      public function QuerySetNonecar()
      {
        if (session()->has('user'))
        {
          $username = session()->get('user');
          $users = DB::table('tb_employee')
                  ->join("tb_department",function($join){
                        $join->on('tb_employee.dep_id', '=', 'tb_department.dep_id')
                             ->on("tb_employee.com_id","=","tb_department.com_id");
                              })
                  ->join('tb_employee_login', 'tb_employee.emp_id', '=', 'tb_employee_login.emp_id')
                  ->where('tb_employee.emp_email','=',$username)->get();
          return view('booking.queryNonecar',['users'=>$users,'valuepage'=>'']);
        }
        else
        {
          exit("<script>window.location='/';</script>");
        }

      }

      public function DriverView()
      {
        if (session()->has('user'))
        {
          $username = session()->get('user');
          $users = DB::table('tb_employee')
                  ->join("tb_department",function($join){
                        $join->on('tb_employee.dep_id', '=', 'tb_department.dep_id')
                             ->on("tb_employee.com_id","=","tb_department.com_id");
                              })
                  ->join('tb_employee_login', 'tb_employee.emp_id', '=', 'tb_employee_login.emp_id')
                  ->where('tb_employee.emp_email','=',$username)->get();
          return view('booking.driver',['users'=>$users,'valuepage'=>'']);
        }
        else
        {
          exit("<script>window.location='/';</script>");
        }

    }

    public function Approver()
    {
      if (session()->has('user'))
      {
        $username = session()->get('user');
        $users = DB::table('tb_employee')
                ->join("tb_department",function($join){
                        $join->on('tb_employee.dep_id', '=', 'tb_department.dep_id')
                             ->on("tb_employee.com_id","=","tb_department.com_id");
                              })
                ->join('tb_employee_login', 'tb_employee.emp_id', '=', 'tb_employee_login.emp_id')
                ->where('tb_employee.emp_email','=',$username)->get();
                foreach ($users as $lv) {
                  $lv_user = $lv->emp_level;
                }
                if ($lv_user >= '1') {
                  return view('booking.queryApprove',['users'=>$users,'valuepage'=>'approve']);
                }
                else {
                    exit("<script>window.location='/dashboard';</script>");
                }
      }
      else
      {
        exit("<script>window.location='/';</script>");
      }

  }

    public function InsertsBooking(Request $req)
    {
      $mtel = $req->input('mtel');
      $ttel = $req->input('ttel');
      $date_start = explode("/",$req->input('bk_start_date'));
      $bk_start_start = $date_start[0]."-".$date_start[1]."-".$date_start[2]." ".$req->input('bk_start_start');
      date_default_timezone_set("Asia/Bangkok");
      $date_now = date("Y/m/d H:i");
      if ($bk_start_start<$date_now) {
      if ($mtel == ""){
          $msg = array("bk_id"=>"","success"=>false,"msg"=>"กรุณากรอกเบอร์โทรศัพท์".$mtel."23");
      }
      else{
        if (!preg_match("/^\d{10}$/", $mtel)) {
            $msg = array("bk_id"=>"","success"=>false,"msg"=>"กรุณากรอกเบอร์โทรศัพท์ให้ถูกต้องและต้องไม่มีเครื่องหมาย");
            }
        else {
              $date_end = explode("/",$req->input('bk_end_date'));
              $bk_end_start = $date_end[0]."-".$date_end[1]."-".$date_end[2]." ".$req->input('bk_end_start');
              $emp_id = $req->input('emp_id');
              $com_id = $req->input('com_id');
              $ctype = $req->input('ctype');
              $cdep = $req->input('cdep');
              $dep_id = $req->input('dep_id');
              $job_id = $req->input('job_id');
              $sitecode = $req->input("site");
              $person = $req->input('count_person');
              $obj = $req->input('bk_obj');
              $note = $req->input('bk_note');
              if ($req->input('times') != "") {
                  $times = $req->input('times');
              }else {
                  $times = null;
              }
              if ($req->input('overtime') != "") {
                  $overtime = $req->input('overtime');
              }else {
                  $overtime = null;
              }

              $file = "";
              $date =date("Y-m-d H:i:s");

              //update tel
              $sqlUPDATE = DB::table('tb_employee')
                              ->where('emp_id','=',$emp_id)
                              ->update([
                                         'emp_tel'=>$mtel,
                                         'emp_table'=>$ttel
                                ]);

              //gen_id
              $id = "";
              $ymd = date("ymd");
              $sql_booking = DB::table('tb_booking')->select('bk_id')->where('bk_id','like','BK'.$ymd.'%')->orderBy('bk_id','desc')->limit(1)->get();
              if (count($sql_booking)<=0) {
                $id = 1;
              }
              else{
                foreach ($sql_booking as $bk):
                  $last_id = $bk->bk_id;
                endforeach;
                  $old = explode("BK",$last_id);
                  $old_ymd = substr($old[1],0,6);
                  $old_id = substr($old[1],6,4);
                  if($old_ymd == $ymd){
                    $id = $old_id+1;
                  }else{
                    $id = 1;
                  }
                }
              $bk_id = "BK".$ymd.sprintf("%04d",$id);
              // end gen_id
              //  echo $bk_id;

              $status="wait";
              $sql_user = DB::table('tb_employee_login')->where(['emp_id'=>$emp_id])->get();
              foreach ($sql_user as $user):
                $id_lv = $user->emp_level;
              endforeach;

                  if ($req->hasFile('image')) {
                    //upload file
                      $filename = "doc_".time();
                      $type = $req->image->extension();
                      $nfile = $filename.'.'.$type;
                      $file = $req->image->storeAs('public/document',$nfile);
                  }
                  else {
                    $nfile = "";
                  }

                // if ($tigket == "2") {
                    // $msg = array("bk_id"=>"$bk_id.$status","success"=>true);
                    $sqlbooking =  DB::table('tb_booking')->insert(
                                [ 'bk_id' => "$bk_id",
                                  'com_id' => "$com_id",
                                  'bk_date' => "$date",
                                  'bk_start_start' => "$bk_start_start",
                                  'bk_end_start' => "$bk_end_start",
                                  'bk_use' => $times,
                                  'bk_percon' => "$person",
                                  'bk_obj' => "$obj",
                                  'bk_note' => "$note",
                                  'dep_id' => "$dep_id",
                                  'job_id' => "$job_id",
                                  'bk_sitecode'=>"$sitecode",
                                  'dep_car'=> "$cdep",
                                  'bk_mtel'=>"$mtel",
                                  'bk_ttel'=>"$ttel",
                                  'emp_id' => "$emp_id",
                                  'bk_status' => "$status",
                                  'ctype_id' => "$ctype",
                                  'bk_doc' => "$nfile",
                                  'bk_ot'=>$overtime,
                                  // 'approve_by' =>"$emp_id",
                                  // 'approve_date'=>"$date",
                                  // 'bk_tigket'=>"$tigket"
                                ]);
                // }
                // else {
                //     $sqlbooking =  DB::table('tb_booking')->insert(
                //                 [ 'bk_id' => "$bk_id",
                //                   'com_id' => "$com_id",
                //                   'bk_date' => "$date",
                //                   'bk_start_start' => "$bk_start_start",
                //                   'bk_start_end' => "$bk_start_end",
                //                   'bk_percon' => "$person",
                //                   'bk_obj' => "$obj",
                //                   'bk_note' => "$note",
                //                   'dep_id' => "$dep_id",
                //                   'bk_mtel'=>"$mtel",
                //                   'emp_id' => "$emp_id",
                //                   'bk_status' => "$status",
                //                   'ctype_id' => "$ctype",
                //                   // 'bk_doc' => "$bk_file",
                //                   // 'bk_tigket'=>"$tigket"
                //                 ]);
                //
                // }

                            $msg ="";
                            if (!$sqlbooking) {
                              $msg = array("bk_id"=>"","success"=>false,"msg"=>"");
                            }
                            else {
                              $msg = array("bk_id"=>"$bk_id","success"=>true,"msg"=>"");
                              session()->put('bk_id', $bk_id);

                            }

            } //else format tel
          }//else mtel ''
        }//จองก่อนเวลา
        else {
          $msg = array("bk_id"=>"","success"=>false,"msg"=>"ไม่สามารถจองย้อนหลังได้");
        }//จองหลังเวลา

      return Response(json_encode($msg));

    }

    public function LocationInserts(Request $req)
    {
      $emp_id = $_POST['emp_id'];
      $bk_id = $_POST['id'];
      $com_id = $_POST['com_id'];
      $count_where = $_POST['count_where'];
      for($i=0;$i<$count_where;$i++){
         $where = $_POST['bk_where'.($i+1)];
            $sqlbooking_locate =DB::table('tb_booking_location')->insert(
              ['bk_id' => "$bk_id",
              'com_id' => "$com_id",
              'location_id' => ($i+1),
              'location_name' => "$where"]);
        }
        $msg ="";
        if (!$sqlbooking_locate) {
          $msg = array("bk_id"=>"","success"=>false);
        }
        else {
          $msg = array("bk_id"=>"$bk_id","success"=>true);
          session()->put('bk_id', $bk_id);
        }
        return Response(json_encode($msg));
    }

    public function LocateDB(Request $req)
    {
      $area = array( );
      $id = $req->input('id');
      $sql = DB::table('tb_booking_location')->where("bk_id","=","$id")->get();
      foreach ($sql as $bk):
       $bk_id = $bk->bk_id;
       $location_id = $bk->location_id;
       $location_name = $bk->location_name;
       $array_locate =  array('id' => $location_id ,'name' => $location_name ,'bk_id' =>$bk_id );
      //  return Response(json_encode($array_locate));
       array_push($area,$array_locate);
      endforeach;
      // echo json_encode($area);
      return Response(json_encode($area));
    }

    public function InsertsLocation(Request $req)
    {
       $bk_id = $req->input('id');
       $location_name = $req->input('location_id');
       $latitude = $req->input('latitude');
       $longitude = $req->input('longitude');

      $sqlbooking_locate =DB::table('tb_booking_location')
                    ->where('bk_id', '=' ,$bk_id)
                    ->where('location_name', '=' ,$location_name)
                    ->update(['location_lat' => $latitude,
                              'location_lng' =>$longitude ]);

        $msg ="";
        if (!$sqlbooking_locate) {
          $msg = array("bk_id"=>"","success"=>false);
        }
        else {
          $msg = array("bk_id"=>"$bk_id","success"=>true);
          session()->put('id', $bk_id);

        }
        return Response(json_encode($msg));
    }

    public function ApproveBooking(Request $req)
    {
      date_default_timezone_set("Asia/Bangkok");
       $bk_id = $req->input("data");
       $emp_id = $req->input("emp");
       $type = $req->input("type");
       $reasons = $req->input("reasons");
       $date =date("Y-m-d H:i:s");
       if($type == 'approve'||$type == 'eject'){
         $sqlUPDATE = DB::table('tb_booking')
                    ->where('bk_id', '=' ,$bk_id)
                    ->update(['bk_status' => $type,
                              'approve_by' =>$emp_id,
                              'approve_date' => $date,
                              'bk_reasons' => $reasons,
                          ]);
        }
        elseif ($type == 'nonecar'||$type == 'success') {
          $sqlUPDATE = DB::table('tb_booking')
                     ->where('bk_id', '=' ,$bk_id)
                     ->update(['bk_status' => $type,
                               'setcar_by' =>$emp_id,
                               'setcar_date' => $date,
                               'bk_reasons' => $reasons,
                           ]);
        }

           if(!$sqlUPDATE){
             $msg = array("success"=>false,"msg"=>"");//2
           }else{
             $msg = array("success"=>true,"msg"=>"","bk_id"=>$bk_id);//1
           }

          return Response(json_encode($msg));
    }

    public function ReApproveBooking(Request $req)
    {
      date_default_timezone_set("Asia/Bangkok");
       $bk_id = $req->input("data");
       $emp_id = $req->input("emp");
       $type = $req->input("type");
       $reasons = $req->input("reasons");
       $date =date("Y-m-d H:i:s");

        $sql = DB::table('tb_booking')->where('bk_id','=',$bk_id)->get();
        foreach ($sql as $bk) {
            $bk_merge = $bk->bk_merge;
        }
        if ($bk_merge !="") {
          $pos = strpos($bk_merge,';');
          if($pos!==FALSE){
             $str = explode(";",$bk_merge);
            }else{
             $str = array($bk_merge);
            }
          for ($i=0; $i < count($str); $i++) {
            $sqlUPDATEMerge = DB::table('tb_booking')
                         ->where('bk_id', '=' ,$str[$i])
                         ->update(['bk_status' => $type,
                                   'edit_by' =>$emp_id,
                                   'edit_date' => $date,
                                   'edit_reasons' => $reasons,
                                   'setcar_by' =>null,
                                   'setcar_date'=>null,
                                   'car_id'=>null,
                                   'drive_id'=>null,
                                   'bk_merge' => null
                               ]);
          }

        }

        $sqlUPDATE = DB::table('tb_booking')
                     ->where('bk_id', '=' ,$bk_id)
                     ->update(['bk_status' => $type,
                               'edit_by' =>$emp_id,
                               'edit_date' => $date,
                               'edit_reasons' => $reasons,
                               'setcar_by' =>null,
                               'setcar_date'=>null,
                               'car_id'=>null,
                               'drive_id'=>null,
                               'bk_merge' => null
                           ]);

           if(!$sqlUPDATE && !$sqlUPDATEMerge){
             $msg = array("success"=>false,"msg"=>"");//2
           }else{
             $msg = array("success"=>true,"msg"=>"","bk_id"=>$bk_id);//1
           }

          return Response(json_encode($msg));
    }


    public function ApproveMailBooking()
    {
      return view('booking.approvemail');
    }

    public function EditBooking(Request $req)
    {
      $mtel = $req->input('mtel');
      $ttel = $req->input('ttel');
      $req->input('bk_start_date');
      $date_start = str_replace("/","-",$req->input('bk_start_date'));
      $bk_start_start = $date_start." ".$req->input('bk_start_start');
      // $bk_start_start = $date_start[0]."-".$date_start[1]."-".$date_start[2]." ".$req->input('bk_start_start');
      date_default_timezone_set("Asia/Bangkok");
      $date_now = date("Y/m/d H:i");
      if ($bk_start_start<$date_now) {
      if ($mtel == ""){
          $msg = array("bk_id"=>"","success"=>false,"msg"=>"กรุณากรอกเบอร์โทรศัพท์".$mtel."23");
      }
      else{
        if (!preg_match("/^\d{10}$/", $mtel)) {
            $msg = array("bk_id"=>"","success"=>false,"msg"=>"กรุณากรอกเบอร์โทรศัพท์ให้ถูกต้อง".$mtel."23");
            }
        else {
              $date_end = explode("/",$req->input('bk_end_date'));
              $bk_end_start = $date_end[0]."-".$date_end[1]."-".$date_end[2]." ".$req->input('bk_end_start');
              $emp_id = $req->input('emp_id');
              $com_id = $req->input('com_id');
              $bk_id = $req->input('bk_id');
              $ctype = $req->input('ctype');
              $cdep = $req->input('cdep');
              $dep_id = $req->input('dep_id');
              $job_id = $req->input('job_id');
              $sitecode = $req->input("site");
              $person = $req->input('count_person');
              $obj = $req->input('bk_obj');
              $note = $req->input('bk_note');
              $times = $req->input('times');
              $overtime = $req->input('overtime');
              $date =date("Y-m-d H:i:s");

              //update tel
              $sqlUPDATE = DB::table('tb_employee')
                              ->where('emp_id','=',$emp_id)
                              ->update([
                                         'emp_tel'=>$mtel,
                                         'emp_table'=>$ttel
                                ]);


              $sql_user = DB::table('tb_employee_login')->where(['emp_id'=>$emp_id])->get();
              foreach ($sql_user as $user):
                $id_lv = $user->emp_level;
              endforeach;

                    $sqlbooking =  DB::table('tb_booking')
                                      ->where('bk_id','=', $bk_id)
                                      ->update([ 'bk_id' => $bk_id,
                                                  'com_id' => $com_id,
                                                  'bk_start_start' => $bk_start_start,
                                                  'bk_end_start' => $bk_end_start,
                                                  'bk_use' => $times,
                                                  'bk_percon' => $person,
                                                  'bk_obj' => $obj,
                                                  'bk_note' => $note,
                                                  'bk_sitecode'=>$sitecode,
                                                  'dep_car'=> $cdep,
                                                  'bk_mtel'=>$mtel,
                                                  'bk_ttel'=>$ttel,
                                                  'ctype_id' => $ctype,
                                                  'edit_by' =>$emp_id,
                                                  'edit_date'=>$date,
                                                  'bk_ot' => $overtime
                                                ]);

                            $msg ="";
                            if (!$sqlbooking) {
                              $msg = array("bk_id"=>"$bk_id","success"=>false,"msg"=>"");
                            }
                            else {
                              $msg = array("bk_id"=>"$bk_id","success"=>true,"msg"=>"");
                            }


            } //else format tel
          }//else mtel ''
        }//จองก่อนเวลา
        else {
          $msg = array("bk_id"=>"","success"=>false,"msg"=>"ไม่สามารถจองย้อนหลังได้");
        }//จองหลังเวลา

      return Response(json_encode($msg));

    }

    public function LocationEdit(Request $req)
    {
      $emp_id = $req->input('emp_id');
      $bk_id = $req->input('id');
      $com_id = $req->input('com_id');
      $count_where = $req->input('count_where');
      $sql = DB::table('tb_booking_location')->where('bk_id','=',$bk_id)->get();
      for($i=0;$i<$count_where;$i++){
         $where = $req->input('bk_where'.($i+1));
         if ( count($sql) >= $i+1) {
           $sqlbooking_locate =DB::table('tb_booking_location')
           ->where('bk_id', "=", $bk_id)
           ->where('location_id', "=", ($i+1))
           ->update(['com_id' => "$com_id",
                     'location_name' => "$where"]);
         }
         else {
           $sqlbooking_locate =DB::table('tb_booking_location')
           ->insert(['bk_id' => "$bk_id",
                     'com_id' => "$com_id",
                     'location_id' => ($i+1),
                     'location_name' => "$where"]);
         }
        }
        if ($count_where < count($sql)) {
          for($i=0;$i<count($sql)-$count_where;$i++){
            $sqlbooking_locate =DB::table('tb_booking_location')
            ->where(['bk_id' => "$bk_id",
                      'location_id' => count($sql)-$i
                    ])
            ->delete();
          }
        }
        // $msg ="";
        // if (!$sqlbooking_locate) {
        //   $msg = array("bk_id"=>"","success"=>false);
        // }
        // else {
          $msg = array("bk_id"=>"$bk_id","success"=>true);

        // }
        return Response(json_encode($msg));
    }

    public function EditLocation(Request $req)
    {
       $bk_id = $req->input('id');
       $location_name = $req->input('location_id');
       $latitude = $req->input('latitude');
       $longitude = $req->input('longitude');

      $sqlbooking_locate =DB::table('tb_booking_location')
                    ->where('bk_id', '=' ,$bk_id)
                    ->where('location_name', '=' ,$location_name)
                    ->update(['location_lat' => $latitude,
                              'location_lng' =>$longitude ]);
        $msg ="";
        if (!$sqlbooking_locate) {
          $msg = array("bk_id"=>"","success"=>false);
        }
        else {
          $msg = array("bk_id"=>"$bk_id","success"=>true);
          session()->put('id', $bk_id);

        }
        return Response(json_encode($msg));
    }


    public function AddBookingOT()
    {
      if (session()->has('user'))
      {
         $username = session()->get('user');
         $users = DB::table('tb_employee')
                   ->join("tb_department",function($join){
                        $join->on('tb_employee.dep_id', '=', 'tb_department.dep_id')
                             ->on("tb_employee.com_id","=","tb_department.com_id");
                              })
                   ->join('tb_employee_login', 'tb_employee.emp_id', '=', 'tb_employee_login.emp_id')
                   ->where('tb_employee.emp_email','=',$username)->get();

        return view('booking.addOT',['users'=>$users]);
      }
      else
      {
        exit("<script>window.location='/';</script>");
      }
    }

}
