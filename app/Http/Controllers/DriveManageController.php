<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class DriveManageController extends Controller
{
  public function AddDriver(Request $req)
    {
                $emp_id =$req->input("id");
                $com_id =$req->input("com_id");
                $dep_id =$req->input("dep_id");
                return view('Models.Driver.adddriver',['emp_id'=>$emp_id,'com_id'=>$com_id,'dep_id'=>$dep_id]);
    }

  public function AddDriverDB(Request $req)
    {
               date_default_timezone_set("Asia/Bangkok");
               $emp_id = $req->input('emp_id');
               $com_id =$req->input("com_id");
               $dep_id =$req->input("dep_id");
               $drive_id = $req->input('drive_id');
               $name = $req->input('drive_fname');
               $lname = $req->input('drive_lname');
               $tel = $req->input('drive_tel');
               $nfile = 'drive.png';

               if ($name <> '' && $lname <> '' && $tel <> '') {
                     if ($req->hasFile('image')) {
                       //upload file
                         $filename = "IMG_".$drive_id;
                         $type = $req->image->extension();
                         $nfile = $filename.'.'.$type;
                         $file = $req->image->storeAs('public/image/driver',$nfile);
                       }
                      $date =date("Y-m-d H:i:s");

                       $sqlUPDATE = DB::table('tb_driver')
                                    ->insert(['drive_id'=>"$drive_id",
                                    'com_id'=>"$com_id",
                                    'dep_id'=>"$dep_id",
                                    'drive_fname'=>"$name",
                                    'drive_lname'=>"$lname",
                                    'drive_img'=>"$nfile",
                                    'drive_tel'=>"$tel",
                                    'create_by'=>"$emp_id",
                                    'create_date'=>"$date",
                                    'drive_status'=>"0"]);

                      if(!$sqlUPDATE){
                          $msg = array("success"=>false,"msg"=>"");//2
                      }else{
                          $msg = array("success"=>true,"msg"=>"");//1
                      }

                }
                else {
                  $msg = array("success"=>false,"msg"=>"");//1
                }


                    return Response(json_encode($msg));
            }


            public function TableDriver(Request $req)
            {
              $typeSearch = $req->input("typeSearch");
              $com_id = $req->input("com_id");
              $txtSearch = $req->input("txtSearch");
              $drive_status = $req->input("drive_status");
              $lv_user = $req->input("lv_user");
              $dep_id = $req->input("dep_id");
              return view('Models.Driver.tabledriver',['com_id'=>$com_id,'typeSearch'=>$typeSearch,
              'txtSearch'=>$txtSearch,'drive_status'=>$drive_status,'lv_user'=>$lv_user,'dep_id'=>$dep_id]);

            }

            public function EjectDriver(Request $req)
            {
                $id = $req->input("id");
                $sqlUPDATE =DB::table('tb_driver')
                              ->where('drive_id', '=' ,$id)
                              ->update(['drive_status'=>"1"]);
                $msg ="";
                if(!$sqlUPDATE){
                    $msg = array("success"=>false,"msg"=>"");//2
                }else{
                    $msg = array("success"=>true,"msg"=>"");//1
                }

                return Response(json_encode($msg));

            }

            public function SetDrive(Request $req)
            {
               $car = $req->input("car");
               $drive =  $req->input("drive");
               $dataid = $req->input("dataid");

               return view('Models.Driver.setdriver',['car'=>$car,'drive'=>$drive,'dataid'=>$dataid]);

            }

}
