<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class CtypeManageController extends Controller
{
    //
    public function AddCtype(Request $req)
    {
      $emp_id =$req->input("id");
      return view('Models.Ctype.addctype',['emp_id'=>$emp_id]);

    }

    public function AddCtypeDB(Request $req)
    {
      date_default_timezone_set("Asia/Bangkok");
      $emp_id = strtoupper($req->input("emp_id")) ;
      $ctype_id=  $req->input("ctype_id");
      $com_id = $req->input("com_id");
      $ctype_name = $req->input("ctype_name");
      $date =date("Y-m-d H:i:s");
      $msg = array();

               $sql = DB::table('tb_car_type')
                            ->insert(['ctype_id' => $ctype_id,
                                      'com_id' => $com_id,
                                      'ctype_name' => $ctype_name,
                                      'ctype_createby' =>$emp_id,
                                      'ctype_createdate'=>$date]);
              if(!$sql){
                    $msg = array("type"=>"","success"=>false,"msg"=>"");

              }else{
                  $msg = array("type"=>"","success"=>true,"msg"=>"");
              }

              return Response(json_encode($msg));
      }

      public function EditcType(Request $req)
      {
        $emp_id =$req->input("emp_id");
        $ctype_id=$req->input("ctype");
        $com_id = $req->input("com_id");
        return view('Models.Ctype.editctype',['emp_id'=>$emp_id,'ctype_id'=>$ctype_id,'com_id'=>$com_id]);

      }

      public function UpdateCtypeDB(Request $req)
      {
        date_default_timezone_set("Asia/Bangkok");
        $emp_id = strtoupper($req->input("emp_id")) ;
        $ctype_id=  $req->input("ctype_id");
        $com_id = $req->input("com_id");
        $ctype_name = $req->input("ctype_name");
        $date =date("Y-m-d H:i:s");
        $msg = array();

        $sqlUPDATE = DB::table('tb_car_type')
                      ->where('ctype_id', '=' ,$ctype_id)
                      ->where('com_id','=',$com_id)
                      ->update(['ctype_name' => $ctype_name]);

                    $msg = array("type"=>"","success"=>true,"msg"=>"");

                return Response(json_encode($msg));
        }

}
