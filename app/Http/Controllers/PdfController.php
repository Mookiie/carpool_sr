<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Mpdf;
use View;

class PdfController extends Controller
{
    public function booking_pdf()
    {
      $view = View::make('pdf.booking');
      $contents = $view->render();

        $mpdf = new \Mpdf\Mpdf([
           'fontdata' => [
               'frutiger' => [
                   'R' => 'THSarabunNew.ttf',
                   'I' => 'THSarabunNew Italic.ttf',
                   'B' => 'THSarabunNew Bold.ttf',
               ]
           ],
           'default_font' => 'frutiger',
           'format' => 'A5'
       ]);

           $mpdf->AddPage('','','','','',5,5,5,5,0,0);
            // $html = 'text';
            $mpdf->SetWatermarkImage('image/siamraj-a4.png',0.1,'D','F');
            $mpdf->showWatermarkImage = true;
            $mpdf->WriteHTML($contents);
            $mpdf->Output();
    }

    public function ReportCar_pdf(Request $req)
    {
      $view = View::make('Models.Report.reportcar_tb',['date'=>$req->input('date'),'dep_car'=>$req->input('dep_car'),'com_id'=>$req->input('com_id')]);
      $contents = $view->render();
        $mpdf = new \Mpdf\Mpdf([
          'default_font_size' => 18,
           'fontdata' => [
               'frutiger' => [
                   'R' => 'THSarabunNew.ttf',
                   'I' => 'THSarabunNew Italic.ttf',
                   'B' => 'THSarabunNew Bold.ttf',
               ]
           ],
           'default_font' => 'frutiger',
           'format' => 'A4-L'
       ]);

           $mpdf->AddPage('','','','','',5,5,5,5,0,0);
            // $html = 'text';
            $htmlHeader = "<table>
                  <tr>
                    <td>
                      <img src='image/siamraj-a4.png' width='60'>
                    </td>
                    <td>
                      <span style='color:#000;'>บริษัท สยามราชธานี จำกัด (มหาชน)</span>
                      <br />
                      <span style='color:#000;'>CarPool Service</span>
                    </td>
                  </tr>
            </table>";
            date_default_timezone_set('Asia/Bangkok');
            $htmlFooter = " <div style='font-size:16px;right:0px;bottom:0px' align='right'>
                        Print Form Carpool Service (".date('d M Y').", ".date('H:i:s').")
                        Page : {PAGENO}/{nbpg}</div>";

            $mpdf->SetWatermarkImage('image/siamraj-a4.png',0.1,'D','F');
            $mpdf->showWatermarkImage = true;
            $mpdf->WriteHTML($htmlHeader);
            $mpdf->SetHTMLFooter($htmlFooter);
            $mpdf->WriteHTML($contents);
            $mpdf->Output();
    }

    public function ReportCarOT_pdf(Request $req)
    {
      $view = View::make('Models.Report.reportcar_tbOT',['date'=>$req->input('date'),'dep_car'=>$req->input('dep_car'),'com_id'=>$req->input('com_id')]);
      $contents = $view->render();
        $mpdf = new \Mpdf\Mpdf([
          'default_font_size' => 18,
           'fontdata' => [
               'frutiger' => [
                   'R' => 'THSarabunNew.ttf',
                   'I' => 'THSarabunNew Italic.ttf',
                   'B' => 'THSarabunNew Bold.ttf',
               ]
           ],
           'default_font' => 'frutiger',
           'format' => 'A4-L'
       ]);

           $mpdf->AddPage('','','','','',5,5,5,5,0,0);
            // $html = 'text';
            $htmlHeader = "<table>
                  <tr>
                    <td>
                      <img src='image/siamraj-a4.png' width='60'>
                    </td>
                    <td>
                      <span style='color:#000;'>บริษัท สยามราชธานี จำกัด (มหาชน)</span>
                      <br />
                      <span style='color:#000;'>CarPool Service</span>
                    </td>
                  </tr>
            </table>";
            date_default_timezone_set('Asia/Bangkok');
            $htmlFooter = " <div style='font-size:16px;right:0px;bottom:0px' align='right'>
                        Print Form Carpool Service (".date('d M Y').", ".date('H:i:s').")
                        Page : {PAGENO}/{nbpg}</div>";

            $mpdf->SetWatermarkImage('image/siamraj-a4.png',0.1,'D','F');
            $mpdf->showWatermarkImage = true;
            $mpdf->WriteHTML($htmlHeader);
            $mpdf->SetHTMLFooter($htmlFooter);
            $mpdf->WriteHTML($contents);
            $mpdf->Output();
    }

}
