<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class DashboardController extends Controller
{

  public function DashboardView()
    {
      if (session()->has('user'))
      {
            $username = session()->get('user');

            $users = DB::table('tb_employee')
                        ->join("tb_department",function($join){
                        $join->on('tb_employee.dep_id', '=', 'tb_department.dep_id')
                             ->on("tb_employee.com_id","=","tb_department.com_id");
                              })
                        ->join('tb_employee_login', 'tb_employee.emp_id', '=', 'tb_employee_login.emp_id')
                        ->where('tb_employee.emp_email','=',$username)->get();
        return view('dashboard.dashboard',['users'=>$users]);
      }
      else
      {
        exit("<script>window.location='/';</script>");
      }

    }

  public function AddBooking()
  {
    if (session()->has('user'))
    {
       $username = session()->get('user');
       $users = DB::table('tb_employee')
                 ->join("tb_department",function($join){
                        $join->on('tb_employee.dep_id', '=', 'tb_department.dep_id')
                             ->on("tb_employee.com_id","=","tb_department.com_id");
                              })
                 ->join('tb_employee_login', 'tb_employee.emp_id', '=', 'tb_employee_login.emp_id')
                 ->where('tb_employee.emp_email','=',$username)->get();

      return view('booking.add',['users'=>$users]);
    }
    else
    {
      exit("<script>window.location='/';</script>");
    }
  }

  public function EditBooking(Request $req)
  {
    if (session()->has('user'))
    {
       $bk_id = $req->input('data');
       $username = session()->get('user');
       $users = DB::table('tb_employee')
                 ->join("tb_department",function($join){
                        $join->on('tb_employee.dep_id', '=', 'tb_department.dep_id')
                             ->on("tb_employee.com_id","=","tb_department.com_id");
                              })
                 ->join('tb_employee_login', 'tb_employee.emp_id', '=', 'tb_employee_login.emp_id')
                 ->where('tb_employee.emp_email','=',$username)->get();

      return view('booking.edit',['users'=>$users,'bk_id'=>$bk_id]);
    }
    else
    {
      exit("<script>window.location='/';</script>");
    }
  }

}
