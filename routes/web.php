<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::any('/', 'LoginController@loginView');
Route::any('/login','LoginController@loginCheck');
Route::any('/logout','LoginController@Logout');
Route::any('/', 'LoginController@loginView');
Route::any('/change','LoginController@changePass');
Route::any('/changepass','LoginController@passDB');
Route::any('/firstchange','LoginController@FirstLogin');
Route::any('/firstchangeDB','LoginController@FirstLoginpassDB');
Route::any('/forget','LoginController@ForgetPass');
Route::any('/setpassword','LoginController@ReSetPass');
Route::any('/forgetpass','LoginController@ReSetPassDB');
Route::any('/locatelogin','LoginController@LocateLogin');
Route::any('/SetEmail','LoginController@SetEmail');
Route::any('/updateemail','LoginController@updateemail');
//--------------------------------------------------------------------------
Route::any('/insertemp','EmpManageController@InsertEmp');
Route::any('/empcreate','EmpManageController@EmpCreate');
Route::any('/emp','EmpManageController@ShowEmp');
Route::any('/empdep','EmpManageController@DepEmp');
Route::any('/tableemp','EmpManageController@TableEmp');
Route::any('/detailemp','EmpManageController@DetailEmp');
Route::any('/dislv','EmpManageController@DisableLevel');
Route::any('/editemp','EmpManageController@EmpEdit');
Route::any('/empedit','EmpManageController@EmpEditDB');
Route::any('/register','EmpManageController@RegisterEmp');
Route::any('/invite','EmpManageController@InviteEmp');
Route::any('/comtodep','EmpManageController@ComToDep');
Route::any('/deptojob','EmpManageController@DepToJob');
Route::any('/emailinvite','EmpManageController@CheckInviteEmp');
Route::any('/inviteregister','EmpManageController@InviteEmpRegister');
Route::any('/insertinvite','EmpManageController@InviteEmpRegisterDB');
Route::any('/modalEmpUp','EmpManageController@modalEmpUp');
Route::any('/updateEmp','EmpManageController@updateEmp');
Route::any('/updateemail','EmpManageController@updateemail');
Route::any('/SetPermission','EmpManageController@SetPermission');
Route::any('/SetPermissioDB','EmpManageController@SetPermissionDB');
Route::any('/modalsPermission','EmpManageController@Permission');
Route::any('/SetApprove','EmpManageController@SetApprove');
Route::any('/SetOrganize','EmpManageController@SetOrganize');
Route::any('/modalSetApprove','EmpManageController@Approver');
Route::any('/SetApproveDB','EmpManageController@SetApproveDB');
Route::any('/SetApproveShow','EmpManageController@ApproveShow');
Route::any('/SetApproveDep','EmpManageController@SetApproveDep');
Route::any('/SetApproveDelete','EmpManageController@SetApproveDelete');
Route::any('/userEmpUp','EmpManageController@updateUsername');
Route::any('/resetModals','EmpManageController@resetModals');
Route::any('/resetByAdmin','EmpManageController@ResetPassword');

//--------------------------------------------------------------------------
Route::any('/dashboard','DashboardController@DashboardView');
Route::any('/add','DashboardController@AddBooking');
Route::any('/edit','DashboardController@EditBooking');
//--------------------------------------------------------------------------
Route::any('/booking','QueryBookingController@QueryView');
Route::any('/wait','QueryBookingController@QueryWait');
Route::any('/approve','QueryBookingController@QueryApprove');
Route::any('/eject','QueryBookingController@QueryEject');
Route::any('/nonecar','QueryBookingController@QueryNonecar');
Route::any('/success','QueryBookingController@QuerySuccess');
Route::any('/bookingcar','QueryBookingController@QueryCar');
Route::any('/bookingOT','QueryBookingController@QueryCarOT');
Route::any('/bookingnonecar','QueryBookingController@QuerySetNonecar');
Route::any('/driver','QueryBookingController@DriverView');
Route::any('/insert','QueryBookingController@InsertsBooking');
Route::any('/approvestatus','QueryBookingController@ApproveBooking');
Route::any('/waittoapprove','QueryBookingController@Approver');
Route::any('/approvemail','QueryBookingController@ApproveMailBooking');
Route::any('/approvemailDB','QueryBookingController@ApproveMailBookingDB');
Route::any('/ReApprove','QueryBookingController@ReApproveBooking');
Route::any('/EditDB','QueryBookingController@EditBooking');
Route::any('/addOT','QueryBookingController@AddBookingOT');
//--------------------------------------------------------------------------
Route::any('/satisfaction',"satisfactionController@satisfaction");
Route::any('/satisfactionDB',"satisfactionController@satisfactionDB");

//--------------------------------------------------------------------------
Route::any('/sendbooking',"mailController@sendBooking");
Route::any('/sendeditbooking',"mailController@sendEditBooking");
Route::any('/sendbookingApprove',"mailController@sendBookingApprove");
Route::any('/sendemp',"mailController@sendEmp");
Route::any('/sendapprove',"mailController@sendApprove");
Route::any('/sendSetCar',"mailController@sendApproveSetcar");
Route::any('/sendcar',"mailController@sendSetCar");
Route::any('/sendcarOT',"mailController@sendSetCarOT");
Route::any('/sendeject',"mailController@sendEject");
Route::any('/sendnonecar',"mailController@sendNocar");
Route::any('/sendpass',"mailController@sendPass");
Route::any('/sendinvite',"mailController@sendInvite");
Route::any('/sendforget',"mailController@sendforget");
Route::any('/sendalertstart',"mailController@sendalertstart");
Route::any('/sendsatisfaction',"mailController@sendalertsatisfaction");

//--------------------------------------------------------------------------
Route::any('/detail','DetailController@DetailBooking');
Route::any('/map','DetailController@DetailMap');
//--------------------------------------------------------------------------
Route::any('/detailcar','CarManageController@DetailCar');
Route::any('/addcar','CarManageController@AddCar');
Route::any('/createcar','CarManageController@AddCarDB');
Route::any('/car','CarManageController@CarView');
Route::any('/tablecar','CarManageController@TableCar');
Route::any('/setcar','CarManageController@SetCar');
Route::any('/setCarDB','CarManageController@SetCarDB');
Route::any('/addcardb','CarManageController@AddCarDB');
Route::any('/tbsetcar','CarManageController@TbSetCar');
Route::any('/ReSetcar','CarManageController@ReSetcar');
Route::any('/ReUseCar','CarManageController@ReUseCar');
Route::any('/changecar','CarManageController@ChangeCar');
Route::any('/ChangeCarDB','CarManageController@ChangeCarDB');
Route::any('/statuscar','CarManageController@StatusCar');
Route::any('/loandue','CarManageController@LoanDue');
Route::any('/loancar','CarManageController@LoanCar');
Route::any('/merge','CarManageController@MergeCar');
Route::any('/mergeDB','CarManageController@MergeCarDB');
Route::any('/mergeOT','CarManageController@MergeCarOT');
Route::any('/mile','CarManageController@MileView');
Route::any('/setmile','CarManageController@setmile');
Route::any('/setmileDB','CarManageController@setmileDB');

//--------------------------------------------------------------------------
Route::any('/adddriver','DriveManageController@AddDriver');
Route::any('/createdriver','DriveManageController@AddDriverDB');
Route::any('/tabledriver','DriveManageController@TableDriver');
Route::any('/ejectdriver','DriveManageController@EjectDriver');
Route::any('/setdrive','DriveManageController@SetDrive');
//--------------------------------------------------------------------------
Route::any('/otheradd','OtherController@addAll');
Route::any('/otheredit','OtherController@EditShow');
Route::any('/editdep','OtherController@EditDep');
Route::any('/editjob','OtherController@EditJob');
Route::any('/editctype','OtherController@EditcType');
Route::any('/editcolor','OtherController@EditColor');
Route::any('/editfuel','OtherController@EditFuel');
Route::any('/editbrand','OtherController@EditBrand');
//--------------------------------------------------------------------------
Route::any('/ctypeedit','CtypeManageController@EditcType');
Route::any('/updatectype','CtypeManageController@UpdateCtypeDB');
Route::any('/addctype','CtypeManageController@AddCtype');
Route::any('/createctype','CtypeManageController@AddCtypeDB');
//--------------------------------------------------------------------------
Route::any('/coloredit','ColorManageController@EditColor');
Route::any('/updatecolor','ColorManageController@UpdateColorDB');
Route::any('/addcolor','ColorManageController@AddShowcolor');
Route::any('/createcolor','ColorManageController@AddcolorDB');
//--------------------------------------------------------------------------
Route::any('/fueledit','FuelManageController@EditFuel');
Route::any('/updatefuel','FuelManageController@UpdateFuelDB');
Route::any('/addfuel','FuelManageController@AddFuel');
Route::any('/createfuel','FuelManageController@AddFuelDB');
//--------------------------------------------------------------------------
Route::any('/brandedit','BrandManageController@EditBrand');
Route::any('/updatebrand','BrandManageController@UpdateBrandDB');
Route::any('/addbrand','BrandManageController@AddBrand');
Route::any('/createbrand','BrandManageController@AddBrandDB');
//--------------------------------------------------------------------------
Route::post('/insertsLocation','QueryBookingController@LocationInserts');
Route::any('/locate','LocationController@ViweLocation');
Route::any('/location','QueryBookingController@InsertsLocation');
Route::any('/locateDB','QueryBookingController@LocateDB');
Route::any('/editLocation','QueryBookingController@LocationEdit');
//--------------------------------------------------------------------------
Route::any('/adddep','DepartmentController@UPDATEDepartment');
Route::any('/createdep','DepartmentController@UPDATEDB');
Route::any('/depedit','DepartmentController@EditDep');
Route::any('/updatedep','DepartmentController@UpdateDepDB');
//--------------------------------------------------------------------------
Route::any('/addjob','JobController@UPDATEDJob');
Route::any('/createjob','JobController@UPDATEDB');
Route::any('/jobedit','JobController@EditJob');
Route::any('/updatejob','JobController@UpdateDB');

// Route::any('/addjobsr','JobController@UPDATEDepartment');
// Route::any('/createjobsr','JobController@UPDATEDB');
//--------------------------------------------------------------------------
Route::any('/addcom','CompanyController@AddCompany');
Route::any('/createcom','CompanyController@AddDB');
Route::any('/comedit','CompanyController@EditJob');
Route::any('/updatecom','CompanyController@UpdateDB');
//--------------------------------------------------------------------------
Route::any('/reportbooking','ReportController@ReportBookingShow');
Route::any('/searchbooking','ReportController@ReportBookingSearch');
Route::any('/semp','ReportController@ReportBookingSearchDep');
Route::any('/reportcar','ReportController@ReportCarShow');
Route::any('/searchcar','ReportController@ReportCarSearch');
Route::any('/reportcarOT','ReportController@ReportCarShowOT');
Route::any('/searchcarOT','ReportController@ReportCarSearchOT');

//--------------------------------------------------------------------------
Route::any('/calendar','CalendarController@Calendar');
Route::any('/calendardep','CalendarController@CalendarDep');
Route::any('/selectcar','CalendarController@SelectCar');
Route::any('/allcarEvent','CalendarController@AllCarEvent');
Route::any('/BkEvents','CalendarController@BkEvent');
//--------------------------------------------------------------------------
Route::any('/bus','BusController@BusView');
Route::any('/selectbus','BusController@SelectCar');
Route::any('/insertbus','BusController@InsertBus');
// Route::any('/insert_bus','BusController@BusView');
//--------------------------------------------------------------------------
Route::any('/bookingpdf', 'PdfController@booking_pdf');
Route::any('/reportcarpdf', 'PdfController@ReportCar_pdf');
Route::any('/reportcarotpdf', 'PdfController@ReportCarOT_pdf');


//--------------------------------------------------------------------------
Route::any('/InsertBusOT','OverTimeController@InsertBusOT');

//--------------------------------------------------------------------------
Route::get('/sync', function () {
    return view('sync');
});
Route::get('/sendmail', function () {
    return view('sendmail');
});
// Route::any('/mail', function () {
//     return view('Mail.mailsatisfaction');
// });
// Route::any('/pdf', 'PdfController@ReportCar_pdf');
// Route::any('/pdf',  function () {
//     return view('pdf.booking');
// });
// Route::any('/pagination','PaginateController@Pagination');
// Route::any('/testdb','PaginateController@testDB');
//--------------------------------------------------------------------------

// Auth::routes();
// Route::get('/home', 'HomeController@index')->name('home');
