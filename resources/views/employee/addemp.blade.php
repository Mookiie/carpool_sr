@extends('welcome')
@section('content')
@include('dashboard.topNavbar')
@include('dashboard.SideNavbar')
<link rel="stylesheet" type="text/css" href="{{ asset('js/datetimepicker/jquery.datetimepicker.css')}}"/>
<script src="{{asset('js/datetimepicker/jquery.js')}}"></script>
<script src="{{asset('js/datetimepicker/build/jquery.datetimepicker.full.min.js')}}"></script>
<meta name="viewport" content="initial-scale=1.0, user-scalable=no">
@foreach ($users as $r)
  <?php
        $emp_id = $r->emp_id;
        $lv_user = $r->emp_level;
        $com_id = $r->com_id;
   ?>
@endforeach
<div class="container-dashboard">

<div class="card col-md-12 row">
  <div class="card-header">
    <span class="fa fa-plus">  เพิ่มข้อมูลพนักงาน</span>
  </div>
  <div class="card-block">
    <div>

      <form id="frm_EmpCreate">


            <div class="card offset-sm-2 col-md-8">
              <div class="card-block">
                <input id="emp_id" name="emp_id" type="hidden" class="form-control" value="{{$emp_id}}">

                  <div class="form-group row">
                    <label for="emp_fname" class="col-md-3 col-form-label">ชื่อ - นามสกุล</label>
                    <div class="col-md-4">
                      <input id="emp_fname" name="emp_fname" type="text" class="form-control"  placeholder="ชื่อพนักงาน" onfocus="rmErr(this);" onkeypress="rmErr(this);">
                    </div>
                    <div class="col-md-4">
                      <input id="emp_lname" name="emp_lname" type="text" class="form-control" placeholder="นามสกุลพนักงาน" onfocus="rmErr(this);" onkeypress="rmErr(this);">
                    </div>
                  </div>

                  <div class="form-group row">
                    <label for="emp_email" class="col-md-3 col-form-label">Email</label>
                    <div class="col-sm-8">
                      <input type="email" class="form-control" id="emp_email" name="emp_email" placeholder="you@example.com" onfocus="rmErr(this);" onkeypress="rmErr(this);">
                        <div hidden="true" id="fbemp_email" class="form-control-feedback"><br></div>
                    </div>
                  </div>

                  <div class="form-group row">
                    <label for="emp_sex" class="col-md-3 col-form-label">เพศ</label>
                    <div class="col-8">
                    <select class="form-control mr-sm-2"  id="emp_sex" name="emp_sex">
                      <option value="0">กรุณาเลือกเพศ</option>
                      <option value="1">ชาย</option>
                      <option value="2">หญิง</option>
                    </select>
                  </div>
                  </div>

                  <div class="form-group row">
                    <label for="com_id" class="col-md-3 col-form-label">บริษัท</label>
                    <div class="col-8">
                    <select class="form-control mr-sm-2"  id="com_id" name="com_id">
                      <?php
                          $sql_com = DB::table('tb_company')->get();
                      ?>
                        @foreach ($sql_com as $com)
                          <option value="{{$com->com_id}}">{{$com->com_name}}</option>
                        @endforeach
                    </select>
                  </div>
                </div>

                  <div class="form-group row">
                    <label for="dep_id" class="col-md-3 col-form-label">หน่วยงาน</label>
                    <div class="col-8 select-dep">
                      <select class="form-control mr-sm-2"  id="dep_id" name="dep_id">
                        <?php
                            // $sql_dep = DB::table('tb_department')->get();
                          ?>
                          {{-- @foreach ($sql_dep as $dep)
                            <option value="{{$dep->dep_id}}">{{$dep->dep_name}}</option>
                          @endforeach --}}
                      </select>

                  </div>
                  </div>

                  <div class="form-group row">
                    <label for="job_id" class="col-md-3 col-form-label">ตำแหน่ง</label>
                    <div class="col-8">
                    <select class="form-control mr-sm-2"  id="job_id" name="job_id">
                      <?php
                          // $sqljob = DB::table('tb_job')->get();

                        ?>
                        {{-- @foreach ($sqljob as $job)
                          <option value="{{$job->job_id}}">{{$job->job_name}}</option>
                        @endforeach --}}
                    </select>
                  </div>
                  </div>

                  {{-- <div class="form-group row">
                    <label for="count_person" class="col-md-3 col-form-label">รูปภาพพนักงาน </label>
                    <div class="col-8">
                    <div class="input-group">
                      <span class="input-group-btn">
                        <label class="btn btn-primary btn-file">
                            <span class="fa fa-file" style="color:#fff;"></span> <input type="file" id="img" style="display: none;" name="img">
                        </label>
                      </span>
                      <input type="text" class="form-control" id="imgName" placeholder="รูปภาพพนักงาน" >
                    </div>
                    </div>
                  </div> --}}

                  <div class="form-group row">
                    <label for="tel" class="col-md-3 col-form-label">มือถือ</label>
                    <div class="col-md-8">
                      <input id="mtel" name="mtel" type="text" class="form-control" placeholder="กรอกเบอร์โทรศัพท์มือถือ" onfocus="rmErr(this);" onkeypress="rmErr(this);">
                      <div hidden="true" id="fbmtel" class="form-control-feedback"><br></div>
                    </div>
                  </div>

                  <div class="form-group row">
                    <label for="emp_level" class="col-md-3 col-form-label">สิทธิการใช้งาน</label>
                    <div class="col-8">
                    <select class="form-control mr-sm-2"  id="emp_level" name="emp_level">
                      <option value="">เลือกสิทธิการใช้งาน</option>
                      @if ($lv_user == 999)
                        <option value="99">เจ้าหน้าที่ดูแลระบบ</option>
                      @endif
                      <option value="0">เจ้าหน้าที่(จอง)</option>
                      <option value="1">หัวหน้างาน(อนุมัติ)</option>
                      <option value="2">เจ้าหน้าที่(จัดรถ)</option>
                    </select>
                    </div>
                  </div>

                  <div class="form-group row">
                    <label for="emp_pass" class="col-md-3 col-form-label">Password ยืนยัน</label>
                    <div class="col-sm-8">
                      <input type="password" class="form-control" id="emp_pass" name="emp_pass" placeholder="กรุณากรอกรหัสผ่านเพื่อยืนยันการบันทึกข้อมูล" onfocus="rmErr(this);" onkeypress="rmErr(this);" disabled>
                        <div hidden="true" id="fbemp_pass" class="form-control-feedback"><br></div>
                    </div>
                  </div>

                  <div class="form-group form-group-row" align="center">
                      <button type="button" class="btn btn-success" id="btn_save"  disabled>บันทึก</button>
                      <a href="/dashboard"><button type="button" class="btn btn-danger">ยกเลิก</button></a>
                  </div>

              </div>
                </div>
              </div>

      </form>
    </div>
  </div>
</div>
</div>

<script>

$(function() {

  // We can attach the `fileselect` event to all file inputs on the page
  $(document).on('change', ':file', function() {
    var input = $(this),
        numFiles = input.get(0).files ? input.get(0).files.length : 1,
        label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
    input.trigger('fileselect', [numFiles, label]);
  });
  // We can watch for our custom `fileselect` event like this
  $(document).ready( function() {
      $(':file').on('fileselect', function(event, numFiles, label) {
          var input = $(this).parents('.input-group').find(':text'),
              log = numFiles > 1 ? numFiles + ' files selected' : label;
          if( input.length ) {
              input.val(log);
          } else {
              if( log ) alert(log);
          }
      });
  });
  });
$(document).ready(function () {
  var com_id = $("#com_id").val();
  comtodep(com_id);
})

$("#com_id").change(function () {
  var com_id = $("#com_id").val();
  comtodep(com_id);
})
function comtodep(id) {
  $.ajax({
    url:"/comtodep",
    type:"GET",
    data:"com_id="+id,
    contentType: false,
    processData: false,
    success:function(data){
   $("#dep_id").html(data);
     var dep_id = $("#dep_id").val();
     deptojob(dep_id,id);
    }
  })
}

$("#dep_id").change(function () {
  var dep_id = $("#dep_id").val();
  var com_id = $("#com_id").val();
  deptojob(dep_id,com_id);
})

function deptojob(id1,id2) {
  $.ajax({
    url:"/deptojob",
    type:"GET",
    data:"dep_id="+id1+"&com_id="+id2,
    contentType: false,
    processData: false,
    success:function(data){
   $("#job_id").html(data);
    }
  })
}

  $("#emp_level").change(function () {
    var emplv = $(this).val();
    if (emplv != '') {
      delDisabled("emp_pass");
    }
    else {
      $("#emp_pass").attr("disabled","disabled");
    }
  })

  $("#emp_pass").change(function () {
      delDisabled("btn_save");
  })

  function delDisabled(id){
    $("#"+id).removeAttr("disabled");
  }

$("#btn_save").click(function(){
  var formSend = $("#frm_EmpCreate").serialize();
  // var img = $( '#img' )[0].files[0];
console.log(formSend);
        $.ajax({
          url:"/insertemp",
          type:"GET",
          data:formSend,
          contentType: false,
          processData: false,
          success:function(data){
            var obj = JSON.parse(data);
            if (obj['success']==true) {
              $.ajax({
                url:"/sendemp",
                type:"GET",
                data:"id="+obj['id']+"&pass="+obj['pass'],
                contentType: false,
                processData: false,
                success:function(data){
                  swal({
                            title: "บันทึกข้อมูลพนักงานสำเร็จ",
                            text: "ตรวจสอบอีเมลเพื่อรับรหัสผ่าน",
                            type: "success",
                            showCancelButton: false,
                            confirmButtonColor: "#2ECC71",
                            confirmButtonText: "ตกลง",
                            closeOnConfirm: false,
                          },
                            function(isConfirm){
                              if (isConfirm) {
                                window.location = "/dashboard";
                          }
                      });
                }
              })

            }
            else
            {
              addErr(obj['type'],obj['msg'])
            }

        }
    })
});

function rmErr(input){
  $("#group"+input.id).removeClass("has-danger");
  $("#group"+input.id+" input").removeClass("form-control-danger");
  $("#fb"+input.id).attr("hidden","hidden");
}

function addErr(type,msg){
    $("#group"+type).addClass("has-danger");
    $("#group"+type+" input").addClass("form-control-danger");
    $("#fb"+type).html(msg);
    $("#fb"+type).removeAttr("hidden");
}

</script>

@endsection
