@extends('welcome')
@section('content')

<link rel="stylesheet" type="text/css" href="{{ asset('js/datetimepicker/jquery.datetimepicker.css')}}"/>
<script src="{{asset('js/datetimepicker/jquery.js')}}"></script>
<script src="{{asset('js/datetimepicker/build/jquery.datetimepicker.full.min.js')}}"></script>
<meta name="viewport" content="initial-scale=1.0, user-scalable=no">
<?php
    $url=  $_SERVER['REQUEST_URI'];
    $pieces = explode("?", $url);
    $data = base64_decode($pieces[1]);
    $split = explode("&", $data);
    $email=$split[0];
    // $email = "prapas.s@siamraj.com";
    $com_id=$split[1];
    $level=$split[2];
$sql = DB::table('tb_employee')->where('emp_email','=',$email)->get();
if (count($sql)>0) {
  ?>
  <div class="container-fluid">
    <div class="block block-login">
      <img src="{{$url_img_logo = Storage::url('image/logo/logo_siamraj.png')}}">
      <h4>Carpool Siamraj Service</h4>
       <small>อีเมลนี้มีในระบบแล้ว</small>
      <div class="block-login block-login-form">

      <div class="col-md-12 text-black">

          อีเมลนี้มีในระบบแล้วหากจำรหัสผ่านไม่ได้ <br />
          กรุณากด <a href="/forget">ลืมรหัสผ่าน</a> เพื่อเปลี่ยนรหัสผ่านใหม่

        <div  align="center">
          <button type="button" class="btn btn-success" id="btn-close">ปิด</button>
        </div>

      </div>

      <script>
      $("#btn-close").click(function(){
            window.close()
      });
      </script>
  <?php
  }
else {
 ?>
 @include('dashboard.SideNav')

<div class="container" style="margin-top: 5%;">

  <div class="card-block">
    <div>

      <form id="frm_EmpCreate">


            <div class="card offset-sm-2 col-md-8">
              <div class="card-block">

                  <div class="form-group row">
                    <label for="emp_fname" class="col-md-3 col-form-label">ชื่อ - นามสกุล<label class="text-red">*</label></label>
                    <div class="col-md-4">
                      <input id="emp_fname" name="emp_fname" type="text" class="form-control"  placeholder="ชื่อพนักงาน" onfocus="rmErr(this);" onkeypress="rmErr(this);">
                    </div>
                    <div class="col-md-4">
                      <input id="emp_lname" name="emp_lname" type="text" class="form-control" placeholder="นามสกุลพนักงาน" onfocus="rmErr(this);" onkeypress="rmErr(this);">
                    </div>
                  </div>

                  <div class="form-group row">
                    <label for="emp_email" class="col-md-3 col-form-label">Email</label>
                    <div class="col-sm-8">
                      <input type="email" class="form-control" id="emp_email" name="emp_email"  value="{{$email}}" disabled>
                        <div hidden="true" id="fbemp_email" class="form-control-feedback"><br></div>
                    </div>
                  </div>

                  <div class="form-group row">
                    <label for="emp_sex" class="col-md-3 col-form-label">เพศ</label>
                    <div class="col-8">
                    <select class="form-control mr-sm-2"  id="emp_sex" name="emp_sex">
                      <option value="">กรุณาเลือกเพศ</option>
                      <option value="1">ชาย</option>
                      <option value="2">หญิง</option>
                    </select>
                  </div>
                  </div>

                  <div class="form-group row">
                    <label for="com_id" class="col-md-3 col-form-label">บริษัท</label>
                    <div class="col-8">
                    <select class="form-control mr-sm-2"  id="com_id" name="com_id" disabled>
                      <?php
                          $sql_com = DB::table('tb_company')->get();
                      ?>
                        @foreach ($sql_com as $com)
                          @if ($com->com_id == $com_id)
                            <option value="{{$com->com_id}}" selected>{{$com->com_name}}</option>
                          @endif
                          <option value="{{$com->com_id}}">{{$com->com_name}}</option>
                        @endforeach
                    </select>
                  </div>
                </div>

                  <div class="form-group row">
                    <label for="dep_id" class="col-md-3 col-form-label">หน่วยงาน<label class="text-red">*</label></label>
                    <div class="col-8 select-dep">
                      <select class="form-control mr-sm-2"  id="dep_id" name="dep_id">
                      </select>
                    </div>
                  </div>

                  <div class="form-group row">
                    <label for="job_id" class="col-md-3 col-form-label">ตำแหน่ง<label class="text-red">*</label></label>
                      <div class="col-8">
                        <select class="form-control mr-sm-2"  id="job_id" name="job_id">
                        </select>
                      </div>
                  </div>

                  <div class="form-group row">
                    <label for="tel" class="col-md-3 col-form-label">มือถือ</label>
                    <div class="col-md-8">
                      <input id="mtel" name="mtel" type="text" class="form-control" placeholder="กรอกเบอร์โทรศัพท์มือถือ" onfocus="rmErr(this);" onkeypress="rmErr(this);">
                      <div hidden="true" id="fbmtel" class="form-control-feedback"><br></div>
                    </div>
                  </div>

                  <div class="form-group row">
                    <label for="emp_level" class="col-md-3 col-form-label">สิทธิการใช้งาน</label>
                    <div class="col-8">
                    <select class="form-control mr-sm-2"  id="emp_level" name="emp_level" disabled>
                      <?php
                          $sql_lv = DB::table('tb_level')->where('lv_id','<','99')->get();
                      ?>
                      <option value="">เลือกสิทธิการใช้งาน</option>
                      @foreach ($sql_lv as $lv)
                        @if ($lv->lv_id == $level)
                          <option value="{{$lv->lv_id}}" selected>{{$lv->lv_name}}</option>
                        @endif
                        <option value="{{$lv->lv_id}}">{{$lv->lv_name}}</option>
                      @endforeach
                    </select>
                    </div>
                  </div>

                  <div class="form-group form-group-row" align="center">
                      <button type="button" class="btn btn-success" id="btn_save">บันทึก</button>
                      <a href="/dashboard"><button type="button" class="btn btn-danger">ยกเลิก</button></a>
                  </div>

              </div>
                </div>
              </div>

      </form>
    </div>

</div>

<script>

$(document).ready(function () {
  var com_id = $("#com_id").val();
  comtodep(com_id);
})

$("#com_id").change(function () {
  var com_id = $("#com_id").val();
  comtodep(com_id);
})
function comtodep(id) {
  $.ajax({
    url:"/comtodep",
    type:"GET",
    data:"com_id="+id,
    contentType: false,
    processData: false,
    success:function(data){
   $("#dep_id").html(data);
     var dep_id = $("#dep_id").val();
     deptojob(dep_id,id);
    }
  })
}

$("#dep_id").change(function () {
  var dep_id = $("#dep_id").val();
  var com_id = $("#com_id").val();
  deptojob(dep_id,com_id);
})

function deptojob(id1,id2) {
  $.ajax({
    url:"/deptojob",
    type:"GET",
    data:"dep_id="+id1+"&com_id="+id2,
    contentType: false,
    processData: false,
    success:function(data){
   $("#job_id").html(data);
    }
  })
}

$("#btn_save").click(function(){
  var formSend = $("#frm_EmpCreate").serialize();
  var emp_level = $("#emp_level").val();
  var com_id = $("#com_id").val();
  var emp_email = $("#emp_email").val();
  // console.log(formSend+"&emp_level="+emp_level+"&com_id="+com_id+"&emp_email="+emp_email);

        $.ajax({
          url:"/insertinvite",
          type:"GET",
          data:formSend+"&emp_level="+emp_level+"&com_id="+com_id+"&emp_email="+emp_email,
          contentType: false,
          processData: false,
          success:function(data){
            // console.log(data);
            var obj = JSON.parse(data);
            if (obj['success']==true) {
              $.ajax({
                url:"/sendemp",
                type:"GET",
                data:"id="+obj['id']+"&pass="+obj['pass'],
                contentType: false,
                processData: false,
                success:function(data){
                  swal({
                            title: "บันทึกข้อมูลพนักงานสำเร็จ",
                            text: "ตรวจสอบอีเมลเพื่อรับรหัสผ่าน",
                            type: "success",
                            showCancelButton: false,
                            confirmButtonColor: "#2ECC71",
                            confirmButtonText: "ตกลง",
                            closeOnConfirm: false,
                          },
                            function(isConfirm){
                              if (isConfirm) {
                                window.location = "/";
                          }
                      });
                }
              })

            }
            else
            {
              addErr(obj['type'],obj['msg'])
            }

        }
    })
});

function rmErr(input){
  $("#group"+input.id).removeClass("has-danger");
  $("#group"+input.id+" input").removeClass("form-control-danger");
  $("#fb"+input.id).attr("hidden","hidden");
}

function addErr(type,msg){
    $("#group"+type).addClass("has-danger");
    $("#group"+type+" input").addClass("form-control-danger");
    $("#fb"+type).html(msg);
    $("#fb"+type).removeAttr("hidden");
}

</script>
<?php }?>
@endsection
