@extends('welcome')
@section('content')
@include('dashboard.topNavbar')
@include('dashboard.SideNavbar')
<link rel="stylesheet" type="text/css" href="{{ asset('js/datetimepicker/jquery.datetimepicker.css')}}"/>
<script src="{{asset('js/datetimepicker/jquery.js')}}"></script>
<script src="{{asset('js/datetimepicker/build/jquery.datetimepicker.full.min.js')}}"></script>
<meta name="viewport" content="initial-scale=1.0, user-scalable=no">
@foreach ($users as $r)
  <?php
        $emp_id = $r->emp_id;
        $com_id = $r->com_id;
   ?>
@endforeach
<div class="container-dashboard">

<div class="card col-md-12 row">
  <div class="card-header">
    <span class="fa fa-plus">  เพิ่มพนักงาน</span>
  </div>
  <div class="card-block">
    <div>

      <form id="frm_EmpInvite">


            <div class="card offset-sm-2 col-md-8">
              <div class="card-block">
                <input id="emp_id" name="emp_id" type="hidden" class="form-control" value="{{$emp_id}}">
                <input id="com_id" name="com_id" type="hidden" class="form-control" value="{{$com_id}}">



                    <ul class="nav flex-column" id="area-email">
                      <li>
                        <div class="form-group row">
                            <label for="emp_email" class="col-md-3 col-form-label">Email</label>
                            <div class="col-sm-8">
                                <input type="email" class="form-control addinput" id="emp_email1" name="emp_email1"  placeholder="you@example.com" onfocus="rmErr(this);" onkeypress="rmErr(this);">
                                <div hidden="true" id="fbemp_email1" class="form-control-feedback"><br></div>
                            </div>
                        </div>
                      </li>
                  </ul>

                  {{-- <div class="form-group">
                      <label for="exampleFormControlTextarea1">Example textarea</label>
                        <span class="chip">
                          John Doe
                          <span class="closebtn" onclick="this.parentElement.style.display='none'">&times;</span>
                        </span>
                      </textarea>
                  </div> --}}


                  <div class="form-group row container" align="center">
                    <div class="col-sm-6">
                      <button type="button" class="btn btn-block btn-sm btn-success btn-Email"><span class="fa fa-plus" style="color:#fff;"></span></button>
                    </div>
                    <div class="col-sm-6">
                      <button type="button" class="btn btn-block btn-sm btn-danger btn-eraser"><span class="fa fa-minus" style="color:#fff;"></span></button>
                    </div>
                  </div>

                  <div class="form-group row">
                    <label for="emp_level" class="col-md-3 col-form-label">สิทธิการใช้งาน</label>
                    <div class="col-8">
                    <select class="form-control mr-sm-2"  id="emp_level" name="emp_level">
                      <option value="">เลือกสิทธิการใช้งาน</option>
                      <option value="0">ผู้ใช้งานทั่วไป</option>
                      <option value="1">ผู้อนุมัติ</option>
                      <option value="2">เจ้าหน้าจัดรถ</option>
                    </select>
                    </div>
                  </div>

                  <div class="form-group row">
                    <label for="emp_pass" class="col-md-3 col-form-label">Password ยืนยัน</label>
                    <div class="col-sm-8">
                      <input type="password" class="form-control" id="emp_pass" name="emp_pass" placeholder="กรุณากรอกรหัสผ่านเพื่อยืนยันการบันทึกข้อมูล" onfocus="rmErr(this);" onkeypress="rmErr(this);" disabled>
                        <div hidden="true" id="fbemp_pass" class="form-control-feedback"><br></div>
                    </div>
                  </div>

                  <div class="form-group form-group-row" align="center">
                      <button type="button" class="btn btn-success" id="btn_save"  disabled>ส่งคำเชิญ</button>
                      <a href="/dashboard"><button type="button" class="btn btn-danger">ยกเลิก</button></a>
                  </div>

              </div>
                </div>
              </div>

      </form>
    </div>
  </div>
</div>
</div>

<script>

$(function() {

  // We can attach the `fileselect` event to all file inputs on the page
  $(document).on('change', ':file', function() {
    var input = $(this),
        numFiles = input.get(0).files ? input.get(0).files.length : 1,
        label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
    input.trigger('fileselect', [numFiles, label]);
  });
  // We can watch for our custom `fileselect` event like this
  $(document).ready( function() {
      $(':file').on('fileselect', function(event, numFiles, label) {
          var input = $(this).parents('.input-group').find(':text'),
              log = numFiles > 1 ? numFiles + ' files selected' : label;
          if( input.length ) {
              input.val(log);
          } else {
              if( log ) alert(log);
          }
      });
  });
  });

  $("#emp_level").change(function () {
    var emplv = $(this).val();
    if (emplv != '') {
      delDisabled("emp_pass");
    }
    else {
      $("#emp_pass").attr("disabled","disabled");
    }
  })

  $("#emp_pass").keyup(function () {
    var text = $('#emp_pass').val();
    delDisabled("btn_save");
  })

  function delDisabled(id){
    $("#"+id).removeAttr("disabled");
  }

  $(".btn-Email").click(function(){
      AddEmailInput();
  });

  $(".addinput").keypress(function(event){
     var kc = event.keyCode;
     if(kc==13){
      AddEmailInput()
     }
  });

  $(".addinput").blur(function() {
    var num = $("#area-email li").length;
    var email = $("#emp_email1").val();
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (re.test(email)==false) {
      addErr('emp_email'+(num-1),'กรุณากรอกอีเมลให้ถูกต้อง')
    }
  })

  function AddEmailInput() {
    var num = $("#area-email li").length;
    $("#area-email li:last").after
    (
      "<li>"
      +"<div class='form-group row'>"
      +"<label for='emp_email' class='col-md-3 col-form-label'></label>"
          +"<div class='col-sm-8'>"
              +"<input type='email' class='form-control addinput' id='emp_email"+(num+1)+"' name='emp_email"+(num+1)+"' placeholder='you@example.com' onfocus='rmErr(this);' onkeypress='rmErr(this);'>"
              +"<div hidden='true' id='fbemp_email"+(num+1)+"' class='form-control-feedback'><br></div>"
         +"</div>"
     +" </div>"
     +"</li>"
   );
   document.getElementById("emp_email"+(num+1)).focus();
   $(".addinput").keypress(function(event){
      var kc = event.keyCode;
      if(kc==13){
       AddEmailInput()
      }
   });
   $(".addinput").blur(function() {
     var num = $("#area-email li").length;
     var email = $('#emp_email'+(num-1)).val();
     var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
     if (re.test(email)==false) {
       addErr('emp_email'+(num-1),'กรุณากรอกอีเมลให้ถูกต้อง')
     }
   })
 }//function AddEmailInput

  $(".btn-eraser").click(function(){
    var num = ($("#area-email li").length)-1;
    if(num > 0){
        $("#area-email li:eq("+num+")").remove();
    }

  });

  $("#btn_save").click(function() {
    var Sdata = $("#frm_EmpInvite").serialize();
    var num = $("#area-email li").length;
        $.ajax({
          url:"/emailinvite",
          type:"get",
          data:Sdata+"&num="+num,
          success:function(data){
             var obj = JSON.parse(data);
             if (obj['success']==true) {
              for (var i = 0; i < obj['msg'].length; i++) {
                var mail = obj['msg'][i];
                         $.ajax({
                           url:"/sendinvite",
                           type:"GET",
                           data:"mail="+mail,
                           success:function(mail){
                             var objm = JSON.parse(mail);
                             if (objm['success']==true) {
                               swal({
                                         title: "ส่งคำเชิญเรียบร้อยแล้ว",
                                         type: "success",
                                         showCancelButton: false,
                                         confirmButtonColor: "#2ECC71",
                                         confirmButtonText: "ตกลง",
                                         closeOnConfirm: false,
                                       },
                                         function(isConfirm){
                                           if (isConfirm) {
                                             window.location = "/invite";
                                       }
                                   });
                             }
                           }
                         })

              //    console.log(i);
              //    console.log(obj['msg'][i]);
               }
             }//if success
          }//function
        });
  })

function rmErr(input){
  $("#group"+input.id).removeClass("has-danger");
  $("#group"+input.id+" input").removeClass("form-control-danger");
  $("#fb"+input.id).attr("hidden","hidden");
}

function addErr(type,msg){
    $("#group"+type).addClass("has-danger");
    $("#group"+type+" input").addClass("form-control-danger");
    $("#fb"+type).html(msg);
    $("#fb"+type).removeAttr("hidden");
}
function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

</script>

@endsection
