@extends('welcome')
@extends('dashboard.topNavbar')
@extends('dashboard.SideNavbar')

@section('content')

@foreach ($users as $r)
  <?php
        $lv_user = $r->emp_level;
        $id = $r->emp_id;
        $com = $r->com_id;
   ?>
@endforeach
<link href="{{ asset('js/viewer/dist/viewer.css')}}" rel="stylesheet">
<script src="{{ asset('js/viewer/dist/viewer.js')}}"></script>

 <div class="container-dashboard">
<div class="card col-md-12 row">
<input type="hidden"  id="emp_id" value="{{$id}}">
<input type="hidden" id="com_id" value="{{$com}}">
<input type="hidden" id="lv_user" value="{{$lv_user}}">
  <div class="card-header">
    <span class="fa fa-address-book mr-auto" style="color:#000;" > ข้อมูลพนักงาน</span>
  </div>

  <div class="table-resposive" id="table_emp">
  <nav class="navbar navbar-light bg-faded">

    <form class="form-inline" id="frmSearch_emp">
      <div class="form-group">
        <select class="form-control mr-sm-2"  id="typeSearch" name="typeSearch">
          <option value="0">ค้นหาทั้งหมด</option>
          {{-- <option value="1">รหัสพนักงาน</option> --}}
          <option value="2">แผนก</option>
          {{-- <option value="3">ตำแหน่ง</option> --}}
          <option value="4">สิทธิการใช้งาน</option>
          <option value="5">สถานะการใช้งาน</option>
        </select>
      </div>
      <div class="form-group" id="groupTxt" >
        <div class="input-group my-2 my-sm-0">
          <input type="text" class="form-control" id="txtSearch" name="txtSearch" placeholder="ค้นหาทั้งหมด" disabled>
          <span class="input-group-btn">
            <button class="btn btn-secondary btnSearch_emp" type="button"><span class="fa fa-search"></span></button>
          </span>
        </div>
      </div>
      <div class="form-group" id="groupemp" style="display:none" >
        <div class="input-group my-2 my-sm-0">
        <select class="form-control"  id="emp_oc" name="emp_oc">
          <?php
            $sqltype = DB::table('tb_department')->where('com_id','=',$com)->get();
            foreach ($sqltype as $oc):
              echo "<option value='$oc->dep_id'> ".$oc->dep_name."</option>";
            endforeach;
          ?>
        </select>
          <span class="input-group-btn">
            <button class="btn btn-secondary btnSearch_emp" type="button"><span class="fa fa-search"></span></button>
          </span>
        </div>
      </div>

      <div class="form-group" id="groupjob" style="display:none" >
        <div class="input-group my-2 my-sm-0">
        <select class="form-control"  id="emp_job" name="emp_job">
          <?php
            $sql = DB::table('tb_job')->get();
            foreach ($sql as $job):
              echo "<option value='$job->job_id'> ".$job->job_name."</option>";
            endforeach;
           ?>
        </select>
          <span class="input-group-btn">
            <button class="btn btn-secondary btnSearch_emp" type="button"><span class="fa fa-search"></span></button>
          </span>
        </div>
      </div>

      <div class="form-group" id="grouplv" style="display:none" >
        <div class="input-group my-2 my-sm-0">
        <select class="form-control"  id="emp_lv" name="emp_lv">
          <?php
            $sql = DB::table('tb_level')->where('lv_id','<>','999')->get();
            foreach ($sql as $lv):
              echo "<option value='$lv->lv_id'> ".$lv->lv_name."</option>";
            endforeach;
           ?>
        </select>
          <span class="input-group-btn">
            <button class="btn btn-secondary btnSearch_emp" type="button"><span class="fa fa-search"></span></button>
          </span>
        </div>
      </div>

      <div class="form-group" id="groupstatus" style="display:none" >
        <div class="input-group my-2 my-sm-0">
        <select class="form-control"  id="emp_status" name="emp_status">
            <option value='0'>ใช้งาน</option>
            <option value='1'>ยกเลิกการใช้งาน</option>
           ?>
        </select>
          <span class="input-group-btn">
            <button class="btn btn-secondary btnSearch_emp" type="button"><span class="fa fa-search"></span></button>
          </span>
        </div>
      </div>

    </form>
    <table class="table table-bordered">
      <thead>
        <th colspan="2"></th>
      </thead>
        <tbody id="tbody_emp">
        </tbody>
    </table>
  </div>
  </nav>
  <div id="detail_emp">

  </div>

  </div>


    </div>
<div id="modal-area"></div>
</div>
<script>
  $(document).ready(function(){
    var data = "com_id="+$("#com_id").val()+"&lv_user="+$("#lv_user").val()+"&typeSearch=0";
    callTbl_emp(data);

  });

  $("#typeSearch").change(function(){
    var typeVal = $(this).val();
    if(typeVal == 1){
      $("#txtSearch").attr("placeholder","รหัสพนักงาน");
      $("#groupemp").hide();
      $("#grouplv").hide();
      $("#groupjob").hide();
      $("#groupTxt").show();
      $("#groupstatus").hide();
     delDisabled("txtSearch");
    }else if(typeVal == 2){
      $("#groupemp").show();
      $("#groupjob").hide();
      $("#groupTxt").hide();
      $("#grouplv").hide();
      $("#groupstatus").hide();
      delDisabled("txtSearch");
    }else if(typeVal == 3){
      $("#groupemp").hide();
      $("#groupjob").show();
      $("#groupTxt").hide();
      $("#grouplv").hide();
      $("#groupstatus").hide();
      delDisabled("txtSearch");
    }else if(typeVal == 4){
      $("#groupemp").hide();
      $("#groupjob").hide();
      $("#groupTxt").hide();
      $("#grouplv").show();
      $("#groupstatus").hide();
    }else if(typeVal == 5){
      $("#groupemp").hide();
      $("#groupjob").hide();
      $("#groupTxt").hide();
      $("#grouplv").hide();
      $("#groupstatus").show();
    }else{
      $("#groupemp").hide();
      $("#groupjob").hide();
      $("#grouplv").hide();
      $("#groupstatus").hide();
      $("#groupTxt").show();
      $("#txtSearch").attr("placeholder","ค้นหาทั้งหมด");
      $("#txtSearch").attr("disabled","disabled");
    }
  });

  function delDisabled(id){
    $("#"+id).removeAttr("disabled");
  }

  $('#txtSearch').keypress(function(event){
        if(event.keyCode == '13'){
          var data = $("#frmSearch_emp").serialize();
          callTbl_emp(data);
          event.preventDefault();
        }
  });


  $(".btnSearch_emp").click(function(){
    var data = $("#frmSearch_emp").serialize()+"&com_id="+$("#com_id").val()+"&lv_user="+$("#lv_user").val();
    callTbl_emp(data);
  });

  function callTbl_emp(dataSearch){
    $.ajax({url:"/tableemp",
            data:dataSearch,
            type:"GET",
            success:function(data){
              $("#tbody_emp").html(data);
              $(".empDetail").click(function(){
                                    var id = $(this).data("id");
                                    $.ajax({ url:"/detailemp",
                                                data:"id="+id,
                                                type:"GET",
                                                success:function(data){
                                                  $("#modal-area").html(data);
                                                  $("#modalemp").modal("show");
                                                }
                                              });
                                        });                }
              });
            }


</script>
@endsection
