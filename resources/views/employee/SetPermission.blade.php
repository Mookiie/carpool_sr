@extends('welcome')
@extends('dashboard.topNavbar')
@extends('dashboard.SideNavbar')

@section('content')

@foreach ($users as $r)
  <?php
        $lv_user = $r->emp_level;
        $id = $r->emp_id;
        $com = $r->com_id;
   ?>
@endforeach
<style media="screen">
.sorting_asc,.sorting_desc,.sorting,td{
  text-align: center
}

</style>
<div class="container-dashboard">

  <div class="card col-md-12 row">
  <input type="hidden"  id="emp_id" value="{{$id}}">
  <input type="hidden" id="com_id" value="{{$com}}">
  <input type="hidden" id="lv_user" value="{{$lv_user}}">
    <div class="card-header row">
      <span class="fa fa-address-book mr-auto" style="color:#000;" > กำหนดสิทธิการใช้งาน</span>

        <?php if ($lv_user =="99"||$lv_user =="999") {?>
          <div>
            <button type="button" class='btn btn-success btn-sm' id="uploadEmp"><span class="fa fa-refresh"> อัพเดทข้อมูล</span></button>
          </div>
          &nbsp;&nbsp;&nbsp;
        <?php  } ?>
    </div>
      <div style="padding:5% 3% 1% 3%;">
        <div class="container">
            <table id="employee" cellspacing="0" width="100%" align='center'>
                      <thead>
                        <tr>
                            <th>ชื่อ-สกุล</th>
                            <th>แผนก</th>
                            <th>สิทธิการใช้งาน</th>
                            <th>กำหนดสิทธิ์</th>
                            <th>ดูข้อมูล</th>
                            <th>รีเซ็ตรหัสผ่าน</th>
                        </tr>
                      </thead>

                      <tbody>
                        <?php
                    $sqlemp = DB::table('tb_employee')
                                ->join('tb_company', 'tb_employee.com_id', '=' , 'tb_company.com_id' )
                                ->join('tb_employee_login', 'tb_employee.emp_id', '=' , 'tb_employee_login.emp_id' )
                                ->join("tb_department",function($join){
                                      $join->on("tb_employee.dep_id","=","tb_department.dep_id")
                                           ->on("tb_employee.com_id","=","tb_department.com_id");
                                  })
                                ->join("tb_job",function($join){
                                        $join->on("tb_employee.job_id","=","tb_job.job_id")
                                             ->on("tb_employee.com_id","=","tb_job.com_id")
                                             ->on("tb_employee.dep_id",'=',"tb_job.dep_id");
                                    })
                                ->join('tb_level', 'tb_level.lv_id', '=' , 'tb_employee_login.emp_level' )
                                ->where('tb_employee_login.emp_id','<>','SYS000000000')
                                ->where('tb_employee.com_id','=',$com)
                                ->orderBy('tb_employee_login.emp_id', 'ASC')->get();

                      foreach ($sqlemp as $emp) {
                         ?>
                         <tr>
                            <td><h6><b>{{$emp->emp_fname." ".$emp->emp_lname}}</b></h6></td>
                            <td>{{$emp->dep_name}}</td>
                            <td>{{$emp->lv_name}}</td>
                            <td><i class="fa fa-pencil-square-o permistion" data-id="{{$emp->emp_id}}"></i></td>
                            <td><i class="fa fa-eye detailemp" data-id="{{$emp->emp_id}}"></i></td>
                            <td><i class="fa fa-refresh passemp" data-id="{{$emp->emp_id}}"></i></td>
                        </tr>
                <?php } ?>
                     </tbody>
            </table>
          </div>
      </div>
      </div>
    </div>
    <div id="modal-area"></div>

<script type="text/javascript">

  $(document).ready(function () {
    $('#employee').DataTable({
      'bSort': true,
       'aoColumn': [
             { sWidth: "50%", bSearchable: true, bSortable: true },
             { sWidth: "20%", bSearchable: false, bSortable: false },
             { sWidth: "15%", bSearchable: false, bSortable: false },
             { sWidth: "5%", bSearchable: false, bSortable: false },
             { sWidth: "5%", bSearchable: false, bSortable: false },
             { sWidth: "5%", bSearchable: false, bSortable: false }

             //match the number of columns here for table2
           ],

        "oLanguage": {
                "sInfo": "_PAGE_ จาก _PAGES_ หน้า",
                "sInfoEmpty": "หน้า 0 จาก 0",
                "sEmptyTable": "ไม่พบข้อมูลที่ค้นหา",
                "sSearch": "ค้นหา",
                "sLengthMenu": "จำนวนแสดง _MENU_ รายการ",
                "sZeroRecords": "ไม่พบข้อมูลที่ค้นหา",
                "oPaginate": {
                    "sPrevious": "ก่อนหน้า",
                    "sNext": "ถัดไป",
                },
                // dom: 'T<"clear">lfrtip',
                // tableTools: {
                //     "sRowSelect": "single"
                // }
              }
    });
  })

  $('#uploadEmp').click(function () {
      var id ="emp_id="+$("#emp_id").val()+"&com_id="+$("#com_id").val();
      $.ajax({
               url:"/modalEmpUp",
               data:id,
               type:"GET",
               success:function(data){
               $("#modal-area").html(data);
               $("#modalBk").modal("show");
             }
         });
  })


  $('.permistion').click(function () {

    if ($(this).data('id') == 'SR600000000') {
      swal({
                title: "ผิดพลาด",
                text: 'ไม่สามารถแก้ไขสิทธิการใช้งานได้',
                type: "error",
                showCancelButton: false,
                confirmButtonColor: "#2ECC71",
                confirmButtonText: "ตกลง",
                closeOnConfirm: true,
          });
    }
    else {
      $.ajax(
      {url:"/modalsPermission",
      type:"GET",
      data:"id="+$(this).data('id'),
      contentType: false,
      processData: false,
      success:function(data){
        // console.log(data);
        $("#modal-area").html(data);
        $("#modalemp").modal("show");
        }
      })
      }

    })

  $('.detailemp').click(function () {
    // console.log($(this).data('id'));
    $.ajax(
    {url:"/detailemp",
    type:"GET",
    data:"id="+$(this).data('id'),
    contentType: false,
    processData: false,
    success:function(data){
      // console.log(data);
      $("#modal-area").html(data);
      $("#modalemp").modal("show");

      }
    })
  })
  $('.passemp').click(function () {
    $.ajax(
    {url:"/resetModals",
    type:"GET",
    data:"id="+$(this).data('id'),
    contentType: false,
    processData: false,
    success:function(data){
      // console.log(data);
      $("#modal-area").html(data);
      $("#modalBk").modal("show");

      }
    })
  })


</script>
@endsection
