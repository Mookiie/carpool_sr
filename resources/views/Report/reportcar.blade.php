@extends('welcome')
@section('content')
@include('dashboard.topNavbar')
@include('dashboard.SideNavbar')

@foreach ($users as $r)
  <?php
        $lv_user = $r->emp_level;
        $id = $r->emp_id;
        $com_id = $r->com_id;
        $job_id = $r->job_id;
   ?>
@endforeach
<?php $dep_car = base64_decode(base64_decode(base64_decode($depcar))) ?>
<link rel="stylesheet" type="text/css" href="{{ asset('js/datetimepicker/jquery.datetimepicker.css')}}"/>
<script src="{{asset('js/datetimepicker/jquery.js')}}"></script>
<script src="{{asset('js/datetimepicker/build/jquery.datetimepicker.full.min.js')}}"></script>
<meta name="viewport" content="initial-scale=1.0, user-scalable=no">

<div class="container-dashboard">

<div class="card col-md-12 row">

  <div class="card-header">
    <div class="row">
    <span class="fa fa-address-book-o text-black mr-auto" style="color:#000;"> รายงานการใช้รถ</span>
  </div>
  </div>

  {{-- <div class="table-resposive" id="table_report"> --}}
  <div class="card" style="margin-top:2%;margin-bottom:2%">
    <div class="card-header">

      <span class="fa fa-search text-black mr-auto" style="color:#000;"> ค้นหารายงาน</span>
      <button type="button" class='btn btn-outline-success btn-sm' id="btn_print">ดาวน์โหลด</button>
      <button class="close" id="detailClose">&times;</button>
    </div>
    <div class="card-block" id="divsearch">
    <form id="frmSearch_report" style="align:center;">
      <input type="hidden" name="_token" value="{{ csrf_token() }}">
      <input type="hidden"  id="com_id" value="{{$com_id}}">
      <input type="hidden"  id="depcar" value="{{$dep_car}}">

      <div class="form-group row" id="groupDate1">
          <label for="date" class ="col-md-3 col-form-label " align="right">วันที่</label>
          <input id="date" name="date" type="text" class="form-control col-md-8 bkDate" value="{{date("Y-m-d")}}" placeholder="{{date("Y-m-d")}}">
      </div>

      <div class="form-group form-group-row" align="center">
        <button class="btn btn-secondary btnSearch_report" type="button"><span class="fa fa-search"> ค้นหา</span></button>
      </div>

    </form>
  </div>
  {{-- </div> --}}


  <div id="tbody_report" class="container" style="padding:2%">
  </div>
  </div>

  <div class="modal-area"></div>
</div>
</div>
<script src="{{asset ("js/html2canvas.js")}}" type="text/javascript"></script>
<script src="{{asset ("js/canvas2image.js")}}" type="text/javascript"></script>
<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
      });

$(document).ready(function() {
  $('#detailClose').hide();
  $("#btn_print").hide();
  $("#tbody_report").hide();
})

$('.btnSearch_report').click(function() {
  var Sdata = $('#frmSearch_report').serialize();
  var depcar = $('#depcar').val();
  var com_id = $('#com_id').val();
  $("#divsearch").hide();
  $("#btn_print").show();
  $('#detailClose').show();
  // console.log(Sdata+'&dep_car='+depcar+'&com_id='+com_id);
  $.ajax({ type:"POST",
           data :Sdata+'&dep_car='+depcar+'&com_id='+com_id,
           url:"/searchcar",
           success:function(data){
             $("#tbody_report").show();
             $("#tbody_report").html(data);
           }
         })
})

$("#btn_print").click(function () {
  var Sdata = $('#frmSearch_report').serialize();
  var depcar = $('#depcar').val();
  var com_id = $('#com_id').val();
  window.location = "/reportcarpdf?"+Sdata+'&dep_car='+depcar+'&com_id='+com_id;
})


$('#detailClose').click(function() {
  // window.location = '/reportbooking';
  $("#divsearch").show();
  $("#btn_print").hide();
  $('#detailClose').hide();
  $("#tbody_report").hide();
})

jQuery(".bkDate").datetimepicker({
        format:"Y-m-d",
        lang:"th",
        // minDate:'-1969/12/30',
        // minDate:'-1970/01/01',
        timepicker:false,
        scrollInput:false
});

</script>
@endsection
