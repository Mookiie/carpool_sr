@extends('welcome')
@section('content')
@include('dashboard.topNavbar')
@include('dashboard.SideNavbar')

@foreach ($users as $r)
  <?php
        $lv_user = $r->emp_level;
        $id = $r->emp_id;
        $com_id = $r->com_id;
        $dep_id = $r->job_id;
   ?>
@endforeach
<link rel="stylesheet" type="text/css" href="{{ asset('js/datetimepicker/jquery.datetimepicker.css')}}"/>
<script src="{{asset('js/datetimepicker/jquery.js')}}"></script>
<script src="{{asset('js/datetimepicker/build/jquery.datetimepicker.full.min.js')}}"></script>
<meta name="viewport" content="initial-scale=1.0, user-scalable=no">

<div class="container-dashboard">

<div class="card col-md-12 row">

  <div class="card-header">
    <div class="row">
    <span class="fa fa-address-book-o text-black mr-auto" style="color:#000;"> รายงานการจองรถ</span>
  </div>
  </div>

  {{-- <div class="table-resposive" id="table_report"> --}}
  <div class="card" style="margin-top:2%;margin-bottom:2%">
    <div class="card-header">

      <span class="fa fa-search text-black mr-auto" style="color:#000;"> ค้นหารายงาน</span>
      <button class="close" id="detailClose">&times;</button>
    </div>
    <div class="card-block" id="divsearch">
    <form id="frmSearch_report" style="align:center;">
      <input type="hidden" name="_token" value="{{ csrf_token() }}">
      <input type="hidden"  id="emp_id" value="{{$id}}">
      <input type="hidden"  id="dep_id" value="{{$dep_id}}">
      <input type="hidden"  id="com_id" value="{{$com_id}}">
      <input type="hidden"  id="lv_user" value="{{$lv_user}}">

      <div class="form-group row">
        <label for="type" class ="col-md-3 col-form-label " align="right">ประเภทการค้นหา</label>
        <select class="form-control col-md-8"  id="type" name="type">
          <option value="0" selected >ค้นหาทั้งหมด</option>
          <option value="1">ค้นหารายวัน</option>
          <option value="2">ค้นหารายเดือน</option>
          <option value="3">ค้นหารายปี</option>
          <option value="4">ค้นหาตามช่วงวันที่</option>
         </select>
      </div>

      <div class="form-group row">
        <label for="typeSearch" class ="col-md-3 col-form-label " align="right">รายละเอียดการค้นหา</label>
        <select class="form-control col-md-8"  id="typeSearch" name="typeSearch">
          <option value="0" selected >ค้นหาทั้งหมด</option>
          <option value="1">สถานะ</option>
          <option value="2">รายชื่อผู้ขอ</option>
         </select>
      </div>

      <div class="form-group row" id="groupstatus" >
        <label for="bk_dep" class ="col-md-3 col-form-label " align="right">แผนก</label>
        <?php if ($lv_user == 99||$lv_user == 999) { ?>
        <select class="form-control col-md-8"  id="bk_dep" name="bk_dep">
            <option value='All' selected >ทั้งหมด</option>
        <?php } else { ?>
          <select class="form-control col-md-8"  id="bk_dep" name="bk_dep">
            <option value='All' selected >ทั้งหมด</option>
        <?php } ?>
            <?php
              $sql_dep = DB::table('tb_job')->where('com_id','=',$com_id)->select(DB::raw("job_id,job_name"))->groupBy("job_id","job_name")->get();
              foreach ($sql_dep as $dep) {

                  if ($dep->job_id == $dep_id) {
                ?>
                <option value='{{$dep->job_id}}' selected>{{$dep->job_name}}</option>
                <?php
                  }
                  else {
                ?>
                    <option value='{{$dep->job_id}}'>{{$dep->job_name}}</option>
                <?php
                  }
                }
                ?>
        </select>
      </div>

      <div class="form-group row" id="groupTxt" >
        <label for="txtSearch" class ="col-md-3 col-form-label " align="right">ค้นหาจากชื่อ</label>
          <select class="form-control col-md-8"  id="txtSearch" name="txtSearch" disabled='disabled'>

          </select>
      </div>
      {{-- <div class="form-group row" id="groupTxt" >
        <label for="txtSearch" class ="col-md-3 col-form-label " align="right">ค้นหาจากชื่อ</label>
        <div class="input-group col-md-8" style="padding:0;">
          <input type="text" class="form-control" id="txtSearch" name="txtSearch" placeholder="คำค้นหา" disabled='disabled'>
        </div>
      </div> --}}


      <div class="form-group row" id="groupstatus" >
        <label for="bk_status" class ="col-md-3 col-form-label " align="right">ประเภทสถานะ</label>
        <select class="form-control col-md-8"  id="bk_status" name="bk_status" disabled='disabled'>
            <option value='All' selected >สถานะทั้งหมด</option>
            <option value='success'>การจองสำเร็จ</option>
            <option value='eject'>การจองยกเลิก</option>
            <option value='ejectcar'>ยกเลิกการใช้งาน</option>
            <option value='nonecar'>การจองที่ไม่มีรถ</option>
            <option value='approve'>การจองรออนุมัติ</option>
            <option value='wait'>การจองรอจัดรถ</option>
        </select>
      </div>

      <div class="form-group row" id="groupDate1" style="display:none;">
          <label for="date" class ="col-md-3 col-form-label " align="right">วันที่</label>
          <input id="date1" name="date1" type="text" class="form-control col-md-8 bkDate" value="{{date("Y-m-d")}}">
      </div>
      <div class="form-group row" id="groupDate2" style="display:none;">
          <label for="date" class ="col-md-3 col-form-label " align="right">ถึง</label>
          <input id="date2" name="date2" type="text" class="form-control col-md-8 bkDate" value="{{date("Y-m-d")}}">
      </div>

      <div class="form-group row" id="groupDate3" style="display:none;">

          <label for="date" class ="col-md-3 col-form-label " align="right">ปี</label>
            <select class="form-control col-md-1"  id="year1" name="year1">
              <?php
                     $sql_y = DB::table('tb_booking')->select(DB::raw("YEAR(bk_date) as years"))->where('com_id','=',$com_id)->groupBy(DB::raw("YEAR(bk_date)"))->orderBy(DB::raw("YEAR(bk_date)"),'desc')->get();
                    foreach ($sql_y as $y) {
                      $year = $y->years;
                      echo "<option value='".$year."'>".$year."</option>";
                    }
               ?>
            </select>

          <label for="date" class =" col-form-label " align="center"> &nbsp;&nbsp;&nbsp;เดือน&nbsp; </label>
            <select class="form-control col-md-1"  id="month" name="month">
              <?php
                $month = array("มกราคม ","กุมภาพันธ์ ","มีนาคม ","เมษายน ","พฤษภาคม ","มิถุนายน ","กรกฎาคม ","สิงหาคม ","กันยายน ","ตุลาคม ","พฤศจิกายน ","ธันวาคม ");
                for ($i=0; $i < sizeof($month) ; $i++) {
                  echo "<option value='".($i+1)."'>".$month[$i]."</option>";
                }
               ?>
            </select>

      </div>

      <div class="form-group row" id="groupDate4" style="display:none;">
          <label for="date" class ="col-md-3 col-form-label " align="right">ปี</label>
          <select class="form-control col-md-1"  id="year2" name="year2">
            <?php
                   $sql_y = DB::table('tb_booking')->select(DB::raw("YEAR(bk_date) as years"))->where('com_id','=',$com_id)->groupBy(DB::raw("YEAR(bk_date)"))->orderBy(DB::raw("YEAR(bk_date)"),'desc')->get();
                  foreach ($sql_y as $y) {
                    $year = $y->years;
                    echo "<option value='".$year."'>".$year."</option>";
                  }
             ?>
          </select>
      </div>

      <div class="form-group form-group-row" align="center">
        <button class="btn btn-secondary btnSearch_report" type="button"><span class="fa fa-search"> ค้นหา</span></button>
      </div>

    </form>
  </div>
  {{-- </div> --}}


  {{--  --}}
  <div id="tbody_report"></div>
  </div>

  <div class="modal-area"></div>
</div>
</div>
<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
      });

$(document).ready(function() {
  $('#detailClose').hide();

  $.ajax({type:"POST",
           data :'dep_id='+$('#bk_dep').val()+'&com_id='+$('#com_id').val(),
           url:"/semp",
           success:function(data){
             $("#txtSearch").html(data);
           }
        })
})

$('#type').change(function () {
  var type = $(this).val();

  if (type == 0) {
    $("#groupDate1").hide();
    $("#groupDate2").hide();
    $("#groupDate3").hide();
    $("#groupDate4").hide();
  }
  else if (type == 1) {
    $("#groupDate1").show();
    $("#groupDate2").hide();
    $("#groupDate3").hide();
    $("#groupDate4").hide();
  }
  else if (type == 2) {
    $("#groupDate1").hide();
    $("#groupDate2").hide();
    $("#groupDate3").show();
    $("#groupDate4").hide();
  }
  else if (type == 3) {
    $("#groupDate1").hide();
    $("#groupDate2").hide();
    $("#groupDate3").hide();
    $("#groupDate4").show();
  }
  else {
    $("#groupDate1").show();
    $("#groupDate2").show();
    $("#groupDate3").hide();
    $("#groupDate4").hide();
  }
})

$('#typeSearch').change(function() {
  var type = $(this).val();
  if (type == 0) {
    $("#txtSearch").attr("disabled","disabled");
    $("#bk_status").attr("disabled","disabled");
  }
  else if (type == 1) {
    $("#txtSearch").attr("disabled","disabled");
    $("#bk_status").removeAttr("disabled","disabled");
  }
  else if (type == 2) {
    $("#txtSearch").removeAttr("disabled","disabled");
    $("#bk_status").removeAttr("disabled","disabled");
  }
  else {

  }
})
$("#bk_dep").change(function() {
  $.ajax({type:"POST",
           data :'dep_id='+$('#bk_dep').val()+'&com_id='+$('#com_id').val(),
           url:"/semp",
           success:function(data){
             $("#txtSearch").html(data);
           }
        })
})

$('.btnSearch_report').click(function() {
  var Sdata = $('#frmSearch_report').serialize();
  var com_id = $('#com_id').val();
  var bk_status = $('#bk_status').val();
  var txtSearch = $('#txtSearch').val();
  $("#divsearch").hide();
  $('#detailClose').show();
  // console.log(Sdata+'&com_id='+com_id+'&txtSearch='+txtSearch);
  $.ajax({ type:"POST",
           data :Sdata+'&com_id='+com_id+'&txtSearch='+txtSearch,
           url:"/searchbooking",
           success:function(data){
             $("#tbody_report").show();
             $("#tbody_report").html(data);
             $(".detailBk").click(function(){
              var id = $(this).data("id");
              // console.log(id);
              // callDeatil("bk="+id)
              $.ajax({
                url:"/detail",data:"bk="+id,type:"GET",success:function(data){
                    $(".modal-area").html(data);
                    $("#modalBk").modal("show");
                }
              });
              });
           }
         })
})


function callDeatil(detail){

}


$('#detailClose').click(function() {
  // window.location = '/reportbooking';
  $("#divsearch").show();
  $('#detailClose').hide();
  $("#tbody_report").hide();
})

jQuery(".bkDate").datetimepicker({
        format:"Y-m-d",
        lang:"th",
        // minDate:'-1969/12/30',
        // minDate:'-1970/01/01',
        timepicker:false,
        scrollInput:false
});

jQuery(".bkMonth").datetimepicker({
         format: 'Y-m',
         lang:"th",
          // minDate:'-1969/12/30',
          // minDate:'-1970/01/01',
         timepicker:false,
         scrollInput:false,

});

jQuery(".bkYear").datetimepicker({
        format:"Y",
        lang:"th",
        // minDate:'-1969/12/30',
        // minDate:'-1970/01/01',
        timepicker:false,
        scrollInput:false
});



</script>
@endsection
