<!DOCTYPE html>
<html>
<?php
      $mail = session()->get('imail');
      $com_id = session()->get('com_id');
      $emp_level = session()->get('emp_level');

      $sql = DB::table("tb_company")->where("com_id","=",$com_id)->get();
      foreach ($sql as $com) {
        $com_name = $com->com_name;
      }
      $text = $mail."&".$com_id."&".$emp_level;
      $url = "http://172.16.0.20:98/inviteregister?".base64_encode($text);
      // $url = "http://127.0.0.1:8000/inviteregister?".base64_encode($text);

 ?>
  <head>
    <meta charset="utf-8">
    <title>Carpool Siamraj Service</title>
  </head>
  <body>
    <h4></h4>
    <div>
    <div style=' font-family:Verdana, Geneva, sans-serif;
              padding:10px; border:solid 3px #FFA835 ;
              height:100%;' align='center'>
              <div align="center" style="padding: 0.5% 5% 0.5% 5%; color:#fff;
                          background:#FFBF3C; ">
              {{-- <img src="../image/logo.png" width="70"> --}}
              <p style="margin:0px;">
                <h3>Carpool Siamraj Service</h3>
              </p>
              </div>
        <div align='left' style='padding: 2% 20% 2% 20%;'>
            <p><b>คุณได้รับคำเชิญจาก{{$com_name}} ,</b></p>
            <p><dd>
                คุณได้รับคำเชิญเข้าใช้บริการ Carpool Siamraj Service กดปุ่มด้านล่างเพื่อลงทะเบียนเข้าใช้งาน
            </dd></p>
            <div align="center">
              <div style='padding-top:0px;'>
                <div style='padding-top:20px;'>
                  <a align="center" href="{{$url}}"
                    style='text-decoration:none;
                    background:#2196f3;
                    color:#fff;
                    border:none;
                    transition: .5s;
                    cursor:pointer;
                    border-bottom: solid 2px #0d47a1;
                    padding:15px 15px;border-radius:10px;width:100%;'>
                    สมัครใช้งาน
                  </a>
                </div>
                </a>
              </div>
            </div>
        </div>

        <div align='center' style='padding: 2% 20% 1% 50%;'>
          <p>ขอบคุณที่ใช้บริการ</p>
          <p>ขอแสดงความนับถือ,<br/>Carpool Siamraj Service</p>
        </div>
        <div align='center'>
        <p><small>หากท่านมีข้อสงสัยหรือต้องการสอบถามรายละเอียดเพิ่มเติม กรุณาติดต่อที่ Contact Center 5303</small></p>
        </div>
      </div>
    </div>
  </body>
</html>
