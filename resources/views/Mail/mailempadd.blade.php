<!DOCTYPE html>
<?php
 $id = session()->get('new_id');
// $id ='SYS600010004';
$pass = session()->get('new_pass');
$sql = DB::table('tb_employee')->where('emp_id','=',$id)->get();
foreach ($sql as $emp) {
  $name = $emp->emp_fname." ".$emp->emp_lname;
  $email = $emp->emp_email;
}

?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Carpool Siamraj Service</title>
    <link rel="icon" href="../image/logo_siamraj.png" type="image/gif" sizes="16x16">
  </head>
  <body>
    <h4></h4>
    <div>
    <div style=' font-family:Verdana, Geneva, sans-serif;
              padding:10px; border:solid 3px #FFA835 ;
              height:100%;' align='center'>
              <div align="center" style="padding: 0.5% 5% 0.5% 5%; color:#fff;
                          background:#FFBF3C; ">
              <p style="margin:0px;">
                <h3>Carpool Siamraj Service</h3>
              </p>
              </div>
        <div align='left' style='padding: 2% 20% 2% 20%;'>
            <p><b>เรียนคุณ {{$name}} ,</b></p>
              <p>Carpool Siamraj Service ได้ลงทะเบียนผู้ใช้งานเรียบร้อยแล้ว กรุณากดปุ่มด้านล่างเพื่อทำการเข้าสู่ระบบและเปลี่ยนแปลงรหัสผ่าน โดยใช้ </p>

                <div align="center" style="border:solid 2px #0d47a1 ;">
                  <p>
                    Username : {{$email}}
                  </p>
                  <p>
                    Password : {{$pass}}
                  </p>
                </div>

        </div>
            <div style='padding-top:0px;'>
              <div style='padding-top:20px;'>
                <a align="center" href="http://172.16.0.20:98/"
                {{-- http://127.0.0.1:8000/ --}}
                  style='text-decoration:none;
                  background:#2196f3;
                  color:#fff;
                  border:none;
                  transition: .5s;
                  cursor:pointer;
                  border-bottom: solid 2px #0d47a1;
                  padding:15px 15px;border-radius:10px;width:100%;'>
                  เข้าสู่ระบบ
                </a>
              </div>
              </a>
            </div>
        <div align='center' style='padding: 2% 20% 1% 50%;'>
          <p>ขอบคุณ<br>Carpool Siamraj Service</p>
        </div>
        <div align='center'>
        <p><small>หากท่านมีข้อสงสัยหรือต้องการสอบถามรายละเอียดเพิ่มเติม กรุณาติดต่อที่ ADMIN Tel.5305</small></p>
        </div>
      </div>
    </div>
  </body>
</html>
