<!DOCTYPE html>
<?php
$id = session()->get('bk_id');
// $id='BK1801150001';
$sql = DB::table('tb_booking')->join('tb_employee','tb_booking.emp_id','=','tb_employee.emp_id')
        ->join("tb_department",function($join){
               $join->on('tb_department.dep_id', '=', 'tb_booking.dep_id')
                    ->on("tb_department.com_id","=","tb_booking.com_id");
          })
        ->where('bk_id','=',$id)->get();
foreach ($sql as $bk) {
  $name = $bk->emp_fname." ".$bk->emp_lname;
  $com_id = $bk->com_id;
  $bk_percon = $bk->bk_percon;
  $obj = $bk->bk_obj;
  $note = $bk->bk_note;
  $mtel = $bk->bk_mtel;
  $bk_start_start = $bk->bk_start_start;
  $bk_end_start = $bk->bk_end_start;
  $status = $bk->bk_status;
  $approve_by = $bk->approve_by;
  $approve_date = $bk->approve_date;
  $setcar_by = $bk->setcar_by;
  $setcar_date = $bk->setcar_date;
  $car_id = $bk->car_id;
  $driver_id = $bk->drive_id;
  $bkuse = $bk->bk_use;
}
$appprove = DB::table('tb_employee')->where('emp_id','=',$approve_by)->get();
foreach ($appprove as $ap) {
  $approve_name = $ap->emp_fname." ".$ap->emp_lname;
}
$setcar = DB::table('tb_employee')->where('emp_id','=',$setcar_by)->get();
foreach ($setcar as $sc) {
  $setcar_name = $sc->emp_fname." ".$sc->emp_lname;
}
$sqlcar=DB::table('tb_car')->where('car_id','=',$car_id)->where("com_id","=",$com_id)->get();
foreach ($sqlcar as $car) {
  $car_number = $car->car_number;
  $car_model = $car->car_model;
}

  if($driver_id != 'D0000000000') {
  $sqldriver=DB::table('tb_driver')->where('drive_id','=',$driver_id)->where("com_id","=",$com_id)->get();
  foreach ($sqldriver as $driver) {
    $name_d = $driver->drive_fname." ".$driver->drive_lname;
    $tel = $driver->drive_tel;
  }
}else {
  $name_d = "ไม่ต้องการคนขับ";
  $tel = "-";
}
$arr = array();
if($status == "wait"){
    array_push($arr,"รอการอนุมัติ","warning","#F39C12");
  }else if($status == "approve"){
    array_push($arr,"รอการจัดรถ","info","#3498DB");
  }else if($status == "success"){
    array_push($arr,"สำเร็จ","success","#2ECC71");
  }else if($status == "merge"){
    array_push($arr,"สำเร็จ(ร่วมเดินทาง)","success","#2ECC71");
  }else if($status == "eject"){
    array_push($arr,"ยกเลิกการจอง","danger","#E74C3C");
  }else if($status == "ejectcar"){
    array_push($arr,"ยกเลิกการเดินทาง","danger","#E74C3C");
  }else if($status == "nonecar"){
    array_push($arr,"ไม่มีรถ","danger","#E74C3C");
  }

if(!function_exists('datetimefull')){
function datetimefull($datetime)
{
  $y = substr($datetime,0,4);
  $m = substr($datetime,5,2);
  $d = substr($datetime,8,2);
  $h = substr($datetime,11,2);
  $i = substr($datetime,14,2);
  return $d."/".$m."/".$y." เวลา ".$h.":".$i." น." ;
}}
if(!function_exists('datetimesome')){
function datetimesome($datetime1,$datetime2)
{
  $y1 = substr($datetime1,0,4);
  $m1 = substr($datetime1,5,2);
  $d1 = substr($datetime1,8,2);
  $h1 = substr($datetime1,11,2);
  $i1 = substr($datetime1,14,2);
  $y2 = substr($datetime2,0,4);
  $m2 = substr($datetime2,5,2);
  $d2 = substr($datetime2,8,2);
  $h2 = substr($datetime2,11,2);
  $i2 = substr($datetime2,14,2);
  return $d1."/".$m1."/".$y1." ถึง ".$d2."/".$m2."/".$y2." ช่วงเวลา ".$h1.":".$i1."น. ถึง ".$h2.":".$i2."น." ;
}}

$qr = 'http://chart.apis.google.com/chart?chs=300x300&cht=qr&chl='.$id.'&choe=UTF-8';
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Carpool Siamraj Service</title>
    <link rel="icon" href="../image/logo_siamraj.png" type="image/gif" sizes="16x16">
  </head>
  <body>
    <h4></h4>
    <div>
    <div style=' font-family:Verdana, Geneva, sans-serif;
              padding:10px; border:solid 3px #FFA835 ;
              height:100%;' align='center'>
              <div align="center" style="padding: 0.5% 5% 0.5% 5%; color:#fff;
                          background:#FFBF3C; ">
              {{-- <img src="../image/logo.png" width="70"> --}}
              <p style="margin:0px;">
                <h3>Carpool Siamraj Service</h3>
              </p>
              </div>
        <div align='left' style='padding: 2% 20% 2% 20%;'>
            <p><b>เรียนคุณ {{$name}} ,</b></p>
            <p>รายการของท่านได้รับการจัดรถเรียบร้อยแล้ว โดยมีรายละเอียด ดังนี้</p>
            <div align="center">
              <table class="table">
                <tbody>
                  <tr>
                    <td>เลขที่อ้างอิง</td>
                    <td>:</td>
                    <td>{{$id}}</td>
                  </tr>
                  <tr>
                    <td>สถานะ</td>
                    <td>:</td>
                    <td><button type="text" name="button" style="background:{{$arr[2]}}">{{$arr[0]}}</button></td>
                  </tr>
                  <tr>
                    <td>อนุมัติโดย</td>
                    <td>:</td>
                    <td>คุณ{{$approve_name}} เมื่อวันที่ {{datetimefull($approve_date)}}</td>
                  </tr>
                  <tr>
                    <td>จัดรถโดย</td>
                    <td>:</td>
                    <td>คุณ{{$setcar_name}} เมื่อวันที่ {{datetimefull($setcar_date)}}</td>
                  </tr>
                  <tr>
                    <td>วันและเวลาที่เดินทาง</td>
                    <td>:</td>
                    <td>
                      <?php if (!$bkuse) {
                          echo datetimefull($bk_start_start); echo " ถึง "; echo datetimefull($bk_end_start);
                      } else {
                        echo datetimesome($bk_start_start,$bk_end_start);
                      }?>

                    </td>
                  </tr>
                  <tr>
                    <td colspan="3">รายละเอียดการเดินทาง ดังนี้</td>
                  </tr>

                  <tr>
                    <td colspan="3">
                  <?php  $locate = DB::table('tb_booking_location')->where('bk_id','=',$id)->get();
                  foreach ($locate as $lo) {
                    $lo_id =$lo->location_id;
                      if ($lo->location_id == "1") {
                        echo "<dd>สถานที่เริ่มต้น : ".$lo->location_name."<br>";
                      }
                      else {
                        echo "สถานที่ ".($lo->location_id-1)." : ".$lo->location_name."<br>";
                      }
                    }
                   ?>
                   </td>
                   </tr>
                  <tr>
                    <td colspan="3">รายละเอียด ดังนี้</td>
                  </tr>
                  <tr>
                      <td colspan="2">
                      </td>
                    <td>
                       <table class="table">
                         <tbody>
                           <tr>
                             <td>คนขับ</td>
                             <td>:</td>
                             <td> {{$name_d}}<br>
                           </tr>
                           <tr>
                             <td>เบอร์โทร</td>
                             <td>:</td>
                             <td> {{$tel}}<br></td>
                           </tr>
                           <tr>
                             <td>รถ</td>
                             <td>:</td>
                             <td>หมายเลขทะเบียน {{$car_number}}<br>
                           </tr>
                           <tr>
                             <td></td>
                             <td></td>
                             <td>รุ่น {{$car_model}}<br>
                           </tr>
                         </tbody>
                       </table>
                     </td>
                   </tr>
                </tbody>
              </table>
            </div>


        </div>
            {{-- <div>
              <p>QR CODE สำหรับเช็คสถานะและใช้บริการ</p>
                <img style='text-decoration:none;
                background:#4b2886;
                color:#fff;
                border:none;
                transition: .5s;
                cursor:pointer;
                border-bottom: solid 2px #8E44AD;
                padding:20px 20px;border-radius:10px;width:15%;' src={{$qr}}/>
            </div> --}}

        <div align='center' style='padding: 2% 20% 1% 50%;'>
          <p>ขอบคุณที่ใช้บริการ</p>
          <p>ขอแสดงความนับถือ,<br/>Carpool Siamraj Service </p>
        </div>
        <div align='center'>
        <p><small>หากท่านมีข้อสงสัยหรือต้องการสอบถามรายละเอียดเพิ่มเติม กรุณาติดต่อที่ Contact Center 5303</small></p>
        </div>
      </div>
    </div>
  </body>
</html>
