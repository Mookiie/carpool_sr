<!DOCTYPE html>
<?php
 $token = session()->get('token');
$sql = DB::table('tb_employee_login')->where('token','=',$token)->get();
foreach ($sql as $id) {
  $emp = $id->emp_id;
}
$sqlname = DB::table('tb_employee')->where('emp_id','=',$emp)->get();
foreach ($sqlname as $emp) {
  $full = $emp->emp_fname." ".$emp->emp_lname;
}
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Carpool Siamraj Service</title>
    <link rel="icon" href="../image/logo_siamraj.png" type="image/gif" sizes="16x16">
  </head>
  <body>
    <h4></h4>
    <div>
    <div style=' font-family:Verdana, Geneva, sans-serif;
              padding:10px; border:solid 3px #FFA835 ;
              height:100%;' align='center'>
              <div align="center" style="padding: 0.5% 5% 0.5% 5%; color:#fff;
                          background:#FFBF3C; ">
              <p style="margin:0px;">
                <h3>Carpool Siamraj Service</h3>
              </p>
              </div>
        <div align='left' style='padding: 2% 20% 2% 20%;'>
            <p><b>เรียนคุณ {{$full}} ,</b></p>
            <p>ระบบ Carpool Siamraj Service ได้รับคำร้องขอการเปลี่ยนแปลงรหัสผ่านของระบบ
            หากคุณได้ทำการส่งคำร้องและต้องการเปลี่ยนแปลงรหัสผ่าน กรุณากดปุ่มด้านล่างเพื่อทำการยืนยันและเปลี่ยนแปลงรหัสผ่าน</p>
        </div>
            <div style='padding-top:20px;'>
              <a align="center" href="http://172.16.0.20:98/setpassword?id={{$token}}"
              {{-- http://127.0.0.1:8000/setpassword?id={{$token}} --}}
                style='text-decoration:none;
                background:#2196f3;
                color:#fff;
                border:none;
                transition: .5s;
                cursor:pointer;
                border-bottom: solid 2px #0d47a1;
                padding:15px 15px;border-radius:10px;width:100%;'>
                เปลี่ยนรหัสผ่าน
              </a>
            </div>
            <div align='left' style='padding: 2% 20% 0% 20%;'>
            <p>หากคุณไม่ได้เป็นผู้ร้องขอการเปลี่ยนแปลงรหัสผ่านของระบบ คุณไม่จำเป็นต้องดำเนินการใดๆ และไม่จำเป็นต้องสนใจอีเมลฉบับนี้</p>
            </div>
        <div align='center' style='padding: 0% 20% 1% 50%;'>
          <p>ขอบคุณ<br>Carpool Siamraj Service</p>
        </div>
        <div align='center'>
        <p><small>หากท่านมีข้อสงสัยหรือต้องการสอบถามรายละเอียดเพิ่มเติม กรุณาติดต่อที่ ADMIN Tel.5305</small></p>
        </div>
      </div>
    </div>
  </body>
</html>
