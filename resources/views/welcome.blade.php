
<!doctype html>
<?php
//header('X-Frame-Options: GOFORIT');
?>
<html>

    <head>
        <meta charset="utf-8">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- <link rel="stylesheet" href="{{ asset('css/scb_global_style.css') }}">
        <link rel="stylesheet" href="{{ asset('css/global_style.css') }}"> -->
        <link rel="stylesheet" href="{{ asset('css/login.css') }}">
        <link rel="stylesheet" href="{{ asset('css/global.css') }}">
        <link rel='stylesheet' href="{{ asset('/css/bootstrap.min.css') }}">
        <link rel='stylesheet' href="{{ asset('/tether/dist/css/tether.min.css') }}">
        <link rel="stylesheet" href="{{ asset('js/sweetalert/dist/sweetalert.css') }}">
        <link rel="stylesheet" href="{{ asset('js/fullcalendar/fullcalendar.min.css') }}">
        <link rel="stylesheet" href="{{ asset('fontawesome/css/font-awesome.min.css')}}">
        <link href="{{ asset ("DataTables/css/jquery.dataTables.min.css")}}" rel="stylesheet">


        <script src="{{ asset('/js/Jquery-3.2.1.min.js') }}"></script>
        <script src="{{ asset('/tether/dist/js/tether.min.js') }}"></script>
        <script src="{{ asset('/js/bootstrap.min.js') }}"></script>
        <script src="{{ asset('js/ui_function.js') }}"></script>
        <script src="{{ asset('js/moment/moment.js') }}"></script>
        <script src="{{ asset('js/fullcalendar/fullcalendar.min.js') }}"></script>
        <script src="{{ asset('js/fullcalendar/locale/th.js') }}"></script>
        <script src="{{ asset('js/sweetalert/dist/sweetalert.min.js')}}"></script>
        <script src="{{ asset('js/PDFObject/pdfobject.js')}}"></script>
        <script src="{{ asset ("DataTables/js/jquery.dataTables.min.js")}}"></script>

        <title>CarPool</title>
        <link rel="icon" href="../image/logo.png" type="image/gif" sizes="16x16">

    </head>
    <body>

                @yield('side-nav')
                @yield('top-nav')

      <section class="section">
                @section('content')
                  @show
      </section>

      {{-- <div class="modal-area"></div> --}}
      <button class="btn-page-up"><span class="fa fa-chevron-up" style="color:#fff;"></span></button>
      <span class="version"><small style="color:#fff;">v.1.5.0</small></span>

    </body>
</html>
<script>

$(document).ready(function(){
  $.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
  });
  $(".version").css({'display':'block','position':'fixed','bottom': 0});
});

$(window).scroll(function(e){
  var scroll_top = $(window).scrollTop();
  var body_top = $('body').offset().top;
  if(scroll_top > 0 ){
      $(".btn-page-up").css({'display':'block','position':'fixed'});
  }else{
      $(".btn-page-up").css({'display':'none','position':'absolute'});
  }
});

$(".btn-page-up").click(function(){
    $("html , body").animate({scrollTop:0},'fast');
});

</script>
