@extends('welcome')

@section('content')
  <div class="container">
    <div class="container-fluid">
      <div class="block block-login">
        <img class="img-login" src="{{$url_img_logo = Storage::url('image/logo/logo_siamraj.png')}}">
        <h4 style="color:#fff;">Carpool Siamraj Service</h4>
         <small style="color:#fff;">กรุณากรอกอีเมล</small>
        <div class="block-login block-login-form">

        <div class="col-md-12">

          <form id="frmChangePass" >
            <div class="card-block">
            <input type="hidden" class="form-control" id="emp_id" name="emp_id" value="{{$users}}">

          <div class="form-group row" id="groupemail"><!-- email -->
            <label for="new_pass" class =" col-form-label text-black" align="right">Email</label>
            <input type="email" class="form-control" id="email" name="email" placeholder="example@example.com" onfocus="rmErr(this);" onkeypress="rmErr(this);">
            <div hidden="true" id="fbemail" class="form-control-feedback"></div>
            {{-- <small class="form-text text-muted">Passwords must contain at least eight characters, including uppercase, lowercase letters and numbers.</small> --}}
          </div><!-- รหัสผ่านใหม่่ -->
          <div class="form-group form-group-row" align="center">
            <button type="button" class="btn btn-success" id="btn-change" style="color:#fff;" disabled>ยืนยัน</button>
          </div>
        </div>
          </form>
          <div align="right">
            <a href="/logout">ออกจากระบบ</a>
          </div>
        </div>
      </div>

<script>
    $(document).ready(function(){
      $("#email").focus();
    });

    $("#email").keyup(function() {
      $("#btn-change").removeAttr("disabled")
    })
    $("#btn-change").click(function() {

      if (validateEmail($("#email").val())) {
        $.ajax({
         url:"/updateemail",
         data:"emp_id="+$("#emp_id").val()+"&email="+$("#email").val(),
         type:"POST",
         success:function(data){
           var obj =JSON.parse(data);
           if(obj['success']==true)
             {
               swal({
                         title: "อัพเดทข้อมูลสำเร็จ",
                         text: obj['msg'],
                         type: "success",
                         showCancelButton: false,
                         confirmButtonColor: "#2ECC71",
                         confirmButtonText: "ตกลง",
                         closeOnConfirm: false,
                       },
                         function(isConfirm){
                           if (isConfirm) {
                             window.location = "/dashboard";
                       }
                   });
             }
           else
             {
               swal({
                         title: "ผิดพลาด",
                         text: obj['msg'],
                         type: "error",
                         showCancelButton: false,
                         confirmButtonColor: "#2ECC71",
                         confirmButtonText: "ตกลง",
                         closeOnConfirm: true,

                   });
             }
         }
        });
      }else {
        swal({
                  title: "ผิดพลาด",
                  text: "รูปแบบอีเมล์ไม่ถูกต้อง",
                  type: "error",
                  showCancelButton: false,
                  confirmButtonColor: "#2ECC71",
                  confirmButtonText: "ตกลง",
                  closeOnConfirm: true,

            });
      }


    })

    function validateEmail(email) {
      var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      return re.test(email);
    }

     function checkPassword(str)
     {
       // at least one number, one lowercase and one uppercase letter
       // at least six characters that are letters, numbers or the underscore
       var re = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])\w{8,10}$/;
       return re.test(str);
     }

     function delDisabled(id){
       $("#"+id).removeAttr("disabled");
     }

    function rmErr(input){
      $("#group"+input.id).removeClass("has-danger");
      $("#group"+input.id+" input").removeClass("form-control-danger");
      $("#fb"+input.id).attr("hidden","hidden");
    }

    function addErr(type,msg){
        $("#group"+type).addClass("has-danger");
        $("#group"+type+" input").addClass("form-control-danger");
        $("#fb"+type).html(msg);
        $("#fb"+type).removeAttr("hidden");
    }
    </script>
@endsection
