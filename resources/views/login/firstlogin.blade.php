@extends('welcome')

@section('content')
  @foreach ($users as $r)
    <?php
          $emp_id = $r->emp_id;
          $password = $r->password;
          $update = $r->update_date;
     ?>
  @endforeach
  @if ($update !="0000-00-00 00:00:00" and $update !="")
    <script type="text/javascript">
      window.location = "/dashboard";
    </script>
  @endif
  <div class="container">
    <div class="container-fluid">
      <div class="block block-login">
        <img class="img-login" src="{{$url_img_logo = Storage::url('image/logo/logo_siamraj.png')}}">
        <h4 style="color:#fff;">Carpool Siamraj Service</h4>
         <small style="color:#fff;">กรุณาเปลี่ยนรหัสผ่านใหม่</small>
        <div class="block-login block-login-form">

        <div class="col-md-12">

          <form id="frmChangePass" >
            <div class="card-block">
            <input type="hidden" class="form-control" id="emp_id" name="emp_id" value="{{$emp_id}}">
            <input type="hidden" class="form-control" id="old_pass" name="old_pass" value="{{$password}}">

          <div class="form-group row" id="groupnew_pass"><!-- รหัสผ่านใหม่่ -->
            <label for="new_pass" class =" col-form-label text-black" align="right">รหัสผ่านใหม่</label>
            <input type="password" class="form-control" id="new_pass" name="new_pass" placeholder="รหัสผ่านใหม่" onfocus="rmErr(this);" onkeypress="rmErr(this);">
            <div hidden="true" id="fbnew_pass" class="form-control-feedback"></div>
            {{-- <small class="form-text text-muted">Passwords must contain at least eight characters, including uppercase, lowercase letters and numbers.</small> --}}
          </div><!-- รหัสผ่านใหม่่ -->

          <div class="form-group row" id="groupcon_pass"><!-- ยืนยันรหัสผ่านใหม่ -->
            <label for="con_pass" class ="col-form-label text-black" align="right">ยืนยันรหัสผ่านใหม่</label>
            <input type="password" class="form-control" id="con_pass" name="con_pass" placeholder="ยืนยันรหัสผ่านใหม่" onfocus="rmErr(this);" onkeypress="rmErr(this);" disabled>
            <div hidden="true" id="fbcon_pass" class="form-control-feedback"></div>
            {{-- <small class="form-text text-muted">Passwords must contain at least eight characters, including uppercase, lowercase letters and numbers.</small> --}}
          </div><!-- ยืนยันรหัสผ่านใหม -->

          <div class="form-group form-group-row" align="center">
            <button type="button" class="btn btn-success" id="btn-change" style="color:#fff;" disabled>ยืนยัน</button>
          </div>
        </div>
          </form>
          <div align="right">
            <a href="/logout">ออกจากระบบ</a>
          </div>
        </div>
      </div>

<script>
    $(document).ready(function(){
      $("#new_pass").focus();
    });

    $("#new_pass").keyup(function () {
      delDisabled("con_pass");
    })

    $("#con_pass").keyup(function () {
      delDisabled("btn-change");
    });

    $("#frmChangePass").keypress(function(event){
       var kc = event.keyCode;
       if(kc==13){
          $(".btn-change").trigger("click");
       }
     })

    $("#btn-change").click(function(){
          change();
    });

     function change(){
      var dString = $("#frmChangePass").serialize();
      var valnewpass = checkPassword($("#new_pass").val());
      var valconpass= checkPassword($("#con_pass").val());
      // console.log(dString+"&valnew="+valnewpass+"&valcon="+valconpass);
       $.ajax({ type:"POST",
                data :dString+"&valnew="+valnewpass+"&valcon="+valconpass,
                url:"/firstchangeDB",
                success:function(data){
                  // console.log(data);
                 var obj = JSON.parse(data);
                //  console.log(obj);
                // //  console.log(obj['success']);
                // //  console.log(obj['type']);
                // //  console.log(obj['msg']);
                 if (obj['success']==true) {
                   swal({
                             title: "สำเร็จ",
                             text: "เปลี่ยนรหัสผ่านคุณสำเร็จ",
                             type: "success",
                             showCancelButton: false,
                             confirmButtonColor: "#2ECC71",
                             confirmButtonText: "ตกลง",
                             closeOnConfirm: false,
                           },
                             function(isConfirm){
                               if (isConfirm) {
                                 window.location = "/dashboard";
                           }
                       });
                 }
                 else
                 {
                   addErr(obj['type'],obj['msg'])
                 }

               }
        });
     }

     function checkPassword(str)
     {
       // at least one number, one lowercase and one uppercase letter
       // at least six characters that are letters, numbers or the underscore
       var re = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])\w{8,10}$/;
       return re.test(str);
     }

     function delDisabled(id){
       $("#"+id).removeAttr("disabled");
     }

    function rmErr(input){
      $("#group"+input.id).removeClass("has-danger");
      $("#group"+input.id+" input").removeClass("form-control-danger");
      $("#fb"+input.id).attr("hidden","hidden");
    }

    function addErr(type,msg){
        $("#group"+type).addClass("has-danger");
        $("#group"+type+" input").addClass("form-control-danger");
        $("#fb"+type).html(msg);
        $("#fb"+type).removeAttr("hidden");
    }
    </script>
@endsection
