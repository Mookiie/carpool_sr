@extends('welcome')

@section('content')
  <?php
        session()->forget('user');

  ?>
  <div class="container">

    <div class="container-fluid">
      <div class="block block-login">
        <img class="img-login" src="{{$url_img_logo = Storage::url('image/logo/Logo.png')}}">
        {{-- <img class="img-login" src="{{ asset('image/Logo.png')}}"> --}}
        <h4>Carpool Service</h4>
        <small>เข้าสู่ระบบ</small>
        <div class="block-login-form">

        <div class="col-md-12">

          <form id="frmLogin" >
            {{ csrf_field() }}
            <div class="form-group form-group-row" id="groupusername">
              <input type="text" id="username" name="username"  class="form-control" placeholder="Username" onfocus="rmErr(this);" onkeypress="rmErr(this);">
              <div hidden="true" id="fbusername" class="form-control-feedback"><br></div>
            </div>

            <div class="form-group form-group-row" id="grouppassword">
              <input type="password" id="password" name="password"  class="form-control" placeholder="Password" onfocus="rmErr(this);" onkeypress="rmErr(this);">
              <div hidden="true" id="fbpassword" class="form-control-feedback"></div>
            </div>

            <div class="form-group form-group-row">
              <button type="button" class="btn btn-login" style="color:#fff;">เข้าสู่ระบบ</button>
            </div>
          </form>

          <div align='center'>

            {{-- <a href="/forget">ลืมรหัสผ่าน?</a><br /> --}}
            {{-- <a href="#" id='btn-register'>ทดลองใช้งาน</a> --}}

          </div>
        </div>

        </div>
      </div>
    </div>

  </div>
  <div class="modal-area">
  </div>
<script>

$(document).ready(function(){
  $("#username").focus();
});

$("#btn-register").click(function() {
  $.ajax({type:"POST",
           data :"",
           url:"/register",
           success:function(data){
             $(".modal-area").html(data);
             $('#modalregister').modal('show');
           }
  })
})

// navigator.geolocation.getCurrentPosition(function(location) {
//     var location = location.coords;
//     var lat = location.latitude;
//     var lng = location.longitude;
//     var latlng = "lat="+lat+"&lng="+lng

    $("#frmLogin").keypress(function(event){
       var kc = event.keyCode;
       if(kc==13){
          // login_auth();
          login();
       }
    });

    $(".btn-login").click(function(){
        // login_auth();
        login();
    });

function login_auth() {

}


 function login(){
  var dString = $("#frmLogin").serialize();
  var user = $("#username").val();
   $.ajax({ type:"POST",
            data :dString,
            url:"/login",
            success:function(data){
             var obj = JSON.parse(data);
             console.log(obj);
             if (obj['success']==true) {
               if (obj['data']==1) {
                 window.location ="{{ url("/firstchange") }}";
               }else if (obj['data']==3) {
                 window.location ="{{ url("/SetEmail") }}";
               }
               else {
                 window.location ="{{ url("/dashboard") }}";
               }
                // $.ajax({ url:"/locatelogin",
                //       data:latlng+"&user="+user,
                //       type:"GET",
                //       success:function(data){
                //         console.log(data);
                //     }
                //  })
             }
             else
             {
               if (obj['type'] =='error') {
                 alert('ชื่อผู้ใช้หรือรหัสผ่านไม่ถูกต้อง')
               }
               else {
                 addErr(obj['type'],obj['msg'])
               }
             }

           }
    });
  }
 // });





function rmErr(input){
  $("#group"+input.id).removeClass("has-danger");
  $("#group"+input.id+" input").removeClass("form-control-danger");
  $("#fb"+input.id).attr("hidden","hidden");
}

function addErr(type,msg){
    $("#group"+type).addClass("has-danger");
    $("#group"+type+" input").addClass("form-control-danger");
    $("#fb"+type).html(msg);
    $("#fb"+type).removeAttr("hidden");
}

</script>

@endsection
