@extends('welcome')

@section('content')
  <?php session()->forget('user'); ?>
  <div class="container">
    <div class="container-fluid">
      <div class="block block-login">
        <img class="img-login" src="{{ asset('image/logo.png')}}">
        <h4 style="color:#fff;">Carpool Siamraj Service</h4>
        <small style="color:#fff;">ลืมรหัสผ่าน</small>
        <div class="block-login block-login-form">

        <div class="col-md-12">

          <form id="frmForget" >
              <?php
                $token = md5(uniqid(rand(), true))
               ?>
            <div class="form-group form-group-row" id="grouptoken">
              <input type="hidden" id="token" name="token"  class="form-control" value="{{$token}}">
            </div>

            <div class="form-group form-group-row" id="groupemail">
              <input type="email" id="email" name="email"  class="form-control" placeholder="กรุณากรอกอีเมล์" onfocus="rmErr(this);" onkeypress="rmErr(this);">
              <div hidden="true" id="fbemail" class="form-control-feedback"></div>
            </div>

            <div class="form-group form-group-row">
              <button type="button" class="btn btn-login" id="btn-forget" style="color:#fff;">ลืมรหัสผ่าน</button>
            </div>
            <div align="right">
              <a href="/">กลับหน้าล็อกอิน</a>
            </div>
          </form>

        </div>

        </div>
      </div>
    </div>
  </div>
    <script>
    $('#email').keypress(function(event){
          if(event.keyCode == '13'){
            forget();
          }
    });

    $("#btn-forget").click(function(){
        forget();
      })

      function forget() {
      var data = $("#frmForget").serialize();
      // console.log(data);
        $.ajax({url:"/sendforget",
                data:data,
                type:"GET",
                success:function(data){
                  var obj = JSON.parse(data);
                  if (obj['success']==true) {
                    swal({
                              title: "ส่งอีเมลสำเร็จ",
                              text: "กรุณาเช็คอีเมลเพื่อตั้งรหัสผ่านใหม่",
                              type: "success",
                              showCancelButton: false,
                              confirmButtonColor: "#2ECC71",
                              confirmButtonText: "ปิด",
                              closeOnConfirm: false,
                            },
                              function(isConfirm){
                                if (isConfirm) {
                                  window.location = "/";
                            }
                        });
                  }
                  else
                  {
                    addErr(obj['type'],obj['msg'])
                  }
                }
        })
      }
      function rmErr(input){
        $("#group"+input.id).removeClass("has-danger");
        $("#group"+input.id+" input").removeClass("form-control-danger");
        $("#fb"+input.id).attr("hidden","hidden");
      }

      function addErr(type,msg){
          $("#group"+type).addClass("has-danger");
          $("#group"+type+" input").addClass("form-control-danger");
          $("#fb"+type).html(msg);
          $("#fb"+type).removeAttr("hidden");
      }
    </script>

@endsection
