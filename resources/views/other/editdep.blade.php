@extends('welcome')
@extends('dashboard.topNavbar')
@extends('dashboard.SideNavbar')
@section('content')
@foreach ($users as $r)
  <?php
        $emp_id=$r->emp_id;
        $lv_user = $r->emp_level;
        $com_id = $r->com_id;
   ?>
@endforeach

<div class="container-dashboard">
  <input type="hidden"  id="emp_id" value="{{$emp_id}}">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          <span class="fa fa-university" style="color:#000;">&nbsp;&nbsp; ข้อมูลหน่วยงาน</span>
        </div>
          <div class="card-block row offset-md-1 col-md-10" >


             <table class="table table-bordered tblBk">
                <thead>
                  <th class="text-black" width="20%">รหัสหน่วยงาน</th>
                  <th class="text-black">ชื่อหน่วยงาน</th>
                  {{-- <th class="text-black">เบอร์โทร</th> --}}
                  <th class="text-black" width="2%">แก้ไข</th>
                </thead>

                <tbody class="">
                  <?php
                  $department = DB::table('tb_department')->where('com_id','=',$com_id)->orderBy('dep_name')->get();
                  $numdepartment = DB::table('tb_department')->where('com_id','=',$com_id)->count();
                  if($numdepartment > 0){
                  ?>
                    @foreach ($department as $dep)
                              <tr>
                                 <td class="text-black">{{$dep->dep_id}}</td>
                                 <td class="text-black">{{$dep->dep_name}}</td>
                                 {{-- <td class="text-black">{{$dep->dep_tel}}</td> --}}
                                 <td class="text-black" align='center'>
                                   <button class="btn btn-sm btn-warning btn-edit" data-id="dep={{$dep->dep_id}}">แก้ไข</button>
                                </td>
                              </tr>
                    @endforeach

                  <?php }else{  ?>
                              <tr>
                                  <td colspan="4" align="center" class="text-black"><h5>ไม่พบข้อมูล</h5></td>
                              </tr>
                  <?php } ?>
                </tbody>
              </table>
          </div>
          <div align="center" style="padding-bottom:15px">
            <a href="/otheredit"><button class="btn btn-sm btn-warning">กลับหน้าหลัก</button></a>
          </div>
        </div>
      </div>
</div>
<div class="modal-area"></div>

<script>

$(".btn-edit").click(function(){
  var id =$("#emp_id").val();
  var detail = $(this).data("id");
  $.ajax({
           url:"/depedit",
           data:"emp_id="+id+"&"+detail,
           type:"GET",
           success:function(data){
           $(".modal-area").html(data);
           $("#modalBk").modal("show");
         }
     });
});

</script>
@endsection
