@extends('welcome')
@extends('dashboard.topNavbar')
@extends('dashboard.SideNavbar')

@section('content')

@foreach ($users as $r)
  <?php
        $emp_id=$r->emp_id;
        $lv_user = $r->emp_level;
        $com_id = $r->com_id;
   ?>
@endforeach
<style>
  .inCard{
    margin: 5px auto;
    padding: 2px;
    border:solid 1px #0d47a1;
    border-radius: 0px;
  }
  .text-black{
  color: #000;
  }
  .card-outline-scb{
    background-color:#42a5f5;
  }
  .dash-headder{
    padding: 5px;
    margin-top:15px;
    border-bottom: solid 0px #ccc;
  }

</style>

<div class="container-dashboard">
    <div class="col-md-12 row">
      <input type="hidden"  id="emp_id" value="{{$emp_id}}">
      <input type="hidden"  id="com_id" value="{{$com_id}}">
<?php if ($lv_user == 999) {?>
      <div class="col-md-12">
        <div class="card">
          <div class="dash-headder">
            <span class="fa fa-university" style="color:#000;">&nbsp;&nbsp; รายการข้อมูลบริษัท</span>
          </div>
            <div class="card-block row" >

              <div class="card inCard col-md-10">
                  <div class="card-outline-scb card-block">
                    <h5><small>ข้อมูลบริษัท</small></h5>
                    <?php

                      $com = DB::table('tb_company')->count();

                    ?>
                    <h2>{{ $com }}<small><small><small> รายการ</small></small></small></h2>

                  </div>
                  <div class="card-block text-right">
                    <button class="btn btn-sm btn-outline-success fa fa-plus" id="addcom"> เพิ่มข้อมูล</span></button>
                    {{-- <button class="btn btn-sm btn-outline-warning">ดูข้อมูล</span></button> --}}
                  </div>
              </div>

            </div>
          </div>
        </div>
        <?php } ?>

        <div class="col-md-12" style="margin-top:5px;">
          <div class="card">
            <div class="dash-headder">
              <span class="fa fa-university" style="color:#000;">&nbsp;&nbsp; รายการข้อมูลอื่นๆ</span>
            </div>
              <div class="card-block row" >

                <div class="card inCard col-md-5" >
                    <div class="card-outline-scb card-block">
                      <h5><small>ข้อมูลแผนก</small></h5>
                      <?php
                        $department = DB::table('tb_department')->where('com_id','=',$com_id)->count();
                      ?>
                      <h2>{{ $department }}<small><small><small> รายการ</small></small></small></h2>
                    </div>

                    <div class="card-block text-right" >
                      <div>
                        <button class="btn btn-sm btn-outline-success fa fa-plus" id="adddep" > อัพเดทข้อมูล</span></button>
                        {{-- <button class="btn btn-sm btn-outline-warning">ดูข้อมูล</span></button> --}}
                      </div>

                    </div>
                </div>

                <div class="card inCard col-md-5">
                    <div class="card-outline-scb card-block">
                      <h5><small>ข้อมูลตำแหน่งงาน</small></h5>
                      <?php

                        $job = DB::table('tb_job')->where('com_id','=',$com_id)->select('job_id')->groupBy('job_id')->get();

                      ?>
                      <h2>{{ count($job) }}<small><small><small> รายการ</small></small></small></h2>

                    </div>
                    <div class="card-block text-right">
                      <button class="btn btn-sm btn-outline-success fa fa-plus" id="addjob"> เพิ่มข้อมูล</span></button>
                      {{-- <button class="btn btn-sm btn-outline-warning">ดูข้อมูล</span></button> --}}
                    </div>
                </div>

              </div>
            </div>
          </div>

          <div class="col-md-12">
            <div class="card" style="margin-top:5px;">
              <div class="dash-headder">
                <span class="fa fa-car" >&nbsp;&nbsp; รายการข้อมูลรถ</span>
              </div>
                <div class="card-block row">

                {{-- <div class="card inCard col-md-2" >
                    <div class="card-outline-scb card-block">
                      <h5><small>รายการรถยนต์</small></h5> --}}
                      <?php
                    //  $car = DB::table('tb_car')->count();
                        ?>
                      {{-- <h2>{{$car}}<small><small><small> รายการ</small></small></small></h2>

                    </div>
                    <div class="card-block text-right">
                      <button class="btn btn-sm btn-outline-success fa fa-plus" style="margin-right:10px" id="addcar"> เพิ่มข้อมูล</span></button>
                    </div>
                </div> --}}

                <div class="card inCard col-md-2">
                    <div class="card-outline-scb card-block">
                      <h5><small>ประเภทรถยนต์</small></h5>
                      <?php $ctype = DB::table('tb_car_type')->where('com_id','=',$com_id)->count(); ?>
                      <h2>{{$ctype}}<small><small><small> รายการ</small></small></small></h2>

                    </div>
                    <div class="card-block text-right">
                      <button class="btn btn-sm btn-outline-success fa fa-plus" style="margin-right:10px" id="addctype">เพิ่มข้อมูล</span></button>
                      {{-- <button class="btn btn-sm btn-outline-warning">ดูข้อมูล</span></button> --}}
                    </div>
                </div>

                <div class="card inCard col-md-2">
                    <div class="card-outline-scb card-block">
                      <h5><small>สีรถยนต์</small></h5>
                       <?php $color = DB::table('tb_color')->count(); ?>
                      <h2>{{$color}}<small><small><small> รายการ</small></small></small></h2>
                    </div>
                    <div class="card-block text-right">
                      <button class="btn btn-sm btn-outline-success fa fa-plus" style="margin-right:10px" id="addcolor">เพิ่มข้อมูล</span></button>
                      {{-- <button class="btn btn-sm btn-outline-warning">ดูข้อมูล</span></button> --}}
                    </div>
                </div>

              <div class="card inCard col-md-2">
                  <div class="card-outline-scb card-block">
                    <h5><small>เชื้อเพลิง</small></h5>
                    <?php $fuel = DB::table('tb_fuel')->count(); ?>
                    <h2>{{$fuel}}<small><small><small> รายการ</small></small></small></h2>

                  </div>
                  <div class="card-block text-right">
                    <button class="btn btn-sm btn-outline-success fa fa-plus" style="margin-right:10px" id="addfuel">เพิ่มข้อมูล</span></button>
                    {{-- <button class="btn btn-sm btn-outline-warning">ดูข้อมูล</span></button> --}}
                  </div>
              </div>

              <div class="card inCard col-md-2">
                  <div class="card-outline-scb card-block">
                    <h5><small>ยี่ห้อรถยนต์</small></h5>
                     <?php $brand = DB::table('tb_brand')->count(); ?>
                    <h2>{{$brand}}<small><small><small> รายการ</small></small></small></h2>
                  </div>
                  <div class="card-block text-right">
                    <button class="btn btn-sm btn-outline-success fa fa-plus" style="margin-right:10px" id="addbrand">เพิ่มข้อมูล</span></button>
                    {{-- <button class="btn btn-sm btn-outline-warning">ดูข้อมูล</span></button> --}}
                  </div>
              </div>
            </div>

            </div>
        </div>

    </div>
  </div>
    <div class="modal-area"></div>

    <script type="text/javascript">

    $("#addcom").click(function(){
      var id =$("#emp_id").val();
      $.ajax({
               url:"/addcom",
               data:"id="+id,
               type:"GET",
               success:function(data){
               $(".modal-area").html(data);
               $("#modalBk").modal("show");
             }
         });
    });

    $("#adddep").click(function(){
      var id ="emp_id="+$("#emp_id").val()+"&com_id="+$("#com_id").val();
      $.ajax({
               url:"/adddep",
               data:id,
               type:"GET",
               success:function(data){
               $(".modal-area").html(data);
               $("#modalBk").modal("show");
             }
         });
    });

    $("#addjob").click(function(){
      var id ="emp_id="+$("#emp_id").val()+"&com_id="+$("#com_id").val();
      $.ajax({
               url:"/addjob",
               data:id,
               type:"GET",
               success:function(data){
               $(".modal-area").html(data);
               $("#modalBk").modal("show");
             }
         });

    });

    $("#addcar").click(function(){
      var id =$("#emp_id").val();
     $.ajax({
              url:"/addcar",
              data:"id="+id,
              type:"GET",
              success:function(data){
              $(".modal-area").html(data);
              $("#modalBk").modal("show");
            }
        });
    });

    $("#addctype").click(function(){
      var id =$("#emp_id").val();
      $.ajax({
               url:"/addctype",
               data:"id="+id,
               type:"GET",
               success:function(data){
               $(".modal-area").html(data);
               $("#modalBk").modal("show");
             }
         });
    });

    $("#addcolor").click(function(){
      var id =$("#emp_id").val();
      $.ajax({
               url:"/addcolor",
               data:"id="+id,
               type:"GET",
               success:function(data){
               $(".modal-area").html(data);
               $("#modalBk").modal("show");
             }
         });
    });

    $("#addfuel").click(function(){
      var id =$("#emp_id").val();
      $.ajax({
               url:"/addfuel",
               data:"id="+id,
               type:"GET",
               success:function(data){
               $(".modal-area").html(data);
               $("#modalBk").modal("show");
             }
         });
    });

    $("#addbrand").click(function(){
      var id =$("#emp_id").val();
      $.ajax({
               url:"/addbrand",
               data:"id="+id,
               type:"GET",
               success:function(data){
               $(".modal-area").html(data);
               $("#modalBk").modal("show");
             }
         });
    });

    </script>

@endsection
