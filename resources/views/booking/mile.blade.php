@extends('welcome')
@extends('dashboard.topNavbar')
@extends('dashboard.SideNavbar')

@section('content')

@foreach ($users as $r)
  <?php
        $lv_user = $r->emp_level;
        $emp_id = $r->emp_id;
        $com_id = $r->com_id;
        $dep_id = $r->job_id;
   ?>
@endforeach

<div class="container-dashboard">
<div class="card col-md-12 row">
<input type="hidden"  id="emp_id" value="{{$emp_id}}">
<input type="hidden"  id="com_id" value="{{$com_id}}">
<input type="hidden"  id="dep_id" value="{{$dep_id}}">
<input type="hidden"  id="lv_user" value="{{$lv_user}}">
  <div class="card-header">
    <div class="row">
      <span class="fa fa-car mr-auto " style="color:#000;"> บันทึกไมล์</span>
    </div>
  </div>

  <div id='car'>

       <?php
          $sqlbooking_sql = DB::table('tb_booking')
                          ->join("tb_car_type",function($join){
                                $join->on("tb_booking.ctype_id","=","tb_car_type.ctype_id");
                                $join->on("tb_booking.com_id","=","tb_car_type.com_id");
                            })
                          ->join('tb_employee', 'tb_employee.emp_id', '=' , 'tb_booking.emp_id')
                          ->where([['tb_booking.com_id','=',$com_id],['tb_booking.bk_status', '=', 'success']])
                          // ->where()
                          ->orderBy('bk_start_start')->get();
          $numbooking = count($sqlbooking_sql);
        ?>
    <div id="tblBk" class="table-responsive" Data-id="emp=<?php echo $emp_id;?>">
      <table class="table table-bordered tblBk">
         <thead>
          <th>เลขที่อ้างอิง</th>
          <th>ผู้จอง</th>
          <th>วันที่ขอใช้รถ</th>
          <th>เลขไมล์</th>
         </thead>
        @foreach ($sqlbooking_sql as $bc)
         <tbody class="">
          <td class="detailBk text-black" data-id="bk=<?php echo $bc->bk_id;?>">
                  <?php echo $bc->bk_id ;?>
          </td>
          <td class="detailBk text-black fsize" data-id="bk=<?php echo $bc->bk_id;?>">
                  <?php echo $bc->emp_fname." ".$bc->emp_lname;
                  $sql_dep = DB::table('tb_department')->where('com_id','=',$com_id)->where('dep_id','=',$bc->dep_id)->get();
                    foreach ($sql_dep as $dep) {
                      echo  " (".$dep->dep_name.")";
                    }?>
          </td>
          <td class="detailBk text-black" width="40%" data-id="bk=<?php echo $bc->bk_id;?>">
                  <?php  if (!$bc->bk_use) {
                    echo datetime($bc->bk_start_start);echo " ถึง ";datetime($bc->bk_end_start);
                  } else {
                    echo datetimesome($bc->bk_start_start,$bc->bk_end_start);
                  }

                  $locate = DB::table('tb_booking_location')->where('bk_id','=',$bc->bk_id)->get();
                  foreach ($locate as $lo) {
                    $lo_id =$lo->location_id;
                      if ($lo->location_id == "1") {
                        echo "<br /><b>สถานที่เริ่มต้น</b> : ".$lo->location_name;
                      }
                      else {
                        echo "<br /><b>สถานที่ ".($lo->location_id-1)."</b> : ".$lo->location_name;
                      }
                    }
                   ?>
                   <br />
                   <b>วัตถุประสงค์</b> :
                   <?php
                         if ($bc->bk_obj == "") {
                           echo "-";
                         }else {
                           echo $bc->bk_obj ;
                         }
                   ?>
                   <br>
                   <b>หมายเหตุ</b> :
                   <?php
                         if ($bc->bk_note == "") {
                           echo "-";
                         }else {
                           echo $bc->bk_note ;
                         }
                   ?>
                   <br>
                   <b>จำนวนผู้เดินทาง</b> :{{$bc->bk_percon}} คน

          </td>
          <td>
            <button class="btn btn-sm btn-success mile" data-id="bk=<?php echo $bc->bk_id; ?>">
              <span class="fa fa-pencil-square-o"> บันทึกเลขไมล์</span>
            </button>
          </td>

         </tbody>
        @endforeach

      </table>
    </div>

  </div>



<div class="modal-area" ></div>
</div>
</div>
<?php
    function datetime($datetime)
    {
      $y = substr($datetime,0,4);
      $m = substr($datetime,5,2);
      $d = substr($datetime,8,2);
      $h = substr($datetime,11,2);
      $i = substr($datetime,14,2);
      echo $d."/".$m."/".$y." เวลา ".$h.":".$i." น." ;
    }
    function datetimesome($datetime1,$datetime2)
    {
      $y1 = substr($datetime1,0,4);
      $m1 = substr($datetime1,5,2);
      $d1 = substr($datetime1,8,2);
      $h1 = substr($datetime1,11,2);
      $i1 = substr($datetime1,14,2);
      $y2 = substr($datetime2,0,4);
      $m2 = substr($datetime2,5,2);
      $d2 = substr($datetime2,8,2);
      $h2 = substr($datetime2,11,2);
      $i2 = substr($datetime2,14,2);
      return $d1."/".$m1."/".$y1." ถึง ".$d2."/".$m2."/".$y2." ช่วงเวลา ".$h1.":".$i1."น. ถึง ".$h2.":".$i2."น." ;
    }
     ?>
<script>

  $(".mile").click(function() {
    var id = $(this).data("id")
    var emp = $("#emp_id").val();
    $.ajax({
      url:"/setmile",data:id+"&emp_id="+emp,type:"POST",success:function(data){
        $(".modal-area").html(data);
        $("#modalBk").modal("show");
      }
    })

  })
</script>
@endsection
