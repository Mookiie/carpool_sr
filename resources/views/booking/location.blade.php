<!doctype html>
<html>
    <head>
    <?php $bk_id = session()->get('bk_id'); ?>
        <meta charset="utf-8">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel='stylesheet' href="{{ asset('/css/bootstrap.min.css') }}">
        <link rel='stylesheet' href="{{ asset('/tether/dist/css/tether.min.css') }}">
        <script src="https://code.jquery.com/jquery-3.2.1.js" integrity="sha256-DZAnKJ/6XZ9si04Hgrsxu/8s717jcIzLy3oi35EouyE=" crossorigin="anonymous"></script>
        <script src="{{ asset('/js/Jquery-3.2.1.min.js') }}"></script>
        <script src="{{ asset('/tether/dist/js/tether.min.js') }}"></script>
        <script src="{{ asset('/js/bootstrap.min.js') }}"></script>

        <title>Carpool Siamraj Service</title>
        <link rel="icon" href="{{$url_img_logo = Storage::url('image/logo/logo.png')}}" type="image/gif" sizes="16x16">
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        <style>
          /* Always set the map height explicitly to define the size of the div
           * element that contains the map. */
          #map {
            height: 100%;
            width: 100%
          }
          /* Optional: Makes the sample page fill the window. */
          /*html, body {
            height: 100%;
            margin: 0;
            padding: 0;
          }*/
        </style>
    </head>
    <body>
      <div class="row" >
        <div class="col-md-12">
          <div class="card card_bk">
            <div class="card-header">
              <span class="	fa fa-map-marker"></span>  สถานที่
            </div>
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="card-block" id="map" style="height:800px;"></div>
                <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAaXMuANTFVYidZ3gQaPaUVQU4zeEmk33U&callback=initMap"></script>
              </div>
            </div>
          </div>
    </body>
</html>
<script>

$(document).ready(function(){
  $.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
  });
});


function initMap(address) {

  var obj = [];


  <?php
  $sqlbooking_locate = DB::table('tb_booking_location')->where('bk_id', '=', $bk_id)->get();
  foreach ($sqlbooking_locate as $locate):
    $id = $locate->bk_id;
    $name = $locate->location_name;
    $address =  $locate->location_id;
    $latitude = $locate->location_lat;
    $longitude = $locate->location_lng;?>
    var location = ["<?php echo $address ?>",<?php echo $latitude ?>,<?php echo $longitude ?>,"<?php echo $name ?>"];
    obj.push(location);
  <?php endforeach; ?>
  // console.log(obj);
  // console.log(name);
    var map = new google.maps.Map(document.getElementById('map'), {
    zoom: 10,
    center: new google.maps.LatLng(obj[0][1],obj[0][2]),
    mapTypeId: google.maps.MapTypeId.ROADMAP
    });

    var infowindow = new google.maps.InfoWindow();

    var marker, i;

    for (i = 0; i < obj.length; i++) {
    marker = new google.maps.Marker({
      position: new google.maps.LatLng(obj[i][1], obj[i][2]),
      map: map
       });
       if (i==0) {
         var circle = new google.maps.Circle({
           map: map,
           radius: 10000,    // 30km to metres
           fillColor: '#B2E5B2'
         });
         circle.bindTo('center', marker, 'position');
         }

    google.maps.event.addListener(marker, 'click', (function(marker, i) {
          return function() {
            var name = "สถานที่ : "+obj[i][0]+"<br> ตำแหน่ง : "+obj[i][3]
            infowindow.setContent(name);
            infowindow.open(map, marker);
          }
        }
      )

     (marker, i));
    }
  }



</script>
