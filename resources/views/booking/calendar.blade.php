@extends('welcome')
@extends('dashboard.topNavbar')
@extends('dashboard.SideNavbar')

@section('content')

<div class="container-dashboard">
  <div class="card col-md-12 row">

    <div class="card-header">
      <span class="fa fa-calendar-o text-black"> ปฏิทินการใช้รถ</span>
    </div>

      <div class="col-12">
        <br>
        <div id="calendarCar_select">

        </div>
      </div>
  </div>
</div>

  <div class="modal-area"></div>

<script>

$(function(){
// $('#calendarCar_select').fullCalendar({
    // header: {
    //     left: 'today',  //  prevYear nextYea
    //     center: 'title',
    //     right: 'prevYear,prev,next,nextYear',
    // },
    // defaultView: 'agendaWeek',
    // buttonIcons:{
    //     prev: 'left-single-arrow',
    //     next: 'right-single-arrow',
    //     prevYear: 'left-double-arrow',
    //     nextYear: 'right-double-arrow'
    // },
    // events: {
    //   url:"/allcarEvent?"+$("#ansValue").val(),
    //   error:function(data){
    //     console.log(data.responseText);
    //   }
    // },eventClick: function(calEvent, jsEvent, view) {
    //   callDeatil("bk="+calEvent.id);
    // },
    // eventLimit:true,
    // lang: 'th'
// });
});


function callSelect_car(){
  $.ajax({
    url:"/selectcar",data:"",type:"GET",success:function(data){
        $(".modal-area").html(data);
        $("#modalSelectCar").modal("show");

    }
  });
}

// function callDeatil(detail){
//   $.ajax({
//     url:"/detail",data:detail,type:"GET",success:function(data){
//         $(".modal-area").html(data);
//         $("#modalBk").modal("show");
//     }
//   });
// }

</script>
@endsection
