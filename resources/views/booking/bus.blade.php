@extends('welcome')
@extends('dashboard.topNavbar')
@extends('dashboard.SideNavbar')

@section('content')
  <link rel="stylesheet" type="text/css" href="{{ asset('js/datetimepicker/jquery.datetimepicker.css')}}"/>
  <script src="{{asset('js/datetimepicker/jquery.js')}}"></script>
  <script src="{{asset('js/datetimepicker/build/jquery.datetimepicker.full.min.js')}}"></script>
  <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
@foreach ($users as $r)
  <?php
        $emp_id = $r->emp_id;
        $dep_id = $r->dep_id;
        $job_id = $r->job_id;
        $com_id = $r->com_id;
        $emp_tel = $r->emp_tel;
   ?>
@endforeach

  <div class="container-dashboard">
    <div class="card  col-md-12 row">
      <div class="card-header">
        <span class="fa fa-bus" style="color:#000;">  จัดรถรับ-ส่ง</span>
      </div>

      <div class="card-block">
        <form id="frm_bookingbus">
          <input id="emp_id" name="emp_id" type="hidden" class="form-control" value="{{$emp_id}}">
          <input id="com_id" name="com_id" type="hidden" class="form-control" value="{{$com_id}}">
          <input id="dep_id" name="dep_id" type="hidden" class="form-control" value="{{$dep_id}}">
          <input id="job_id" name="job_id" type="hidden" class="form-control" value="{{$job_id}}">
          <input id="mtel" name="mtel" type="hidden" class="form-control" value="{{$emp_tel}}">
          <?php
            date_default_timezone_set("Asia/Bangkok");
            $date = date("Y-m-d");
            $time =date("H:i");
          ?>
            <input id="date" name="date" type="hidden" class="form-control" value="{{$date}}">

            {{-- <input id="time" name="time" type="hidden" class="form-control" value="{{$time}}"> --}}

            <div class="form-group row">
              <label for="round_am" class="col-md-2 col-form-label">วันที่</label>
              <div class="col-md-10">
                <input id="bk_start_date" name="bk_start_date" type="text" class="form-control bkDate" value="{{date("Y/m/d")}}">
              </div>
            </div>

            <div class="form-group row">
              <label for="round_am" class="col-md-2 col-form-label">จำนวนรอบ (เช้า) </label>
              <div class="col-md-7">
                <input id="round_am" name="round_am" type="number" class="form-control" min="1" max="10">
              </div>
              <label for="time_am" class="col-md-1 col-form-label">เวลา</label>
              <div class="col-md-2">
                <input id="time_am" name="time_am" type="text" class="form-control bkTime" value="07:00" >
              </div>
            </div>
            <div id="am"></div>

            <div class="form-group row">
              <label for="round_pm" class="col-md-2 col-form-label">จำนวนรอบ (เย็น) </label>
              <div class="col-md-7">
                <input id="round_pm" name="round_pm" type="number" class="form-control" min="1" max="10">
              </div>
              <label for="time_pm" class="col-md-1 col-form-label">เวลา</label>
              <div class="col-md-2">
                <input id="time_pm" name="time_pm" type="text" class="form-control bkTime" value="17:00" >
              </div>
            </div>
            <div id="pm"></div>

            <div class="form-group row">
                <label for="bk_where" class="col-md-2 col-form-label">สถานที่ไป </label>
                <div class="col-10">
                  <ul class="nav flex-column" id="bk_whereList">
                    <li>
                      <div class="form-group">
                        <input type="text" data-id="1"  name="bk_where1" id="bk_where1" onfocus=callmap(this);
                               class="form-control" placeholder="กรุณากรอกสถานที่เริ่มต้น" value="บริษัท สยามราชธานี จำกัด (มหาชน)">
                      </div>
                    </li>
                    <li>
                      <div class="form-group">
                          <input type="text" data-id="2"  name="bk_where2" id="bk_where2" onfocus=callmap(this);
                                 class="form-control" placeholder="กรุณากรอกสถานที่ไป1" value="อิมพีเรียลเวิลด์ สำโรง">
                      </div>
                    </li>
                    {{-- <li>
                      <button type="button" class="btn btn-sm btn-success btn-location"><span class="fa fa-plus"></span> เพิ่มสถานที่</button>
                      <button type="button" class="btn btn-sm btn-danger btn-relocate"><span class="fa fa-minus"></span> ลบสถานที่</button>
                    </li> --}}
                  </ul>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-12" align="center">
                  <button type="submit" class="btn btn-success" id="btn_save">บันทึก</button>
                  <button type="reset" class="btn btn-danger">ยกเลิก</button>
                </div>
            </div>
        </form>
      </div>
    </div>
  </div>

  <script type="text/javascript">
    $('#round_am').keyup(function() {
      var round = $(this).val();
      if(round == ""){
        $(".am").remove();
      }
      else {
        for(i=1; i<=round; i++) {
          var formData = "com_id="+$("#com_id").val()+"&job_id="+$("#job_id").val()+"&round="+i+"&time=am"
          $.ajax({
            url:"/selectbus",
            type:"GET",
            data:"com_id="+$("#com_id").val()+"&job_id="+$("#job_id").val()+"&round="+i+"&time=am",
            async: true,
            cache: false,
            contentType: false,
            processData: false,
            success:function(data){
              $("#am").before(data);
            }
          });
        }//end for
      }
    })

    $('#round_pm').keyup(function() {
      var round = $(this).val();
      if(round == ""){
        $(".pm").remove();
      }
      else {
        for(i=1; i<=round; i++) {
          var formData = "com_id="+$("#com_id").val()+"&job_id="+$("#job_id").val()+"&round="+i+"&time=pm"
          $.ajax({
            url:"/selectbus",
            type:"GET",
            data:"com_id="+$("#com_id").val()+"&job_id="+$("#job_id").val()+"&round="+i+"&time=pm",
            async: true,
            cache: false,
            contentType: false,
            processData: false,
            success:function(data){
              $("#pm").before(data);
            }
          });
        }//end for
      }
    })
    // ==========================================================================================

    $(".btn-location").click(function(){
        var num = $("#bk_whereList li").length;
         $("#bk_whereList li:last").before("<li>"
                                          +"<div class='form-group'>"
                                          +"<input type='text' class='form-control' id='bk_where"+num+"' name='bk_where"+num+"' onfocus='callmap(this);' "
                                          +"placeholder='กรุณากรอกสถานที่ไป"+(num-1)+"'>"
                                          +"</div>"
                                          +"</li>");
    });

    $(".btn-relocate").click(function(){
      var num = ($("#bk_whereList li").length)-2;
      if(num > 0){
          $("#bk_whereList li:eq("+num+")").remove();
      }

    });

    // ==========================================================================================
    $('#frm_bookingbus').submit(function(ev) {
      ev.preventDefault();
      // var formData = $(this).serialize();
      var count_where = ($("#bk_whereList li").length)-1;
      var formData = new FormData(this);
      for (var value of formData.values()) {
               console.log(value);
            }
      // console.log(formData);
      console.log("ok");
      $.ajax({
        url:"/insertbus",
        type:"POST",
        data:formData,
        async: true,
        cache: false,
        contentType: false,
        processData: false,
        success:function(data){
          // console.log(data);
          var obj = JSON.parse(data);
          if (obj['success']==true) {
              swal({
                    title: "สำเร็จ",
                    text: "บันทึกการจองของคุณสำเร็จ",
                    type: "success",
                    showCancelButton: false,
                    confirmButtonColor: "#2ECC71",
                    confirmButtonText: "ตกลง",
                    closeOnConfirm: false,
                    },
                    function(isConfirm){
                      if (isConfirm) {
                      window.location = "/dashboard";
                      }
                    }
                  )
            }
        }
      })
    })


    jQuery(".bkDate").datetimepicker({
            format:"Y/m/d",
            lang:"th",
            minDate:'-1970/01/01',
            timepicker:false,
            scrollInput:false
    });

    jQuery(".bkTime").datetimepicker({
            format:"H:i",
            datepicker:false,
            scrollInput:false,
    });

    // ==========================================================================================

  function callmap(e){
    initAutocomplete(e.id);
  }
  function initAutocomplete(id){
    if(typeof id == "undefined"){
      id = "bk_where1";
    }
    autocomplete = new  google.maps.places.Autocomplete((document.getElementById(id)),{types: ['geocode']});
    autocomplete.addListener('place_changed',fillInAddress);
  }
  function fillInAddress() {
      var place = autocomplete.getPlace();
      var address = place.formatted_address;
  }

  function GetValue(address,callback) {
    var geocoder = new google.maps.Geocoder();
    var  latitude ="";
    var  longitude ="";
    geocoder.geocode( { 'address': address }, function(results, status) {
      if (status == google.maps.GeocoderStatus.OK) {
         latitude = results[0].geometry.location.lat();
         longitude = results[0].geometry.location.lng();
         }
        callback(address,latitude,longitude);
     })
  }

  </script>
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAaXMuANTFVYidZ3gQaPaUVQU4zeEmk33U&libraries=places&callback=initAutocomplete" async defer></script>

@endsection
