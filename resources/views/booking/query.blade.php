@extends('welcome')
@extends('dashboard.topNavbar')
@extends('dashboard.SideNavbar')

@section('content')

@foreach ($users as $r)
  <?php
        $id = $r->emp_id;
        $lv_user = $r->emp_level;
        $com_id = $r->com_id;
   ?>
@endforeach
<div class="container-dashboard">

<div class="card col-md-12 row">
<div class="card-header">
  <span class="fa fa-list">    รายการจองของฉัน</span>

</div>
<ul class="nav nav-pills  bkItem" >

  <!-- <li class="nav-item">
  <a class="nav-link active" id="viewTbl">
    <span class="fa fa-list"></span>
  </a>
  </li> -->

  {{-- <li class="nav-item">
  <a class="nav-link " id="viewCalendar">
    <span class="fa fa-calendar-o"></span>
  </a>
  </li> --}}

</ul>

<div class="card-block">
  <div class="row">
    <div class="col-4">
      <div class="form-group">
        <div class="input-group">
          <?php
            $chkall ="";
            $chkwait ="";
            $chkapprove="";
            $chkeject="";
            $chkejectcar="";
            $chknonecar="";
            $chksuccess="";
              if($valuepage == "wait"){
                $chkwait = "selected";
              }elseif($valuepage == "approve"){
                $chkapprove = "selected";
              }elseif($valuepage == "eject"){
                $chkeject = "selected";
              }elseif($valuepage == "ejectcar"){
                $chkejectcar = "ejectcar";
              }elseif($valuepage == "nonecar"){
                $chknonecar = "selected";
              }elseif($valuepage == "success"){
                $chksuccess = "selected";
              }else {
                $chkall="selected";
              }
            ?>

          <select class="form-control" id="bksearch">
              <option value="" {{$chkall}}>ค้นหาทั้งหมด</option>
              <option value="wait" {{$chkwait}} >รอการอนุมัติ</option>
              <option value="approve" {{$chkapprove}}>รอการจัดรถ</option>
              <option value="eject" {{$chkeject}} >ยกเลิกการจอง</option>
              <option value="ejectcar" {{$chkejectcar}} >ยกเลิกการใช้งาน</option>
              <option value="nonecar" {{$chknonecar}}>ไม่มีรถ</option>
              <option value="success" {{$chksuccess}} >การจองสำเร็จ</option>
          </select>
          <span class="input-group-btn">
              <button class="btn btn-primary" id="btnsearch" type="button" name="button">
                <span class="fa fa-search"></span>
              </button>
          </span>
        </div>

      </div>
    </div>
  </div>

<div id="tblBk" class="table-responsive">
<table class="table table-bordered tblBk">
  <thead >
    <th class="text-black">สถานะ</th>
    <th class="text-black">เลขที่อ้างอิง</th>
    <th class="text-black">วันที่ขอใช้รถ</th>
    <th class="text-black">ประเภทรถ</th>
    <th class="text-black">วัตถุประสงค์</th>
  </thead>

  <tbody class="">
    <?php
    $sqlbooking="";
      if($valuepage != ""){
            $sqlbooking = DB::table('tb_booking')
                          ->join("tb_car_type",function($join){
                                $join->on("tb_booking.ctype_id","=","tb_car_type.ctype_id")
                                     ->on("tb_booking.com_id","=","tb_car_type.com_id");

                            })
                          ->where('emp_id', '=', $id)->where('bk_status', '=', $valuepage)->orderBy('bk_id', 'desc');
      }
      else {
          $sqlbooking = DB::table('tb_booking')
                                        ->join("tb_car_type",function($join){
                                              $join->on("tb_booking.ctype_id","=","tb_car_type.ctype_id")
                                                  ->on("tb_booking.com_id","=","tb_car_type.com_id");
                                          })
                                        ->where('emp_id', '=', $id)->orderBy('bk_id', 'desc');
      }//else

      $qrybooking = $sqlbooking->get();
      $numbooking = $sqlbooking->count();
      if($numbooking > 0){
      ?>


        @foreach ($qrybooking as $b)

        <?php
              if($numbooking > 0){

                  ?>
                  <tr class="detailBk" data-id="bk={{$b->bk_id}}">
                    <?php
                    $arr = array();
                          if($b->bk_status == "wait"){
                              array_push($arr,"รอการอนุมัติ","warning","#F39C12");
                            }else if($b->bk_status == "approve"){
                              array_push($arr,"รอการจัดรถ","info","#3498DB");
                            }else if($b->bk_status == "success"){
                              array_push($arr,"สำเร็จ","success","#2ECC71");
                            }else if($b->bk_status == "merge"){
                              array_push($arr,"สำเร็จ(ร่วมเดินทาง)","success","#2ECC71");
                            }else if($b->bk_status == "eject"){
                              array_push($arr,"ยกเลิกการจอง","danger","#E74C3C");
                            }else if($b->bk_status == "ejectcar"){
                              array_push($arr,"ยกเลิกการเดินทาง","danger","#E74C3C");
                            }else if($b->bk_status == "nonecar"){
                              array_push($arr,"ไม่มีรถ","danger","#E74C3C");
                            }

                     ?>
                     <td width="10px">
                       <h6><span class="badge badge-{{$arr[1]}}">{{$arr[0]}}</span></h6>
                     </td>
                     <td class="text-black">{{$b->bk_id }}<br>
                       <?php
                       if ($b->setcar_date !="" and $b->setcar_date !="0000-00-00 00:00:00" ) {
                         echo "จัดรถเมื่อ";datetime($b->setcar_date);
                       }elseif ($b->approve_date !="") {
                        echo "อนุมัติเมื่อ";datetime($b->approve_date);
                       }else {
                         echo "จองเมื่อ";datetime($b->bk_date);
                       }?>
                     </td>
                     <td class="text-black">
                         <?php
                          echo "วันที่ขอใช้ ";datetime($b->bk_start_start);echo " ถึง ";datetime($b->bk_end_start);
                        ?>

                     </td>
                     <td class="text-black">{{$b->ctype_name}}</td>
                     <td class="text-black">{{$b->bk_obj}}</td>
                   </tr>
                   <?php }
                    elseif ($numbooking = 0){ ?>
                      <tr>
                          <td colspan="5" align="center" class="text-black"><h5>ไม่พบกิจกรรม</h5></td>
                      </tr>
                    <?php }?>
            @endforeach
      <?php }else{  ?>
                  <tr>
                      <td colspan="5" align="center" class="text-black"><h5>ไม่พบกิจกรรม</h5></td>
                  </tr>
      <?php } ?>
  </tbody>

  </table>
  </div>
  <div id="calendarBk"  hidden="true" ></div>

  </div>
  <div class="modal-area"></div>
</div>
</div>
<?php

function datetime($datetime)
{
  $y = substr($datetime,0,4);
  $m = substr($datetime,5,2);
  $d = substr($datetime,8,2);
  $h = substr($datetime,11,2);
  $i = substr($datetime,14,2);
  echo $d."/".$m."/".$y." เวลา ".$h.":".$i." น." ;
}

 ?>
<script>

$("#btnsearch").click(function(){
  var search = $("#bksearch").val();
  //console.log(search);
  if(search == ""){
    window.location = '/booking';
  }else{
    window.location = '/'+search;
  }
});

$(".detailBk").click(function(){
  var detail = $(this).data("id");
  // console.log(detail);
 $.ajax({
          url:"/detail",
          data:detail,
          type:"GET",
          success:function(data){
            // console.log(data);
          $(".modal-area").html(data);
          $("#modalBk").modal("show");
        }
    });
});
$("#viewCalendar").click(function(){
  $("#calendarBk").removeAttr("hidden");
  $("#calendarBk").show();
  $('#calendarBk').fullCalendar('today');
  $("#tblBk").hide();
  $("#viewCalendar").addClass("active");
  $("#viewTbl").removeClass("active");
});

$("#viewTbl").click(function(){
  $("#tblBk").removeAttr("hidden");
  $("#tblBk").show();
  $("#calendarBk").hide();
  $("#viewTbl").addClass("active");
  $("#viewCalendar").removeClass("active");
});

$(function(){
  var search = $("#bksearch").val();
  // console.log(search);
  $('#calendarBk').fullCalendar({
      header: {
          left: 'today',  //  prevYear nextYea
          center:'title',
          right: 'prevYear,prev,next,nextYear',
      },
      height: 650,
      buttonIcons:{
          prev: 'left-single-arrow',
          next: 'right-single-arrow',
          prevYear: 'left-double-arrow',
          nextYear: 'right-double-arrow'
      },
      events: {
        url:"/BkEvents?search="+search,
        error:function(data){
          console.log(data.responseText);
        }
      },
      eventLimit:true,
      lang: 'th',
       eventClick: function(calEvent, jsEvent, view) {
         $.ajax({
                  url:"/detail",
                  data:"bk="+calEvent.id,
                  type:"GET",
                  success:function(data){
                    // console.log(data);
                  $(".modal-area").html(data);
                  $("#modalBk").modal("show");
                }
            });
       }
  });
});
</script>
@endsection
