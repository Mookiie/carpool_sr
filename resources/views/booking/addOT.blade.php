@extends('welcome')
@extends('dashboard.topNavbar')
@extends('dashboard.SideNavbar')

@section('content')
  <link rel="stylesheet" type="text/css" href="{{ asset('js/datetimepicker/jquery.datetimepicker.css')}}"/>
  <script src="{{asset('js/datetimepicker/jquery.js')}}"></script>
  <script src="{{asset('js/datetimepicker/build/jquery.datetimepicker.full.min.js')}}"></script>
  <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
@foreach ($users as $r)
  <?php
        $emp_id = $r->emp_id;
        $dep_id = $r->dep_id;
        $job_id = $r->job_id;
        $com_id = $r->com_id;
        $emp_tel = $r->emp_tel;
        $emp_table = $r->emp_table;
        $emp_lv = $r->emp_level;
   ?>
@endforeach
<div class="container-dashboard">

<div class="card  col-md-12 row">
  <div class="card-header">
    <span class="fa fa-plus" style="color:#000;">  จัดรถOT</span>
  </div>
  <div class="card-block">

      <form id="frm_booking">


            <div class="card container">
              <div class="card-block ">
                <input id="emp_id" name="emp_id" type="hidden" class="form-control" value="{{$emp_id}}">
                <input id="com_id" name="com_id" type="hidden" class="form-control" value="{{$com_id}}">
                <input id="dep_id" name="dep_id" type="hidden" class="form-control" value="{{$dep_id}}">
                <input id="job_id" name="job_id" type="hidden" class="form-control" value="{{$job_id}}">
                <input id="mtel" name="mtel" type="hidden" class="form-control" value="{{$emp_tel}}">
                <input id="ttel" name="ttel" type="hidden" class="form-control" value="{{$emp_table}}">

              <?php
                date_default_timezone_set("Asia/Bangkok");
                $date = date("Y/m/d");
                $time =date("H:i");
                function datetime($datetime)
                {
                  $y = substr($datetime,0,4);
                  $m = substr($datetime,5,2);
                  $d = substr($datetime,8,2);
                  $h = substr($datetime,11,2);
                  $i = substr($datetime,14,2);
                  echo $d."/".$m."/".$y." เวลา ".$h.":".$i." น." ;
                }
                function datetimesome($datetime1,$datetime2)
                {
                  $y1 = substr($datetime1,0,4);
                  $m1 = substr($datetime1,5,2);
                  $d1 = substr($datetime1,8,2);
                  $h1 = substr($datetime1,11,2);
                  $i1 = substr($datetime1,14,2);
                  $y2 = substr($datetime2,0,4);
                  $m2 = substr($datetime2,5,2);
                  $d2 = substr($datetime2,8,2);
                  $h2 = substr($datetime2,11,2);
                  $i2 = substr($datetime2,14,2);
                  return $d1."/".$m1."/".$y1." ถึง ".$d2."/".$m2."/".$y2." ช่วงเวลา ".$h1.":".$i1."น. ถึง ".$h2.":".$i2."น." ;
                }
              ?>
                <input id="date" name="date" type="hidden" class="form-control" value="{{$date}}">
                <input id="time" name="time" type="hidden" class="form-control" value="{{$time}}">
                  {{-- <div class="form-group row">
                    <label for="tigket" class="col-md-2 col-form-label">การเดินทาง </label>
                    <div class="col-md-10">
                      <select class="form-control" id="tigket" name="tigket">
                        <option value="0">เลือกการเดินทาง</option>
                        <option value="1">เที่ยวเดียว</option>
                        <option value="2">ไป-กลับ</option>
                      </select>
                    </div>
                  </div> --}}
                  <div class="form-group row">
                    <label for="bk_start_date" class="col-md-2 col-form-label">วันที่เดินทาง </label>
                    <div class="col-md-4" id="groupbk_start_date">
                      <input id="bk_start_date" name="bk_start_date" type="text" class="form-control bkDate" value="{{date("Y/m/d")}}">
                      <div hidden="true" id="fbbk_start_date" class="form-control-feedback"></div>
                    </div>
                    <label for="bk_start_start" class="col-md-1 col-form-label">เวลา </label>
                    <div class="col-md-2"  id="groupbk_start_start">
                      <input id="bk_start_start" name="bk_start_start" type="text" class="form-control bkTime" value="22:00">
                      <div hidden="true" id="fbbk_start_start" class="form-control-feedback"></div>
                    </div>
                    {{-- <label for="bk_start_end" class="col-md-1 col-form-label">ถึง </label>
                    <div class="col-md-2">
                      <input id="bk_start_end" name="bk_start_end" type="text" class="form-control bkTime" value="08:00" disabled>
                    </div> --}}
                  </div>

                  <div class="form-group row">
                      <label for="bk_end_date" class="col-md-2 col-form-label">วันที่ทางกลับ </label>
                      <div class="col-md-4" id="groupbk_end_date">
                        {{-- <input id="bk_end_date" name="bk_end_date" type="text" class="form-control bkDate" value="{{date("d/m/Y",strtotime("+1 day"))}}" readonly> --}}
                        <input id="bk_end_date" name="bk_end_date" type="text" class="form-control bkEndDate" value="{{date("Y/m/d")}}">
                        <div hidden="true" id="fbbk_end_date" class="form-control-feedback"></div>
                      </div>
                      <label for="bk_end_start" class="col-md-1 col-form-label">เวลา </label>
                      <div class="col-md-2" id="groupbk_end_start">
                        <input id="bk_end_start" name="bk_end_start" type="text" class="form-control bkTime" value="23:00">
                        <div hidden="true" id="fbbk_end_start" class="form-control-feedback"></div>
                      </div>
                      {{-- <label for="bk_end_end" class="col-md-1 col-form-label">ถึง </label>
                      <div class="col-md-2">
                        <input id="bk_end_end" name="bk_end_end" type="text" class="form-control bkTime" value="17:00" disabled>
                      </div> --}}
                  </div>
                <div class="form-group row">
                  <label for="bk_end_date" class="col-md-2 col-form-label"> จัดรถ </label>
                  <div class="col-md-3">
                    <select class="form-control" id="car" name="car">
                      <option value="" disabled>เลือกทะเบียนรถ</option>
                      <?php
                        $sql_car = DB::table('tb_car')->where('com_id','=',$com_id)->where('dep_id','=',$job_id)->get();
                        foreach ($sql_car as $car) {
                          echo "<option value='".$car->car_id."'>".$car->car_number."</option>";
                        }
                       ?>
                    </select>
                  </div>
                  <div class="col-md-4">
                    <select class="form-control" id="driver" name="driver">
                      <option value="" disabled>เลือกคนขับ</option>
                      <?php
                        $sql_driver = DB::table('tb_driver')->where('com_id','=',$com_id)->where('dep_id','=',$job_id)->where('drive_status','=','0')->get();
                        foreach ($sql_driver as $driver) {
                            echo " <option value='".$driver->drive_id."'>".$driver->drive_fname." ".$driver->drive_lname."</option>";
                        }
                       ?>
                    </select>
                  </div>
                </div>

                <div class="form-group row">
                  <label for="count_person" class="col-md-2 col-form-label">ผู้ร่วมเดินทาง </label>
                      <div class="input-group col-4">
                        <input id="count_person" name="count_person" type="number" class="form-control" value="0" min="0" max="100">
                       <span class="input-group-addon" id="btnGroupAddon">คน</span>
                     </div>
                </div>

                <div class="form-group row">
                    <label for="bk_where" class="col-md-2 col-form-label">สถานที่เริ่มต้น </label>
                    <div class="col-10">
                      <ul class="nav flex-column" id="bk_whereList">
                        <li>
                          <div class="form-group">
                            <input type="text" data-id="1"  name="bk_where1" id="bk_where1" onfocus=callmap(this);
                                   class="form-control" placeholder="กรุณากรอกสถานที่เริ่มต้น" value="บริษัท สยามราชธานี จำกัด (มหาชน)">
                          </div>
                        </li>
                        <label for="bk_where" class="col-md-2 col-form-label">สถานที่ไป </label>
                        <li>
                          <div class="form-group">
                              <input type="text" data-id="2"  name="bk_where2" id="bk_where2" onfocus=callmap(this);
                                     class="form-control" placeholder="กรุณากรอกสถานที่ไป1" disabled>
                          </div>
                        </li>
                        <li>
                          <button type="button" class="btn btn-sm btn-success btn-location"><span class="fa fa-plus"></span> เพิ่มสถานที่</button>
                          <button type="button" class="btn btn-sm btn-danger btn-relocate"><span class="fa fa-minus"></span> ลบสถานที่</button>
                        </li>
                      </ul>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="obj" class="col-md-2 col-form-label">วัตถุประสงค์ </label>
                    <div class="col-md-10">
                       <textarea id="obj" name="bk_obj" class="form-control" rows="5">ส่ง OT.</textarea>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="note" class="col-md-2 col-form-label">หมายเหตุ </label>
                    <div class="col-md-10">
                    <textarea id="note" name="bk_note" class="form-control" rows="5"></textarea>
                    </div>
                </div>

                <div class="form-group row">
                  <label for="count_person" class="col-md-2 col-form-label">ส่งพนักงานล่วงเวลา </label>
                  <div class="col-10">

                      <label class="custom-control custom-checkbox">
                        <input type="checkbox" id="overtime" name="overtime" class="custom-control-input" checked>
                        <span class="custom-control-indicator"></span>
                        <span class="custom-control-description" style="color:#000;">ส่งOT.</span>
                      </label>
                  </div>

                </div>

                <div class"form-group">
                  <div id="tblBk" class="table-responsive" Data-id="emp=<?php echo $emp_id;?>">
                  <label for="bookingOT">เลือกรายการส่งOT. </label>

                  <table class="table table-bordered tblBk">
                    <thead>

                      <th>เลขที่อ้างอิง</th>
                      <th>ผู้จอง</th>
                      <th>แผนก</th>
                      <th>วันที่ขอใช้รถ</th>
                      <th>ประเภทรถ</th>
                      <th></th>
                    </thead>
                    <tbody class="">
                      <?php

                        $sqlbooking_sql = DB::table('tb_booking')
                                      ->join("tb_car_type",function($join){
                                            $join->on("tb_booking.ctype_id","=","tb_car_type.ctype_id");
                                            $join->on("tb_booking.com_id","=","tb_car_type.com_id");
                                        })
                                      ->join('tb_employee', 'tb_employee.emp_id', '=' , 'tb_booking.emp_id')
                                      ->whereIn('tb_booking.bk_status', ['approve', 'merge'])
                                      ->where('tb_booking.bk_ot','=','on' )
                                      ->Where('tb_booking.bk_start_start','>=',date('Y-m-d').'%');
                        if ($emp_lv<9) {
                          $sqlbooking = $sqlbooking_sql->where('tb_booking.dep_car', '=', $job_id)
                          ->orderBy('bk_start_start')->get();
                        }
                        else {
                          $sqlbooking = $sqlbooking_sql->orderBy('bk_start_start')->get();
                        }

                        $numbooking = count($sqlbooking);
                      if($numbooking > 0){

                      ?>
                            @foreach ($sqlbooking as $bc)
                      <?php
                          $d1= new DateTime($bc->bk_start_start);
                          $d2 = new DateTime(date( "Y-m-d", strtotime("+1 days")));
                          $diff = $d1->diff($d2)->format("%a");

                          if($numbooking > 0){
                            if ($bc->bk_date > date("Y-m-d 15:30:00")) {
                              echo "<tr bgcolor='#ccc'>";
                            }
                            if ($bc->bk_date < date("Y-m-d 00:00:00", strtotime("+1 days"))) {
                              if ($diff==0) {
                                  echo "<tr>";
                              }elseif ($diff==1) {
                                  echo "<tr bgcolor='#fce4ec'>";
                              }elseif ($diff==2) {
                                  echo "<tr bgcolor='#fff8e1'>";
                              }elseif ($diff==3) {
                                  echo "<tr bgcolor='#e8f5e9'>";
                              }else {
                                echo "<tr bgcolor='#ccc'>";
                              }
                            }else {
                              echo "<tr bgcolor='#ccc'>";
                            }

                        ?>
                            <td class="detailBk text-black" data-id="bk=<?php echo $bc->bk_id;?>">
                              <?php echo $bc->bk_id ?></td>
                            <td class="detailBk text-black fsize" data-id="bk=<?php echo $bc->bk_id;?>">
                              <?php echo $bc->emp_fname." ".$bc->emp_lname; ?></td>
                              <td>
                                <?php
                                $sql_dep = DB::table('tb_department')->where('com_id','=',$com_id)->where('dep_id','=',$bc->dep_id)->get();
                                foreach ($sql_dep as $dep) {
                                  echo  $dep->dep_name;
                                }
                                ?>
                              </td>
                              <td class="detailBk text-black" width="40%" data-id="bk=<?php echo $bc->bk_id;?>">
                              <?php  if (!$bc->bk_use) {
                                echo datetime($bc->bk_start_start);echo " ถึง ";datetime($bc->bk_end_start);
                              } else {
                                echo datetimesome($bc->bk_start_start,$bc->bk_end_start);
                              }

                              $locate = DB::table('tb_booking_location')->where('bk_id','=',$bc->bk_id)->get();
                              foreach ($locate as $lo) {
                                $lo_id =$lo->location_id;
                                  if ($lo->location_id == "1") {
                                    echo "<br /><b>สถานที่เริ่มต้น</b> : ".$lo->location_name;
                                  }
                                  else {
                                    echo "<br /><b>สถานที่ ".($lo->location_id-1)."</b> : ".$lo->location_name;
                                  }
                                }
                               ?>
                               <br />
                               <b>วัตถุประสงค์</b> :
                               <?php
                                     if ($bc->bk_obj == "") {
                                       echo "-";
                                     }else {
                                       echo $bc->bk_obj ;
                                     }
                               ?>
                               <br>
                               <b>หมายเหตุ</b> :
                               <?php
                                     if ($bc->bk_note == "") {
                                       echo "-";
                                     }else {
                                       echo $bc->bk_note ;
                                     }
                               ?>
                               <br>
                               <b>จำนวนผู้เดินทาง</b> :{{$bc->bk_percon}} คน

                            </td>
                            <td class="detailBk text-black" width="10%" data-id="bk=<?php echo $bc->bk_id;?>">
                              {{$bc->ctype_name}}
                              <?php $job_car = DB::table('tb_job')->where('job_id','=',$bc->dep_car)->groupBy('job_name')->select('job_name')->get(); ?>
                              @foreach ($job_car as $job)
                                [{{$job->job_name}}]
                              @endforeach
                            </td>

                            <td align="center">

                              <div class="">
                                <label class="custom-control custom-checkbox">
                                  <input type="checkbox" id="" name="" class="custom-control-input overtime" value="{{$bc->bk_id}}">
                                  <span class="custom-control-indicator"></span>
                                  <span class="custom-control-description" style="color:#000;">เลือก</span>
                                </label>
                              </div>
                            </td>
                          </tr>
                          <?php }
                           elseif ($numbooking = 0){ ?>
                             <tr>
                                 <td colspan="7" align="center"><h5>ไม่พบกิจกรรม</h5></td>
                             </tr>
                           <?php }?>
                   @endforeach

                  <?php }else{  ?>
                            <tr>
                              <td colspan="7" align="center"><h5>ไม่พบกิจกรรม</h5></td>
                            </tr>
                    <?php } ?>
                      </tbody>
                  </table>

                  </div>
                </div>
                <input class="col-12" type="hidden" id="overtimei" name="overtimei">

                <div class="form-group row">
                    <div class="col-12" align="center">
                      <button type="submit" class="btn btn-success" id="btn_save" disabled>บันทึก</button>
                      <button type="reset" class="btn btn-danger">ยกเลิก</button>
                    </div>
                </div>

              </div>
                </div>


      </form>

    </div>
  </div>
</div>
</div>

<script>

  $(document).ready(function () {
    $(".times").hide();
    var start = $('#bk_start_start').val();
    var end = $('#bk_end_start').val();
    var now = $('#time').val();

    if (start<now) {
      $("#bk_where2").attr("disabled","disabled");
    }
    else {
      if (start>end) {
        $("#bk_where2").attr("disabled","disabled");
      }
      else {
        delDisabled("bk_where2");
      }
    }
    jQuery(".bkEndDate").datetimepicker({
            format:"Y/m/d",
            lang:"th",
            minDate:$('#bk_start_date').val(),
            timepicker:false,
            scrollInput:false
    });
  })

      $('#bk_start_date').change(function () {

            jQuery(".bkEndDate").datetimepicker({
                    format:"Y/m/d",
                    lang:"th",
                    minDate:$('#bk_start_date').val(),
                    timepicker:false,
                    scrollInput:false
            });
            ChkDateTime();
            rmErr("bk_start_date");
      })

      $('#bk_end_date').change(function () {
        ChkDateTime();
        rmErr("bk_end_date");
      })

      $('#bk_start_start').change(function () {
        ChkDateTime();
      })

      $('#bk_end_start').change(function () {
        ChkDateTime();
      })

      $('#textbox1').val($(this).is(':checked'));

      $('#times').change(function() {
          if($(this).is(":checked")) {
            ChkDateTime();
              // var returnVal = confirm("Are you sure?");
              // $(this).attr("checked", returnVal);
          }
          else {
            rmErr("bk_end_start");
          }
          // $('#times').val($(this).is(':checked'));
      });

      $('.overtime').change(function() {
        var a = $(this).val();
        if ($(this).is(':checked')) {
            if ($('#overtimei').val()=='') {
              $('#overtimei').val(a);
            }
            else {
              $('#overtimei').val($('#overtimei').val()+a);
            }
        }
        else {
            var b = $('#overtimei').val();
            var newstr1 = b.replace(a, '');
            $('#overtimei').val(newstr1);
        }
      })

      // $('#btn_save').click(function() {
      //     console.log('11');
      // })

      function ChkDateTime() {

          var start_date = $('#bk_start_date').val();
          var end_date = $('#bk_end_date').val();
          var start_time = $('#bk_start_start').val();
          var end_time = $('#bk_end_start').val();
          var today_date = $('#date').val();
          var today_time = $('#time').val();
          var sometimes = false;

          // วันเดียวกัน
            if (start_date == end_date) {
                $(".times").hide();
                      //วันนี้
                        if (start_date == today_date) {
                          if (start_time > today_time) {
                              rmErr("bk_start_start");
                             //เวลาไปมากกว่าเวลาจริง
                            if (start_time > end_time) {
                              //เวลาไปมากกว่าเวลากลับ
                              addErr("bk_end_start","เวลาไม่ถูกต้อง")
                              $("#bk_where2").attr("disabled","disabled");
                            }//start_time > end_time
                            else {
                              //เวลาไปน้อยกว่าเวลากลับ
                              delDisabled("bk_where2");
                              rmErr("bk_end_start");
                            }//start_time > end_time
                          }//start_time > today_time
                          else {
                            //เวลาไปน้อยกว่าเวลาจริง
                            addErr("bk_start_start","เวลาไม่ถูกต้อง")
                            $("#bk_where2").attr("disabled","disabled");
                          }//start_time > today_time
                        }//start_date == today_date
                      //วันนี้

                      //วันอื่น
                        else {
                          if (start_time > end_time) {
                            //เวลาไปมากกว่าเวลากลับ
                            addErr("bk_end_start","เวลาไม่ถูกต้อง")
                            $("#bk_where2").attr("disabled","disabled");
                          }//start_time > end_time
                          else {
                            //เวลาไปน้อยกว่าเวลากลับ
                            delDisabled("bk_where2");
                            rmErr("bk_end_start");
                          }//start_time > end_time
                        }
                      //วันอื่น
                    }

          // วันเดียวกัน

          //คนละวัน
            else {
              $(".times").show();
                  if (!sometimes) {
                    if(start_date < end_date){
                          //เริ่มวันนี้
                            if (start_date == today_date) {
                              if (start_time > today_time) {
                                //เวลาไปมากกว่าเวลาจริง
                                  delDisabled("bk_where2");
                                  rmErr("bk_start_start");
                                // }//start_time > end_time
                              }//start_time > end_time
                              else {
                                //เวลาไปน้อยกว่าเวลาจริง
                                addErr("bk_start_start","เวลาไม่ถูกต้อง")
                                $("#bk_where2").attr("disabled","disabled");

                              }//start_time > end_time
                            }//start_date == today_date
                          //เริ่มวันนี้

                          //เริ่มวันอื่น
                            else {
                                delDisabled("bk_where2");
                                rmErr("bk_start_start");
                                rmErr("bk_end_start");
                            }//start_date == today_date
                          //เริ่มวันอื่น
                        }//start_date < end_date
                    else {
                      //ย้อนเวลา
                      addErr("bk_end_date","วันที่ไม่ถูกต้อง")
                      $("#bk_where2").attr("disabled","disabled");
                    }//start_date < end_date
                  }//allday
                  else {
                    if(start_date < end_date){
                          //เริ่มวันนี้
                            if (start_date == today_date) {
                              if (start_time > today_time) {
                                //เวลาไปมากกว่าเวลาจริง
                                  rmErr("bk_start_start");
                                  if (start_time > end_time) {
                                    //เวลาไปมากกว่าเวลากลับ
                                    addErr("bk_end_start","เวลาไม่ถูกต้อง")
                                    $("#bk_where2").attr("disabled","disabled");
                                  }//start_time > end_time
                                  else {
                                    //เวลาไปน้อยกว่าเวลากลับ
                                    delDisabled("bk_where2");
                                    rmErr("bk_end_start");
                                  }//start_time > end_time
                                // }//start_time > end_time
                              }//start_time > end_time
                              else {
                                //เวลาไปน้อยกว่าเวลาจริง
                                addErr("bk_start_start","เวลาไม่ถูกต้อง")
                                $("#bk_where2").attr("disabled","disabled");

                              }//start_time > end_time
                            }//start_date == today_date
                          //เริ่มวันนี้

                          //เริ่มวันอื่น
                            else {
                                delDisabled("bk_where2");
                                rmErr("bk_start_start");
                                rmErr("bk_end_start");
                            }//start_date == today_date
                          //เริ่มวันอื่น
                        }//start_date < end_date
                    else {
                      //ย้อนเวลา
                      addErr("bk_end_date","วันที่ไม่ถูกต้อง")
                      $("#bk_where2").attr("disabled","disabled");
                    }//start_date < end_date
                  }//sometime
            }//start_date == today_date
          //คนละวัน

      }
  // ==========================================================================================

  $("#bk_where2").keyup(function () {
    delDisabled("btn_save");
  })

  $("#tigket").change(function(){
    var tigketVal = $(this).val();
    if(tigketVal == 1){
      delDisabled("bk_start_date");
      delDisabled("bk_start_start");
      delDisabled("bk_start_end");
      $("#bk_end_date").attr("disabled","disabled");
      $("#bk_end_start").attr("disabled","disabled");
      $("#bk_end_end").attr("disabled","disabled");
    }else if(tigketVal == 2){
      delDisabled("bk_start_date");
      delDisabled("bk_start_start");
      delDisabled("bk_start_end");

      delDisabled("bk_end_date");
      delDisabled("bk_end_start");
      delDisabled("bk_end_end");
    }
    else {
      $("#bk_start_date").attr("disabled","disabled");
      $("#bk_start_start").attr("disabled","disabled");
      $("#bk_start_end").attr("disabled","disabled");

      $("#bk_end_date").attr("disabled","disabled");
      $("#bk_end_start").attr("disabled","disabled");
      $("#bk_end_end").attr("disabled","disabled");
    }
  });

  function delDisabled(id){
    $("#"+id).removeAttr("disabled");
  }

  jQuery(".bkDate").datetimepicker({
          format:"Y/m/d",
          lang:"th",
          minDate:'-1970/01/01',
          timepicker:false,
          scrollInput:false
  });

  jQuery(".bkTime").datetimepicker({
          format:"H:i",
          datepicker:false,
          scrollInput:false,
  });

  // ==========================================================================================

  $(".btn-location").click(function(){
      var num = $("#bk_whereList li").length;
       $("#bk_whereList li:last").before("<li>"
                                        +"<div class='form-group'>"
                                        +"<input type='text' class='form-control' id='bk_where"+num+"' name='bk_where"+num+"' onfocus='callmap(this);' "
                                        +"placeholder='กรุณากรอกสถานที่ไป"+(num-1)+"'>"
                                        +"</div>"
                                        +"</li>");
  });

  $(".btn-relocate").click(function(){
    var num = ($("#bk_whereList li").length)-2;

    if(num > 0){
        $("#bk_whereList li:eq("+num+")").remove();
    }

  });

  // ==========================================================================================

  $("form#frm_booking").submit(function(ev){
    ev.preventDefault();
    var formData = new FormData(this);
    // for (var value of formData.values()) {
    //          console.log(value);
    //       }
    var start_date = $('#bk_start_date').val();
    var end_date = $('#bk_end_date').val();
    var start_time = $('#bk_start_start').val();
    var end_time = $('#bk_end_start').val();
    var today_date = $('#date').val();
    var today_time = $('#time').val();

    // วันเดียวกัน
      if (start_date == end_date) {

                //วันนี้
                  if (start_date == today_date) {
                    if (start_time > today_time) { //เวลาไปมากกว่าเวลาจริง
                        rmErr("bk_start_start");
                      if (start_time > end_time) {
                        //เวลาไปมากกว่าเวลากลับ
                        addErr("bk_end_start","เวลาไม่ถูกต้อง")
                      }//start_time > end_time
                      else {
                        //เวลาไปน้อยกว่าเวลากลับ
                        sendBookingDB(formData)
                      }//start_time > end_time
                    }//start_time > today_time
                    else {
                      //เวลาไปน้อยกว่าเวลาจริง
                      addErr("bk_start_start","เวลาไม่ถูกต้อง")
                    }//start_time > today_time
                  }//start_date == today_date
                //วันนี้

                //วันอื่น
                  else {
                    if (start_time > end_time) {
                      //เวลาไปมากกว่าเวลากลับ
                      addErr("bk_end_start","เวลาไม่ถูกต้อง")
                      $("#bk_where2").attr("disabled","disabled");
                    }//start_time > end_time
                    else {
                      //เวลาไปน้อยกว่าเวลากลับ
                      rmErr("bk_end_start");
                      sendBookingDB(formData)
                    }//start_time > end_time
                  }
                //วันอื่น
              }
    // วันเดียวกัน

    //คนละวัน
      else {
        if (!sometimes) {
          if(start_date < end_date){
                //เริ่มวันนี้
                  if (start_date == today_date) {
                    if (start_time > today_time) {
                      //เวลาไปมากกว่าเวลาจริง
                        sendBookingDB(formData)
                        rmErr("bk_start_start");
                    }//start_time > end_time
                    else {
                      //เวลาไปน้อยกว่าเวลาจริง
                        addErr("bk_start_start","เวลาไม่ถูกต้อง")
                    }//start_time > end_time
                  }//start_date == today_date
                //เริ่มวันนี้

                //เริ่มวันอื่น
                  else {
                      sendBookingDB(formData)
                  }//start_date == today_date
                //เริ่มวันอื่น
              }//start_date < end_date
          else {
            //ย้อนเวลา
            addErr("bk_end_date","วันที่ไม่ถูกต้อง")
          }//start_date < end_date
        }//allDay
        else {
          if(start_date < end_date){
                //เริ่มวันนี้
                  if (start_date == today_date) {
                    if (start_time > today_time) {
                      //เวลาไปมากกว่าเวลาจริง
                        rmErr("bk_start_start");
                        if (start_time > end_time) {
                          //เวลาไปมากกว่าเวลากลับ
                          addErr("bk_end_start","เวลาไม่ถูกต้อง")
                          $("#bk_where2").attr("disabled","disabled");
                        }//start_time > end_time
                        else {
                          //เวลาไปน้อยกว่าเวลากลับ
                          rmErr("bk_end_start");
                          sendBookingDB(formData)
                        }//start_time > end_time
                    }//start_time > end_time
                    else {
                      //เวลาไปน้อยกว่าเวลาจริง
                        addErr("bk_start_start","เวลาไม่ถูกต้อง")
                    }//start_time > end_time
                  }//start_date == today_date
                //เริ่มวันนี้

                //เริ่มวันอื่น
                  else {
                      sendBookingDB(formData)
                  }//start_date == today_date
                //เริ่มวันอื่น
              }//start_date < end_date
          else {
            //ย้อนเวลา
            addErr("bk_end_date","วันที่ไม่ถูกต้อง")
          }//start_date < end_date
        }


      }//start_date == today_date
    //คนละวัน
  })

  function sendBookingDB(formData) {
          $.ajax({
            url:"/InsertBusOT",
            type:"POST",
            data:formData,
            // data:formData+"&count_where="+count_where,
            async: true,
            cache: false,
            contentType: false,
            processData: false,
            success:function(data){

              var obj = JSON.parse(data);
                if (obj['success']==true) {
                  var formSend = $('#frm_booking').serialize();
                  var count_where = ($("#bk_whereList li").length)-1;
                  $.ajax({
                    url:"/insertsLocation",
                    type:"POST",
                    data:formSend+"&count_where="+count_where+"&id="+obj['bk_id'],
                    success:function(data1){
                      var obj1 = JSON.parse(data);
                      if (obj1['success']==true) {
                        console.log(obj['bk_id']);
                        console.log(obj['bk_merge']);
                      //wait email

                          swal({
                            title: "การจองสำเร็จ",
                            type: "success",
                            text: "ระบบกำลังส่ง Email แจ้งเตือน กรุณารอสักครู่",
                            timer: 5000,
                            showConfirmButton: false
                          },function () {
                             window.location = "/dashboard";
                          });

                          $.ajax({url:"/sendcarOT",data:"id="+obj['bk_merge'],type:"POST",success:function(data2){
                            console.log(data2);
                            // var obj2 = JSON.parse(data2);
                            // console.log(obj2);
                            //   if (obj['success']==true) {
                            //
                            //     }
                              }
                            })

                      //wait email
                      }
                    }
                  })// insert

                       //  $.ajax({
                       //   url:"/locateDB",
                       //   type:"GET",
                       //   data:"id="+obj['bk_id'],
                       //   contentType: false,
                       //   processData: false,
                       //   success:function(datalocate){
                       //     var locate = JSON.parse(datalocate);
                       //     var id = obj['bk_id'];
                       //     var obj_locate = [];

                       //       for (var i = 0; i < locate.length; i++)
                       //       {
                       //         obj_locate.push(locate[i]["name"]);
                       //       }

                       //       for (var i = 0; i < obj_locate.length; i++) {
                       //                var objall = GetValue(obj_locate[i],function (address,latitude,longitude)
                       //              {
                       //                //  console.log("location="+address+"&latitude="+latitude+"&longitude="+longitude+"&id="+id);
                       //                  $.ajax({
                       //                    url:"/location",
                       //                    data:"location_id="+address+"&latitude="+latitude+"&longitude="+longitude+"&id="+id,
                       //                    type: "POST",
                       //                    success: function (datalatlng) {
                       //                      // console.log(datalatlng);
                       //                          //  var obj = JSON.parse(locations);
                       //                          //  console.log(obj);
                       //                                                                           }


                       //                  })
                       //              });
                       //        }
                       //     }
                       // })
                }
                else {
                  swal({
                            title: "ผิดพลาด",
                            text: obj['msg'],
                            type: "error",
                            showCancelButton: false,
                            confirmButtonColor: "#2ECC71",
                            confirmButtonText: "ตกลง",
                            closeOnConfirm: true,

                      });
                }
              }
          })

  };

  function rmErr(input){
    $("#group"+input).removeClass("has-danger");
    $("#group"+input+" input").removeClass("form-control-danger");
    $("#fb"+input).attr("hidden","hidden");
  }

  function addErr(type,msg){
      $("#group"+type).addClass("has-danger");
      $("#group"+type+" input").addClass("form-control-danger");
      $("#fb"+type).html(msg);
      $("#fb"+type).removeAttr("hidden");
  }

  // ==========================================================================================

function callmap(e){
  initAutocomplete(e.id);
}
function initAutocomplete(id){
  if(typeof id == "undefined"){
    id = "bk_where1";
  }
  autocomplete = new  google.maps.places.Autocomplete((document.getElementById(id)),{types: ['geocode']});
  autocomplete.addListener('place_changed',fillInAddress);
}
function fillInAddress() {
    // var place = autocomplete.getPlace();
    // var address = place.formatted_address;
}

function GetValue(address,callback) {
  var geocoder = new google.maps.Geocoder();
  var  latitude ="";
  var  longitude ="";
  geocoder.geocode( { 'address': address }, function(results, status) {
    if (status == google.maps.GeocoderStatus.OK) {
       latitude = results[0].geometry.location.lat();
       longitude = results[0].geometry.location.lng();
       }
      callback(address,latitude,longitude);
   })
}

</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAaXMuANTFVYidZ3gQaPaUVQU4zeEmk33U&libraries=places&callback=initAutocomplete" async defer></script>

@endsection
