@extends('welcome')
@section('content')
@include('dashboard.topNavbar')
@include('dashboard.SideNavbar')

@foreach ($users as $r)
  <?php
        $lv_user = $r->emp_level;
        $id = $r->emp_id;
        $com_id = $r->com_id;
        $dep_id = $r->job_id;
   ?>
@endforeach
<div class="container-dashboard">

<div class="card col-md-12 row">
<input type="hidden"  id="emp_id" value="{{$id}}">
<input type="hidden"  id="com_id" value="{{$com_id}}">
<input type="hidden"  id="lv_user" value="{{$lv_user}}">
<input type="hidden"  id="dep_id" value="{{$dep_id}}">

  <div class="card-header">
    <div class="row">
    <span class="fa fa-address-book-o text-black mr-auto"> คนขับรถ</span>
    <?php if ($lv_user =="2" OR $lv_user =="99" OR $lv_user =="999") {?>
      <button type="button" class='btn btn-success btn-sm' id="adddriver">เพิ่มข้อมูล</button>
    <?php  } ?>
  </div>
  </div>

  <div class="table-resposive" id="table_car">
  <nav class="navbar navbar-light bg-faded">

    <form class="form-inline" id="frmSearch_car">

      <div class="form-group">
        <select class="form-control mr-sm-2"  id="typeSearch" name="typeSearch">
          <option value="0">ค้นหาทั้งหมด</option>
          <option value="1">ชื่อ</option>
          <option value="2">เบอร์โทรศัพท์</option>
          <option value="3">สถานะการใช้งาน</option>
         </select>
      </div>
      <div class="form-group" id="groupTxt" >
        <div class="input-group my-2 my-sm-0">
          <input type="text" class="form-control" id="txtSearch" name="txtSearch" placeholder="ค้นหาทั้งหมด" disabled>
          <span class="input-group-btn">
            <button class="btn btn-primary btnSearch_car" type="button"><span class="fa fa-search"></span></button>
          </span>
        </div>
      </div>
      <div class="form-group" id="groupstatus" style="display:none" >
        <div class="input-group my-2 my-sm-0">
        <select class="form-control"  id="drive_status" name="drive_status">
            <option value='0'>ใช้งาน</option>
            <option value='1'>ยกเลิกการใช้งาน</option>
           ?>
        </select>
          <span class="input-group-btn">
            <button class="btn btn-primary btnSearch_car" type="button"><span class="fa fa-search"></span></button>
          </span>
        </div>
      </div>
    </form>

  </nav>
    <table class="table table-bordered">
      <thead>
        <th colspan="2"></th>
      </thead>
        <tbody id="tbody_driver">
        </tbody>
    </table>
  </div>

  <div class="modal-area"></div>
</div>
</div>
<script>

    $(document).ready(function(){
      var lv_user =$('#lv_user').val();
      var dep_id =$('#dep_id').val();
      var data = "typeSearch=0&com_id="+$('#com_id').val()+"&lv_user="+lv_user+"&dep_id="+dep_id;
      callTbl_driver(data);
    });

    $("#typeSearch").change(function(){
      var typeVal = $(this).val();
      // console.log(typeVal);
      if(typeVal == 1){
        $("#txtSearch").attr("placeholder","ชื่อ");
        delDisabled("txtSearch");
        $("#groupstatus").hide();
        $("#groupTxt").show();
      }else if(typeVal == 2){
        $("#txtSearch").attr("placeholder","เบอร์โทรศัพท์");
        delDisabled("txtSearch");
        $("#groupstatus").hide();
        $("#groupTxt").show();
      }else if (typeVal == 3) {
        $("#groupTxt").hide();
        $("#groupstatus").show();
      }
      else{
        $("#txtSearch").attr("placeholder","ค้นหาทั้งหมด");
        $("#txtSearch").attr("disabled","disabled");
        $("#groupstatus").hide();
        $("#groupTxt").show();
      }
    });

    $("#adddriver").click(function(){
      var id =$('#emp_id').val();
      var com_id =$('#com_id').val();
      var dep_id =$('#dep_id').val();

     $.ajax({
              url:"/adddriver",
              data:"id="+id+"&com_id="+com_id+"&lv_user="+lv_user+"&dep_id="+dep_id,
              type:"GET",
              success:function(data){
              $(".modal-area").html(data);
              $("#modalBk").modal("show");
            }
        });
    });

    function delDisabled(id){
      $("#"+id).removeAttr("disabled");
    }

      $('#txtSearch').keypress(function(event){
          if(event.keyCode == '13'){
            var lv_user =$('#lv_user').val();
            var dep_id =$('#dep_id').val();
            var data = $("#frmSearch_car").serialize()+"&lv_user="+lv_user+"&dep_id="+dep_id;
            callTbl_driver(data);
            event.preventDefault();
          }
    });
    $(".btnSearch_car").click(function(){
      var lv_user =$('#lv_user').val();
      var dep_id =$('#dep_id').val();
      var data = $("#frmSearch_car").serialize()+"&com_id="+$('#com_id').val()+"&lv_user="+lv_user+"&dep_id="+dep_id;
      callTbl_driver(data);
    });

  function callTbl_driver(dataSearch){
    $.ajax({
              url:"/tabledriver",
              data:dataSearch,
              type:"GET",
              success:function(data){
               $("#tbody_driver").html(data);

               $(".DriverDetail").click(function () {
                 var id = $(this).data("id");
               })

               $(".btn-driver").click(function () {
                 var detail = $(this).data("id");
                 $.ajax({
                           url:"/ejectdriver",
                           data:"id="+detail,
                           type:"GET",
                           success:function(data){
                             var obj = JSON.parse(data);
                               if (obj['success']==true) {
                                 swal({
                                   type:"success",
                                   title:"สำเร็จ",
                                   text:"คุณทำการบันทึกสำเร็จ",
                                   confirmButtonText:"ตกลง",
                                   confirmButtonColor:"#2ECC71",
                                   closeOnConfirm:true,
                                 },function(isConfirm){
                                   location.reload();
                                 });
                               }
                           }
                   });
               })

              }
      });
  }





</script>
@endsection
