@extends('welcome')
@extends('dashboard.topNavbar')
@extends('dashboard.SideNavbar')

@section('content')
  <link rel="stylesheet" type="text/css" href="{{ asset('js/datetimepicker/jquery.datetimepicker.css')}}"/>
  <script src="{{asset('js/datetimepicker/jquery.js')}}"></script>
  <script src="{{asset('js/datetimepicker/build/jquery.datetimepicker.full.min.js')}}"></script>
  <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
@foreach ($users as $r)
  <?php
        $emp_id = $r->emp_id;
        $dep_id = $r->dep_id;
        $job_id = $r->job_id;
        $com_id = $r->com_id;
        $emp_tel = $r->emp_tel;
        $emp_table = $r->emp_table;
   ?>
@endforeach
<div class="container-dashboard">

<div class="card  col-md-12 row">
  <div class="card-header">
    <span class="fa fa-plus" style="color:#000;">  เพิ่มการจอง</span>
  </div>
  <div style="padding:10px">
      <form id="frm_booking" method="post">
            <div class="card">
              <div class="card-block ">
                <input id="emp_id" name="emp_id" type="hidden" class="form-control" value="{{$emp_id}}">
                <input id="com_id" name="com_id" type="hidden" class="form-control" value="{{$com_id}}">
                <input id="com_id" name="job_id" type="hidden" class="form-control" value="{{$job_id}}">
              <?php
                date_default_timezone_set("Asia/Bangkok");
                $date = date("Y/m/d");
                $time =date("H:i");
              ?>
                <input id="date" name="date" type="hidden" class="form-control" value="{{$date}}">
                <input id="time" name="time" type="hidden" class="form-control" value="{{$time}}">
                  {{-- <div class="form-group row">
                    <label for="tigket" class="col-md-2 col-form-label">การเดินทาง </label>
                    <div class="col-md-10">
                      <select class="form-control" id="tigket" name="tigket">
                        <option value="0">เลือกการเดินทาง</option>
                        <option value="1">เที่ยวเดียว</option>
                        <option value="2">ไป-กลับ</option>
                      </select>
                    </div>
                  </div> --}}
                  <div class="form-group row">
                    <label for="bk_start_date" class="col-md-2 col-form-label">วันที่เดินทาง </label>
                    <div class="col-md-4" id="groupbk_start_date">
                      <input id="bk_start_date" name="bk_start_date" type="text" class="form-control bkDate" value="{{date("Y/m/d")}}">
                      <div hidden="true" id="fbbk_start_date" class="form-control-feedback"></div>
                    </div>
                    <label for="bk_start_start" class="col-md-1 col-form-label">เวลา </label>
                    <div class="col-md-2"  id="groupbk_start_start">
                      <input id="bk_start_start" name="bk_start_start" type="text" class="form-control bkTime" value="08:00">
                      <div hidden="true" id="fbbk_start_start" class="form-control-feedback"></div>
                    </div>
                    {{-- <label for="bk_start_end" class="col-md-1 col-form-label">ถึง </label>
                    <div class="col-md-2">
                      <input id="bk_start_end" name="bk_start_end" type="text" class="form-control bkTime" value="08:00" disabled>
                    </div> --}}

                  </div>

                  <div class="form-group row">
                      <label for="bk_end_date" class="col-md-2 col-form-label">วันที่ทางกลับ </label>
                      <div class="col-md-4" id="groupbk_end_date">
                        {{-- <input id="bk_end_date" name="bk_end_date" type="text" class="form-control bkDate" value="{{date("d/m/Y",strtotime("+1 day"))}}" readonly> --}}
                        <input id="bk_end_date" name="bk_end_date" type="text" class="form-control bkEndDate" value="{{date("Y/m/d")}}">
                        <div hidden="true" id="fbbk_end_date" class="form-control-feedback"></div>
                      </div>
                      <label for="bk_end_start" class="col-md-1 col-form-label">เวลา </label>
                      <div class="col-md-2" id="groupbk_end_start">
                        <input id="bk_end_start" name="bk_end_start" type="text" class="form-control bkTime" value="12:00">
                        <div hidden="true" id="fbbk_end_start" class="form-control-feedback"></div>
                      </div>
                      {{-- <label for="bk_end_end" class="col-md-1 col-form-label">ถึง </label>
                      <div class="col-md-2">
                        <input id="bk_end_end" name="bk_end_end" type="text" class="form-control bkTime" value="17:00" disabled>
                      </div> --}}
                  </div>

                  <div class="form-group row times">
                    <div class="col-md-10 offset-md-2">
                      <label class="custom-control custom-checkbox">
                        <input type="checkbox" id="times" name="times" class="custom-control-input">
                        <span class="custom-control-indicator"></span>
                        <span class="custom-control-description" style="color:#000;">เฉพาะช่วงเวลา</span>
                      </label>
                    </div>
                  </div>

                  <div class="form-group row">
                      <label for="ctype" class="col-md-2 col-form-label">ประเภทรถ </label>
                      <div class="col-md-10">
                        <select id="ctype" name="ctype" class="form-control">
                          <?php
                              $sqlcar_type = DB::table('tb_car_type')->where('com_id','=',$com_id)->get();

                            ?>
                            @foreach ($sqlcar_type as $type)
                              <option value="{{$type->ctype_id}}">{{$type->ctype_name}}</option>
                            @endforeach


                        </select>
                      </div>
                  </div>

                  <div class="form-group row">
                      <label for="cdep" class="col-md-2 col-form-label">รถที่ต้องการใช้ </label>
                      <div class="col-md-10">
                        <select id="cdep" name="cdep" class="form-control">
                          <?php
                              $sqlcar_dep = DB::table('tb_car')->where('com_id','=',$com_id)->groupBy('dep_id')->select('dep_id')->get();

                            ?>
                            @foreach ($sqlcar_dep as $car_dep)
                              <?php $tb_department = DB::table('tb_job')->where('com_id','=',$com_id)
                                                    ->where('job_id','=',$car_dep->dep_id)->groupBy('job_id','job_name')->select('job_id','job_name')->get()
                              ?>
                              @foreach ($tb_department as $dep_n)
                                <option value="{{$dep_n->job_id}}">{{$dep_n->job_name}}</option>
                              @endforeach
                            @endforeach


                        </select>
                      </div>
                  </div>

                  <div class="form-group row">
                    <label for="occode" class="col-md-2 col-form-label">แผนก</label>
                    <?php
                        $sqldep = DB::table('tb_department')->where('dep_id','=',$dep_id)->get();
                        foreach ($sqldep as $dep):
                          $tel_dep = $dep->dep_tel;
                          $dep_name = $dep->dep_name;
                        endforeach
                     ?>
                    <div class="col-md-10">
                      <input id="dep_id" name="dep_id" type="hidden" class="form-control" value="{{$dep_id}}" readonly="readonly">
                      <input id="dep_name" name="dep_name" type="text" class="form-control" value="{{$dep_name}}" readonly="readonly">
                    </div>
                  </div>

                  {{-- <div class="form-group row">
                    <label for="tel" class="col-md-2 col-form-label">เบอร์โทรศัพท์ </label>
                    <div class="col-md-10"> --}}
                      <?php
                      // $sqldep = DB::table('tb_department')->where('dep_id','=',$dep_id)->get();
                      // foreach ($sqldep as $dep):
                      //   $tel_dep = $dep->dep_tel;
                      // endforeach
                      ?>
                      {{-- <input id="tel" name="tel" type="text" class="form-control" value="{{$tel_dep}}" readonly="true">
                    </div>
                  </div> --}}
                  <div class="form-group row">
                    <label for="site" class="col-md-2 col-form-label">รหัสไซต์งาน</label>
                    <div class="col-md-10">
                      <input id="site" name="site" type="text" class="form-control" placeholder="กรุณากรอกรหัสไซต์งาน" >
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="tel" class="col-md-2 col-form-label">เบอร์ภายใน<label class="text-red">*</label></label>
                    <div class="col-md-10">
                      <input id="ttel" name="ttel" type="text" class="form-control" value="{{$emp_table}}" placeholder="กรอกเบอร์โต๊ะ" >
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="tel" class="col-md-2 col-form-label">มือถือ<label class="text-red">*</label></label>
                    <div class="col-md-10">
                      <input id="mtel" name="mtel" type="text" class="form-control" value="{{$emp_tel}}" placeholder="กรอกเบอร์โทรศัพท์มือถือ" >
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="count_person" class="col-md-2 col-form-label">ผู้ร่วมเดินทาง </label>
                        <div class="input-group col-4">
                          <input id="count_person" name="count_person" type="number" class="form-control" value="0" min="0" max="100">
                         <span class="input-group-addon" id="btnGroupAddon">คน</span>
                       </div>
                </div>

                <div class="form-group row">
                    <label for="bk_where" class="col-md-2 col-form-label">สถานที่เริ่มต้น </label>
                    <div class="col-10">
                      <ul class="nav flex-column" id="bk_whereList">
                        <li>
                          <div class="form-group">
                            <input type="text" data-id="1"  name="bk_where1" id="bk_where1"
                                   class="form-control" placeholder="กรุณากรอกสถานที่เริ่มต้น" value="บริษัท สยามราชธานี จำกัด (มหาชน)">
                                   <!-- <input type="text" data-id="1"  name="bk_where1" id="bk_where1" onfocus=callmap(this);
                                   class="form-control" placeholder="กรุณากรอกสถานที่เริ่มต้น" value="บริษัท สยามราชธานี จำกัด"> -->
                          </div>
                        </li>
                        <label for="bk_where" class="col-md-2 col-form-label">สถานที่ไป </label>
                        <li>
                          <div class="form-group">
                              <input type="text" data-id="2"  name="bk_where2" id="bk_where2"
                                     class="form-control" placeholder="กรุณากรอกสถานที่ไป1" disabled>
                                     <!-- <input type="text" data-id="2"  name="bk_where2" id="bk_where2" onfocus=callmap(this);
                                     class="form-control" placeholder="กรุณากรอกสถานที่ไป1" disabled> -->
                          </div>
                        </li>
                        <li>
                          <button type="button" class="btn btn-sm btn-success btn-location"><span class="fa fa-plus"></span> เพิ่มสถานที่</button>
                          <button type="button" class="btn btn-sm btn-danger btn-relocate"><span class="fa fa-minus"></span> ลบสถานที่</button>
                        </li>
                      </ul>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="obj" class="col-md-2 col-form-label">วัตถุประสงค์ </label>
                    <div class="col-md-10">
                       <textarea id="obj" name="bk_obj" class="form-control" rows="5"></textarea>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="note" class="col-md-2 col-form-label">หมายเหตุ </label>
                    <div class="col-md-10">
                    <textarea id="note" name="bk_note" class="form-control" rows="5"></textarea>
                    </div>
                </div>

                <div class="form-group row">
                  <label for="count_person" class="col-md-2 col-form-label">เอกสารการจอง </label>
                  <div class="col-10">
                  {{-- <div class="input-group">
                    <span class="input-group-btn">
                      <label class="btn btn-primary btn-file">
                          <span class="fa fa-file"></span> --}}
                          {{-- <input type="file" id="bkFile"  name="bkFile"> --}}
                          <input type="file" id="image" name="image">
                      {{-- </label>
                    </span>
                    <input type="text" class="form-control" id="bkFileName" placeholder="เอกสารการจอง">
                  </div> --}}
                  </div>
                </div>

                <div class="form-group row">
                  <label for="count_person" class="col-md-2 col-form-label">ส่งพนักงานล่วงเวลา </label>
                  <div class="col-10">

                      <label class="custom-control custom-checkbox">
                        <input type="checkbox" id="overtime" name="overtime" class="custom-control-input">
                        <span class="custom-control-indicator"></span>
                        <span class="custom-control-description" style="color:#000;">ส่งOT.</span>
                      </label>
                  </div>

                </div>


                <div class="form-group row">
                    <div class="col-12" align="center">
                      <button type="submit" class="btn btn-success" id="btn_save" disabled>บันทึก</button>
                      <button type="reset" class="btn btn-danger">ยกเลิก</button>
                    </div>
                </div>

              </div>
            </div>
      </form>
    </div>
  </div>
</div>

<script>

$(function() {

  // We can attach the `fileselect` event to all file inputs on the page
  // $(document).on('change', ':file', function() {
  //   var input = $(this),
  //       numFiles = input.get(0).files ? input.get(0).files.length : 1,
  //       label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
  //   input.trigger('fileselect', [numFiles, label]);
  // });
  // We can watch for our custom `fileselect` event like this
  // $(document).ready( function() {
  //     $(':file').on('fileselect', function(event, numFiles, label) {
  //         var input = $(this).parents('.input-group').find(':text'),
  //             log = numFiles > 1 ? numFiles + ' files selected' : label;
  //         if( input.length ) {
  //             input.val(log);
  //         } else {
  //             if( log ) alert(log);
  //         }
  //     });
  // });
  });

  $(document).ready(function () {
    $(".times").hide();
    var start = $('#bk_start_start').val();
    var end = $('#bk_end_start').val();
    var now = $('#time').val();

    if (start<now) {
      $("#bk_where2").attr("disabled","disabled");
    }
    else {
      if (start>end) {
        $("#bk_where2").attr("disabled","disabled");
      }
      else {
        delDisabled("bk_where2");
      }
    }
    jQuery(".bkEndDate").datetimepicker({
            format:"Y/m/d",
            lang:"th",
            minDate:$('#bk_start_date').val(),
            timepicker:false,
            scrollInput:false
    });
  })

      // $('#bk_start_date').change(function () {

      //       jQuery(".bkEndDate").datetimepicker({
      //               format:"Y/m/d",
      //               lang:"th",
      //               minDate:$('#bk_start_date').val(),
      //               timepicker:false,
      //               scrollInput:false
      //       });
      //       ChkDateTime();
      //       rmErr("bk_start_date");
      // })

      // $('#bk_end_date').change(function () {
      //   ChkDateTime();
      //   rmErr("bk_end_date");
      // })

      // $('#bk_start_start').change(function () {
      //   ChkDateTime();
      // })

      // $('#bk_end_start').change(function () {
      //   ChkDateTime();
      // })

      $('#bk_start_date').change(function () {

            jQuery(".bkEndDate").datetimepicker({
                    format:"Y/m/d",
                    lang:"th",
                    minDate:$('#bk_start_date').val(),
                    timepicker:false,
                    scrollInput:false
            });
            ChkDateTime();
            rmErr("bk_start_date");
      })

      $('#bk_end_date').change(function () {
        ChkDateTime();
        rmErr("bk_end_date");
      })

      $('#bk_start_start').change(function () {
        ChkDateTime();
        rmErr("bk_start_start");
      })

      $('#bk_end_start').change(function () {
        ChkDateTime();
        // rmErr("bk_end_start");
      })

      $('#textbox1').val($(this).is(':checked'));

      $('#times').change(function() {
          if($(this).is(":checked")) {
            ChkDateTime();
              // var returnVal = confirm("Are you sure?");
              // $(this).attr("checked", returnVal);
          }
          else {
            rmErr("bk_end_start");
          }
          // $('#times').val($(this).is(':checked'));
      });

      function ChkDateTime() {

          var start_date = $('#bk_start_date').val();
          var end_date = $('#bk_end_date').val();
          var start_time = $('#bk_start_start').val();
          var end_time = $('#bk_end_start').val();
          var today_date = $('#date').val();
          var today_time = $('#time').val();
          var sometimes = document.getElementById("times").checked;

          // วันเดียวกัน
            if (start_date == end_date) {
                $(".times").hide();

                          if (start_time > end_time) {
                            //เวลาไปมากกว่าเวลากลับ
                            addErr("bk_end_start","เวลาไม่ถูกต้อง")
                            $("#bk_where2").attr("disabled","disabled");
                          }//start_time > end_time
                          else {
                            //เวลาไปน้อยกว่าเวลากลับ
                            delDisabled("bk_where2");
                            rmErr("bk_end_start");
                          }//start_time > end_time

                      //วันอื่น
                    }

          // วันเดียวกัน

          //คนละวัน
            else {
              $(".times").show();
                  if (!sometimes) {
                    if(start_date < end_date){
                          //เริ่มวันนี้
                            if (start_time > end_time) {
                              //เวลาไปมากกว่าเวลากลับ
                              addErr("bk_end_start","เวลาไม่ถูกต้อง")
                              $("#bk_where2").attr("disabled","disabled");
                            }//start_time > end_time
                            else {
                              //เวลาไปน้อยกว่าเวลากลับ
                                rmErr("bk_end_start");
                               delDisabled("bk_where2");
                            }//start_time > end_time
                          //เริ่มวันอื่น
                        }//start_date < end_date
                    else {
                      //ย้อนเวลา
                      addErr("bk_end_date","วันที่ไม่ถูกต้อง")
                      $("#bk_where2").attr("disabled","disabled");
                    }//start_date < end_date
                  }//allday
                  else {
                    if(start_date < end_date){
                          //เริ่มวันนี้
                          delDisabled("bk_where2");
                          rmErr("bk_start_start");
                          rmErr("bk_end_start");

                        }//start_date < end_date
                    else {
                      //ย้อนเวลา
                      addErr("bk_end_date","วันที่ไม่ถูกต้อง")
                      $("#bk_where2").attr("disabled","disabled");
                    }//start_date < end_date
                  }//sometime
            }//start_date == today_date
          //คนละวัน
      }

      // function ChkDateTime() {

      //     var start_date = $('#bk_start_date').val();
      //     var end_date = $('#bk_end_date').val();
      //     var start_time = $('#bk_start_start').val();
      //     var end_time = $('#bk_end_start').val();
      //     var today_date = $('#date').val();
      //     var today_time = $('#time').val();
      //     var sometimes = document.getElementById("times").checked;

      //     // วันเดียวกัน
      //       if (start_date == end_date) {
      //           $(".times").hide();
      //                 //วันนี้
      //                   if (start_date == today_date) {
      //                     if (start_time > today_time) {
      //                         rmErr("bk_start_start");
      //                        //เวลาไปมากกว่าเวลาจริง
      //                       if (start_time > end_time) {
      //                         //เวลาไปมากกว่าเวลากลับ
      //                         addErr("bk_end_start","เวลาไม่ถูกต้อง")
      //                         $("#bk_where2").attr("disabled","disabled");
      //                       }//start_time > end_time
      //                       else {
      //                         //เวลาไปน้อยกว่าเวลากลับ
      //                         delDisabled("bk_where2");
      //                         rmErr("bk_end_start");
      //                       }//start_time > end_time
      //                     }//start_time > today_time
      //                     else {
      //                       //เวลาไปน้อยกว่าเวลาจริง
      //                       addErr("bk_start_start","เวลาไม่ถูกต้อง")
      //                       $("#bk_where2").attr("disabled","disabled");
      //                     }//start_time > today_time
      //                   }//start_date == today_date
      //                 //วันนี้

      //                 //วันอื่น
      //                   else {
      //                     if (start_time > end_time) {
      //                       //เวลาไปมากกว่าเวลากลับ
      //                       addErr("bk_end_start","เวลาไม่ถูกต้อง")
      //                       $("#bk_where2").attr("disabled","disabled");
      //                     }//start_time > end_time
      //                     else {
      //                       //เวลาไปน้อยกว่าเวลากลับ
      //                       delDisabled("bk_where2");
      //                       rmErr("bk_end_start");
      //                     }//start_time > end_time
      //                   }
      //                 //วันอื่น
      //               }

      //     // วันเดียวกัน

      //     //คนละวัน
      //       else {
      //         $(".times").show();
      //             if (!sometimes) {
      //               if(start_date < end_date){
      //                     //เริ่มวันนี้
      //                       if (start_date == today_date) {
      //                         if (start_time > today_time) {
      //                           //เวลาไปมากกว่าเวลาจริง
      //                             delDisabled("bk_where2");
      //                             rmErr("bk_start_start");
      //                           // }//start_time > end_time
      //                         }//start_time > end_time
      //                         else {
      //                           //เวลาไปน้อยกว่าเวลาจริง
      //                           addErr("bk_start_start","เวลาไม่ถูกต้อง")
      //                           $("#bk_where2").attr("disabled","disabled");

      //                         }//start_time > end_time
      //                       }//start_date == today_date
      //                     //เริ่มวันนี้

      //                     //เริ่มวันอื่น
      //                       else {
      //                           delDisabled("bk_where2");
      //                           rmErr("bk_start_start");
      //                           rmErr("bk_end_start");
      //                       }//start_date == today_date
      //                     //เริ่มวันอื่น
      //                   }//start_date < end_date
      //               else {
      //                 //ย้อนเวลา
      //                 addErr("bk_end_date","วันที่ไม่ถูกต้อง")
      //                 $("#bk_where2").attr("disabled","disabled");
      //               }//start_date < end_date
      //             }//allday
      //             else {
      //               if(start_date < end_date){
      //                     //เริ่มวันนี้
      //                       if (start_date == today_date) {
      //                         if (start_time > today_time) {
      //                           //เวลาไปมากกว่าเวลาจริง
      //                             rmErr("bk_start_start");
      //                             if (start_time > end_time) {
      //                               //เวลาไปมากกว่าเวลากลับ
      //                               addErr("bk_end_start","เวลาไม่ถูกต้อง")
      //                               $("#bk_where2").attr("disabled","disabled");
      //                             }//start_time > end_time
      //                             else {
      //                               //เวลาไปน้อยกว่าเวลากลับ
      //                               delDisabled("bk_where2");
      //                               rmErr("bk_end_start");
      //                             }//start_time > end_time
      //                           // }//start_time > end_time
      //                         }//start_time > end_time
      //                         else {
      //                           //เวลาไปน้อยกว่าเวลาจริง
      //                           addErr("bk_start_start","เวลาไม่ถูกต้อง")
      //                           $("#bk_where2").attr("disabled","disabled");

      //                         }//start_time > end_time
      //                       }//start_date == today_date
      //                     //เริ่มวันนี้

      //                     //เริ่มวันอื่น
      //                       else {
      //                           delDisabled("bk_where2");
      //                           rmErr("bk_start_start");
      //                           rmErr("bk_end_start");
      //                       }//start_date == today_date
      //                     //เริ่มวันอื่น
      //                   }//start_date < end_date
      //               else {
      //                 //ย้อนเวลา
      //                 addErr("bk_end_date","วันที่ไม่ถูกต้อง")
      //                 $("#bk_where2").attr("disabled","disabled");
      //               }//start_date < end_date
      //             }//sometime
      //       }//start_date == today_date
      //     //คนละวัน
      // }


      //       function ChkDateTime() {
      //   var start_date = $('#bk_start_date').val();
      //   var end_date = $('#bk_end_date').val();
      //   var start_time = $('#bk_start_start').val();
      //   var end_time = $('#bk_end_start').val();
      //   var today_date = $('#date').val();
      //   var today_time = $('#time').val();
      //   //ไปกลับวันเดียว
      //     if (start_date == end_date) {
      //       check();
      //        $(".times").hide();
      //         if (start_date == today_date) {
      //           if (start_time > today_time) {
      //             rmErr("bk_start_start");
      //             if (start_time < end_time) {
      //               rmErr("bk_end_start");
      //             }
      //             else {
      //               addErr("bk_end_start","Please enter a valid time ");
      //             }
      //           }
      //           else {
      //             addErr("bk_start_start","Please enter a valid time ");
      //             if (start_time < end_time) {
      //               rmErr("bk_end_start");
      //             }
      //             else {
      //               addErr("bk_end_start","Please enter a valid time ");
      //             }
      //           }
      //         }
      //         else {
      //             if (start_time < end_time) {
      //               rmErr("bk_end_start");
      //             }
      //             else {
      //               addErr("bk_end_start","Please enter a valid time ");
      //             }
      //         }
      //     }
      //   //ไปกลับมากกว่า1วัน
      //     else {
      //       $(".times").show();
      //         if (start_date == today_date) {
      //           if (start_time > today_time) {
      //             rmErr("bk_start_start");
      //             if (start_date < end_date) {
      //               rmErr("bk_end_date");
      //               if (document.getElementById("times").checked) {
      //                 if (start_time < end_time) {
      //                   rmErr("bk_end_start");
      //                 }
      //                 else {
      //                   addErr("bk_end_start","Please enter a valid time ");
      //                 }
      //               }
      //               else {
      //                 rmErr("bk_end_start");
      //               }
      //             }
      //             else {
      //               addErr("bk_end_date","Please enter a valid date");
      //             }
      //           }
      //           else {
      //             addErr("bk_start_start","Please enter a valid time ");
      //           }
      //         }
      //         else {
      //           rmErr("bk_start_start");
      //           if (start_date < end_date) {
      //             rmErr("bk_end_date");
      //             if (document.getElementById("times").checked) {
      //               if (start_time < end_time) {
      //                 rmErr("bk_end_start");
      //               }
      //               else {
      //                 addErr("bk_end_start","Please enter a valid time ");
      //               }
      //             }
      //             else {
      //               rmErr("bk_end_start");
      //             }
      //           }
      //           else {
      //             addErr("bk_end_date","Please enter a valid date ");
      //           }
      //         }
      //     }
      // }

  // ==========================================================================================

  // $("#bk_where1").keyup(function () {
  //   delDisabled("bk_where2");
  // })
  $("#bk_where2").keyup(function () {
    delDisabled("btn_save");
  })

  $("#tigket").change(function(){
    var tigketVal = $(this).val();
    if(tigketVal == 1){
      delDisabled("bk_start_date");
      delDisabled("bk_start_start");
      delDisabled("bk_start_end");
      $("#bk_end_date").attr("disabled","disabled");
      $("#bk_end_start").attr("disabled","disabled");
      $("#bk_end_end").attr("disabled","disabled");
    }else if(tigketVal == 2){
      delDisabled("bk_start_date");
      delDisabled("bk_start_start");
      delDisabled("bk_start_end");

      delDisabled("bk_end_date");
      delDisabled("bk_end_start");
      delDisabled("bk_end_end");
    }
    else {
      $("#bk_start_date").attr("disabled","disabled");
      $("#bk_start_start").attr("disabled","disabled");
      $("#bk_start_end").attr("disabled","disabled");

      $("#bk_end_date").attr("disabled","disabled");
      $("#bk_end_start").attr("disabled","disabled");
      $("#bk_end_end").attr("disabled","disabled");
    }
  });

  function delDisabled(id){
    $("#"+id).removeAttr("disabled");
  }

  jQuery(".bkDate").datetimepicker({
          format:"Y/m/d",
          lang:"th",
          minDate:'-1970/01/01',
          timepicker:false,
          scrollInput:false
  });

  jQuery(".bkTime").datetimepicker({
          format:"H:i",
          datepicker:false,
          scrollInput:false,
  });

  // ==========================================================================================

  $(".btn-location").click(function(){
      var num = $("#bk_whereList li").length;
       $("#bk_whereList li:last").before("<li>"
                                        +"<div class='form-group'>"
                                        +"<input type='text' class='form-control' id='bk_where"+num+"' name='bk_where"+num+"' "
                                        // +"<input type='text' class='form-control' id='bk_where"+num+"' name='bk_where"+num+"' onfocus='callmap(this)' "
                                        +"placeholder='กรุณากรอกสถานที่ไป"+(num-1)+"'>"
                                        +"</div>"
                                        +"</li>");
  });

  $(".btn-relocate").click(function(){
    var num = ($("#bk_whereList li").length)-2;

    if(num > 0){
        $("#bk_whereList li:eq("+num+")").remove();
    }

  });

  // ==========================================================================================

  $("form#frm_booking").submit(function(ev){
    ev.preventDefault();
    var formData = new FormData(this);
    // for (var value of formData.values()) {
    //          console.log(value);
    //       }
    var start_date = $('#bk_start_date').val();
    var end_date = $('#bk_end_date').val();
    var start_time = $('#bk_start_start').val();
    var end_time = $('#bk_end_start').val();
    var today_date = $('#date').val();
    var today_time = $('#time').val();
    var sometimes = document.getElementById("times").checked;

    // วันเดียวกัน
              if (start_date == end_date) {
                $(".times").hide();

                          if (start_time > end_time) {
                            //เวลาไปมากกว่าเวลากลับ
                            addErr("bk_end_start","เวลาไม่ถูกต้อง")

                          }//start_time > end_time
                          else {
                            //เวลาไปน้อยกว่าเวลากลับ
                            rmErr("bk_end_start");
                            sendBookingDB(formData)
                          }//start_time > end_time

                      //วันอื่น
                    }
    // วันเดียวกัน

    //คนละวัน
       else {
              $(".times").show();
                  if (!sometimes) {
                    if(start_date < end_date){
                          if (start_time > end_time) {
                            //เวลาไปมากกว่าเวลากลับ
                            addErr("bk_end_start","เวลาไม่ถูกต้อง")

                          }//start_time > end_time
                          else {
                            //เวลาไปน้อยกว่าเวลากลับ
                            rmErr("bk_end_start");
                            sendBookingDB(formData)
                          }//start_time > end_time
                        }//start_date < end_date
                    else {
                      //ย้อนเวลา
                      addErr("bk_end_date","วันที่ไม่ถูกต้อง")
                      $("#bk_where2").attr("disabled","disabled");
                    }//start_date < end_date
                  }//allday
                  else {
                    if(start_date < end_date){
                          //เริ่มวันนี้
                          delDisabled("bk_where2");
                          rmErr("bk_start_start");
                          rmErr("bk_end_start");
                          sendBookingDB(formData)
                        }//start_date < end_date
                    else {
                      //ย้อนเวลา
                      addErr("bk_end_date","วันที่ไม่ถูกต้อง")
                      $("#bk_where2").attr("disabled","disabled");
                    }//start_date < end_date
                  }//sometime
            }//start_date == today_date
    //คนละวัน
  })

  // $("form#frm_booking").submit(function(ev){
  //   ev.preventDefault();
  //   var formData = new FormData(this);
  //   // for (var value of formData.values()) {
  //   //          console.log(value);
  //   //       }
  //   var start_date = $('#bk_start_date').val();
  //   var end_date = $('#bk_end_date').val();
  //   var start_time = $('#bk_start_start').val();
  //   var end_time = $('#bk_end_start').val();
  //   var today_date = $('#date').val();
  //   var today_time = $('#time').val();
  //   var sometimes = document.getElementById("times").checked;

  //   // วันเดียวกัน
  //     if (start_date == end_date) {

  //               //วันนี้
  //                 if (start_date == today_date) {
  //                   if (start_time > today_time) { //เวลาไปมากกว่าเวลาจริง
  //                       rmErr("bk_start_start");
  //                     if (start_time > end_time) {
  //                       //เวลาไปมากกว่าเวลากลับ
  //                       addErr("bk_end_start","เวลาไม่ถูกต้อง")
  //                     }//start_time > end_time
  //                     else {
  //                       //เวลาไปน้อยกว่าเวลากลับ
  //                       sendBookingDB(formData)
  //                     }//start_time > end_time
  //                   }//start_time > today_time
  //                   else {
  //                     //เวลาไปน้อยกว่าเวลาจริง
  //                     addErr("bk_start_start","เวลาไม่ถูกต้อง")
  //                   }//start_time > today_time
  //                 }//start_date == today_date
  //               //วันนี้

  //               //วันอื่น
  //                 else {
  //                   if (start_time > end_time) {
  //                     //เวลาไปมากกว่าเวลากลับ
  //                     addErr("bk_end_start","เวลาไม่ถูกต้อง")
  //                     $("#bk_where2").attr("disabled","disabled");
  //                   }//start_time > end_time
  //                   else {
  //                     //เวลาไปน้อยกว่าเวลากลับ
  //                     rmErr("bk_end_start");
  //                     sendBookingDB(formData)
  //                   }//start_time > end_time
  //                 }
  //               //วันอื่น
  //             }

  //   // วันเดียวกัน

  //   //คนละวัน
  //     else {
  //       if (!sometimes) {
  //         if(start_date < end_date){
  //               //เริ่มวันนี้
  //                 if (start_date == today_date) {
  //                   if (start_time > today_time) {
  //                     //เวลาไปมากกว่าเวลาจริง
  //                       sendBookingDB(formData)
  //                       rmErr("bk_start_start");
  //                   }//start_time > end_time
  //                   else {
  //                     //เวลาไปน้อยกว่าเวลาจริง
  //                       addErr("bk_start_start","เวลาไม่ถูกต้อง")
  //                   }//start_time > end_time
  //                 }//start_date == today_date
  //               //เริ่มวันนี้

  //               //เริ่มวันอื่น
  //                 else {
  //                     sendBookingDB(formData)
  //                 }//start_date == today_date
  //               //เริ่มวันอื่น
  //             }//start_date < end_date
  //         else {
  //           //ย้อนเวลา
  //           addErr("bk_end_date","วันที่ไม่ถูกต้อง")
  //         }//start_date < end_date
  //       }//allDay
  //       else {
  //         if(start_date < end_date){
  //               //เริ่มวันนี้
  //                 if (start_date == today_date) {
  //                   if (start_time > today_time) {
  //                     //เวลาไปมากกว่าเวลาจริง
  //                       rmErr("bk_start_start");
  //                       if (start_time > end_time) {
  //                         //เวลาไปมากกว่าเวลากลับ
  //                         addErr("bk_end_start","เวลาไม่ถูกต้อง")
  //                         $("#bk_where2").attr("disabled","disabled");
  //                       }//start_time > end_time
  //                       else {
  //                         //เวลาไปน้อยกว่าเวลากลับ
  //                         rmErr("bk_end_start");
  //                         sendBookingDB(formData)
  //                       }//start_time > end_time
  //                   }//start_time > end_time
  //                   else {
  //                     //เวลาไปน้อยกว่าเวลาจริง
  //                       addErr("bk_start_start","เวลาไม่ถูกต้อง")
  //                   }//start_time > end_time
  //                 }//start_date == today_date
  //               //เริ่มวันนี้

  //               //เริ่มวันอื่น
  //                 else {
  //                     sendBookingDB(formData)
  //                 }//start_date == today_date
  //               //เริ่มวันอื่น
  //             }//start_date < end_date
  //         else {
  //           //ย้อนเวลา
  //           addErr("bk_end_date","วันที่ไม่ถูกต้อง")
  //         }//start_date < end_date
  //       }


  //     }//start_date == today_date
  //   //คนละวัน
  // })

  function sendBookingDB(formData) {
                  var count_where = ($("#bk_whereList li").length)-1;

          $.ajax({
            url:"/insert",
            type:"POST",
            data:formData,
            // data:formData+"&count_where="+count_where,
            async: true,
            cache: false,
            contentType: false,
            processData: false,
            success:function(data){
              var obj = JSON.parse(data);
                if (obj['success']==true) {
                  var formSend = $('#frm_booking').serialize();
                  var count_where = ($("#bk_whereList li").length)-1;
                  $.ajax({
                    url:"/insertsLocation",
                    type:"POST",
                    data:formSend+"&count_where="+count_where+"&id="+obj['bk_id'],
                    success:function(data1){
                      console.log(data1);
                      var obj1 = JSON.parse(data);
                      if (obj1['success']==true) {
                      //wait email

                          swal({
                            title: "การจองสำเร็จ",
                            type: "success",
                            text: "ระบบกำลังส่ง Email แจ้งเตือน กรุณารอสักครู่",
                            timer: 5000,
                            showConfirmButton: false
                          },function () {
                             window.location = "/dashboard";
                             // Location.reload()
                          });


                          $.ajax({
                          url:"/sendbooking",
                          type:"GET",
                          data:"id="+obj['bk_id'],
                          contentType: false,
                          processData: false,
                          success:function(email){
                            var text = JSON.parse(email);
                            if (text['success']==true) {
                                      // swal({
                                      //   title: "สำเร็จ",
                                      //   text: "บันทึกการจองของคุณสำเร็จ",
                                      //   type: "success",
                                      //   showCancelButton: false,
                                      //   confirmButtonColor: "#2ECC71",
                                      //   confirmButtonText: "ตกลง",
                                      //   closeOnConfirm: false,
                                      //   },
                                      //   function(isConfirm){
                                      //   if (isConfirm) {
                                      //   window.location = "/dashboard";
                                      //   }
                                      //   }
                                      // );

                              $.ajax({
                                url:"/sendbookingApprove",
                                type:"GET",
                                data:"bk_id="+text['bk_id'],
                                contentType: false,
                                processData: false,
                                success:function(approve){
                                  // console.log(approve);
                                  var approver = JSON.parse(approve);
                                  // console.log(approver);
                                  if (approver['success']==true) {
                                      // swal({
                                      //   title: "สำเร็จ",
                                      //   text: "บันทึกการจองของคุณสำเร็จ",
                                      //   type: "success",
                                      //   showCancelButton: false,
                                      //   confirmButtonColor: "#2ECC71",
                                      //   confirmButtonText: "ตกลง",
                                      //   closeOnConfirm: false,
                                      //   },
                                      //   function(isConfirm){
                                      //   if (isConfirm) {
                                        // window.location = "/dashboard";
                                      //   }
                                      //   }
                                      // );
                                    }
                                  }
                                })// sendbookingApprove
                            }
                          }
                        })// sendbooking

                        //wait email
                      }
                    }
                  })// insert

                       //  $.ajax({
                       //   url:"/locateDB",
                       //   type:"GET",
                       //   data:"id="+obj['bk_id'],
                       //   contentType: false,
                       //   processData: false,
                       //   success:function(datalocate){
                       //     var locate = JSON.parse(datalocate);
                       //     var id = obj['bk_id'];
                       //     var obj_locate = [];

                       //       for (var i = 0; i < locate.length; i++)
                       //       {
                       //         obj_locate.push(locate[i]["name"]);
                       //       }

                       //       for (var i = 0; i < obj_locate.length; i++) {
                       //                var objall = GetValue(obj_locate[i],function (address,latitude,longitude)
                       //              {
                       //                //  console.log("location="+address+"&latitude="+latitude+"&longitude="+longitude+"&id="+id);
                       //                  $.ajax({
                       //                    url:"/location",
                       //                    data:"location_id="+address+"&latitude="+latitude+"&longitude="+longitude+"&id="+id,
                       //                    type: "POST",
                       //                    success: function (datalatlng) {
                       //                      // console.log(datalatlng);
                       //                          //  var obj = JSON.parse(locations);
                       //                          //  console.log(obj);
                       //                                                                           }


                       //                  })
                       //              });
                       //        }
                       //     }
                       // })
                }
                else {
                  swal({
                            title: "ผิดพลาด",
                            text: obj['msg'],
                            type: "error",
                            showCancelButton: false,
                            confirmButtonColor: "#2ECC71",
                            confirmButtonText: "ตกลง",
                            closeOnConfirm: true,

                      });
                }
              }
          })

  };

  function rmErr(input){
    $("#group"+input).removeClass("has-danger");
    $("#group"+input+" input").removeClass("form-control-danger");
    $("#fb"+input).attr("hidden","hidden");
  }

  function addErr(type,msg){
      $("#group"+type).addClass("has-danger");
      $("#group"+type+" input").addClass("form-control-danger");
      $("#fb"+type).html(msg);
      $("#fb"+type).removeAttr("hidden");
  }

  // ==========================================================================================

function callmap(e){
  initAutocomplete(e.id);
}
function initAutocomplete(id){
  if(typeof id == "undefined"){
    id = "bk_where1";
  }
  autocomplete = new  google.maps.places.Autocomplete((document.getElementById(id)),{types: ['geocode']});
  autocomplete.addListener('place_changed',fillInAddress);
}
function fillInAddress() {
    // var place = autocomplete.getPlace();
    // var address = place.formatted_address;
}

function GetValue(address,callback) {
  var geocoder = new google.maps.Geocoder();
  var  latitude ="";
  var  longitude ="";
  geocoder.geocode( { 'address': address }, function(results, status) {
    if (status == google.maps.GeocoderStatus.OK) {
       latitude = results[0].geometry.location.lat();
       longitude = results[0].geometry.location.lng();
       }
      callback(address,latitude,longitude);
   })
}

</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAaXMuANTFVYidZ3gQaPaUVQU4zeEmk33U&libraries=places&callback=initAutocomplete" async defer></script>

@endsection
