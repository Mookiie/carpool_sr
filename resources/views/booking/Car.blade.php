@extends('welcome')
@extends('dashboard.topNavbar')
@extends('dashboard.SideNavbar')

@section('content')
<link rel="stylesheet" type="text/css" href="{{ asset('js/datetimepicker/jquery.datetimepicker.css')}}"/>
<script src="{{asset('js/datetimepicker/jquery.js')}}"></script>
<script src="{{asset('js/datetimepicker/build/jquery.datetimepicker.full.min.js')}}"></script>

@foreach ($users as $r)
  <?php
        $lv_user = $r->emp_level;
        $id = $r->emp_id;
        $com_id = $r->com_id;
        $dep_id = $r->job_id;
   ?>
@endforeach
<link href="{{ asset('js/viewer/dist/viewer.css')}}" rel="stylesheet">
<script src="{{ asset('js/viewer/dist/viewer.js')}}"></script>

<div class="container-dashboard">
<div class="card col-md-12 row">
<input type="hidden"  id="emp_id" value="{{$id}}">
<input type="hidden"  id="com_id" value="{{$com_id}}">
<input type="hidden"  id="dep_id" value="{{$dep_id}}">
<input type="hidden"  id="lv_user" value="{{$lv_user}}">
  <div class="card-header">
    <div class="row">
      <span class="fa fa-car mr-auto "> ข้อมูลรถ</span>
      <?php if ($lv_user =="2" OR $lv_user =="99" OR $lv_user =="999") {?>
        <button type="button" class='btn btn-success btn-sm' id="addcar">เพิ่มข้อมูล</button>
      <?php  } ?>
    </div>
  </div>

  <div class="table-resposive" id="table_car">
  <nav class="navbar navbar-light bg-faded">

    <form class="form-inline" id="frmSearch_car">

      <div class="form-group">
        <select class="form-control mr-sm-2"  id="typeSearch" name="typeSearch">
          <option value="0">ค้นหาทั้งหมด</option>
          <option value="1">เลขทะเบียน</option>
          <option value="2">รุ่นรถ</option>
          <option value="3">ประเภทรถ</option>
          <option value="4">ยี่ห้อรถ</option>
        </select>
      </div>
      <div class="form-group" id="groupTxt" >
        <div class="input-group my-2 my-sm-0">
          <input type="text" class="form-control" id="txtSearch" name="txtSearch" placeholder="ค้นหาทั้งหมด" disabled>
          <span class="input-group-btn">
            <button class="btn btn-primary btnSearch_car" type="button"><span class="fa fa-search"></span></button>
          </span>
        </div>
      </div>
      <div class="form-group" id="groupCar" style="display:none" >
        <div class="input-group my-2 my-sm-0">
        <select class="form-control"  id="carType" name="carType">
          <?php
            $sqltypeCar = DB::table('tb_car_type')->get();
            foreach ($sqltypeCar as $car):
              echo "<option value='$car->ctype_id'> ".$car->ctype_name."</option>";
            endforeach;
          ?>
        </select>
          <span class="input-group-btn">
            <button class="btn btn-primary btnSearch_car" type="button"><span class="fa fa-search"></span></button>
          </span>
        </div>
      </div>

      <div class="form-group" id="groupBrand" style="display:none" >
        <div class="input-group my-2 my-sm-0">
        <select class="form-control"  id="carBrand" name="carBrand">
          <?php
            $sqlcarBrand = DB::table('tb_brand')->get();
            foreach ($sqlcarBrand as $cb):
              echo "<option value='$cb->brand_id'> ".$cb->brand_name."</option>";
            endforeach;
           ?>
        </select>
          <span class="input-group-btn">
            <button class="btn btn-primary btnSearch_car" type="button"><span class="fa fa-search"></span></button>
          </span>
        </div>
      </div>
    </form>
    <div id="tbody_car"> </div>

    </nav>
    </div>
  <div id="Loan_car" ></div>
</div>

<div id="detail_car">
</div>
<div id="add_car" ></div>

</div>
<div class="modal-area" ></div>
  </div>

<script>
$("#add_car").hide();
  $(document).ready(function(){
    var data = "typeSearch=0&com_id="+$('#com_id').val()+"&dep_id="+$('#dep_id').val();
    callTbl_car(data);

  });

  $("#typeSearch").change(function(){
    var typeVal = $(this).val();
    if(typeVal == 1){
      $("#txtSearch").attr("placeholder","เลขทะเบียน");
      $("#groupCar").hide();
      $("#groupBrand").hide();
      $("#groupTxt").show();
     delDisabled("txtSearch");
    }else if(typeVal == 2){
      $("#txtSearch").attr("placeholder","รุ่นรถ");
      $("#groupCar").hide();
      $("#groupTxt").show();
      $("#groupBrand").hide();
      delDisabled("txtSearch");
    }else if(typeVal == 4){
      $("#groupCar").hide();
      $("#groupTxt").hide();
      $("#groupBrand").show();
      delDisabled("txtSearch");
    }  else if(typeVal == 3){
      $("#groupCar").show();
      $("#groupTxt").hide();
      $("#groupBrand").hide();
    }else{
      $("#groupCar").hide();
      $("#groupBrand").hide();
      $("#groupTxt").show();
      $("#txtSearch").attr("placeholder","ค้นหาทั้งหมด");
      $("#txtSearch").attr("disabled","disabled");
    }
  });

  function delDisabled(id){
    $("#"+id).removeAttr("disabled");
  }

  $('#txtSearch').keypress(function(event){
        if(event.keyCode == '13'){
          var data = $("#frmSearch_car").serialize();
          callTbl_car(data);
          event.preventDefault();
        }
  });

   $("#addcar").click(function(){
    $("#table_car").hide();
    $("#detail_car").hide();
    var id =$("#emp_id").val();
    var com_id =$("#com_id").val();
    var dep_id =$("#dep_id").val();
   $.ajax({
            url:"/addcar",
            data:"id="+id+"&com_id="+com_id+"&dep_id="+dep_id,
            type:"GET",
            success:function(data){
            $("#add_car").show();
            $("#add_car").html(data);

            $("#btnClose").click(function(){
              $("#table_car").show();
              $("#detail_car").hide();
              $("#add_car").hide();
            });

            $("#btn-addexit").click(function(){
              $("#table_car").show();
              $("#detail_car").hide();
              $("#add_car").hide();
            });

            $("form#createcarform").submit(function(ev){
              ev.preventDefault();
              var formData = new FormData(this);
              $.ajax({
                url:"/createcar",
                data:formData,
                type:"POST",
                async: true,
                cache: false,
                contentType: false,
                processData: false,
                success:function(data){
                  var obj = JSON.parse(data);
                  if (obj['success']==true) {
                            swal({
                              title: "SUCCESS",
                              text: "success",
                              type: "success",
                              showCancelButton: false,
                              confirmButtonColor: "#2ECC71",
                              confirmButtonText: "OK",
                              closeOnConfirm: false,
                              },
                              function(isConfirm){
                              if (isConfirm) {
                              window.location = "/car";
                              }
                              }
                            );
                          }else if(obj['success']==false && obj['msg']=="type") {
                            swal({
                                      title: "มีบางอย่างผิดพลาดขณะบันทึก",
                                      text: "กรุณาตรวจสอบประเภทไฟล์รูปภาพ",
                                      type: "error",
                                      showCancelButton: false,
                                      confirmButtonColor: "#2ECC71",
                                      confirmButtonText: "ตกลง",
                                      closeOnConfirm: true,
                                    },
                                      function(isConfirm){
                                        if (isConfirm) {

                                    }
                                });
                          }else {
                            swal({
                                      title: "มีบางอย่างผิดพลาดขณะบันทึก",
                                      text: "กรุณาตรวจสอบข้อมูลใหม่อีกครั้ง",
                                      type: "error",
                                      showCancelButton: false,
                                      confirmButtonColor: "#2ECC71",
                                      confirmButtonText: "ตกลง",
                                      closeOnConfirm: true,
                                    },
                                      function(isConfirm){
                                        if (isConfirm) {

                                    }
                                });
                          }
                }
              })
            })
          }
      });
  });

  $(".btnSearch_car").click(function(){
    var data = $("#frmSearch_car").serialize()+"&com_id="+$('#com_id').val();
    callTbl_car(data);
  });
  function callTbl_car(dataSearch){
    var lv_user =$('#lv_user').val();
    $.ajax({url:"/tablecar",
            data:dataSearch+"&lv_user="+lv_user,
            type:"GET",
            success:function(data){
             //console.log(data);
              $("#tbody_car").html(data);

                //detail
                  $(".carDetail").click(function(){
                      var id = $(this).data("id");
                      // console.log(id);
                      $.ajax({ url:"/detailcar",
                                  data:"data="+id,
                                  type:"GET",
                                  success:function(data){
                                    // console.log(data);
                                    $("#table_car").hide();
                                    $("#detail_car").show();
                                    $("#detail_car").html(data);
                                    $('.image').viewer();

                                    $("#detailClose").click(function(){
                                      $("#table_car").show();
                                      $("#detail_car").hide();

                                    });
                                  }
                                });
                  });

                  //Loan
                  $(".btn-Loan").click(function () {
                    var data = "ss=L&com_id="+$('#com_id').val()+"&dep_id="+$('#dep_id').val()
                    +"&id="+$(this).data("id");
                    // set_status("id="+$(this).data("id")+"&status=4")
                    Loan_Due("id="+$(this).data("id")+"&status=4",data)

                  })

                  //due
                  $(".btn-Due").click(function () {
                    var data = "ss=D&com_id="+$('#com_id').val()+"&dep_id="+$('#dep_id').val()
                    +"&id="+$(this).data("id");
                    // set_status("id="+$(this).data("id")+"&status=1")
                    Loan_Due("id="+$(this).data("id")+"&status=0",data)
                  })

                  //cancel
                  $(".btn-cancelcar").click(function() {
                    var id = "id="+$(this).data("id")+"&status=1";
                    set_status(id)
                  })
                  //car
                  $(".btn-car").click(function() {
                  var id = "id="+$(this).data("id")+"&status=0";
                  set_status(id)
                  })
                  //wait
                  $(".btn-waitcar").click(function() {
                    var id = "id="+$(this).data("id")+"&status=2";
                    set_status(id)
                  })

                  //function ststuscar
                  function set_status(id) {
                    $.ajax({
                      url:"/statuscar",
                      data:id,
                      type:"POST",
                      success:function(data){
                        var obj = JSON.parse(data);
                          if (obj['success']==true) {
                            swal({
                              type:"success",
                              title:"สำเร็จ",
                              text:"คุณทำการบันทึกสำเร็จ",
                              confirmButtonText:"ตกลง",
                              confirmButtonColor:"#2ECC71",
                              closeOnConfirm:true,
                            },function(isConfirm){
                              window.location="/car"
                            });
                          }
                      }
                    })
                  }

                  function Loan_Due(id,fdata) {
                    $.ajax({
                      url:"/statuscar",
                      data:id,
                      type:"POST",
                      success:function(data){
                        var obj = JSON.parse(data);
                          if (obj['success']==true) {
                                $.ajax({
                                  url:"/loandue",
                                  data:fdata,
                                  type:"POST",
                                  success:function(loandue){
                                    // console.log(loandue);
                                    if(loandue==1){
                                      swal({
                                                title: "อัพเดทข้อมูลสำเร็จ",
                                                // text: obj['msg'],
                                                type: "success",
                                                showCancelButton: false,
                                                confirmButtonColor: "#2ECC71",
                                                confirmButtonText: "ตกลง",
                                                closeOnConfirm: false,
                                              },
                                                function(isConfirm){
                                                  if (isConfirm) {

                                                    window.location = "/car";
                                              }
                                          });
                                    }
                                    else {
                                      $("#Loan_car").html(loandue);
                                      $("#table_car").hide();
                                      $("#addcar").hide();
                                    }
                                }
                             })
                          }
                      }
                    })
                  }
          }
    });
  }
</script>
@endsection
