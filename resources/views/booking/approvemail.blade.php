@extends('welcome')
@section('content')

<link rel="stylesheet" type="text/css" href="{{ asset('js/datetimepicker/jquery.datetimepicker.css')}}"/>
<script src="{{asset('js/datetimepicker/jquery.js')}}"></script>
<script src="{{asset('js/datetimepicker/build/jquery.datetimepicker.full.min.js')}}"></script>
<meta name="viewport" content="initial-scale=1.0, user-scalable=no">
<?php
    $url=  $_SERVER['REQUEST_URI'];
    $pieces = explode("?", $url);
    $data = base64_decode($pieces[1]);
    $split = explode("&", $data);
    $bk_id=$split[0];
    $emp_id=$split[1];
    $emp_email=$split[2];
$sql = DB::table('tb_booking')->join('tb_employee','tb_employee.emp_id','=','tb_booking.emp_id')->where('bk_id','=',$bk_id)->get();
foreach ($sql as $bk) {
  $name = $bk->emp_fname." ".$bk->emp_lname;
}
$chk = DB::table('tb_booking')->where('bk_id','=',$bk_id)->where('bk_status','=','wait')->get();
if (count($chk)<1) {
  ?>
  <div class="container-fluid">
    <div class="block block-login">
      <img src=".{{$url_img_logo = Storage::url('image/logo/logo.png')}}">
      <h4>Carpool Siamraj Service</h4>
       <small></small>
      <div class="block-login block-login-form" style="width:70%;">

      <div class="col-md-12 text-black">
        การจองรถหมายเลขใบจอง {{$bk_id}} ของ คุณ {{$name}} <br />
        ได้รับการตอบรับไปเรียบร้อยแล้ว
        <div  align="center">
          <button type="button" class="btn btn-success" id="btn-close">ปิด</button>
        </div>

      </div>
    </div>
  </div>
</div>
<script>
$("#btn-close").click(function(){
  window.close();
});
</script>

<?php
}else {
  ?>
  <div class="container-fluid">
    <div class="block block-login">
      <img src="{{$url_img_logo = Storage::url('image/logo/logo.png')}}">
      <h4>Carpool Siamraj Service</h4>
       <small></small>
      <div class="block-login block-login-form" style="width:70%;">

      <div class="col-md-12 text-black">
        <input type="hidden" name="bk_id" id="bk_id" value="{{$bk_id}}">
        <input type="hidden" name="emp_id" id="emp_id" value="{{$emp_id}}">

          คุณได้อนุมัติการจองรถ <br />
          หมายเลขใบจอง {{$bk_id}} ของ คุณ {{$name}} <br />
          เรียบร้อยแล้ว

        {{-- <div  align="center">
          <button type="button" class="btn btn-success" id="btn-close" >ปิด</button>
        </div> --}}

      </div>
    </div>
  </div>
</div>
<script>
$(document).ready(function () {
  var data = $('#bk_id').val();
  var emp = $('#emp_id').val();
  $.ajax({
    url:"/approvestatus",
    type:"GET",
    data:'data='+data+'&emp='+emp+'&type=approve',
    contentType: false,
    processData: false,
    success:function(data){
      var obj = JSON.parse(data);
        if (obj['success']==true) {
          $.ajax({url:"/sendapprove",
                  data:"id="+obj['bk_id'],
                  type:"POST",success:function(mail){
                  }
          })
          $.ajax({url:"/sendSetCar",
                  data:"bk_id="+obj['bk_id'],
                  type:"POST",success:function(mail){
                  }
          })
        }
    }
  })
})
$("#btn-close").click(function(){
  window.open(location, '_self', '');
  window.close();
});
</script>


<?php } ?>



@endsection
