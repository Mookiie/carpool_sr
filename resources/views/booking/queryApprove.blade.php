
@extends('welcome')
@extends('dashboard.topNavbar')
@extends('dashboard.SideNavbar')

@section('content')

@foreach ($users as $r)
  <?php
        $id = $r->emp_id;
        $lv_user = $r->emp_level;
        $com_id = $r->com_id;
        $dep_id = $r->dep_id;
        $emp_id = $r->emp_id;
        $job_id = $r->job_id;
   ?>
@endforeach
<div class="container-dashboard">

<div class="card col-md-12 row">
<div class="card-header">
  <span class="fa fa-list text-black" style="color:#000;">    รายการรออนุมัติ</span>

</div>
<ul class="nav nav-pills  bkItem" >

  <!-- <li class="nav-item">
  <a class="nav-link active" id="viewTbl">
    <span class="fa fa-list"></span>
  </a>
  </li> -->

</ul>

<div class="card-block">

<div id="tblBk" class="table-responsive">
<table class="table table-bordered tblBk">
  <thead >
    <th>สถานะ</th>
    <th>เลขที่อ้างอิง</th>
    <th>วันที่ขอใช้รถ</th>
    <th>ประเภทรถ</th>
    <th>วัตถุประสงค์</th>
    <th>แผนก</th>
  </thead>

  <tbody class="">
    <?php


                        ;
      if ($lv_user == '2') {
        $sqlbooking = DB::table('tb_booking')
                          ->join("tb_car_type",function($join){
                                 $join->on('tb_booking.ctype_id', '=', 'tb_car_type.ctype_id');
                                 $join->on("tb_booking.com_id","=","tb_car_type.com_id");
                            })
                          ->join("tb_employee",function($join){
                                 $join->on('tb_booking.emp_id', '=', 'tb_employee.emp_id')
                                      ->on("tb_booking.com_id","=","tb_employee.com_id");
                            })

                          ->where('tb_booking.bk_status', '=', 'wait')
                          ->where('tb_booking.com_id', '=', $com_id)
                          ->where('tb_booking.dep_car', '=', $job_id);
      }
      elseif ($lv_user<'98') {
        $sqlbooking = DB::table('tb_booking')
                          ->join("tb_car_type",function($join){
                                 $join->on('tb_booking.ctype_id', '=', 'tb_car_type.ctype_id');
                                 $join->on("tb_booking.com_id","=","tb_car_type.com_id");
                            })
                          ->join("tb_employee",function($join){
                                 $join->on('tb_booking.emp_id', '=', 'tb_employee.emp_id')
                                      ->on("tb_booking.com_id","=","tb_employee.com_id");
                            })

                          ->where('tb_booking.bk_status', '=', 'wait')
                          ->where('tb_booking.com_id', '=', $com_id)
                          ->join("tb_approver",function($join){
                                  $join->on('tb_approver.dep_id', '=', 'tb_booking.dep_id');
                                })
                          ->where('tb_approver.app_id', '=', $emp_id);
      }
      else {
        $sqlbooking = DB::table('tb_booking')
                          ->join("tb_car_type",function($join){
                                 $join->on('tb_booking.ctype_id', '=', 'tb_car_type.ctype_id');
                                 $join->on("tb_booking.com_id","=","tb_car_type.com_id");
                            })
                          ->join("tb_employee",function($join){
                                 $join->on('tb_booking.emp_id', '=', 'tb_employee.emp_id')
                                      ->on("tb_booking.com_id","=","tb_employee.com_id");
                            })

                          ->where('tb_booking.bk_status', '=', 'wait')
                          ->where('tb_booking.com_id', '=', $com_id);

      }
      $qrybooking = $sqlbooking->orderBy('tb_booking.bk_id', 'desc')->get();
      $numbooking = $sqlbooking->orderBy('tb_booking.bk_id', 'desc')->count();

      if($numbooking > 0){
      ?>


        @foreach ($qrybooking as $b)

        <?php
              if($numbooking > 0){
                // if ($lv_user <= 2) {
                //   $sql = DB::table('tb_approver')->where('app_id','=',$id)->where('dep_id','=',$dep_id)->get();
                // }
                // else {
                  // $sql = 1;
                // }

                  ?>
                  <tr class="detailBk" data-id="bk={{$b->bk_id}}">
                    <?php
                    $arr = array();
                          if($b->bk_status == "wait"){
                              array_push($arr,"รอการอนุมัติ","warning","#F39C12");
                            }else if($b->bk_status == "approve"){
                              array_push($arr,"รอการจัดรถ","info","#3498DB");
                            }else if($b->bk_status == "success"){
                              array_push($arr,"สำเร็จ","success","#2ECC71");
                            }else if($b->bk_status == "eject"){
                              array_push($arr,"ยกเลิกการจอง","danger","#E74C3C");
                            }else if($b->bk_status == "nonecar"){
                              array_push($arr,"ไม่มีรถ","danger","#E74C3C");
                            }

                     ?>
                     <td width="10px">
                       <h6><span class="badge badge-{{$arr[1]}}">{{$arr[0]}}</span></h6>
                     </td>
                     <td>{{$b->bk_id }}<br>
                       <?php
                       if ($b->setcar_date !="" and $b->setcar_date !="0000-00-00 00:00:00" ) {
                         echo "จัดรถเมื่อ";datetime($b->setcar_date);
                       }elseif ($b->approve_date !="") {
                        echo "อนุมัติเมื่อ";datetime($b->approve_date);
                       }else {
                         echo "จองเมื่อ";datetime($b->bk_date);
                       }?>
                     </td>
                     <td>
                       <?php  if (!$b->bk_use) {
                           echo   datetime($b->bk_start_start);echo " ถึง ";datetime($b->bk_end_start);
                       } else {
                           echo datetimesome($b->bk_start_start,$b->bk_end_start);
                       }
                       ?>
                     </td>
                     <td>
                       {{$b->ctype_name}}
                       <?php $job_car = DB::table('tb_job')->where('job_id','=',$b->dep_car)->groupBy('job_name')->select('job_name')->get(); ?>
                       @foreach ($job_car as $job)
                         [{{$job->job_name}}]
                       @endforeach
                     </td>
                     <td>{{$b->bk_obj}}</td>
                     <td>
                       <?php
                       $sql_dep = DB::table('tb_department')->where('dep_id','=',$b->dep_id)->where('com_id','=',$com_id)->get();
                       foreach ($sql_dep as $dep) {
                         echo  $dep->dep_name;
                       }
                       ?>
                     </td>
                   </tr>
                   <?php  }
                    elseif ($numbooking = 0){ ?>
                      <tr>
                          <td colspan="6" align="center"><h5>ไม่พบกิจกรรม</h5></td>
                      </tr>
                    <?php }?>

                  @endforeach
      <?php }else{  ?>
                  <tr>
                      <td colspan="6" align="center"><h5>ไม่พบกิจกรรม</h5></td>
                  </tr>
      <?php } ?>
  </tbody>

  </table>
  </div>
  <div id="calendarBk"  hidden="true" ></div>

  </div>
  <div class="modal-area"></div>
</div>
</div>
<?php

function datetime($datetime)
{
  $y = substr($datetime,0,4);
  $m = substr($datetime,5,2);
  $d = substr($datetime,8,2);
  $h = substr($datetime,11,2);
  $i = substr($datetime,14,2);
  echo $d."/".$m."/".$y." เวลา ".$h.":".$i." น." ;
}
function datetimesome($datetime1,$datetime2)
{
  $y1 = substr($datetime1,0,4);
  $m1 = substr($datetime1,5,2);
  $d1 = substr($datetime1,8,2);
  $h1 = substr($datetime1,11,2);
  $i1 = substr($datetime1,14,2);
  $y2 = substr($datetime2,0,4);
  $m2 = substr($datetime2,5,2);
  $d2 = substr($datetime2,8,2);
  $h2 = substr($datetime2,11,2);
  $i2 = substr($datetime2,14,2);
  return $d1."/".$m1."/".$y1." ถึง ".$d2."/".$m2."/".$y2." ช่วงเวลา ".$h1.":".$i1."น. ถึง ".$h2.":".$i2."น." ;
}

 ?>
<script>

$(".detailBk").click(function(){
  var detail = $(this).data("id");
 $.ajax({
          url:"/detail",
          data:detail,
          type:"GET",
          success:function(data){
          $(".modal-area").html(data);
          $("#modalBk").modal("show");
        }
    });
});
$("#viewCalendar").click(function(){
  $("#calendarBk").removeAttr("hidden");
  $("#calendarBk").show();
  $('#calendarBk').fullCalendar('today');
  $("#tblBk").hide();
  $("#viewCalendar").addClass("active");
  $("#viewTbl").removeClass("active");
});

$("#viewTbl").click(function(){
  $("#tblBk").removeAttr("hidden");
  $("#tblBk").show();
  $("#calendarBk").hide();
  $("#viewTbl").addClass("active");
  $("#viewCalendar").removeClass("active");
});

$(function(){
  var search = $("#bksearch").val();
  // console.log(search);
  $('#calendarBk').fullCalendar({
      header: {
          left: 'today',  //  prevYear nextYea
          center:'title',
          right: 'prevYear,prev,next,nextYear',
      },
      height: 650,
      buttonIcons:{
          prev: 'left-single-arrow',
          next: 'right-single-arrow',
          prevYear: 'left-double-arrow',
          nextYear: 'right-double-arrow'
      },
      events: {
        url:"/BkEvents?search="+search,
        error:function(data){
          console.log(data.responseText);
        }
      },
      eventLimit:true,
      lang: 'th',
       eventClick: function(calEvent, jsEvent, view) {
         $.ajax({
                  url:"/detail",
                  data:"bk="+calEvent.id,
                  type:"GET",
                  success:function(data){
                    // console.log(data);
                  $(".modal-area").html(data);
                  $("#modalBk").modal("show");
                }
            });
       }
  });
});
</script>
@endsection
