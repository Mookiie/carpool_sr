<!DOCTYPE html>

<?php
$bk_id = $_GET['data'];
date_default_timezone_set("Asia/Bangkok");
$sqlbooking = DB::table('tb_booking')
               ->join("tb_employee",'tb_booking.emp_id','=','tb_employee.emp_id')
               ->join("tb_car_type",function($join){
                     $join->on('tb_booking.ctype_id', '=' , 'tb_car_type.ctype_id');
                     $join->on('tb_booking.com_id', '=' , 'tb_car_type.com_id');
                 })
               ->join("tb_department",function($join){
                      $join->on('tb_booking.dep_id', '=' , 'tb_department.dep_id')
                           ->on("tb_booking.com_id","=","tb_department.com_id");
                 })
               ->where('bk_id', '=', $bk_id)->get();
                foreach ($sqlbooking as $bk):
                 $empid = $bk->emp_id;
                 $fullname = $bk->emp_fname." ".$bk->emp_lname;
                 $status = $bk->bk_status;
                 $date = $bk->bk_date;
                 $bk_start_start = $bk->bk_start_start;
                 $bk_end_start = $bk->bk_end_start;
                 $ctype= $bk->ctype_name;
                 $dep_id = $bk->dep_id;
                 $bk_sitecode = $bk->bk_sitecode;
                 $dep_name = $bk->dep_name;
                 $dep_car = $bk->dep_car;
                 $tel = $bk->dep_tel;
                 $mtel = $bk->bk_mtel;
                 $ttel = $bk->bk_ttel;
                 $percon = $bk->bk_percon;
                 $obj = $bk->bk_obj;
                 $note = $bk->bk_note;
                 $doc = $bk->bk_doc;
                 $car = $bk->car_id;
                 $drive = $bk->drive_id;
                 $reasons = $bk->bk_reasons;
                 $approve_by = $bk->approve_by;
                 $approve_date = $bk->approve_date;
                 $setcar_by = $bk->setcar_by;
                 $setcar_date = $bk->setcar_date;
                 $com_id = $bk->com_id;
                 $bkuse = $bk->bk_use;
                 $edit_by=$bk->edit_by;
                 $edit_date=$bk->edit_date;
                 $edit_reasons = $bk->edit_reasons;
                 $bk_reasons=$bk->bk_reasons;
                 $success_by=$bk->success_by;
                 $success_date=$bk->success_date;
                 $success_reasons=$bk->success_reasons;
                 $change_detail = $bk->change_detail;
                 $change_by= $bk->change_by;
                 $bk_merge = $bk->bk_merge;
                 $bk_ot = $bk->bk_ot;
                endforeach;

                $appprove = DB::table('tb_employee')->where('emp_id','=',$approve_by)->get();
                foreach ($appprove as $ap) {
                  $approve_name = $ap->emp_fname." ".$ap->emp_lname;
                }
                $setcar = DB::table('tb_employee')->where('emp_id','=',$setcar_by)->get();
                foreach ($setcar as $sc) {
                  $setcar_name = $sc->emp_fname." ".$sc->emp_lname;
                }
                $sqlcar=DB::table('tb_car')->join('tb_color','tb_car.color_id','=','tb_color.color_id')->where('car_id','=',$car)->where("com_id","=",$com_id)->get();
                // $sqldriver=DB::table('tb_driver')->where('drive_id','=',$drive)->where("com_id","=",$com_id)->get();
                if ($drive != 'D0000000000') {
                  $sqldriver=DB::table('tb_driver')->where('drive_id','=',$drive)->where("com_id","=",$com_id)->get();
                  foreach ($sqldriver as $driver) {
                    $name_d = $driver->drive_fname." ".$driver->drive_lname;
                    $tel = $driver->drive_tel;
                  }
                }else {
                  $name_d = "Do not need driver";
                  $tel = "-";
                }
                $arr = array();
                if($status == "wait"){
                    array_push($arr,"รอการอนุมัติ","warning","#F39C12");
                  }else if($status == "approve"){
                    array_push($arr,"รอการจัดรถ","info","#3498DB");
                  }else if($status == "success"){
                    array_push($arr,"สำเร็จ","success","#2ECC71");
                  }else if($status == "merge"){
                    array_push($arr,"สำเร็จ(ร่วมเดินทาง)","success","#2ECC71");
                  }else if($status == "eject"){
                    array_push($arr,"ยกเลิกการจอง","danger","#E74C3C");
                  }else if($status == "ejectcar"){
                    array_push($arr,"ยกเลิกการเดินทาง","danger","#E74C3C");
                  }else if($status == "nonecar"){
                    array_push($arr,"ไม่มีรถ","danger","#E74C3C");
                  }

                  function datetimefull($datetime)
                  {
                    $y = substr($datetime,0,4);
                    $m = substr($datetime,5,2);
                    $d = substr($datetime,8,2);
                    $h = substr($datetime,11,2);
                    $i = substr($datetime,14,2);
                    return "วันที่ ".$d."/".$m."/".$y." เวลา ".$h.":".$i." น." ;
                  }
                  function datetimesome($datetime1,$datetime2)
                  {
                    $y1 = substr($datetime1,0,4);
                    $m1 = substr($datetime1,5,2);
                    $d1 = substr($datetime1,8,2);
                    $h1 = substr($datetime1,11,2);
                    $i1 = substr($datetime1,14,2);
                    $y2 = substr($datetime2,0,4);
                    $m2 = substr($datetime2,5,2);
                    $d2 = substr($datetime2,8,2);
                    $h2 = substr($datetime2,11,2);
                    $i2 = substr($datetime2,14,2);
                    return $d1."/".$m1."/".$y1." ถึง ".$d2."/".$m2."/".$y2." ช่วงเวลา ".$h1.":".$i1."น. ถึง ".$h2.":".$i2."น." ;
                  }

 ?>

 <style>
 .header{text-align:center; padding-bottom:3px; font-size:18px; font-weight:bold;}
 .sub-header{text-align:center; padding-bottom:3px; font-size:16px;}
 .footer {
               position:fixed;
               right:0px;
               bottom:0px;
          }
 </style>

{{-- header --}}
<table>
      <tr>
        <td>
          <img src="image/logo_siamraj.png" width="60">
        </td>
        <td>
          <span class='header'>บริษัท สยามราชธานี จำกัด</span>
          <br />
          <span class='sub-header'>CarPool Service</span>
          {{-- <span class='sub-header'> เลขที่ 329 หมู่ที่ 10 ถนนรถรางสายเก่า ตำบลสำโรง อำเภอพระประแดง จังหวัดสมุทรปราการ 10130</span> --}}
        </td>
      </tr>
</table>
<div class='header'>
  ใบขอใช้รถยนต์
</div>

{{-- content --}}
<table class="content" style="padding-left:10%;">
        <tr>
          <td>เลขที่อ้างอิง</td>
          <td>:</td>
          <td>{{$bk_id}}
            @if ($bk_ot=='on')
              (OT)
            @endif
          </td>
        </tr>
        <tr>
          <td>สถานะการจอง</td>
          <td>:</td>
          <td>{{$arr[0]}}</td>
        </tr>
        <tr>
          <td>ชื่อผู้จอง</td>
          <td>:</td>
          <td>{{$fullname}} ({{$dep_name}})</td>
        </tr>
        <tr>
          <td>ไซต์งาน</td>
          <td>:</td>
          <td>
            <?php if ($bk_sitecode == '') {echo 'ไม่ระบุไซต์งาน';}
                  else { echo $bk_sitecode;}
            ?>
          </td>
        </tr>
        <tr>
          <td>เบอร์โทรศัพท์</td>
          <td>:</td>
          <td><?php if ($ttel == '') {echo 'ไม่ระบุเบอร์โทรศัพท์';}
                else { echo $ttel;}
          ?></td>
        </tr>
        <tr>
          <td>เบอร์โทรศัพท์(มือถือ)</td>
          <td>:</td>
          <td>{{$mtel}}</td>
        </tr>
        <tr>
          <td>วันและเวลาที่เดินทาง</td>
          <td>:</td>
          <td>
            <?php if (!$bkuse) {
              echo datetimefull($bk_start_start); echo " ถึง "; echo datetimefull($bk_end_start);
            } else {
              echo datetimesome($bk_start_start,$bk_end_start);
            }?>

          </td>
        </tr>
        <tr>
          <td>จำนวนผู้เดินทาง</td>
          <td>:</td>
          <td>
            <?php if ($percon == 0) {echo 'ไม่ระบุจำนวนคน';}
                  else { echo $percon." คน";}
            ?>
          </td>
        </tr>
        <tr>
          <td style="vertical-align: top;">รายละเอียดการเดินทาง</td>
          <td  style="vertical-align: top;">:</td>
          <td>
            <?php
            $locate = DB::table('tb_booking_location')->where('bk_id','=',$bk_id)->get();
            foreach ($locate as $lo) {
              $lo_id =$lo->location_id;
                if ($lo->location_id == "1") {
                  echo "<dd>สถานที่เริ่มต้น : ".$lo->location_name."<br>";
                }
                else {
                  echo "สถานที่ ".($lo->location_id-1)." : ".$lo->location_name."<br>";
                }
              }
             ?>
           </td>
        </tr>

        <tr>
          <td>วัตถุประสงค์</td>
          <td>:</td>
          <td>
            <?php if ($obj == '') {echo 'ไม่ระบุวัตถุประสงค์';}
                else { echo $obj;}
            ?>
          </td>
        </tr>
        <tr>
          <td>หมายเหตุ</td>
          <td>:</td>
          <td>
            <?php if ($note == '') {echo '-';}
                else { echo $note;}
            ?>
          </td>
        </tr>
@if ($approve_by != '')
        <tr>
          <td>อนุมัติโดย</td>
          <td>:</td>
          <td>
            {{$approve_name}} {{datetimefull($approve_date)}}
            @if ($status=='eject')
               : {{$bk_reasons}}
            @endif

          </td>
        </tr>
@endif
@if ($setcar_by != '')
        <tr>
          <td>จัดรถโดย</td>
          <td>:</td>
          <td>{{$setcar_name}} {{datetimefull($setcar_date)}}
          </td>
        </tr>
        @if ($status=='success')
        <?php
        foreach ($sqlcar as $car) {
          $car_number = $car->car_number;
          $car_model = $car->car_model;
          $color_name = $car->color_name;
        }
        // foreach ($sqldriver as $driver) {
        //   $name = $driver->drive_fname." ".$driver->drive_lname;
        //   $tel = $driver->drive_tel;

        // }
         ?>
        <tr>
          <td>คนขับ</td>
          <td>:</td>
          <td> {{$name_d}} ({{$tel}})<br>
        </tr>
        <tr>
          <td>หมายเลขทะเบียนรถ</td>
          <td>:</td>
          <td>{{$car_number}}<br>
        </tr>
        <tr>
          <td>สีรถ</td>
          <td>:</td>
          <td>{{$color_name}}<br>
        </tr>
        <tr>
          <td>เดินทางร่วมกับ</td>
          <td>:</td>
          <td>
        <?php
        if ($bk_merge !="") {

               $pos = strpos($bk_merge,';');
               if($pos!==FALSE){
                 // echo "พบ ; ที่ตำแหน่ง<b> $pos </b>$bk_merge";
                  $str = explode(";",$bk_merge);
                 }else{
                   $str = array($bk_merge);
                 }
                 // print_r($str);
           for ($i=0; $i < count($str); $i++) {
             $sql = DB::table("tb_booking")
                         ->join("tb_employee",'tb_booking.emp_id','=','tb_employee.emp_id')
                         ->join("tb_car_type",function($join){
                               $join->on('tb_booking.ctype_id', '=' , 'tb_car_type.ctype_id');
                           })
                         ->join("tb_department",function($join){
                                $join->on('tb_booking.dep_id', '=' , 'tb_department.dep_id')
                                     ->on("tb_booking.com_id","=","tb_department.com_id");
                           })
                         ->where("bk_id",'=',$str[$i])->get();
             foreach ($sql as $mbk) {
              echo "หมายเลขอ้างอิงที่".$mbk->bk_id." ของ".$mbk->emp_fname." ".$mbk->emp_lname."<br>" ;
             }

            }
          }
        ?>
      </tr>
        @endif
@endif
@if ($edit_by !="")
  <tr>
    <td>แก้ไขโดย</td>
    <td>:</td>
    <td>
    <?php
    $sql_setcar = DB::table('tb_employee')->where('emp_id','=',$edit_by)->get();
    foreach ($sql_setcar as $emp_setcar) {
      $fullset = $emp_setcar->emp_fname." ".$emp_setcar->emp_lname;
    }
     echo $fullset."  แก้ไขเมื่อ ".datetimefull($edit_date) ?>
   </td>
  </tr>
@endif
@if ($success_by !="")
  <tr>
    <td>แก้ไขวันใช้งานโดย</td>
    <td>:</td>
    <td><?php
    $sql_setcar = DB::table('tb_employee')->where('emp_id','=',$success_by)->get();
    foreach ($sql_setcar as $emp_setcar) {
      $fullset = $emp_setcar->emp_fname." ".$emp_setcar->emp_lname;
    }
     echo $fullset."  แก้ไขเมื่อ ".datetimefull($success_date) ?>
     เหตุผลที่แก้ไข
     <?php if ($success_reasons == '') {echo '-';}
      else { echo $success_reasons;}
     ?>
   </td>
 </tr>
@endif
</table>


{{-- footer --}}
<div class="footer">
  Print Form Carpool Service ({{date('d M Y')}}, {{date('H:i:s')}})
</div>
