@extends('welcome')
@extends('dashboard.topNavbar')
@extends('dashboard.SideNavbar')

@section('content')
  @foreach ($users as $r)
    <?php
          $com_id = $r->com_id;
          $job_id = "JOB011";
     ?>
  @endforeach
  <style media="screen">
  .fc-event-container{text-align: center;}
  </style>
<div class="container-dashboard">
  <div class="card col-md-12 row">

    <div class="card-header">
      <div class="row">
        <span class="fa fa-calendar-o mr-auto" style="color:#000;"> ปฏิทินการใช้รถ</span>
        <button type="button" class='btn btn-success btn-sm' id="todaycar">รายการใช้รถ</button>
        &nbsp;
        <button type="button" class='btn btn-success btn-sm' id="todaycarOT">รายการใช้รถOT</button>
      </div>
    </div>
    <input type="hidden"  id="com_id" value="{{$com_id}}">
    <input type="hidden"  id="job_id" value="{{$job_id}}">

      <div class="col-12">
        <br>
        <div id="calendarCar_select">

        </div>
      </div>
  </div>
</div>

  <div class="modal-area"></div>

<script>
$(function(){
console.log($('#job_id').val());
$('#calendarCar_select').fullCalendar({
  fixedWeekCount: false,
  // weekends: false,
  //   defaultView: 'agenda',
  //   dayCount: 1,
    header: {
        left: 'today',  //  prevYear nextYea
        center: 'title',
        right: 'prevYear,prev,next,nextYear',
    },
    buttonIcons:{
        prev: 'left-single-arrow',
        next: 'right-single-arrow',
        prevYear: 'left-double-arrow',
        nextYear: 'right-double-arrow'
    },

    events: {
      url:"/allcarEvent?data="+$('#com_id').val()+"&job_id="+$('#job_id').val(),
      error:function(data){
        console.log(data.responseText);
      }
    },eventClick: function(calEvent, jsEvent, view) {
      callDeatil("bk="+calEvent.id);
    },
    //=======================
    // eventRender: function(event, element) {
    //         element.qtip({
    //             content: event.description
    //         });
    //     },
    //=======================
    eventLimit:true,
    lang: 'th'
});
});


function callSelect_car(){
  $.ajax({
    url:"/selectcar",data:"",type:"GET",success:function(data){
        $(".modal-area").html(data);
        $("#modalSelectCar").modal("show");

    }
  });
}

function callDeatil(detail){
  $.ajax({
    url:"/detail",data:detail,type:"GET",success:function(data){
        $(".modal-area").html(data);
        $("#modalBk").modal("show");
    }
  });
}

$("#todaycar").click(function () {
  var data =btoa(btoa(btoa($("#job_id").val())));
    window.location="/reportcar?w="+data;
})
$("#todaycarOT").click(function () {
  var data =btoa(btoa(btoa($("#job_id").val())));
    window.location="/reportcarOT?w="+data;
})

</script>
@endsection
