<?php
if($typeSearch == 0){
  $sqlemp = DB::table('tb_employee')
          ->join('tb_company', 'tb_employee.com_id', '=' , 'tb_company.com_id' )
          ->join('tb_employee_login', 'tb_employee.emp_id', '=' , 'tb_employee_login.emp_id' )
          // ->join("tb_job",function($join){
          //       $join->on("tb_employee.emp_job","=","tb_job.job_id")
          //            ->on("tb_employee.com_id","=","tb_job.com_id");
          //   })
          ->join("tb_department",function($join){
                $join->on("tb_employee.dep_id","=","tb_department.dep_id")
                     ->on("tb_employee.com_id","=","tb_department.com_id");
            })
          ->join('tb_level', 'tb_level.lv_id', '=' , 'tb_employee_login.emp_level' )
          ->where('tb_employee_login.emp_id','<>','SYS000000000')
          ->orderBy('tb_employee_login.emp_id', 'ASC');
}else if($typeSearch == 1){
  $sqlemp = DB::table('tb_employee')
          ->join('tb_company', 'tb_employee.com_id', '=' , 'tb_company.com_id' )
          ->join('tb_employee_login', 'tb_employee.emp_id', '=' , 'tb_employee_login.emp_id' )
          // ->join("tb_job",function($join){
          //       $join->on("tb_employee.emp_job","=","tb_job.job_id")
          //           ->on("tb_employee.com_id","=","tb_job.com_id");
          //   })
          ->join("tb_department",function($join){
                $join->on("tb_employee.dep_id","=","tb_department.dep_id")
                    ->on("tb_employee.com_id","=","tb_department.com_id");
            })
          ->join('tb_level', 'tb_level.lv_id', '=' , 'tb_employee_login.emp_level' )
          ->where('tb_employee.emp_id','LIKE','%'.$txtSearch.'%')->where('tb_employee_login.emp_id','<>','SYS000000000')
          ->orderBy('tb_employee_login.emp_id', 'ASC');
}else if($typeSearch == 2){
  $sqlemp = DB::table('tb_employee')
            ->join('tb_company', 'tb_employee.com_id', '=' , 'tb_company.com_id' )
            ->join('tb_employee_login', 'tb_employee.emp_id', '=' , 'tb_employee_login.emp_id' )
            // ->join("tb_job",function($join){
            //       $join->on("tb_employee.emp_job","=","tb_job.job_id")
            //           ->on("tb_employee.com_id","=","tb_job.com_id");
            //   })
            ->join("tb_department",function($join){
                  $join->on("tb_employee.dep_id","=","tb_department.dep_id")
                      ->on("tb_employee.com_id","=","tb_department.com_id");
              })
            ->join('tb_level', 'tb_level.lv_id', '=' , 'tb_employee_login.emp_level' )
            ->where('tb_department.dep_id','=',$emp_oc)->where('tb_employee_login.emp_id','<>','SYS000000000')
            ->orderBy('tb_employee_login.emp_id','ASC');
}else if($typeSearch == 3){
  $sqlemp = DB::table('tb_employee')
            ->join('tb_company', 'tb_employee.com_id', '=' , 'tb_company.com_id' )
            ->join('tb_employee_login', 'tb_employee.emp_id', '=' , 'tb_employee_login.emp_id' )
            // ->join("tb_job",function($join){
            //       $join->on("tb_employee.emp_job","=","tb_job.job_id")
            //           ->on("tb_employee.com_id","=","tb_job.com_id");
            //   })
            ->join("tb_department",function($join){
                  $join->on("tb_employee.dep_id","=","tb_department.dep_id")
                      ->on("tb_employee.com_id","=","tb_department.com_id");
              })
            ->join('tb_level', 'tb_level.lv_id', '=' , 'tb_employee_login.emp_level' )
            ->where('job_id','LIKE','%'.$emp_job.'%')->where('tb_employee_login.emp_id','<>','SYS000000000')
            ->orderBy('tb_employee_login.emp_id', 'ASC');
}else if($typeSearch == 4){
  $sqlemp = DB::table('tb_employee')
            ->join('tb_company', 'tb_employee.com_id', '=' , 'tb_company.com_id' )
            ->join('tb_employee_login', 'tb_employee.emp_id', '=' , 'tb_employee_login.emp_id' )
            // ->join("tb_job",function($join){
            //       $join->on("tb_employee.emp_job","=","tb_job.job_id")
            //           ->on("tb_employee.com_id","=","tb_job.com_id");
            //   })
            ->join("tb_department",function($join){
                  $join->on("tb_employee.dep_id","=","tb_department.dep_id")
                      ->on("tb_employee.com_id","=","tb_department.com_id");
              })
            ->join('tb_level', 'tb_level.lv_id', '=' , 'tb_employee_login.emp_level' )
            ->where('emp_level','LIKE','%'.$emp_lv.'%')->where('tb_employee_login.emp_id','<>','SYS000000000')
            ->orderBy('tb_employee_login.emp_id', 'ASC');
}else if($typeSearch == 5){
  $sqlemp = DB::table('tb_employee')
            ->join('tb_company', 'tb_employee.com_id', '=' , 'tb_company.com_id' )
            ->join('tb_employee_login', 'tb_employee.emp_id', '=' , 'tb_employee_login.emp_id' )
            // ->join("tb_job",function($join){
            //       $join->on("tb_employee.emp_job","=","tb_job.job_id")
            //           ->on("tb_employee.com_id","=","tb_job.com_id");
            //   })
            ->join("tb_department",function($join){
                  $join->on("tb_employee.dep_id","=","tb_department.dep_id")
                      ->on("tb_employee.com_id","=","tb_department.com_id");
              })
            ->join('tb_level', 'tb_level.lv_id', '=' , 'tb_employee_login.emp_level' )
            ->where('emp_status','LIKE','%'.$emp_status.'%')->where('tb_employee_login.emp_id','<>','SYS000000000')
            ->orderBy('tb_employee_login.emp_id', 'ASC');
}

    if ($com_id == 'C0000' && $lv_user == '999') {
      $qryemp = $sqlemp->get();
      $numemp = $sqlemp->count();
    }else{
      $qryemp = $sqlemp->where('tb_employee_login.com_id','=',$com_id)->get();
      $numemp = $sqlemp->where('tb_employee_login.com_id','=',$com_id)->count();
    }

if($numemp > 0){

    foreach ($qryemp as $emp):


      ?>
      <?php

          if($numemp > 0){

          ?>

          <tr class="empDetail" data-id="{{$emp->emp_id}}">
            <td width="15%" align="center" style="border-right:solid 0px;">
              <img width="70%" src="image/profile/{{$emp->emp_img}}"></td>
            <td style="border-left:solid 0px;"  >
              <h6><b>{{$emp->emp_fname." ".$emp->emp_lname}}</b></h6>
              {{"แผนก ".$emp->dep_name}}<br>
              {{-- <small>{{"ตำแหน่ง ".$emp->job_name}}</small><br> --}}
              <?php
                if ($com_id == 'C0000' && $lv_user == '999') {
                  $com_name = $emp->com_name;
                  echo $com_name;
                }
               ?>

            </td>
          </tr>
        <?php }else{ ?>
          <tr>
              <td colspan="5" align="center"  ><h5>ไม่พบกิจกรรม</h5></td>
          </tr>
    <?php  }
  endforeach; ?>
  <?php }else{ ?>
          <tr>
              <td colspan="5" align="center"  ><h5>ไม่พบกิจกรรม</h5></td>
          </tr>
  <?php } ?>
