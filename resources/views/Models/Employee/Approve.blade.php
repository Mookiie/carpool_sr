<?php
$sqlemployee = DB::table('tb_employee')->join('tb_employee_login','tb_employee.emp_id','=','tb_employee_login.emp_id')->where('tb_employee.emp_id', '=', $id)->get();
                foreach ($sqlemployee as $emp):
                  $emp_id=$emp->emp_id;
                  $com_id = $emp->com_id;
                  $dep_id= $emp->dep_id;
                  $emp_fname=$emp->emp_fname;
                  $emp_lname=$emp->emp_lname;
                  $emp_lv = $emp->emp_level;
                endforeach;
?>
<div class="modal fade" id="modalemp">
  <div class="modal-dialog" role="document">
  <div class="modal-content">

      <div class="modal-header">
      <h5 class="modal-title  ">กำหนดสิทธิ์อนุมัติ <?php echo $emp_fname." ".$emp_lname; ?></h5>
      <input type="hidden"  id="emp" value="{{$emp_id}}">
      <input type="hidden" id="com" value="{{$com_id}}">

      <div align="right">
      </div>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
      <span aria-hidden="true">&times;</span>
      </button>
      </div>

      <div class="modal-body" id="tab1">

        {{-- <div class="row">
        <label class="col-md-4"><b>รหัสพนักงาน</b></label>
          <div class="col-md-8"> --}}
            <?php //echo $id; ?>
          {{-- </div>
        </div> --}}

        <div class="row">
        <label class="col-md-5"><b>ชื่อ-สกุล</b></label>
          <div class="col-md-7">
            <?php echo $emp_fname." ".$emp_lname; ?>
          </div>
        </div>
        <div class="row">
        <label class="col-md-5"><b>หน่วยงานที่มีสิทธิ์อนุมัติ</b></label>
          <div class="col-md-7" id='ApproveShow'>

          </div>
        </div>

        <form id="SetPermission">

          <div class="form-group row">
            <label for="dep_id" class="col-md-5 col-form-label">หน่วยงาน</label>
            <div class="col-7 select-dep">
              <select class="form-control mr-sm-2"  id="dep_id" name="dep_id">
              </select>
            </div>
          </div>

          <div class="row">
            <div class="col-md-12" align="center">
                <button type="button" class="btn btn-success btn-sm btn-approve" id="btn-save">บันทึก</button>
            </div>
          </div>

        </form>

    </div>
    <script>
      $(document).ready(function () {
        Approver();
      })

      $('.btn-approve').click(function () {

        var dep_id = $('#dep_id').val()
        var com_id = $('#com').val();
        var emp_id = $('#emp').val()
                $.ajax({
                  url:"/SetApproveDB",
                  data:'com_id='+com_id+'&emp_id='+emp_id+'&dep_id='+dep_id,
                  type:"GET",
                  success:function(data){
                    var obj =JSON.parse(data);
                    if(obj['success']==true)
                      {
                         Approver();
                      }
                  }
                })
      })
      function Approver() {
        var com_id = $('#com').val();
        var emp_id = $('#emp').val()
        $.ajax(
        {url:"/SetApproveShow",
        type:"GET",
        data:'com_id='+com_id+'&emp_id='+emp_id,
        contentType: false,
        processData: false,
        success:function(data){
          $("#ApproveShow").html(data);

              $('.deleteApprove').click(function () {
                var dep_id = $(this).data('id');
                $.ajax({
                  url:"/SetApproveDelete",
                  data:'com_id='+com_id+'&emp_id='+emp_id+'&dep_id='+dep_id,
                  type:"GET",
                  success:function(data){
                    var obj =JSON.parse(data);
                    if(obj['success']==true)
                      {
                         Approver();
                      }
                  }
                })
              })
          }
        })
        $.ajax({
          url:"/SetApproveDep",
          type:"GET",
          data:'com_id='+com_id+'&emp_id='+emp_id,
          contentType: false,
          processData: false,
          success:function(data){
            $("#dep_id").html(data);
            }
        })
      }



    </script>
