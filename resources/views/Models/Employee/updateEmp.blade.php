<div class="modal fade" id="modalBk" >
  <div class="modal-dialog modal-lg" role="document" id="dialogBk" >
    <div class="modal-content" >
    <div class="modal-header">
      <h5 class="modal-title text-black"> อัพเดทพนักงาน</h5>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body">

      <form name="createform" id="create-form">

         <div class="form-group row">
            <!-- รหัสพนักงาน -->
             <input type="hidden" name="emp_id" id="emp_id" value="<?php echo $emp_id ?>">
            <!-- รหัสพนักงาน -->
            <!-- รหัสบริษัท -->
              <input type="hidden" name="com_id" id="com_id" value="{{$com_id}}">
            <!-- รหัสบริษัท -->

            <!-- รหัสรหน่วยงาน -->
              {{-- <input type="hidden"  id="dep_id" name="dep_id" value="{{$dep_id.sprintf("%03d",$id1)}}"> --}}
            <!-- รหัสหน่วยงาน -->
          </div>
          {{-- <div class="form-group row"><!-- ชื่อหน่วยงาน -->
            <label for="dep_name" class ="col-md-4 col-form-label text-black">ชื่อหน่วยงาน</label>
            <input type="text" class="form-control col-md-6" id="dep_name" name="dep_name" placeholder="ชื่อหน่วยงาน">
          </div><!-- ชื่อหน่วยงาน -->

          <div class="form-group row"><!-- เบอร์โทร -->
            <label for="tel" class ="col-md-4 col-form-label text-black">เบอร์โทร</label>
            <input type="text" class="form-control col-md-6" id="tel" name="tel" placeholder="เบอร์โทรศัพท์" onfocus="rmErr(this);" onkeypress="rmErr(this);">
            <div hidden="true" id="fbtel" class="offset-md-4 form-control-feedback"><br></div>
          </div><!-- เบอร์โทร --> --}}

          <div class="form-group row"><!-- username -->
            <label for="username" class ="col-md-4 col-form-label" align='right'><span class="fa fa-user text-black" ></span> </label>
            <input type="text" class="form-control col-md-4" id="username" name="username" placeholder="username">
          </div><!-- username -->

          <div class="form-group row"><!-- password -->
            <label for="password" class ="col-md-4 col-form-label" align='right'><span class="fa fa-unlock-alt text-black" ></span> </label>
            <input type="password" class="form-control col-md-4" id="password" name="password" placeholder="password">
            <div hidden="true" id="fbtel" class="offset-md-4 form-control-feedback"><br></div>
          </div><!-- password -->
      </form>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-primary btn-create">อัพเดทข้อมูล</button>
      <button type="button" class="btn btn-secondary" data-dismiss="modal">ยกเลิก</button>
    </div>
  </div>
</div>
</div>
<script>
$(document).ready(function(){
$("#dep_name").focus();
});
$("#create-form").keypress(function(event){
 var kc = event.keyCode;
 if(kc==13){
    createDep();
 }
});
$(".btn-create").click(function(){
  createDep();
});

function createDep(){
var form_create= $("#create-form").serialize();
 $.ajax({
  url:"/updateEmp",
  data:form_create,
  type:"POST",
  success:function(data){
    var obj =JSON.parse(data);
    if(obj['success']==true)
      {
        swal({
                  title: "อัพเดทข้อมูลสำเร็จ",
                  text: obj['msg'],
                  type: "success",
                  showCancelButton: false,
                  confirmButtonColor: "#2ECC71",
                  confirmButtonText: "ตกลง",
                  closeOnConfirm: false,
                },
                  function(isConfirm){
                    if (isConfirm) {
                      window.location = "/SetPermission";
                }
            });
      }
    else
      {
        swal({
                  title: "ผิดพลาด",
                  text: obj['msg'],
                  type: "error",
                  showCancelButton: false,
                  confirmButtonColor: "#2ECC71",
                  confirmButtonText: "ตกลง",
                  closeOnConfirm: true,

            });
      }
  }
 });
};
// function rmErr(input){
//  $("#group"+input.id).removeClass("has-danger");
//  $("#group"+input.id+" input").removeClass("form-control-danger");
//  $("#fb"+input.id).attr("hidden","hidden");
// }
//
// function addErr(type,msg){
//    $("#group"+type).addClass("has-danger");
//    $("#group"+type+" input").addClass("form-control-danger");
//    $("#fb"+type).html(msg);
//    $("#fb"+type).removeAttr("hidden");
// }

</script>
