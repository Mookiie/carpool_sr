<?php
$sqlemployee = DB::table('tb_employee')->join('tb_employee_login','tb_employee.emp_id','=','tb_employee_login.emp_id')->where('tb_employee.emp_id', '=', $id)->get();
                foreach ($sqlemployee as $emp):
                  $emp_id=$emp->emp_id;
                  $com_id = $emp->com_id;
                  $dep_id= $emp->dep_id;
                  $emp_fname=$emp->emp_fname;
                  $emp_lname=$emp->emp_lname;
                  $emp_lv = $emp->emp_level;
                endforeach;
?>
<div class="modal fade" id="modalemp">
  <div class="modal-dialog" role="document">
  <div class="modal-content">

      <div class="modal-header">
      <h5 class="modal-title  ">กำหนดสิทธิ์การใช้งาน <?php echo $emp_fname." ".$emp_lname; ?></h5>
      <div align="right">
      </div>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
      <span aria-hidden="true">&times;</span>
      </button>
      </div>

      <div class="modal-body" id="tab1">

        {{-- <div class="row">
        <label class="col-md-4"><b>รหัสพนักงาน</b></label>
          <div class="col-md-8"> --}}
            <?php //echo $id; ?>
          {{-- </div>
        </div> --}}

        <div class="row">
        <label class="col-md-4"><b>ชื่อ-สกุล</b></label>
          <div class="col-md-8">
            <?php echo $emp_fname." ".$emp_lname; ?>
          </div>
        </div>
        <div class="row">
        <label class="col-md-4"><b>หน่วยงาน</b></label>
          <div class="col-md-8">
            <?php
            $sql=DB::table('tb_department')->where('dep_id','=',$dep_id)->where('com_id','=',$com_id)->get();
            foreach ($sql as $dep):
              echo $dep->dep_name;
            endforeach;
             ?>
          </div>
        </div>

        <form id="SetPermission">
          <input id="emp_update" name="emp_update" type="hidden" class="form-control" value="<?php echo $emp_id; ?>">
          <div class="row">
            <div class="col-md-12" align="center">
              <?php $sql_level = DB::table('tb_level')->where('lv_id','<>','999')->where('lv_id','<>','99')->where('lv_id','<>',$emp_lv)->get() ?>
              @foreach ($sql_level as $lv)
                <button type="button" class="btn btn-success btn-sm btn-permission" id="btn-save" data-id="lv={{$lv->lv_id}}">{{$lv->lv_name}}</button>
              @endforeach
            </div>
          </div>

        </form>

    </div>
    <script>
      $('.btn-permission').click(function () {
        var lv =$(this).data('id');
        var emp_id = $('#emp_id').val()
        var emp_update = $('#emp_update').val()
                $.ajax({
                  url:"/SetPermissioDB",
                  data:lv+'&emp_id='+emp_id+"&emp_update="+emp_update,
                  type:"GET",
                  success:function(data){
                    // console.log(data);
                    var obj =JSON.parse(data);
                    if(obj['success']==true)
                      {
                        swal({
                                  title: "อัพเดทข้อมูลสำเร็จ",
                                  text: obj['msg'],
                                  type: "success",
                                  showCancelButton: false,
                                  confirmButtonColor: "#2ECC71",
                                  confirmButtonText: "ตกลง",
                                  closeOnConfirm: false,
                                },
                                  function(isConfirm){
                                    if (isConfirm) {
                                      window.location = "/SetPermission";
                                }
                            });
                      }
                  }
                })
      })
    </script>
