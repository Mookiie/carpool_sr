<?php
$sqldriver = "";
if($typeSearch == 0){
  $sqldriver = DB::table('tb_driver')->orderBy('drive_id', 'ASC')->where('com_id','=',$com_id)->whereIn('dep_id', [$dep_id,'ALL'])->where('drive_status','=','0')->get();
  }else if($typeSearch == 1){
    $sqldriver = DB::table('tb_driver')->where('drive_fname','like','%'.$txtSearch.'%')->where('com_id','=',$com_id)->whereIn('dep_id',[$dep_id,'ALL'])->where('drive_status','=','0')->orderBy('drive_id', 'ASC')->get();
  }else if($typeSearch == 2){
    $sqldriver = DB::table('tb_driver')->where('drive_tel','like','%'.$txtSearch.'%')->where('com_id','=',$com_id)->whereIn('dep_id',[$dep_id,'ALL'])->where('drive_status','=','0')->orderBy('drive_id', 'ASC')->get();
  }else if($typeSearch == 3){
    if ($drive_status == 0) {
      $sqldriver = DB::table('tb_driver')->where('drive_status','=','0')->where('com_id','=',$com_id)->whereIn('dep_id',[$dep_id,'ALL'])->orderBy('drive_id', 'ASC')->get();
    }
    else {
      $sqldriver = DB::table('tb_driver')->where('drive_status','=','1')->where('com_id','=',$com_id)->whereIn('dep_id',[$dep_id,'ALL'])->orderBy('drive_id', 'ASC')->get();
    }
  }else {
  $sqldriver = DB::table('tb_driver')->where('com_id','=',$com_id)->whereIn('dep_id',[$dep_id,'ALL'])->orderBy('drive_id', 'ASC')->get();
}

$numdriver = count($sqldriver);
if($numdriver > 0){

    foreach ($sqldriver as $drive):

    if($numdriver > 0){
      $url_img = Storage::url('public/image/driver/'.$drive->drive_img);
  ?>
<tr class="DriverDetail" data-id="id=<?php echo $drive->drive_id; ?>">
  <td width="15%" align="center" style="border-right:solid 0px;">
    <img class="" height="100" src="{{$url_img}}"></td>
  <td style="border-left:solid 0px;">
    <h6><b class=""><?php echo $drive->drive_fname." ".$drive->drive_lname; ?></b></h6>
    <small class=""><?php echo $drive->drive_tel; ?></small><br>
    <?php if ($lv_user == 2) {
        if ($drive->drive_status ==0 ){ ?>
      <small class="">สถานะการใช้งาน : ใช้งาน</small><br>
      @if ($drive->drive_id <> 'D0000000000')
        <button type="button" class='btn btn-danger btn-sm btn-driver' data-id="<?php echo $drive->drive_id; ?>">ยกเลิกการใช้งาน</button>
      @endif
    <?php }else{ ?>
      <small><small class=""></small>สถานะการใช้งาน : ยกเลิกการใช้งาน<br></small>

    <?php }
        }?>
  </td>
</tr>
<?php }else{ ?>
  <tr>
      <td colspan="5" align="center" class=""><h5>ไม่พบกิจกรรม</h5></td>
  </tr>
<?php }endforeach;
  }else{ ?>
  <tr>
      <td colspan="5" align="center" class=""><h5>ไม่พบกิจกรรม</h5></td>
  </tr>
<?php } ?>
