<?php
$sqlbooking = DB::table('tb_booking')->where('bk_id','=',$dataid)->get();
$sqlcar = DB::table('tb_car')->where('car_id','=',$car)->get();
$sqldrive = DB::table('tb_driver')->where('drive_id','=',$drive)->get();
foreach ($sqlcar as $car):
 $imgf = $car->car_img_front;
 $num = $car->car_number;
 $model = $car->car_model;
endforeach;
foreach ($sqldrive as $driver):
 $img = $driver->drive_img;
 $name = $driver->drive_fname." ".$driver->drive_lname;
 if ($drive == 'D0000000000') {
  $tel = '-';
 }
 else {
   $tel = $driver->drive_tel;
 }
endforeach;

$url_car = Storage::url('image/car/'.$imgf);
$url_driver = Storage::url('image/driver/'.$img);
?>
<div>
<h4>รถยนต์</h4>
<hr>

    <div class="row">
      <div class="col-md-3">
        <img width="100%" src="{{$url_car}}">
      </div>
      <div class="col-md-9">
        <h4><b><?php echo $num; ?></b></h4>
        <h5><?php echo $model; ?></h5>
      </div>
    </div>
<h4>คนขับ</h4>
<hr>
<div class="row">
<div class="col-md-3">
  <img class="rounded-circle" width="100%" src="{{$url_driver}}">
</div>
<div class="col-md-9">
  <h4><b><?php echo $name; ?></b></h4>
  <h5><?php echo $tel; ?></h5>
</div>
</div>
</div>
<hr>
<div align="right">
<form id="frmSetCar">
  <button type="button" class="btn btn-sm btn-success btnsetCar">บันทึก</button>
  <button class="btn btn-sm btn-danger" data-dismiss="modal">ยกเลิก</button>
</form>
</div>
<script>
$(".btnsetCar").click(function(){
  var data = "dataid="+dataid+"&car="+car+"&drive="+drive
  swal({
    title:"คุณแน่ใจ ?",
    text:"คุณต้องการบันทึกการจัดรถใช่หรือไม่ ",
    type:"warning",
    showCancelButton:true,
    confirmButtonColor:"#2ECC71",
    confirmButtonText:"ตกลง",
    cancelButtonText:"ยกเลิก",
    cancelButtonColor:"#E74C3C",
    closeOnConfirm:false,
  },function(isConfirm){
    if (isConfirm == true)
      {
        $.ajax({url:"/setCarDB",data:data,type:"POST",success:function(data){
          var obj = JSON.parse(data);
          if (obj['success']==true) {
            $.ajax({url:"/sendcar",data:"id="+obj['bk_id'],type:"POST",success:function(data){
              var obj = JSON.parse(data);
              console.log(obj);
                if (obj['success']==true) {
                    swal({
                      type:"success",
                      title:"สำเร็จ",
                      text:"คุณทำการบันทึกการจัดรถสำเร็จ",
                      confirmButtonText:"ตกลง",
                      confirmButtonColor:"#2ECC71",
                      closeOnConfirm:true,
                    },function(isConfirm){
                      location.reload();
                    });
                  }
                }
              })
        }
        else {
          swal("Cancelled!", "ไม่สามารถบันทึกการจัดรถได้", "error");
        }
        }
      });
    }//true

  });
});
</script>
