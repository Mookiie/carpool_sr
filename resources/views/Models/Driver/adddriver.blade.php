<div class="modal fade" id="modalBk" >
  <div class="modal-dialog modal-lg" role="document" id="dialogBk" >
    <div class="modal-content" >
    <div class="modal-header">
      <h5 class="modal-title fa fa-plus "> บันทึกข้อมูลรถยนต์</h5>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body">

      <form name="createdriverform" id="createdriver">

          <div class="form-group row">
            <!-- รหัสพนักงาน -->
              <input type="hidden" name="emp_id" id="emp_id" value="{{$emp_id}}">
              <input type="hidden" name="com_id" id="com_id" value="{{$com_id}}">
              <input type="hidden" name="dep_id" id="com_id" value="{{$dep_id}}">
            <!-- รหัสพนักงาน -->
            <?php
            // driver_id
            $sqldriver = DB::table('tb_driver')->orderBy('drive_id','decs')->limit(1)->get();
            foreach ($sqldriver as $driver):
             $id = $driver->drive_id;
            endforeach;//D1704120001
            $driver_id = substr($id, 0, 1);//D
            $driver_id.= date('y');//60
            $driver_id.= date('m');//07
            $driver_id.= date('d');//12
            $id1= substr($id,8, 4)+1;//00001
            ?>
            <!-- รหัสคนขับ-->
              <input type="hidden"  id="drive_id" name="drive_id" value="<?php echo $driver_id.sprintf("%04d",$id1) ?>">
          </div><!-- รหัสคนขับ-->

          <div class="form-group row"><!-- ชื่อ -->
            <label for="drive_fname" class ="col-md-4 col-form-label">ชื่อ</label>
            <input type="text" class="form-control col-md-6" id="drive_fname" name="drive_fname" placeholder="ชื่อผู้ขับ">
          </div><!-- ชื่อ -->

          <div class="form-group row"><!-- สกุล -->
            <label for="drive_lname" class ="col-md-4 col-form-label">นามสกุล</label>
            <input type="text" class="form-control col-md-6" id="drive_lname" name="drive_lname" placeholder="นามสกุลผู้ขับ">
          </div><!-- สกุล -->

          <div class="form-group row"><!-- เบอร์ -->
            <label for="drive_tel" class ="col-md-4 col-form-label">หมายเลขโทรศัพท์</label>
            <input type="text" class="form-control col-md-6" id="drive_tel" name="drive_tel" placeholder="หมายเลขโทรศัพท์">
          </div><!-- เบอร์ -->

          <div class="form-group row">
            <label for="count_person" class="col-md-4 col-form-label">ภาพคนขับรถ </label>
            <input class="col-md-6" type="file" id="image" name="image">
          </div>
          <div class="modal-footer">
            <button type="submit" class="btn btn-primary btn-create">บันทึกข้อมูล</button>
            <button type="button" class="btn btn-secondary" data-dismiss="modal">ยกเลิก</button>
          </div>
      </form>
    </div>

  </div>
</div>
</div>
<script>
$(function() {

// We can attach the `fileselect` event to all file inputs on the page
// $(document).on('change', ':file', function() {
//   var input = $(this),
//       numFiles = input.get(0).files ? input.get(0).files.length : 1,
//       label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
//   input.trigger('fileselect', [numFiles, label]);
// });
// We can watch for our custom `fileselect` event like this
// $(document).ready( function() {
//     $(':file').on('fileselect', function(event, numFiles, label) {
//         var input = $(this).parents('.input-group').find(':text'),
//             log = numFiles > 1 ? numFiles + ' files selected' : label;
//         if( input.length ) {
//             input.val(log);
//         } else {
//             if( log ) alert(log);
//         }
//     });
// });
});

// $("#createdriver").keypress(function(event){
//  var kc = event.keyCode;
//  if(kc==13){
//     $( ".btn-create" ).trigger( "click" );
//  }
// });

$("form#createdriver").submit(function (ev) {
  ev.preventDefault();
var formData = new FormData(this);
// for (var value of formData.values()) {
//              console.log(value);
// }
  $.ajax({
    url:"/createdriver",
    data:formData,
    type:"POST",
    async: true,
    cache: false,
    contentType: false,
    processData: false,
    success:function(data){
      var obj = JSON.parse(data);
        if (obj['success']==true) {
          swal({
                    title: "สำเร็จ",
                    text: "บันทึกการจองของคุณสำเร็จ",
                    type: "success",
                    showCancelButton: false,
                    confirmButtonColor: "#2ECC71",
                    confirmButtonText: "ตกลง",
                    closeOnConfirm: false,
                  },
                    function(isConfirm){
                      if (isConfirm) {
                        window.location = "/driver";
                  }
              });
        }
        else {
          swal({
                    title: "มีบางอย่างผิดพลาดขณะบันทึก",
                    text: "กรุณาตรวจสอบข้อมูลใหม่อีกครั้ง",
                    type: "error",
                    showCancelButton: false,
                    confirmButtonColor: "#2ECC71",
                    confirmButtonText: "ตกลง",
                    closeOnConfirm: true,
                  },
                    function(isConfirm){
                      if (isConfirm) {

                  }
              });
        }
    }
  });
  })


</script>
