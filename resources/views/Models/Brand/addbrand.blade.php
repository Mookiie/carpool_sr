<div class="modal fade" id="modalBk" >
  <div class="modal-dialog modal-lg" role="document" id="dialogBk" >
    <div class="modal-content" >
      
            <div class="modal-header">
                <h5 class="modal-title fa fa-plus text-black"> บันทึกข้อมูลยี่ห้อรถยนต์</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
              <form name="createform" id="create-form">
                 <div class="form-group row">
                    <!-- รหัสพนักงาน -->
                     <input type="hidden" name="emp_id" id="emp_id" value="<?php echo $emp_id ?>">
                   <!-- รหัสพนักงาน -->
                    <?php
                    // dep_id
                    $sqlfuel = DB::table('tb_fuel')->orderBy('fuel_id','decs')->limit(1)->get();
                    foreach ($sqlfuel as $fuel):
                     $id = $fuel->fuel_id;
                    endforeach;
                     ?>
                    <!-- รหัสยี่ห้อ-->
                    <input type="hidden"  id="brand_id" name="brand_id" value="<?php echo $id+1 ?>">

                  </div><!-- รหัสยี่ห้อ -->

                  <div class="form-group row"><!-- ชื่อยี่ห้อ -->
                    <label for="brand_name" class ="col-md-4 col-form-label text-black">ชื่อยี่ห้อ</label>
                    <input type="text" class="form-control col-md-6" id="brand_name" name="brand_name" placeholder="ชื่อยี่ห้อ">
                  </div><!-- ชื่อยี่ห้อ -->
             </form>
            </div>

            <div class="modal-footer">
              <button type="button" class="btn btn-primary btn-create">บันทึกข้อมูล</button>
              <button type="button" class="btn btn-secondary" data-dismiss="modal">ยกเลิก</button>
            </div>
    </div>
  </div>
</div>
<script>

$(document).ready(function(){
$("#dep_name").focus();
});

$("#create-form").keypress(function(event){
 var kc = event.keyCode;
 if(kc==13){
    createbrand();
 }
});

$(".btn-create").click(function(){
  createbrand();
});

function createbrand(){
var form_create= $("#create-form").serialize();
 $.ajax({
  url:"/createbrand",
  data:form_create,
  type:"POST",
  success:function(data){
      var obj =JSON.parse(data);
    if(obj['success']==true)
      {
        swal({
                  title: "บันทึกข้อมูลสำเร็จ",
                  text: "บันทึกข้อมูลยี่ห้อรถยนต์สำเร็จแล้ว",
                  type: "success",
                  showCancelButton: false,
                  confirmButtonColor: "#2ECC71",
                  confirmButtonText: "ตกลง",
                  closeOnConfirm: false,
                },
                  function(isConfirm){
                    if (isConfirm) {
                      window.location = "/otheradd";
                }
            });
      }
    else
      {
        addErr(obj['type'],obj['msg']);

      }
  }
 });
};


function rmErr(input){
 $("#group"+input.id).removeClass("has-danger");
 $("#group"+input.id+" input").removeClass("form-control-danger");
 $("#fb"+input.id).attr("hidden","hidden");
}

function addErr(type,msg){
   $("#group"+type).addClass("has-danger");
   $("#group"+type+" input").addClass("form-control-danger");
   $("#fb"+type).html(msg);
   $("#fb"+type).removeAttr("hidden");
}

</script>
