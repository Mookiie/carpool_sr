
<style media="screen">
  .header-table{
    background-color: #2196f3 ;
  }
</style>
<div class="header-table" align="center">
  <label>ตารางการใช้งานรถยนต์</label>
</div>
<table class="table" id="todaycar" cellspacing="0" width="100%" border="1" >
          <thead>
            <tr>
                <th>ชื่อ-สกุล</th>
                <th>เวลาเดินทาง</th>
                <th>รายละเอียด</th>
                <th>คนขับ(ทะเบียน)</th>

            </tr>
          </thead>

          <tbody>
            <?php
        $sqlemp = $sqlbooking = DB::table('tb_booking')
        ->join("tb_car_type",function($join){
              $join->on("tb_booking.ctype_id","=","tb_car_type.ctype_id");
              $join->on('tb_booking.com_id', '=' , 'tb_car_type.com_id');
          })
        // ->join('tb_car',function($join){
        //   $join->on('tb_booking.car_id','=','tb_car.car_id')
        //        ->on("tb_booking.com_id","=","tb_car.com_id");
        // })
        ->join('tb_driver',function($join){
          $join->on('tb_booking.drive_id','=','tb_driver.drive_id')
               ->on("tb_booking.com_id","=","tb_driver.com_id");
        })
        ->join("tb_employee",function($join){
               $join->on('tb_booking.emp_id', '=', 'tb_employee.emp_id')
                    ->on("tb_booking.com_id","=","tb_employee.com_id");
          })
        ->join("tb_department",function($join){
                $join->on('tb_department.dep_id', '=', 'tb_booking.dep_id')
                    ->on("tb_department.com_id","=","tb_booking.com_id");
          })
        ->where("tb_booking.com_id",'=',$com_id)
        ->where("tb_booking.dep_car",'=',$dep_car)
        ->where(DB::raw("CAST(bk_start_start AS DATE)"),"<=",$date)
        ->where(DB::raw("CAST(bk_end_start AS DATE)"),">=",$date)
        ->whereIn('bk_status', ['success', 'merge'])
        ->where('tb_booking.bk_ot','=',null)
        ->get();
if (count($sqlemp)>0) {
          foreach ($sqlemp as $emp) {
             ?>
             <tr>

                <td width="10%"><b>{{$emp->emp_fname." ".$emp->emp_lname."(".$emp->dep_name.")"}}</b><br />
                  <small>มือถือ {{$emp->bk_mtel}}</small><br />
                  <small>เบอร์โต๊ะ {{$emp->bk_ttel}}</small>
                </td>

                <td width="20%">
                  <?php
                  $date1 = substr($emp->bk_start_start,0,10);
                  $date2 = substr($emp->bk_end_start,0,10);
                  $d1= new DateTime($date1);
                  $d2 = new DateTime($date2);
                  $diff = $d1->diff($d2)->format("%a");


                  if ($diff>0) {
                    $date2 = date('Y-m-d',strtotime(str_replace('-', '/', $date2) . "+1 days"));
                    if (!$emp->bk_use) {

                      $text1 = datebk($emp->bk_start_start)." เวลา ".substr($emp->bk_start_start,11,2).".".substr($emp->bk_start_start,14,2);
                      $text2 = datebk($emp->bk_end_start)." เวลา ".substr($emp->bk_end_start,11,2).".".substr($emp->bk_end_start,14,2);

                    }
                    else {
                    $text1 = datebk($emp->bk_start_start)."-".datebk($emp->bk_end_start);
                    $text2 = " ช่วงเวลา ".substr($emp->bk_start_start,11,2).".".substr($emp->bk_start_start,14,2).
                    "-".substr($emp->bk_end_start,11,2).".".substr($emp->bk_end_start,14,2)." ของทุกวัน";
                    }
                  }
                  else {
                    $text1  = datebk($emp->bk_start_start)." เวลา ".substr($emp->bk_start_start,11,2).".".substr($emp->bk_start_start,14,2);
                    $text2 =  datebk($emp->bk_end_start)." เวลา ".substr($emp->bk_end_start,11,2).".".substr($emp->bk_end_start,14,2);
                  }
                   ?>
                  {{$text1}}<br>{{$text2}}
                </td>
                <td width="42%">
                  <b>หมายเลขอ้างอิง</b> : {{$emp->bk_id}}
                  <br />
                <?php
                $locate = DB::table('tb_booking_location')->where('bk_id','=',$emp->bk_id)->get();
                foreach ($locate as $lo) {
                  $lo_id =$lo->location_id;
                    if ($lo->location_id == "1") {
                      echo "<b>สถานที่เริ่มต้น</b> : ".$lo->location_name."<br>";
                    }
                    else {
                      echo "<b>สถานที่ ".($lo->location_id-1)."</b> : ".$lo->location_name."<br>";
                    }
                  }
                 ?>
                 <b>วัตถุประสงค์</b> :
                 <?php
                       if ($emp->bk_obj == "") {
                         echo "-";
                       }else {
                         echo $emp->bk_obj ;
                       }
                 ?>
                 <br>
                 <b>หมายเหตุ</b> :
                 <?php
                       if ($emp->bk_note == "") {
                         echo "-";
                       }else {
                         echo $emp->bk_note ;
                       }
                 ?>
                 <br>
                 <b>จำนวนผู้เดินทาง</b> :{{$emp->bk_percon}} คน
                 <br />
                 @if ($emp->bk_merge!="")
                   <b>เดินทางร่วมกับ</b> :
                   <?php
                          $pos = strpos($emp->bk_merge,';');
                          if($pos!==FALSE){
                            // echo "พบ ; ที่ตำแหน่ง<b> $pos </b>$bk_merge";
                             $str = explode(";",$emp->bk_merge);
                            }else{
                              $str = array($emp->bk_merge);
                            }

                              for ($i=0; $i < count($str); $i++) {
                                if ($i>0) {
                                    echo ",".$str[$i];
                                }else {
                                    echo $str[0];
                                }

                              }

                    ?>

                 @endif

               </td>
               <td width="15%">
                 <?php $sql_car = DB::table('tb_car')->where('car_id','=',$emp->car_id)->get();
                 foreach ($sql_car as $car) {
                   $car_number = $car->car_number;
                 }?>
                 {{$emp->drive_fname." ".$emp->drive_lname}}
                 <br />
                 {{"(".$car_number.")"}}
               </td>
            </tr>
    <?php
        }
      }
      else {
    ?>
          <tr>
            <td colspan="4" align="center"><h5>ไม่พบรายการใช้รถยนต์</h5></td>
          </tr>
    <?php
          }
     ?>
    </tbody>
</table>
<?php
function datebk ($date)
{
  $y = substr($date,0,4);
  $m = substr($date,5,2);
  $d = substr($date,8,2);
  return $day = "วันที่ ".$d."/".$m."/".$y;
};
 ?>
<script src="{{ asset ("DataTables/js/jquery.dataTables.min.js")}}" type="text/javascript"></script>


</script>

<script type="text/javascript">
    // $('#todaycar').DataTable({
    //
    //   'bSort': false,
    //    'aoColumn': [
    //          { sWidth: "15%", bSearchable: true, bSortable: true },
    //          { sWidth: "10%", bSearchable: false, bSortable: false },
    //          { sWidth: "50%", bSearchable: false, bSortable: false },
    //          { sWidth: "25%", bSearchable: false, bSortable: false }
    //          //match the number of columns here for table2
    //        ],
    //
    //     "oLanguage": {
    //             "sInfo": "_PAGE_ จาก _PAGES_ หน้า",
    //             "sInfoEmpty": "หน้า 0 จาก 0",
    //             "sEmptyTable": "ไม่พบข้อมูลที่ค้นหา",
    //             "sSearch": "ค้นหา",
    //             "sLengthMenu": "จำนวนแสดง _MENU_ รายการ",
    //             "sZeroRecords": "ไม่พบข้อมูลที่ค้นหา",
    //             "oPaginate": {
    //                 "sPrevious": "ก่อนหน้า",
    //                 "sNext": "ถัดไป",
    //             },
    //
    //       }
    // });
</script>
