<script src="{{ asset('/js/Jquery-3.2.1.min.js') }}"></script>
<script src="{{ asset('/js/bootstrap.min.js') }}"></script>
<?php
if ($type == 0) {
  if ($typeSearch == 0) {
    // ทั้งหมด
    $sql =  DB::table('tb_booking')->where('com_id','=',$com_id);
  }//$typeSearch == 0
  elseif ($typeSearch == 1) {
    // สถานะการจอง
    if ($bk_status == 'All') {
      // ทั้งหมด
      $sql =  DB::table('tb_booking')->where('com_id','=',$com_id);
    }
    else {
      $sql =  DB::table('tb_booking')->where('com_id','=',$com_id)->where('bk_status','=',$bk_status);
    }
  }//$typeSearch == 1
  elseif ($typeSearch == 2) {
    // ชื่อคนจอง
    if ($txtSearch == 'All' && $bk_status == 'All') {
        // ชื่อคนจองว่าง สถานะทั้งหมด
        $sql =  DB::table('tb_booking')->where('com_id','=',$com_id);
    }
    elseif ($txtSearch == 'All' && $bk_status <> 'All') {
      // ชื่อคนจองว่าง มีสถานะ
      $sql =  DB::table('tb_booking')->where('com_id','=',$com_id)->where('bk_status','=',$bk_status);
    }
    elseif ($txtSearch <> 'All' && $bk_status == 'All') {
      // ชื่อคนจองไม่ว่าง สถานะทั้งหมด
      $sql =  DB::table('tb_booking')->where('com_id','=',$com_id)->where('emp_id','=',$txtSearch);
    }
    else {
        // ชื่อคนจองไม่ว่าง มีสถานะ
      $sql =  DB::table('tb_booking')->where('com_id','=',$com_id)->where('emp_id','=',$txtSearch)->where('bk_status','=',$bk_status);
    }
  }//$typeSearch == 2
}
// รายวัน
elseif ($type == 1) {
  if ($typeSearch == 0) {
    // ทั้งหมด
    $sql =DB::table('tb_booking')->where('com_id','=',$com_id)->where('bk_start_start','like',''.$date1.'%');
    // $sql = DB::table('tb_booking')->where('com_id','=',$com_id)->where('bk_start_start','like','2017-12-07%');
  }//$typeSearch == 0
  elseif ($typeSearch == 1) {
    // สถานะการจอง
    if ($bk_status == 'All') {
      // ทั้งหมด
      $sql =  DB::table('tb_booking')->where('com_id','=',$com_id)->where('bk_start_start','like',$date1.'%');
    }
    else {
      $sql =  DB::table('tb_booking')->where('com_id','=',$com_id)->where('bk_status','=',$bk_status)->where('bk_start_start','like',$date1.'%');
    }
  }//$typeSearch == 1
  elseif ($typeSearch == 2) {
    // ชื่อคนจอง
    if ($txtSearch == 'All' && $bk_status == 'All') {
        // ชื่อคนจองว่าง สถานะทั้งหมด
        $sql =  DB::table('tb_booking')->where('com_id','=',$com_id)->where('bk_start_start','like',$date1.'%');
    }
    elseif ($txtSearch == 'All' && $bk_status <> 'All') {
      // ชื่อคนจองว่าง มีสถานะ
      $sql =  DB::table('tb_booking')->where('com_id','=',$com_id)->where('bk_status','=',$bk_status)->where('bk_start_start','like',$date1.'%');
    }
    elseif ($txtSearch <> 'All' && $bk_status == 'All') {
      // ชื่อคนจองไม่ว่าง สถานะทั้งหมด
      $sql =  DB::table('tb_booking')->where('com_id','=',$com_id)->where('emp_id','=',$txtSearch)->where('bk_start_start','like',$date1.'%');
    }
    else {
        // ชื่อคนจองไม่ว่าง มีสถานะ
      $sql =  DB::table('tb_booking')->where('com_id','=',$com_id)->where('emp_id','=',$txtSearch)->where('bk_status','=',$bk_status)->where('bk_start_start','like',$date1.'%');
    }
  }//$typeSearch == 2
}
// รายเดือน
elseif ($type == 2) {
  if ($typeSearch == 0) {
    // ทั้งหมด
    $sql =DB::table('tb_booking')->where('com_id','=',$com_id)->where('bk_start_start','like',''.$date3.'%');
    // $sql = DB::table('tb_booking')->where('com_id','=',$com_id)->where('bk_start_start','like','2017-12-07%');
  }//$typeSearch == 0
  elseif ($typeSearch == 1) {
    // สถานะการจอง
    if ($bk_status == 'All') {
      // ทั้งหมด
      $sql =  DB::table('tb_booking')->where('com_id','=',$com_id)->where('bk_start_start','like',$date3.'%');
    }
    else {
      $sql =  DB::table('tb_booking')->where('com_id','=',$com_id)->where('bk_status','=',$bk_status)->where('bk_start_start','like',$date3.'%');
    }
  }//$typeSearch == 1
  elseif ($typeSearch == 2) {
    // ชื่อคนจอง
    if ($txtSearch == 'All' && $bk_status == 'All') {
        // ชื่อคนจองว่าง สถานะทั้งหมด
        $sql =  DB::table('tb_booking')->where('com_id','=',$com_id)->where('bk_start_start','like',$date3.'%');
    }
    elseif ($txtSearch == 'All' && $bk_status <> 'All') {
      // ชื่อคนจองว่าง มีสถานะ
      $sql =  DB::table('tb_booking')->where('com_id','=',$com_id)->where('bk_status','=',$bk_status)->where('bk_start_start','like',$date3.'%');
    }
    elseif ($txtSearch <> 'All' && $bk_status == 'All') {
      // ชื่อคนจองไม่ว่าง สถานะทั้งหมด
      $sql =  DB::table('tb_booking')->where('com_id','=',$com_id)->where('emp_id','=',$txtSearch)->where('bk_start_start','like',$date3.'%');
    }
    else {
        // ชื่อคนจองไม่ว่าง มีสถานะ
      $sql =  DB::table('tb_booking')->where('com_id','=',$com_id)->where('emp_id','=',$txtSearch)->where('bk_status','=',$bk_status)->where('bk_start_start','like',$date3.'%');
    }
  }//$typeSearch == 2

}
// รายปี
elseif ($type == 3) {
  if ($typeSearch == 0) {
    // ทั้งหมด
    $sql =DB::table('tb_booking')->where('com_id','=',$com_id)->where('bk_start_start','like',''.$date4.'%');
    // $sql = DB::table('tb_booking')->where('com_id','=',$com_id)->where('bk_start_start','like','2017-12-07%');
  }//$typeSearch == 0
  elseif ($typeSearch == 1) {
    // สถานะการจอง
    if ($bk_status == 'All') {
      // ทั้งหมด
      $sql =  DB::table('tb_booking')->where('com_id','=',$com_id)->where('bk_start_start','like',$date4.'%');
    }
    else {
      $sql =  DB::table('tb_booking')->where('com_id','=',$com_id)->where('bk_status','=',$bk_status)->where('bk_start_start','like',$date4.'%');
    }
  }//$typeSearch == 1
  elseif ($typeSearch == 2) {
    // ชื่อคนจอง
    if ($txtSearch == 'All' && $bk_status == 'All') {
        // ชื่อคนจองว่าง สถานะทั้งหมด
        $sql =  DB::table('tb_booking')->where('com_id','=',$com_id)->where('bk_start_start','like',$date4.'%');
    }
    elseif ($txtSearch == 'All' && $bk_status <> 'All') {
      // ชื่อคนจองว่าง มีสถานะ
      $sql =  DB::table('tb_booking')->where('com_id','=',$com_id)->where('bk_status','=',$bk_status)->where('bk_start_start','like',$date4.'%');
    }
    elseif ($txtSearch <> 'All' && $bk_status == 'All') {
      // ชื่อคนจองไม่ว่าง สถานะทั้งหมด
      $sql =  DB::table('tb_booking')->where('com_id','=',$com_id)->where('emp_id','=',$txtSearch)->where('bk_start_start','like',$date4.'%');
    }
    else {
        // ชื่อคนจองไม่ว่าง มีสถานะ
      $sql =  DB::table('tb_booking')->where('com_id','=',$com_id)->where('emp_id','=',$txtSearch)->where('bk_status','=',$bk_status)->where('bk_start_start','like',$date4.'%');
    }
  }//$typeSearch == 2
}
//ช่วงเวลา
else {
  if ($typeSearch == 0) {
    // ทั้งหมด
    $sql =DB::table('tb_booking')->where('com_id','=',$com_id)->whereBetween('bk_start_start', [$date1, $date2]);
    // $sql = DB::table('tb_booking')->where('com_id','=',$com_id)->where('bk_start_start','like','2017-12-07%');
  }//$typeSearch == 0
  elseif ($typeSearch == 1) {
    // สถานะการจอง
    if ($bk_status == 'All') {
      // ทั้งหมด
      $sql =  DB::table('tb_booking')->where('com_id','=',$com_id)->whereBetween('bk_start_start', [$date1, $date2]);
    }
    else {
      $sql =  DB::table('tb_booking')->where('com_id','=',$com_id)->where('bk_status','=',$bk_status)->whereBetween('bk_start_start', [$date1, $date2]);
    }
  }//$typeSearch == 1
  elseif ($typeSearch == 2) {
    // ชื่อคนจอง
    if ($txtSearch == 'All' && $bk_status == 'All') {
        // ชื่อคนจองว่าง สถานะทั้งหมด
        $sql =  DB::table('tb_booking')->where('com_id','=',$com_id)->whereBetween('bk_start_start', [$date1, $date2]);
    }
    elseif ($txtSearch == 'All' && $bk_status <> 'All') {
      // ชื่อคนจองว่าง มีสถานะ
      $sql =  DB::table('tb_booking')->where('com_id','=',$com_id)->where('bk_status','=',$bk_status)->whereBetween('bk_start_start', [$date1, $date2]);
    }
    elseif ($txtSearch <> 'All' && $bk_status == 'All') {
      // ชื่อคนจองไม่ว่าง สถานะทั้งหมด
      $sql =  DB::table('tb_booking')->where('com_id','=',$com_id)->where('emp_id','=',$txtSearch)->whereBetween('bk_start_start', [$date1, $date2]);
    }
    else {
        // ชื่อคนจองไม่ว่าง มีสถานะ
      $sql =  DB::table('tb_booking')->where('com_id','=',$com_id)->where('emp_id','=',$txtSearch)->where('bk_status','=',$bk_status)->whereBetween('bk_start_start', [$date1, $date2]);
    }
  }//$typeSearch == 2

}

if ($dep_id == 'All') {
  $qrybk = $sql->get();
  $numbk = $sql->count();
}
else {
  $qrybk = $sql->where('job_id','=',$dep_id)->get();
  $numbk = $sql->where('job_id','=',$dep_id)->count();
}
?><table class="table table-bordered">
<thead>
  <th colspan="3" style='text-align:right;'> มีรายการทั้งหมด {{$numbk}} รายการ</th>
</thead>
  <tbody><?php
if($numbk > 0){
    foreach ($qrybk as $bk){
      ?>
      <?php

                if($numbk > 0){

                  if($bk->bk_status == "approve"){
                        $bg = "bg-info";
                  }elseif($bk->bk_status == "wait"){
                        $bg = "bg-warning";
                  }elseif($bk->bk_status == "success"||$bk->bk_status == "merge"){
                        $bg = "bg-success";
                  }else{
                        $bg = "bg-danger";
                  }
            ?>

                  <tr class="detailBk" data-id="{{$bk->bk_id}}">
                    <td style="border-right:solid 1px #ccc;" width="10%;">
                      <div class="tdhead {{$bg}}">
                        <?php
                        $status = "";
                        if($bk->bk_status == "wait"){
                                $status = "รอการอนุมัติ";
                        }elseif ($bk->bk_status == "approve"){
                                $status = "รอการจัดรถ";
                        }elseif ($bk->bk_status =="success"){
                                $status = "สำเร็จ";
                        }elseif ($bk->bk_status =="merge"){
                                $status = "สำเร็จ(ร่วมเดินทาง)";
                        }elseif ($bk->bk_status =="eject"){
                                $status = "ยกเลิกการจอง";
                        }elseif($bk->bk_status == "ejectcar"){
                                $status = "ยกเลิกการเดินทาง";
                        }elseif ($bk->bk_status =="nonecar"){
                                $status = "ไม่มีรถ";
                        }
                         ?>
                         <small style="color:#fff;">{{$status}}</small>
                      </div>
                    </td>
                    <td>
                      <h5 >{{ $bk->bk_id}}</h5>
                      <small>
                        <?php
                            $y = substr($bk->bk_date,0,4);
                            $m = substr($bk->bk_date,5,2);
                            $d = substr($bk->bk_date,8,2);
                            $h = substr($bk->bk_date,11,2);
                            $i = substr($bk->bk_date,14,2);
                         ?>
                          {{ "วันที่จอง ".$d."/".$m."/".$y." เวลา ".$h.":".$i." น." }}
                          <br>
                          <?php
                            echo "ออกเดินทางเมื่อ ";datetime($bk->bk_start_start);echo "ถึง ";datetime($bk->bk_end_start);
                          ?>

                      </small>
                    </td>

                     <td width="20%">
                       <?php
                          $sql_emp = DB::table('tb_employee')->where('emp_id','=',$bk->emp_id)->get();
                          foreach ($sql_emp as $emp) {
                            echo $emp->emp_fname." ".$emp->emp_lname;
                          }
                       ?>
                     </td>
                    {{-- <td width="15%">

                    </td> --}}
                  </tr>


    <?php }else{ ?>
            <tr>
              <td colspan="3" align="center" class="text-black"><h5>ไม่พบกิจกรรม</h5></td>
            </tr>
    <?php }
      }}
      else{ ?>
          <tr>
              {{-- {{$text}} --}}
              <td colspan="3" align="center" class="text-black"><h5>ไม่พบกิจกรรม</h5></td>
          </tr>
  <?php } ?>

  <!-- <tr> <td> <?php //echo $qrycar->links(); ?> </td></tr> -->
</tbody>
</table>

<?php
function datetime($datetime)
{
  $y = substr($datetime,0,4);
  $m = substr($datetime,5,2);
  $d = substr($datetime,8,2);
  $h = substr($datetime,11,2);
  $i = substr($datetime,14,2);
  echo $d."/".$m."/".$y." เวลา ".$h.":".$i." น." ;
}
 ?>
 <script type="text/javascript">
 // $(".detailBk").click(function(){
 //  var id = $(this).data("id");
 // $.ajax({
 //     url:"/detail",
 //     data:"bk="+id,
 //     type:"GET",
 //     success:function(data){
 //         $(".modal-area").html(data);
 //         $("#modalBk").modal("show");
 //     }
 //   })
 // });
 </script>
