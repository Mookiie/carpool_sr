<div class="modal fade" id="modalBk" >
  <div class="modal-dialog modal-lg" role="document" id="dialogBk" >
    <div class="modal-content" >

            <div class="modal-header">
                <h5 class="modal-title fa fa-plus text-black"> บันทึกบริษัท</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
              <form name="createform" id="create-form">
                 <div class="form-group row">
                    <!-- รหัสพนักงาน -->
                     <input type="hidden" name="emp_id" id="emp_id" value="<?php echo $emp_id ?>">
                   <!-- รหัสพนักงาน -->
                    <?php
                    // dep_id
                    $sqlcom = DB::table('tb_company')->orderBy('com_id','decs')->limit(1)->get();
                    foreach ($sqlcom as $com):
                     $id = $com->com_id;
                    endforeach;
                    $com_id = substr($id, 0, 1);//c
                    $id1= substr($id, 2, 3)+1;//001
                     ?>
                    <!-- รหัสบริษัท -->
                    <input type="hidden"  id="com_id" name="com_id" value="<?php echo $com_id.sprintf("%04d",$id1) ?>">
                  </div><!-- รหัสบริษัท -->

                  <div class="form-group row"><!-- ชื่อบริษัท -->
                    <label for="com_name" class ="col-md-4 col-form-label text-black">ชื่อบริษัท</label>
                    <input type="text" class="form-control col-md-6" id="com_name" name="com_name" placeholder="ชื่อบริษัท">
                  </div><!-- ชื่อบริษัท -->

                  <div class="form-group row"><!-- Code Conpany -->
                    <label for="com_code" class ="col-md-4 col-form-label text-black">Code Conpany</label>
                    <input type="text" class="form-control col-md-6" id="com_code" name="com_code" placeholder="Code Conpany">
                  </div><!-- Code Conpany -->

             </form>
            </div>

            <div class="modal-footer">
              <button type="button" class="btn btn-primary btn-create">บันทึกข้อมูล</button>
              <button type="button" class="btn btn-secondary" data-dismiss="modal">ยกเลิก</button>
            </div>
    </div>
  </div>
</div>
<script>

$(document).ready(function(){
$("#com_name").focus();
});

$("#create-form").keypress(function(event){
 var kc = event.keyCode;
 if(kc==13){
    createcom();
 }
});

$(".btn-create").click(function(){
  createcom();
});

function createcom(){
var form_create= $("#create-form").serialize();
 $.ajax({
  url:"/createcom",
  data:form_create,
  type:"POST",
  success:function(data){
      var obj =JSON.parse(data);
    if(obj['success']==true)
      {
        swal({
                  title: "บันทึกข้อมูลสำเร็จ",
                  text: "บันทึกข้อมูลสำเร็จแล้ว",
                  type: "success",
                  showCancelButton: false,
                  confirmButtonColor: "#2ECC71",
                  confirmButtonText: "ตกลง",
                  closeOnConfirm: false,
                },
                  function(isConfirm){
                    if (isConfirm) {
                      window.location = "/otheradd";
                }
            });
      }
    else
      {
        addErr(obj['type'],obj['msg']);

      }
  }
 });
};


function rmErr(input){
 $("#group"+input.id).removeClass("has-danger");
 $("#group"+input.id+" input").removeClass("form-control-danger");
 $("#fb"+input.id).attr("hidden","hidden");
}

function addErr(type,msg){
   $("#group"+type).addClass("has-danger");
   $("#group"+type+" input").addClass("form-control-danger");
   $("#fb"+type).html(msg);
   $("#fb"+type).removeAttr("hidden");
}

</script>
