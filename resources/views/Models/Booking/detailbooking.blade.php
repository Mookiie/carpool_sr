<?php
date_default_timezone_set("Asia/Bangkok");
$sqlbooking = DB::table('tb_booking')
               ->join("tb_employee",'tb_booking.emp_id','=','tb_employee.emp_id')
               ->join("tb_car_type",function($join){
                     $join->on('tb_booking.ctype_id', '=' , 'tb_car_type.ctype_id');
                 })
               ->join("tb_department",function($join){
                      $join->on('tb_booking.dep_id', '=' , 'tb_department.dep_id')
                           ->on("tb_booking.com_id","=","tb_department.com_id");
                 })
               ->where('bk_id', '=', $bk_id)->get();
                foreach ($sqlbooking as $bk):
                 $empid = $bk->emp_id;
                 $fullname = $bk->emp_fname." ".$bk->emp_lname;
                 $status = $bk->bk_status;
                 $date = $bk->bk_date;
                 $dStart_s = $bk->bk_start_start;
                 $dEnd_s = $bk->bk_end_start;
                 $ctype= $bk->ctype_name;
                 $dep_id = $bk->dep_id;
                 $bk_sitecode = $bk->bk_sitecode;
                 $dep_name = $bk->dep_name;
                 $dep_car = $bk->dep_car;
                 $tel = $bk->dep_tel;
                 $mtel = $bk->bk_mtel;
                 $ttel = $bk->bk_ttel;
                 $percon = $bk->bk_percon;
                 $obj = $bk->bk_obj;
                 $note = $bk->bk_note;
                 $doc = $bk->bk_doc;
                 $car = $bk->car_id;
                 $drive = $bk->drive_id;
                 $reasons = $bk->bk_reasons;
                 $approve_by = $bk->approve_by;
                 $approve_date = $bk->approve_date;
                 $setcar_by = $bk->setcar_by;
                 $setcar_date = $bk->setcar_date;
                 $com_id = $bk->com_id;
                 $bkuse = $bk->bk_use;
                 $edit_by=$bk->edit_by;
                 $edit_date=$bk->edit_date;
                 $edit_reasons = $bk->edit_reasons;
                 $bk_reasons=$bk->bk_reasons;
                 $success_by=$bk->success_by;
                 $success_date=$bk->success_date;
                 $success_reasons=$bk->success_reasons;
                 $change_detail = $bk->change_detail;
                 $change_by= $bk->change_by;
                 $bk_merge = $bk->bk_merge;
                 $bk_ot = $bk->bk_ot;
                endforeach;

$username = session()->get('user');
$users = DB::table('tb_employee')
        ->join("tb_department",function($join){
               $join->on('tb_employee.dep_id', '=' , 'tb_department.dep_id')
                    ->on("tb_employee.com_id","=","tb_department.com_id");
          })
       ->join('tb_employee_login', 'tb_employee.emp_id', '=', 'tb_employee_login.emp_id')
       ->where('tb_employee.emp_email','=',$username)->get();
         foreach ($users as $u):
          $emp_lv = $u->emp_level;
          $emp_id = $u->emp_id;
          $dep_emp= $u->dep_id;
          $job_id = $u->job_id;
         endforeach;

               $arr = array();

                     if($status == "wait"){
                         array_push($arr,"รอการอนุมัติ","warning","#F39C12");
                       }else if($status== "approve"){
                         array_push($arr,"รอการจัดรถ","info","#3498DB");
                       }else if($status == "success"){
                         array_push($arr,"สำเร็จ","success","#2ECC71");
                       }else if($status == "merge"){
                         array_push($arr,"สำเร็จ(ร่วมเดินทาง)","success","#2ECC71");
                       }else if($status == "eject"){
                         array_push($arr,"ยกเลิกการจอง","danger","#E74C3C");
                       }else if($status == "ejectcar"){
                         array_push($arr,"ยกเลิกการเดินทาง","danger","#E74C3C");
                       }else if($status == "nonecar"){
                         array_push($arr,"ไม่มีรถ","danger","#E74C3C");
                       }
               function datetime($datetime)
               {
                 $y = substr($datetime,0,4);
                 $m = substr($datetime,5,2);
                 $d = substr($datetime,8,2);
                 $h = substr($datetime,11,2);
                 $i = substr($datetime,14,2);
                 echo $d."/".$m."/".$y." เวลา ".$h.":".$i." น." ;
               }
               function datetimesome($datetime1,$datetime2)
               {
                 $y1 = substr($datetime1,0,4);
                 $m1 = substr($datetime1,5,2);
                 $d1 = substr($datetime1,8,2);
                 $h1 = substr($datetime1,11,2);
                 $i1 = substr($datetime1,14,2);
                 $y2 = substr($datetime2,0,4);
                 $m2 = substr($datetime2,5,2);
                 $d2 = substr($datetime2,8,2);
                 $h2 = substr($datetime2,11,2);
                 $i2 = substr($datetime2,14,2);
                 return $d1."/".$m1."/".$y1." ถึง ".$d2."/".$m2."/".$y2." ช่วงเวลา ".$h1.":".$i1."น. ถึง ".$h2.":".$i2."น." ;
               }

              // $bkid = base64_encode($bk_id);

?>
<style>
  img.bkImg{
    box-shadow: 1px 1px 3px 0px #333;
  }
    #tabPrint li{
      float: right;
    }
  #tabPrint li a{
    color: #fff;
  }
  #tabPrint li a:hover{
    background: #ccc;
    color: #333;
    border-radius: 0px;
    cursor:pointer;
  }
</style>

</style>
 <div class="modal fade" id="modalBk" >
   <div class="modal-dialog modal-lg" role="document" id="dialogBk" >
     <div class="modal-content" >

       <div class="modal-header">
           <h6 class="modal-title"><span class="fa fa-edit text-black">  รายการจอง :</span>
             <span class="badge badge-<?php echo $arr[1]; ?>"><?php echo $arr[0];?></span></h6>

             <button type="button" class="close" data-dismiss="modal" aria-label="Close">
               <span aria-hidden="true">&times;</span>
             </button>
       </div>

       <div class="modal-body">

         <input type="hidden" id="bk_id" name="car_id" value="{{$bk_id}}">
         <input type="hidden" id="com_id" name="car_id" value="{{$com_id}}">
         <input type="hidden" id="dep_id" name="car_id" value="{{$dep_id}}">

         <input type="hidden" id="car_id" name="car_id" value="{{$car}}">
         <input type="hidden" id="drive_id" name="drive_id" value="{{$drive}}">

           <?php if(($emp_lv == 2 && $bk->bk_status == "success" && $job_id == $dep_car)
           ||($emp_lv == 2 && $bk->bk_status == "merge" && $job_id == $dep_car)
           ||($emp_lv == '999' && $bk->bk_status == "success" )
           ||($emp_lv == '999' && $bk->bk_status == "merge" )){
             ?>
             <div class="row bg-faded" id="approve_panel" data-id="emp=<?php echo $emp_id;?>">
               <div class="col-4 " align="left">
                 <b>คุณต้องการการใช้งานรถ ?</b>
               </div>
                <div class="col-8" align="right">
               @if ($bk->bk_start_start < date("Y-m-d H:i:s"))
                 <button class="btn btn-sm btn-danger" id="btnChange">
                   <span class="fa fa-refresh"></span> สลับการใช้งาน
                 </button>
               @endif
               @if ($bk->bk_end_start> date("Y-m-d H:i:s"))
                 <button class="btn btn-sm btn-success" id="btnUse">
                   <span class="fa fa-check"></span> ใช้งานเรียบร้อย
                 </button>
               @endif
               @if ($bk->bk_start_start > date("Y-m-d H:i:s"))
                 <button class="btn btn-sm btn-warning" id="btnReApprove">
                   <span class="fa fa-car"></span> จัดรถใหม่
                 </button>
               @endif
               <button class="btn btn-sm btn-danger" id="btnEjectBooking">
                 <span class="fa fa-remove"></span> ยกเลิกการใช้งาน
               </button>
               </div>
             </div>
           <?php
            }
             if(($emp_lv == 2 && $bk->bk_status == "nonecar" && $job_id == $dep_car)||
            ($emp_lv == '999' && $bk->bk_status == "nonecar" )){
              ?>
              <div class="row bg-faded" id="approve_panel" data-id="emp=<?php echo $emp_id;?>">
                <div class="col-4 " align="left">
                  <b>คุณต้องการการใช้งานรถ ?</b>
                </div>
                 <div class="col-8" align="right">
                @if ($bk->bk_start_start > date("Y-m-d H:i:s"))
                  <button class="btn btn-sm btn-warning" id="btnReApprove">
                    <span class="fa fa-car"></span> จัดรถใหม่
                  </button>
                @endif
                <button class="btn btn-sm btn-danger" id="btnEjectBooking">
                  <span class="fa fa-remove"></span> ยกเลิกการใช้งาน
                </button>
                </div>
              </div>
            <?php
             }

            if(($emp_lv == 1 && $bk->bk_status == "wait")||($emp_lv == 11 && $bk->bk_status == "wait")||($emp_lv == '999' && $bk->bk_status == "wait")){
              $sql_lv = DB::table('tb_employee_login')->where('emp_id','=',$empid)->get();
              foreach ($sql_lv as $lv) {
                $lvemp = $lv->emp_level;
              }
              if ($emp_lv > $lvemp || $lvemp >= 99 || $lvemp == 2) {
               ?>
               <div class="row bg-faded" id="approve_panel" data-id="emp=<?php echo $emp_id; ?>">
                 <div class="col-6 " align="left">
                   <b>คุณต้องการอนุมัติการจอง ? </b>
                 </div>
                 <div class="col-6" align="right">
                 <button class="btn btn-sm btn-success" id="btnApprove">
                   <span class="fa fa-check"></span> อนุมัติการจอง
                 </button>
                 <button class="btn btn-sm btn-danger" id="btnEject">
                   <span class="fa fa-remove"></span> ยกเลิกการจอง
                 </button>
                 </div>
               </div>
             <?php
                 }
                 else {
                   ?>
                  <div class="row bg-faded" id="approve_panel" style="background:#ccc; text-align: center;">
                   {{-- <div class="row bg-faded"> --}}
                     {{-- <div class="" align="center" style="background:#ccc;"> --}}
                       คุณไม่สามารถอนุมัติคำขอนี้ได้
                     {{-- </div> --}}
                   {{-- </div> --}}
                 </div>
                  <?php
                 }
               }
             if ($emp_id == $empid||$emp_lv > '98'||$emp_lv =='2') {
               if ($status == 'eject'||$status == 'ejectcar'||$status == 'merge'||$status == 'success'||$status == 'nonecar') {
                if ($emp_lv > '98'||$emp_lv =='2') {
                 ?>
                     <div class="row bg-faded" id="approve_panel" data-id="emp=<?php echo $emp_id; ?>">
                     <div class="col-6 " align="left">
                       <b> กดปุ่มเพื่อแก้ไขการจอง </b>
                     </div>
                     <div class="col-6" align="right">
                       <button class=" btn btn-sm btn-warning" id="btnEdit">
                         <span class="fa fa-pencil"></span> แก้ไขการจอง
                       </button>
                     </div>
                   </div>
                 <?php
                }
               }
                 else{
              ?>
               <div class="row bg-faded" id="approve_panel" data-id="emp=<?php echo $emp_id; ?>">
                 <div class="col-6 " align="left">
                   <b> กดปุ่มเพื่อยกเลิกการจองหรือแก้ไขด้วยตนเอง </b>
                 </div>
                  <div class="col-6" align="right">
                 <button class=" btn btn-sm btn-warning" id="btnEdit">
                   <span class="fa fa-pencil"></span> แก้ไขการจอง
                 </button>
                 <button class=" btn btn-sm btn-danger" id="btnEjectMe">
                   <span class="fa fa-remove"></span> ยกเลิกการจอง
                 </button>
                 </div>
               </div>
             <?php } }?>


             <div class="row">
               <div class="col-md-12">
                 <ul class="nav nav-tabs">
                     <li class="nav-item">
                       <a class="nav-link active" id="tap1" data-id="data=<?php echo $bk_id; ?>"
                          data-bkstart="<?php echo substr($dStart_s,0,10); ?>"
                          onclick="tapChange(1,2,3,4,5);">รายละเอียดการจอง</a>
                     </li>
                     <?php
                       if($status == "merge" || $bk_merge != ""){
                     ?>
                     <li class="nav-item">
                        <a class="nav-link" id="tap2" data-id="data=<?php echo $bk_id; ?>"
                           data-bkstart="<?php echo substr($dStart_s,0,10); ?>"
                           onclick="tapChange(2,1,3,4,5);">รายละเอียดการจอง(ร่วมเดินทาง)</a>
                      </li>
                      <?php }?>
                     <!-- <li class="nav-item">
                       <a class="nav-link" id="tap2" onclick="tapChange(2,1,3,4,5);">ปฏิทินการจอง</a>
                     </li> -->
                     <!-- <li class="nav-item">
                       <a class="nav-link" id="tap3" onclick="tapChange(3,1,2,4,5);">สถานที่</a>
                     </li> -->
                     <li class="nav-item">
                       <a class="nav-link" id="tap5" onclick="tapChange(5,1,2,3,4);">เอกสารที่แนบมา</a>
                     </li>
                     <?php
                       if($status == "success" || $status == "merge"){
                     ?>
                     <li class="nav-item">
                       <a class="nav-link" id="tap4"onclick="tapChange(4,2,3,1,5);">จัดรถ</a>
                     </li>
                    {{-- <li class="nav-item">
                       <a class="nav-link" id="tap3" onclick="tapChange(3,1,2,4,5);">หมายเหตุ</a>
                     </li> --}}
                     <?php }?>
                   </ul>
                 </div>
               </div>

   <!--detail -->
     <div class="row" id="c1">
         <div class="col-md-12">
           <div class="card card_bk">

             <div class="card-header">
               <div class="row">
                 <span class="col-4 fa fa-edit" style="color:#000;">  รายละเอียดการจอง</span>

                   <div class="col-8" align="right">
                    <button class="btn btn-sm btn-success pdf" id="pdf" >
                      <span class="fa fa-download"></span> บันทึกใบจองรถ
                    </button>
                  </div>

               </div>
             </div>

             <div class="card-block text-black">
               <div class="row">

                 <label class="col-md-3"><b>เลขที่อ้างอิง</b></label>
                   <div class="col-md-9 text-black">
                     <?php echo $bk_id; ?>
                     @if ($bk_ot=='on')
                       (OT)
                     @endif
                   </div>

                   <label class="col-md-3"><b>ชื่อผู้จอง</b></label>
                     <div class="col-md-9 text-black">
                       <?php echo $fullname; ?>
                     </div>

                 <label class="col-md-3"><b>วันที่จอง</b></label>
                   <div class="col-md-9 text-black">
                     <?php datetime($date) ?>
                   </div>

                <label class="col-md-3"><b>วันที่ขอใช้</b></label>
                   <div class="col-md-9 text-black">
                      <?php  if (!$bkuse) {
                          echo   datetime($dStart_s);echo " ถึง ";datetime($dEnd_s);
                      } else {
                        echo datetimesome($dStart_s,$dEnd_s);
                      }
                     ?>
                   </div>

                 <label class="col-md-3"><b>ประเภทรถ</b></label>
                   <div class="col-md-9 text-black">
                     <?php
                        $sqldep_car = DB::table('tb_job')->where('job_id','=',$dep_car)->select('job_name')->where('com_id','=',$com_id)->groupBy('job_name')->get();
                        foreach ($sqldep_car as $dc) {
                        echo $ctype."(".$dc->job_name.")";
                        }
                     ?>
                   </div>

                 <label class="col-md-3"><b>แผนก</b></label>
                   <div class="col-md-9 text-black">
                     <?php echo $dep_name; ?>
                   </div>

                   <label class="col-md-3"><b>รหัสไซต์งาน</b></label>
                     <div class="col-md-9 text-black">
                       <?php echo $bk_sitecode; ?>
                     </div>

                 <label class="col-md-3"><b>โทรศัพท์</b></label>
                   <div class="col-md-9 text-black">
                     <?php echo $ttel; ?>
                   </div>

                 <label class="col-md-3"><b>มือถือ</b></label>
                   <div class="col-md-9 text-black">
                     <?php echo $mtel; ?>
                   </div>

                 <label class="col-md-3"><b>ผู้ร่วมเดินทาง</b></label>
                   <div class="col-md-9 text-black">
                     <?php echo $percon; ?> คน
                   </div>

                 <label class="col-md-3"><b>วัตถุประสงค์</b></label>
                   <div class="col-md-9 text-black">
                     <?php echo $obj; ?>
                   </div>

                 <label class="col-md-3"><b>หมายเหตุ</b></label>
                   <div class="col-md-9 text-black">
                     <?php echo $note; ?>
                   </div>

                 <label class="col-md-3"><b>สถานที่</b></label>
                 <div class="col-md-9 text-black">
                 <?php  $locate = DB::table('tb_booking_location')->where('bk_id','=',$bk_id)->get();
                 foreach ($locate as $lo) {
                   $lo_id =$lo->location_id;
                     if ($lo->location_id == "1") {
                       echo "สถานที่เริ่มต้น : ".$lo->location_name."<br>";
                     }
                     else {
                       echo "สถานที่ ".($lo->location_id-1)." : ".$lo->location_name."<br>";
                     }
                   }
                  ?>
                  <button type="button" class="btn btn-primary btn-sm" id="locate">แผนที่</button>
                  </div>
                  @if ($approve_by != '')
                    <label class="col-md-3"><b>อนุมัติโดย</b></label>
                      <div class="col-md-9 text-black">
                          <?php
                          $sql_approve = DB::table('tb_employee')->where('emp_id','=',$approve_by)->get();
                          // echo count($sql_approve);
                          foreach ($sql_approve as $emp_approve) {
                            $fullapp = $emp_approve->emp_fname." ".$emp_approve->emp_lname;
                          }
                          echo $fullapp."  อนุมัติเมื่อ ";datetime($approve_date)?>
                      </div>
                      @if ($status=='eject')
                        <label class="col-md-3"><b>เหตุผล :</b></label>
                          <div class="col-md-9 text-black">
                            <?php echo $bk_reasons;  ?>
                          </div>
                      @endif
                  @endif
                  @if ($setcar_by != '')
                    <label class="col-md-3"><b>จัดรถโดย</b></label>
                      <div class="col-md-9 text-black">
                        <?php
                        $sql_setcar = DB::table('tb_employee')->where('emp_id','=',$setcar_by)->get();
                        foreach ($sql_setcar as $emp_setcar) {
                          $fullset = $emp_setcar->emp_fname." ".$emp_setcar->emp_lname;
                        }
                         echo $fullset."  จัดรถเมื่อ ";datetime($setcar_date) ?>
                      </div>
                  @endif
                  @if ($edit_by !="")
                    <label class="col-md-3"><b>แก้ไขโดย</b></label>
                      <div class="col-md-9 text-black">
                        <?php
                        $sql_setcar = DB::table('tb_employee')->where('emp_id','=',$edit_by)->get();
                        foreach ($sql_setcar as $emp_setcar) {
                          $fullset = $emp_setcar->emp_fname." ".$emp_setcar->emp_lname;
                        }
                         echo $fullset."  แก้ไขเมื่อ ";datetime($edit_date) ?>
                      </div>
                    {{-- <label class="col-md-3"><b>เหตุผลที่แก้ไข :</b></label>
                      <div class="col-md-9 text-black"> --}}
                        <?php // echo $edit_reasons;  ?>
                      {{-- </div> --}}
                    @endif

                    @if ($success_by !="")
                      <label class="col-md-3"><b>แก้ไขวันใช้งานโดย</b></label>
                        <div class="col-md-9 text-black">
                          <?php
                          $sql_setcar = DB::table('tb_employee')->where('emp_id','=',$success_by)->get();
                          foreach ($sql_setcar as $emp_setcar) {
                            $fullset = $emp_setcar->emp_fname." ".$emp_setcar->emp_lname;
                          }
                           echo $fullset."  แก้ไขเมื่อ ";datetime($success_date) ?>
                        </div>
                      <label class="col-md-3"><b>เหตุผลที่แก้ไข :</b></label>
                        <div class="col-md-9 text-black">
                          <?php echo $success_reasons;  ?>
                        </div>
                    @endif
               </div>
             </div>

           </div>
         </div>
       </div>
   <!-- end detail -->
   <!--Detail Merge-->
     <div class="row" id="c2" hidden="hidden">
       <div class="col-md-12">
         <div class="card card_bk">
           <?php
             if ($bk_merge !="") {
                    $pos = strpos($bk_merge,';');
                    if($pos!==FALSE){
                      // echo "พบ ; ที่ตำแหน่ง<b> $pos </b>$bk_merge";
                       $str = explode(";",$bk_merge);
                      }else{
                        $str = array($bk_merge);
                      }
                      // print_r($str);
                for ($i=0; $i < count($str); $i++) {
                $sql = DB::table("tb_booking")
                            ->join("tb_employee",'tb_booking.emp_id','=','tb_employee.emp_id')
                            ->join("tb_car_type",function($join){
                                  $join->on('tb_booking.ctype_id', '=' , 'tb_car_type.ctype_id');
                              })
                            ->join("tb_department",function($join){
                                   $join->on('tb_booking.dep_id', '=' , 'tb_department.dep_id')
                                        ->on("tb_booking.com_id","=","tb_department.com_id");
                              })
                            ->where("bk_id",'=',$str[$i])->get();
                ?>
                @foreach ($sql as $mbk)
                   {{-- start loop --}}
                   <div class="card-header">
                     <span class="fa fa-edit text-black">  รายละเอียดการจอง(ร่วมเดินทาง)</span>
                   </div>
                      <div class="card-block text-black">
                        <div class="row">

                       <label class="col-md-3"><b>เลขที่อ้างอิง</b></label>
                         <div class="col-md-9 text-black">
                           <?php echo $mbk->bk_id; ?>
                         </div>

                         <label class="col-md-3"><b>ชื่อผู้จอง</b></label>
                           <div class="col-md-9 text-black">
                             <?php echo $mbk->emp_fname." ".$mbk->emp_lname; ?>
                           </div>

                       <label class="col-md-3"><b>วันที่จอง</b></label>
                         <div class="col-md-9 text-black">
                           <?php datetime($mbk->bk_date) ?>
                         </div>

                       <label class="col-md-3"><b>วันที่ขอใช้</b></label>
                         <div class="col-md-9 text-black">
                            <?php  if (!$mbk->bk_use) {
                              echo datetime($mbk->bk_start_start);echo " ถึง ";datetime($mbk->bk_end_start);
                            } else {
                              echo datetimesome($mbk->bk_start_start,$mbk->bk_end_start);
                            }
                           ?>
                         </div>

                       <label class="col-md-3"><b>ประเภทรถ</b></label>
                         <div class="col-md-9 text-black">
                           <?php
                              $sqldep_car = DB::table('tb_job')->where('job_id','=',$mbk->dep_car)->select('job_name')->where('com_id','=',$com_id)->groupBy('job_name')->get();
                              foreach ($sqldep_car as $dc) {
                              echo $ctype."(".$dc->job_name.")";
                              }
                           ?>
                         </div>

                       <label class="col-md-3"><b>แผนก</b></label>
                         <div class="col-md-9 text-black">
                           <?php echo $mbk->dep_name; ?>
                         </div>

                         <label class="col-md-3"><b>รหัสไซต์งาน</b></label>
                           <div class="col-md-9 text-black">
                             <?php echo $mbk->bk_sitecode; ?>
                           </div>

                       <label class="col-md-3"><b>โทรศัพท์</b></label>
                         <div class="col-md-9 text-black">
                           <?php echo $mbk->bk_ttel; ?>
                         </div>

                       <label class="col-md-3"><b>มือถือ</b></label>
                         <div class="col-md-9 text-black">
                           <?php echo $mbk->bk_mtel; ?>
                         </div>

                       <label class="col-md-3"><b>ผู้ร่วมเดินทาง</b></label>
                         <div class="col-md-9 text-black">
                           <?php echo $mbk->bk_percon; ?> คน
                         </div>

                       <label class="col-md-3"><b>วัตถุประสงค์</b></label>
                         <div class="col-md-9 text-black">
                           <?php echo $mbk->bk_obj; ?>
                         </div>

                       <label class="col-md-3"><b>หมายเหตุ</b></label>
                         <div class="col-md-9 text-black">
                           <?php echo $mbk->bk_note; ?>
                         </div>

                       <label class="col-md-3"><b>สถานที่</b></label>
                       <div class="col-md-9 text-black">
                       <?php  $locate = DB::table('tb_booking_location')->where('bk_id','=',$mbk->bk_id)->get();
                       foreach ($locate as $lo) {
                         $lo_id =$lo->location_id;
                           if ($lo->location_id == "1") {
                             echo "สถานที่เริ่มต้น : ".$lo->location_name."<br>";
                           }
                           else {
                             echo "สถานที่ ".($lo->location_id-1)." : ".$lo->location_name."<br>";
                           }
                         }
                        ?>
                        </div>
                        @if ($mbk->approve_by != '')
                          <label class="col-md-3"><b>อนุมัติโดย</b></label>
                            <div class="col-md-9 text-black">
                                <?php
                                $sql_approve = DB::table('tb_employee')->where('emp_id','=',$mbk->approve_by)->get();
                                // echo count($sql_approve);
                                foreach ($sql_approve as $emp_approve) {
                                  $fullapp = $emp_approve->emp_fname." ".$emp_approve->emp_lname;
                                }
                                echo $fullapp."  อนุมัติเมื่อ ";datetime($mbk->approve_date)?>
                            </div>
                            @if ($status=='eject')
                              <label class="col-md-3"><b>เหตุผล :</b></label>
                                <div class="col-md-9 text-black">
                                  <?php echo $mbk->bk_reasons;  ?>
                                </div>
                            @endif
                        @endif
                        @if ($mbk->setcar_by != '')
                          <label class="col-md-3"><b>จัดรถโดย</b></label>
                            <div class="col-md-9 text-black">
                              <?php
                              $sql_setcar = DB::table('tb_employee')->where('emp_id','=',$mbk->setcar_by)->get();
                              foreach ($sql_setcar as $emp_setcar) {
                                $fullset = $emp_setcar->emp_fname." ".$emp_setcar->emp_lname;
                              }
                               echo $fullset."  จัดรถเมื่อ ";datetime($mbk->setcar_date) ?>
                            </div>
                            @if ($status=='nonecar')
                              <label class="col-md-3"><b>เหตุผล :</b></label>
                                <div class="col-md-9 text-black">
                                  <?php echo $mbk->bk_reasons;  ?>
                                </div>
                            @endif
                        @endif

                        @if ($mbk->edit_by !="")
                          <label class="col-md-3"><b>แก้ไขการจัดรถโดย</b></label>
                            <div class="col-md-9 text-black">
                              <?php
                              $sql_setcar = DB::table('tb_employee')->where('emp_id','=',$mbk->edit_by)->get();
                              foreach ($sql_setcar as $emp_setcar) {
                                $fullset = $emp_setcar->emp_fname." ".$emp_setcar->emp_lname;
                              }
                               echo $fullset."  แก้ไขเมื่อ ";datetime($edit_date) ?>
                            </div>
                          <label class="col-md-3"><b>เหตุผลที่แก้ไข :</b></label>
                            <div class="col-md-9 text-black">
                              <?php echo $mbk->edit_reasons;  ?>
                            </div>
                          @endif

                          @if ($mbk->success_by !="")
                            <label class="col-md-3"><b>แก้ไขวันใช้งานโดย</b></label>
                              <div class="col-md-9 text-black">
                                <?php
                                $sql_setcar = DB::table('tb_employee')->where('emp_id','=',$mbk->success_by)->get();
                                foreach ($sql_setcar as $emp_setcar) {
                                  $fullset = $emp_setcar->emp_fname." ".$emp_setcar->emp_lname;
                                }
                                 echo $fullset."  แก้ไขเมื่อ ";datetime($success_date) ?>
                              </div>
                            <label class="col-md-3"><b>เหตุผลที่แก้ไข :</b></label>
                              <div class="col-md-9 text-black">
                                <?php echo $mbk->success_reasons;  ?>
                              </div>
                          @endif
                       </div>
                      </div>
                   {{-- start loop --}}
                @endforeach
          <?php
              }//for
            }//if
          ?>
         </div>
       </div>
     </div>
   <!--Detail Merge-->

   <!-- location -->
       <!-- <div class="row" id="c3" hidden="hidden">
         <div class="col-md-12">
           <div class="card card_bk">
             <div class="card-header">
               <span class="	fa fa-map-marker"></span>  สถานที่
             </div>
                 <div class="card-block" id="map" style="height:500px;"></div>
                 <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAaXMuANTFVYidZ3gQaPaUVQU4zeEmk33U&callback=initMap"></script>
               </div>
             </div>
       </div> -->
 <!-- end location -->

 <!-- document -->
  <div class="row" id="c5" hidden="hidden">
   <div class="col-md-12">
     <div class="card card_bk">
       <div class="card-header">
         <span class="	fa fa-file"></span>  เอกสารที่แนบมา
       </div>

         <?php
             if($doc == ""){
                 echo "<div class='card-block' align='center'><h2><span class='	fa fa-exclamation-triangle'></span> ไม่พบเอกสาร</h2></div>";
             }else{
               $url_doc = Storage::url('document/'.$doc);
               $file = explode(".",$doc);
               $type = $file[count($file)-1];
               if($type == "pdf"){
                 echo "<input type='hidden' id='pathtxt' value='".$url_doc."'>";
                 echo "<div id='framPDF' style='height:800px;'></div>";
               }else{ ?>
                 <input type='hidden' id='pathtxt' value='' >

                 <ul class='nav nav-pills bg-inverse justify-content-end' id='tabPrint' >
                   <li class="nav-item">
                     <a class="nav-link" href="{{$url_doc}}" download="{{$url_doc}}">
                       <span class='fa fa-download'></span>
                     </a>
                   </li>
                     <li class="nav-item">
                       <a  class="nav-link"  onclick="VoucherPrint('{{$url_doc}}')">
                         <span class='fa fa-print'></span>
                       </a>
                     </li>
                 </ul>
                 <img width='100%' class='bkImg' src='{{$url_doc}}'>
         <?php  }
             }
         ?>

     </div>
   </div>
   </div>
   <!-- end document-->
<?php if($status == "success"){?>
   <!-- car&driver -->
       <div class="row" id="c4" hidden="hidden">
         <div class="col-md-12">
           <div class="card card_bk">
                 <div class="card-header">
                   <span class="	fa fa-car text-black">  การจัดรถ </span>
                 </div>

   <!-- car -->
             <div class="card-block">
         <?php
         $sqlcar = DB::table('tb_car')->where('com_id','=',$com_id)->where('car_id','=', $car)->get();
               foreach ($sqlcar as $car):
                $cfront = $car->car_img_front;
                $cnum = $car->car_number;
                $cmodel = $car->car_model;
               endforeach;
               $url_imgC = Storage::url('image/car/'.$cfront);
         $sqldrive = DB::table('tb_driver')->where('com_id','=',$com_id)->where('drive_id','=',$drive)->get();
               foreach ($sqldrive as $dr):
                $img = $dr->drive_img;
                $dname = $dr->drive_fname." ".$dr->drive_lname;
                $dtel = $dr->drive_tel;
               endforeach;
               $url_imgD = Storage::url('image/driver/'.$img);

         ?>
         <div class="row">
           <div class="col-md-3">

             <img width="100%" src="{{$url_imgC}}">
           </div>
           <div class="col-md-9">
             <h4><b><?php echo $cnum; ?></b></h4>
             <h5><?php echo $cmodel; ?></h5>
           </div>
         </div>
       </div>
 <!-- end car -->

 <!-- driver -->
       <div class="card card_bk">
         <div class="card-header">
           <span class="	fa fa-car text-black">  คนขับ</span>
         </div>
           <div class="card-block">
             <div class="row">
               <div class="col-md-3">
                 <img class="rounded-circle" width="100%" src="{{$url_imgD}}">
               </div>
               <div class="col-md-9">
                 <h4><b>{{$dname}}</b></h4>
                 <h5>{{$dtel}}</h5>
               </div>
             </div>
           </div>
         </div>
 <!-- end driver -->
 <!-- comment -->
 <?php if ($change_detail<>null) {?>
       <div class="card card_bk">
         <div class="card-header">
           <span class="	fa fa-exclamation-triangle text-black"> หมายเหตุ</span>
         </div>
           <div class="card-block">
             <div class="row">
               <div class="offset-md-1 col-md-11">
                 <?php echo str_replace(";","<br>",$change_detail); ?>
               </div>
             </div>
           </div>
         </div>
         <?php } ?>
 <!-- comment -->


 <?php } ?>
         </div>
       </div>
     </div>
<!-- end car&driver -->
   </div>
 </div>
</div>
</div>
<script src="{{ asset('/js/bootstrap.min.js') }}"></script>
<script>
var pathtxt = $("#pathtxt").val();
 if(pathtxt != ""){
   PDFObject.embed(pathtxt, "#framPDF");
 }
 function VoucherSourcetoPrint(source){
   return "<html><head><script>function step1(){\n" +
       "setTimeout('step2()', 10);}\n" +
       "function step2(){window.print();window.close()}\n" +
       "</scri" + "pt></head><body onload='step1()'>\n" +
       "<img src='" + source + "' /></body></html>";
 }
 function VoucherPrint(source) {
   Pagelink = "about:blank";
   var pwa = window.open(Pagelink, "_new");
   pwa.document.open();
   pwa.document.write(VoucherSourcetoPrint(source));
   pwa.document.close();
 }
function tapChange(tshow,thide1,thide2,thide3,thide4){
   $("#tap"+tshow).addClass("active");
   $("#tap"+thide1).removeClass("active");
   $("#tap"+thide2).removeClass("active");
   $("#tap"+thide3).removeClass("active");
   $("#tap"+thide4).removeClass("active");
   $("#c"+tshow).show("slow");
   $("#c"+tshow).removeAttr("hidden");
   $("#c"+thide1).hide("slow");
   $("#c"+thide2).hide("slow");
   $("#c"+thide3).hide("slow");
   $("#c"+thide4).hide("slow");
 }

 $("#locate").click(function () {
   window.open('/locate','Location', 'width=800,height=800')
 })

 function initMap(address) {

   var obj = [];
   <?php
   $sqlbooking_locate = DB::table('tb_booking_location')->where('bk_id', '=', '<?php echo $bk_id ?>')->get();
   foreach ($sqlbooking_locate as $locate):
     $id = $locate->bk_id;
     $name = $locate->location_name;
     $address =  $locate->location_id;
     $latitude = $locate->location_lat;
     $longitude = $locate->location_lng;?>
     var location = ["<?php echo $address ?>",<?php echo $latitude ?>,<?php echo $longitude ?>,"<?php echo $name ?>"];
     obj.push(location);
   <?php endforeach; ?>

     var map = new google.maps.Map(document.getElementById('map'), {
     zoom: 10,
     center: new google.maps.LatLng(obj[0][1], obj[0][2]),
     mapTypeId: google.maps.MapTypeId.ROADMAP
     });

     var infowindow = new google.maps.InfoWindow();

     var marker, i;

     for (i = 0; i < obj.length; i++) {
     marker = new google.maps.Marker({
       position: new google.maps.LatLng(obj[i][1], obj[i][2]),
       map: map
        });

     google.maps.event.addListener(marker, 'click', (function(marker, i) {
           return function() {
             var name = "สถานที่  :"+obj[i][0]+"<br> ตำแหน่ง :"+obj[i][3]
             infowindow.setContent(name);
             infowindow.open(map, marker);
           }
         }
       )

      (marker, i));
     }

     var circle = new google.maps.Circle({
       map: map,
       radius: 30000,    // 30km to metres
       fillColor: '#B2E5B2'
     });
     circle.bindTo('center', marker, 'position');

     $('modalBK').on('shown.bs.modal', function ()
       {
           google.maps.event.trigger(map, "resize");
       });
 }



 $(document).ready(function(){
   var dSend = $("#tap1").data("id");


});

$(".pdf").click(function() {
  var bk_id = $("#tap1").data("id");
  console.log(bk_id);
  window.location ='/bookingpdf?'+bk_id;


})

$("#btnApprove").click(function(){
   var dSend = $("#tap1").data("id");
   var emp_id = $("#approve_panel").data("id");
   swal({
     title:"คุณแน่ใจ ?",
     text:"คุณต้องการอนุญาติการจองรถใช่หรือไม่ ",
     type:"warning",
     showCancelButton:true,
     confirmButtonColor:"#2ECC71",
     confirmButtonText:"อนุมัติ",
     cancelButtonText:"ยกเลิก",
     cancelButtonColor:"#E74C3C",
     closeOnConfirm:false,
   },function(isConfirm){
     if(isConfirm){
       $.ajax({url:"/approvestatus",data:dSend+"&"+emp_id+"&type=approve",type:"POST",success:function(data){
         var obj = JSON.parse(data);
         // console.log(obj['bk_id']);
           if (obj['success']==true) {
                $(".close").trigger('click');
                $.ajax({url:"/sendapprove",data:"id="+obj['bk_id'],type:"POST",success:function(data){
                  // console.log(data);
                 }
                })
                $.ajax({url:"/sendSetCar",
                       data:"bk_id="+obj['bk_id'],
                       type:"POST",success:function(mail){
                         // console.log(mail);
                       }
                    })
                 swal({
                   title: "การอนุมัติสำเร็จ",
                   type: "success",
                   text: "ระบบกำลังส่ง Email แจ้งเตือน กรุณารอสักครู่",
                   timer: 5000,
                   showConfirmButton: false
                 },function () {
                    window.location = "/waittoapprove";
                 });
           }
         }
       });
     }
   });
 });

$("#btnEject").click(function(){
 var dSend = $("#tap1").data("id");
 var emp_id = $("#approve_panel").data("id");
 swal({
   title:"คุณแน่ใจ ?",
   text:"คุณต้องการยกเลิกการจองรถใช่หรือไม่ ",
   type:"warning",
   showCancelButton:true,
   confirmButtonColor:"#E74C3C",
   confirmButtonText:"ยกเลิกการจอง",
   cancelButtonText:"ยกเลิก",
   closeOnConfirm:false,
 },function(isConfirm){
   if(isConfirm){
     swal({
       title: "ยกเลิก!",
       text: "กรุณากรอกเหตุผลที่ยกเลิก :",
       type: "input",
       showCancelButton: true,
       closeOnConfirm: false,
       inputPlaceholder: "เหตุผล"
     }, function (inputValue) {
       if (inputValue === false) return false;
       if (inputValue === "") {
         swal.showInputError("กรุณากรอกเหตุผลที่ยกเลิก");
         return false
       }
       $.ajax({url:"/approvestatus",data:dSend+"&"+emp_id+"&type=eject&reasons="+inputValue,type:"POST",success:function(data){
         var obj = JSON.parse(data);
         // console.log(obj['bk_id']);
           if (obj['success']==true) {
                       swal({
                         type:"success",
                         title:"บันทึกสำเร็จ",
                         text:"คุณทำการยกเลิกการจองรถเรียบร้อย",
                         confirmButtonText:"ตกลง",
                         confirmButtonColor:"#2ECC71",
                         closeOnConfirm:true,
                       },function(isConfirm){
                         $.ajax({url:"/sendeject",data:"id="+obj['bk_id'],type:"POST",success:function(data){
                           var obj = JSON.parse(data);
                             if (obj['success']==true) {
                                 window.location = "/waittoapprove";
                               }
                             }
                           }) //sendeject
                     });
               }// if
           }
         }); //approvestatus
     });//Confirm
   }//ifisConfirm
 });//sure
});

$("#btnEjectMe").click(function(){
  console.log('btnEjectMe');
 var dSend = $("#tap1").data("id");
 var emp_id = $("#approve_panel").data("id");
 swal({
   title:"คุณแน่ใจ ?",
   text:"คุณต้องการยกเลิกการจองรถใช่หรือไม่ ",
   type:"warning",
   showCancelButton:true,
   confirmButtonColor:"#E74C3C",
   confirmButtonText:"ยกเลิกการจอง",
   cancelButtonText:"ยกเลิก",
   closeOnConfirm:false,
 },function(isConfirm){
   if(isConfirm){
     swal({
       title: "ยกเลิก!",
       text: "กรุณากรอกเหตุผลที่ยกเลิก :",
       type: "input",
       showCancelButton: true,
       closeOnConfirm: false,
       inputPlaceholder: "เหตุผล"
     }, function (inputValue) {
       if (inputValue === false) return false;
       if (inputValue === "") {
         swal.showInputError("กรุณากรอกเหตุผลที่ยกเลิก");
         return false
       }
       $.ajax({url:"/approvestatus",data:dSend+"&"+emp_id+"&type=eject&reasons="+inputValue,type:"POST",success:function(data){
         var obj = JSON.parse(data);
         // console.log(obj['bk_id']);
           if (obj['success']==true) {
                       swal({
                         type:"success",
                         title:"บันทึกสำเร็จ",
                         text:"คุณทำการยกเลิกการจองรถเรียบร้อย",
                         confirmButtonText:"ตกลง",
                         confirmButtonColor:"#2ECC71",
                         closeOnConfirm:true,
                       },function(isConfirm){
                         $.ajax({url:"/sendeject",data:"id="+obj['bk_id'],type:"POST",success:function(data){
                           var obj = JSON.parse(data);
                             if (obj['success']==true) {
                                 window.location = "/waittoapprove";
                               }
                             }
                           }) //sendeject
                     });
               }// if
           }
         }); //approvestatus
     });//Confirm
   }//ifisConfirm
 });//sure
});


$("#btnEdit").click(function() {
  var emp_id = $("#approve_panel").data("id");
  var dSend = $("#tap1").data("id");
  window.location = "/edit?"+dSend;
})

$("#btnUse").click(function(){
    var dSend = $("#tap1").data("id");
    var emp_id = $("#approve_panel").data("id");
    var car_id = $("#car_id").val();
    var drive_id = $("#drive_id").val();
    swal({
      title:"คุณแน่ใจ ?",
      text:"คุณต้องการจัดรถใหม่ใช่หรือไม่ ",
      type:"warning",
      showCancelButton:true,
      confirmButtonColor:"#2ECC71",
      confirmButtonText:"ใช่",
      cancelButtonText:"ไม่ใช่",
      cancelButtonColor:"#E74C3C",
      closeOnConfirm:false,
    },function(isConfirm){
      if(isConfirm){
        swal({
          title: "ใช้งานเรียบร้อยแล้ว!",
          text: "กรุณากรอกเหตุผล :",
          type: "input",
          showCancelButton: true,
          closeOnConfirm: false,
          inputPlaceholder: "เหตุผล"
        }, function (inputValue) {
          if (inputValue === false) return false;
          if (inputValue === "") {
            swal.showInputError("กรุณากรอกเหตุผล");
            return false
          }
                 $.ajax({url:"/ReUseCar",
                 data:dSend+"&car_id="+car_id+"&drive_id="+drive_id+"&reasons="+inputValue,
                 type:"POST",
                 success:function(data){
                   var obj = JSON.parse(data);
                   if (obj['success']==true) {
                     swal({
                                 type:"success",
                                 title:"เรียบร้อย",
                                 text:"",
                                 confirmButtonText:"ตกลง",
                                 confirmButtonColor:"#2ECC71",
                                 closeOnConfirm:true,
                               },function(isConfirm){
                                  window.location = "/bookingcar";
                     });
                   }
           }//function ajax ReUseCar
        });//ReUseCar
      })//swal กรุณากรอกเหตุผลที่จัดรถใหม่
    }//isConfirm คุณต้องการจัดรถใหม่ใช่หรือไม่
  })//swal คุณต้องการจัดรถใหม่ใช่หรือไม่
});//end function

$("#btnReApprove").click(function(){
   var dSend = $("#tap1").data("id");
   var emp_id = $("#approve_panel").data("id");
   var car_id = $("#car_id").val();
   var drive_id = $("#drive_id").val();
   swal({
     title:"คุณแน่ใจ ?",
     text:"คุณต้องการจัดรถใหม่ใช่หรือไม่ ",
     type:"warning",
     showCancelButton:true,
     confirmButtonColor:"#2ECC71",
     confirmButtonText:"ใช่",
     cancelButtonText:"ไม่ใช่",
     cancelButtonColor:"#E74C3C",
     closeOnConfirm:false,
   },function(isConfirm){
     if(isConfirm){
       swal({
         title: "ยกเลิก!",
         text: "กรุณากรอกเหตุผลที่จัดรถใหม่ :",
         type: "input",
         showCancelButton: true,
         closeOnConfirm: false,
         inputPlaceholder: "เหตุผล"
       }, function (inputValue) {
         if (inputValue === false) return false;
         if (inputValue === "") {
           swal.showInputError("กรุณากรอกเหตุผลที่จัดรถใหม่");
           return false
         }
         $.ajax({url:"/ReApprove",data:dSend+"&"+emp_id+"&type=approve&reasons="+inputValue,type:"POST",success:function(data){
           var obj = JSON.parse(data);
           // console.log(obj['bk_id']);
             if (obj['success']==true) {
                $.ajax({url:"/ReSetcar",
                data:dSend+"&car_id="+car_id+"&drive_id="+drive_id,
                type:"POST",
                success:function(data){
                  // console.log(data);
                  var obj = JSON.parse(data);
                  if (obj['success']==true) {
                    swal({
                                type:"success",
                                title:"เรียบร้อย",
                                text:"คุณทำการแก้ไขเพื่อจัดรถใหม่เรียบร้อยแล้ว",
                                confirmButtonText:"ตกลง",
                                confirmButtonColor:"#2ECC71",
                                closeOnConfirm:true,
                              },function(isConfirm){
                                 window.location = "/bookingcar";
                    });
                  }
                }//function ajax ReSetcar
              })//ReSetcar
            }//if obj['success']==true
          }//function ajax ReApprove
       });//ReApprove
     })//swal กรุณากรอกเหตุผลที่จัดรถใหม่
   }//isConfirm คุณต้องการจัดรถใหม่ใช่หรือไม่
 })//swal คุณต้องการจัดรถใหม่ใช่หรือไม่
})//end function

$("#btnEjectApprove").click(function(){
   var dSend = $("#tap1").data("id");
   var emp_id = $("#approve_panel").data("id");
   var car_id = $("#car_id").val();
   var drive_id = $("#drive_id").val();
   swal({
     title:"คุณแน่ใจ ?",
     text:"คุณต้องการจัดรถใหม่ใช่หรือไม่ ",
     type:"warning",
     showCancelButton:true,
     confirmButtonColor:"#2ECC71",
     confirmButtonText:"ใช่",
     cancelButtonText:"ไม่ใช่",
     cancelButtonColor:"#E74C3C",
     closeOnConfirm:false,
   },function(isConfirm){
     if(isConfirm){
       swal({
         title: "ยกเลิก!",
         text: "กรุณากรอกเหตุผลที่จัดรถใหม่ :",
         type: "input",
         showCancelButton: true,
         closeOnConfirm: false,
         inputPlaceholder: "เหตุผล"
       }, function (inputValue) {
         if (inputValue === false) return false;
         if (inputValue === "") {
           swal.showInputError("กรุณากรอกเหตุผลที่จัดรถใหม่");
           return false
         }
         $.ajax({url:"/ReApprove",data:dSend+"&"+emp_id+"&type=eject&reasons="+inputValue,type:"POST",success:function(data){
           var obj = JSON.parse(data);
             if (obj['success']==true) {
                $.ajax({url:"/ReSetcar",
                data:dSend+"&car_id="+car_id+"&drive_id="+drive_id,
                type:"POST",
                success:function(data){
                  var obj = JSON.parse(data);
                  if (obj['success']==true) {
                    swal({
                                type:"success",
                                title:"เรียบร้อย",
                                text:"คุณทำการยกเลิกการใช้งานเรียบร้อยแล้ว",
                                confirmButtonText:"ตกลง",
                                confirmButtonColor:"#2ECC71",
                                closeOnConfirm:true,
                              },function(isConfirm){
                                location.reload();
                    });
                  }
                }//function ajax ReSetcar
              })//ReSetcar
            }//if obj['success']==true
          }//function ajax ReApprove
       });//ReApprove
     })//swal กรุณากรอกเหตุผลที่จัดรถใหม่
   }//isConfirm คุณต้องการจัดรถใหม่ใช่หรือไม่
 })//swal คุณต้องการจัดรถใหม่ใช่หรือไม่
})//end function

$("#btnEjectBooking").click(function(){
   var dSend = $("#tap1").data("id");
   var emp_id = $("#approve_panel").data("id");
   var car_id = $("#car_id").val();
   var drive_id = $("#drive_id").val();
   swal({
     title:"คุณแน่ใจ ?",
     text:"คุณต้องยกเลิกการใช้รถใช่หรือไม่ ",
     type:"warning",
     showCancelButton:true,
     confirmButtonColor:"#2ECC71",
     confirmButtonText:"ใช่",
     cancelButtonText:"ไม่ใช่",
     cancelButtonColor:"#E74C3C",
     closeOnConfirm:false,
   },function(isConfirm){
     if(isConfirm){
       swal({
         title: "ยกเลิก!",
         text: "กรุณากรอกเหตุผลที่ต้องยกเลิกการใช้รถ :",
         type: "input",
         showCancelButton: true,
         closeOnConfirm: false,
         inputPlaceholder: "เหตุผล"
       }, function (inputValue) {
         if (inputValue === false) return false;
         if (inputValue === "") {
           swal.showInputError("กรุณากรอกเหตุผลที่จัดรถใหม่");
           return false
         }
         $.ajax({url:"/ReApprove",data:dSend+"&"+emp_id+"&type=ejectcar&reasons="+inputValue,type:"POST",success:function(data){
           var obj = JSON.parse(data);
             if (obj['success']==true) {
                $.ajax({url:"/ReSetcar",
                data:dSend+"&car_id="+car_id+"&drive_id="+drive_id,
                type:"POST",
                success:function(data){
                  var obj = JSON.parse(data);
                  if (obj['success']==true) {
                    swal({
                                type:"success",
                                title:"เรียบร้อย",
                                text:"คุณทำการยกเลิกการใช้งานเรียบร้อยแล้ว",
                                confirmButtonText:"ตกลง",
                                confirmButtonColor:"#2ECC71",
                                closeOnConfirm:true,
                              },function(isConfirm){
                                location.reload();
                    });
                  }
                }//function ajax ReSetcar
              })//ReSetcar
            }//if obj['success']==true
          }//function ajax ReApprove
       });//ReApprove
     })//swal กรุณากรอกเหตุผลที่จัดรถใหม่
   }//isConfirm คุณต้องการจัดรถใหม่ใช่หรือไม่
 })//swal คุณต้องการจัดรถใหม่ใช่หรือไม่
})//end function


$("#btnChange").click(function () {
    var id = btoa(btoa(btoa($("#bk_id").val())));
    window.location = "/changecar?id="+id;
})
</script>
