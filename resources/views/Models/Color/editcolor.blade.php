<div class="modal fade" id="modalBk" >
  <div class="modal-dialog modal-lg" role="document" id="dialogBk" >
    <div class="modal-content" >
    <div class="modal-header">
      <h5 class="modal-title fa fa-plus text-black"> บันทึกข้อมูลสีรถยนต์</h5>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body">

      <form name="createform" id="create-form">

         <div class="form-group row">
            <!-- รหัสพนักงาน -->
             <input type="hidden" name="emp_id" id="emp_id" value="<?php echo $emp_id ?>">
           <!-- รหัสพนักงาน -->
            <?php
            // dep_id
            $sqlcolor = DB::table('tb_color')->where("color_id",'=',$color_id)->get();
            foreach ($sqlcolor as $color):
             $color_name = $color->color_name;
            endforeach;
             ?>
            <!-- รหัสสี -->
            <label for="color_name" class ="col-md-4 col-form-label text-black">รหัสสี</label>
            <input type="text"  id="color_id" name="color_id" value="<?php echo $color_id ?>" readonly>
          </div><!-- รหัสสี -->

          <div class="form-group row"><!-- ชื่อสี -->
            <label for="color_name" class ="col-md-4 col-form-label text-black">ชื่อสี</label>
            <input type="text" class="form-control col-md-6" id="color_name" name="color_name" value="<?php echo $color_name ?>">
          </div><!-- ชื่อสี -->

      </form>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-primary btn-create">บันทึกข้อมูล</button>
      <button type="button" class="btn btn-secondary" data-dismiss="modal">ยกเลิก</button>
    </div>
  </div>
</div>
</div>
<script>
$(document).ready(function(){
$("#color_name").focus();
});
$("#create-form").keypress(function(event){
 var kc = event.keyCode;
 if(kc==13){
    editcolor();
 }
});
$(".btn-create").click(function(){
  editcolor();
});

function editcolor(){
var form_create= $("#create-form").serialize();
 $.ajax({
   url:"/updatecolor",
   data:form_create,
   type:"POST",
   success:function(data){
      var obj =JSON.parse(data);
    if(obj['success']==true)
      {
        swal({
                  title: "บันทึกข้อมูลสำเร็จ",
                  text: "บันทึกข้อมูลประเภทรถยนต์สำเร็จแล้ว",
                  type: "success",
                  showCancelButton: false,
                  confirmButtonColor: "#2ECC71",
                  confirmButtonText: "ตกลง",
                  closeOnConfirm: false,
                },
                  function(isConfirm){
                    if (isConfirm) {
                      window.location = "/editcolor";
                }
            });
      }
    else
      {
        addErr(obj['type'],obj['msg']);

      }
   }
 });
};


function rmErr(input){
 $("#group"+input.id).removeClass("has-danger");
 $("#group"+input.id+" input").removeClass("form-control-danger");
 $("#fb"+input.id).attr("hidden","hidden");
}

function addErr(type,msg){
   $("#group"+type).addClass("has-danger");
   $("#group"+type+" input").addClass("form-control-danger");
   $("#fb"+type).html(msg);
   $("#fb"+type).removeAttr("hidden");
}

</script>
