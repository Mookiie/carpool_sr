<?php
$sqlbooking = DB::table('tb_booking')
->join("tb_car_type",function($join){
      $join->on("tb_booking.ctype_id","=","tb_car_type.ctype_id");
  })
  ->join("tb_department",function($join){
        $join->on("tb_booking.dep_id","=","tb_department.dep_id")
            ->on("tb_booking.com_id","=","tb_department.com_id");
    })
->join("tb_employee",function($join){
      $join->on("tb_booking.emp_id","=","tb_employee.emp_id")
          ->on("tb_booking.com_id","=","tb_employee.com_id");
  })
->where('bk_id','=',"$bk_id")->get();
foreach ($sqlbooking as $bk):
 $bk_start = explode(" ",$bk->bk_start_start);
 $bk_end = explode(" ",$bk->bk_end_start);
 $status = $bk->bk_status;
 $ctype_id = $bk->ctype_id;
 $com_id = $bk->com_id;
 $dep_car = $bk->dep_car;
 $bk_use = $bk->bk_use;

 $Sh = substr($bk->bk_start_start,11,2);
 $End_h = substr($bk->bk_end_start,11,2);
endforeach;
if ($End_h == '00') {
  $Eh = 23;
}
else {
  $Eh = $End_h;
}
$date1 = $bk_start[0];
$date2 = $bk_end[0];
$d1= new DateTime($date1);
$d2 = new DateTime($date2);
$diff = $d1->diff($d2)->format("%a");
// $chk ='';
?>
     <table class="table table-bordered">
       <thead><th colspan="3"></th></thead>

       <?php
       $sqlcar = DB::table('tb_car')
                 ->where('ctype_id','=',$ctype)
                 ->where('com_id','=',$com_id)
                 ->where('dep_id','=',$dep_car)
                 ->whereIN('car_status', [0, 4])
                 ->orderBy('car_id','ASC')->get();
      if (count($sqlcar)>0) {
       foreach ($sqlcar as $car):
         $b=array();
         $car_id = $car->car_id;
         $url_imgC = Storage::url('image/car/'.$car->car_img_front);
         if ($diff<=0) {
           $log_day =  DB::table('tb_log_car')
                       ->where('car_id','=',$car_id)
                       ->where('dep_car','=',$dep_car)
                       ->where('log_date','=',$date1)
                       ->get();
           if (count($log_day)>0) {
             for ($i=$Sh; $i <= $Eh; $i++) {
               $log_hour = DB::table('tb_log_car')
                             ->where('car_id','=',$car_id)
                             ->where('dep_car','=',$dep_car)
                             ->where('log_date','=',$date1)
                             ->where('log_'.(int)$i,'=',null)
                             ->get();

                if (count($log_hour)>0) {
                  array_push($b,0);
                }else {
                  array_push($b,1);
                }//chk free
             }//log hour
           }//chk log day
           else {array_push($b,0);}//no log
         }//วันเดียว
         else {
           if ($bk_use == null) {
           //first
             $log_day =  DB::table('tb_log_car')
                         ->where('car_id','=',$car_id)
                         ->where('dep_car','=',$dep_car)
                         ->where('log_date','=',$date1)
                         ->get();
             if (count($log_day)>0) {
               for ($i=$Sh; $i <= 23; $i++) {
                 $log_hour = DB::table('tb_log_car')
                               ->where('car_id','=',$car_id)
                               ->where('dep_car','=',$dep_car)
                               ->where('log_date','=',$date1)
                               ->where('log_'.(int)$i,'=',null)
                               ->get();

                 if (count($log_hour)>0) {array_push($b,0);}
                 else {array_push($b,1);}
                 }
               }//have log
             else {array_push($b,0);}//no log

           //between
            for ($i=1; $i < $diff ; $i++) {
             $tomorrow = date('Y-m-d',strtotime(str_replace('-', '/', $date1) . "+".$i." days"));
             $log_day =  DB::table('tb_log_car')
                         ->where('car_id','=',$car_id)
                         ->where('dep_car','=',$dep_car)
                         ->where('log_date','=',$tomorrow)
                         ->get();
             if (count($log_day)>0) {
               for ($j=0; $j <= 23; $j++) {
                 $log_hour = DB::table('tb_log_car')
                               ->where('car_id','=',$car_id)
                               ->where('dep_car','=',$dep_car)
                               ->where('log_date','=',$tomorrow)
                               ->where('log_'.(int)$j,'=',null)
                               ->get();

                 if (count($log_hour)>0) {array_push($b,0);}
                 else {array_push($b,1);}
                }//for hour
               }//have log
             else {array_push($b,0);}//no log
            }//for diffdate

          //last
            $log_day =  DB::table('tb_log_car')
                           ->where('car_id','=',$car_id)
                           ->where('dep_car','=',$dep_car)
                           ->where('log_date','=',$date2)
                           ->get();
            if (count($log_day)>0) {
              for ($i=0; $i <= $Eh; $i++) {
                   $log_hour = DB::table('tb_log_car')
                                 ->where('car_id','=',$car_id)
                                 ->where('dep_car','=',$dep_car)
                                 ->where('log_date','=',$date2)
                                 ->where('log_'.(int)$i,'=',null)
                                 ->get();

                   if (count($log_hour)>0) {array_push($b,0);}
                   else {array_push($b,1);}
                }
             }//have log
            else {array_push($b,0);}//no log

          }//all day
            else {
            //first
              $log_day =  DB::table('tb_log_car')
                          ->where('car_id','=',$car_id)
                          ->where('dep_car','=',$dep_car)
                          ->where('log_date','=',$date1)
                          ->get();
              if (count($log_day)>0) {
                for ($i=$Sh; $i <= $Eh; $i++) {
                  $log_hour = DB::table('tb_log_car')
                                ->where('car_id','=',$car_id)
                                ->where('dep_car','=',$dep_car)
                                ->where('log_date','=',$date1)
                                ->where('log_'.(int)$i,'=',null)
                                ->get();

                  if (count($log_hour)>0) {array_push($b,0);}
                  else {array_push($b,1);}
                  }
                }//have log
              else {array_push($b,0);}//no log

            //between
             for ($i=1; $i <= $diff ; $i++) {
              $tomorrow = date('Y-m-d',strtotime(str_replace('-', '/', $date1) . "+".$i." days"));
              $log_day =  DB::table('tb_log_car')
                          ->where('car_id','=',$car_id)
                          ->where('dep_car','=',$dep_car)
                          ->where('log_date','=',$tomorrow)
                          ->get();
              if (count($log_day)>0) {
                for ($j=$Sh; $j <= $Eh; $j++) {
                  $log_hour = DB::table('tb_log_car')
                                ->where('car_id','=',$car_id)
                                ->where('dep_car','=',$dep_car)
                                ->where('log_date','=',$tomorrow)
                                ->where('log_'.(int)$j,'=',null)
                                ->get();

                  if (count($log_hour)>0) {array_push($b,0);}
                  else {array_push($b,1);}
                  }
                }//have log
              else {array_push($b,0);}//no log
             }//for diffdate

           }//someday

          }//หลายวัน

         for ($i=0; $i < count($b); $i++) {
           if ($b[$i]==1) {
            $chk = 1;break;
           }
           else {
             $chk = 0;
           }
         }

         if ($chk == 0) {
           ?>
           <tr class="carDetail" data-id="{{$car_id}}">
              <td width="10%" align="center" style="border-right:solid 0px;">
                <img width="100%" src="{{$url_imgC}}"></td>
              <td style="border-left:solid 0px;">
                {{$car->car_number}}<br>
                 <small>{{$car->car_model}}</small>
              </td>
              <td width="10%">
                <button class="btn btn-sm btn-primary setCar" data-id="{{$car->car_id}}">
                  <span class="fa fa-sign-in"></span> เลือกรถ
                </button>
              </td>
          </tr>
      <?php
      }
      else {
        ?>
       <tr class="carDetail" data-id="{{$car_id}}">
         <td width="10%" align="center" style="border-right:solid 0px;">
           <img width="100%" src="{{$url_imgC}}"></td>
         <td style="border-left:solid 0px;">
           {{$car->car_number}}<br>
            <small>{{$car->car_model}}</small>
         </td>
         <td width="10%">
           <button class="btn btn-sm  cantCar" disabled>
             <span class="fa fa-minus-circle"></span> ไม่ว่าง
           </button>
         </td>
       </tr>
        <?php
      }
       endforeach; }
       else {?>
         <tr class="carDetail" data-id="">
            <td width="10%" align="center" style="border-right:solid 0px;">
            </td>
            <td style="border-left:solid 0px;">
              ไม่มีรถประเภทนี้<br>
               <small></small>
            </td>
            <td width="10%">
            </td>
        </tr>
         <?php }?>
     </table>
