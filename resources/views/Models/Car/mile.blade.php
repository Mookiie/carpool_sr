<div class="modal fade" id="modalBk" >
  <div class="modal-dialog modal-lg" role="document" id="dialogBk" >
    <div class="modal-content" >

            <div class="modal-header">
                <h5 class="modal-title fa fa-plus text-black"> บันทึกเลขไมล์ของรายการ {{$bk_id}}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
              <form name="createform" id="create-form">
                 <div class="form-group row">
                    <!-- รหัสพนักงาน -->
                     <input type="hidden" name="emp_id" id="emp_id" value="<?php echo $emp_id ?>">
                   <!-- รหัสพนักงาน -->
                  </div>

                  <div class="form-group row"><!-- เลขไมล์เริ่มต้น -->
                    <label for="mile_start" class ="col-md-4 col-form-label text-black">เลขไมล์เริ่มต้น</label>
                    <input type="text" class="form-control col-md-6" id="mile_start" name="mile_start" onfocus="rmErr(this);" onkeypress="rmErr(this);" pattern="\d*" maxlength="6"/>
                    <input type = "number" maxlength = "3" min = "1" max = "999" >
                    <div hidden="true" id="fbmile_start" class="form-control-feedback"><br></div>
                  </div><!-- เลขไมล์เริ่มต้น -->
                  <div class="form-group row"><!-- เลขไมล์สิ้นสุด -->
                    <label for="mile_end" class ="col-md-4 col-form-label text-black">เลขไมล์สิ้นสุด</label>
                    <input type="text" class="form-control col-md-6" id="mile_end" name="mile_end" onfocus="rmErr(this);" onkeypress="rmErr(this);" pattern="\d*" maxlength="6"/>
                    <div hidden="true" id="fbmile_end" class="form-control-feedback"><br></div>
                  </div><!-- เลขไมล์สิ้นสุด -->

            </div>

            <div class="modal-footer">
              <button type="button" class="btn btn-primary btn-create" id='savemile'>บันทึกข้อมูล</button>
              <button type="button" class="btn btn-secondary" data-dismiss="modal">ยกเลิก</button>
            </div>
            </form>
    </div>
  </div>
</div>

<script type="text/javascript">
  $("#savemile").click(function() {
    var start = $("#mile_start").val();
    var end = $("#mile_end").val();
    var dString = $("#create-form").serialize();
    $.ajax({ type:"POST",
             data :dString,
             url:"/setmileDB",
             success:function(data){
               console.log(data);
              // var obj = JSON.parse(data);
              // if (obj['success']==true) {
              //     window.location ="{{ url("/mile") }}";
              // }
              // else
              // {
              //     addErr(obj['type'],obj['msg'])
              // }
            }
     });

  })

  function rmErr(input){
    $("#group"+input.id).removeClass("has-danger");
    $("#group"+input.id+" input").removeClass("form-control-danger");
    $("#fb"+input.id).attr("hidden","hidden");
  }

  function addErr(type,msg){
      $("#group"+type).addClass("has-danger");
      $("#group"+type+" input").addClass("form-control-danger");
      $("#fb"+type).html(msg);
      $("#fb"+type).removeAttr("hidden");
  }
</script>
