<?php
if($typeSearch == 0){
  $sqlcar = DB::table('tb_car')->join('tb_brand', 'tb_car.brand_id', '=' , 'tb_brand.brand_id' )->where('com_id','=',$com_id)->orderBy('car_id', 'ASC');
}else if($typeSearch == 1){
  $sqlcar = DB::table('tb_car')->join('tb_brand', 'tb_car.brand_id', '=' , 'tb_brand.brand_id' )->where('com_id','=',$com_id)->where('car_number','LIKE','%'.$txtSearch.'%')->orderBy('car_id', 'ASC');
}else if($typeSearch == 2){
  $sqlcar = DB::table('tb_car')->join('tb_brand', 'tb_car.brand_id', '=' , 'tb_brand.brand_id' )->where('com_id','=',$com_id)->where('car_model', 'LIKE' ,'%'.$txtSearch.'%')->orderBy('car_id', 'ASC');
}else if($typeSearch == 3){
  $sqlcar = DB::table('tb_car')->join('tb_brand', 'tb_car.brand_id', '=' , 'tb_brand.brand_id' )->where('com_id','=',$com_id)->where('ctype_id' ,'LIKE' ,'%'.$carType.'%')->orderBy('car_id', 'ASC');
}else if($typeSearch == 4){
  $sqlcar = DB::table('tb_car')->join('tb_brand', 'tb_car.brand_id', '=' , 'tb_brand.brand_id' )->where('com_id','=',$com_id)->where('tb_car.brand_id' ,'LIKE' ,'%'.$carBrand.'%')->orderBy('car_id', 'ASC');
}

$qrycar = $sqlcar->Where("dep_car","=",$dep_id)->orWhere("dep_id","=",$dep_id)->get();
$numcar = $sqlcar->Where("dep_car","=",$dep_id)->orWhere("dep_id","=",$dep_id)->count();
?><table class="table table-bordered">
<thead>
  <th colspan="2"></th>
</thead>
  <tbody><?php
if($numcar > 0){
    foreach ($qrycar as $car):
      ?>
      <?php
      $url_imgC = Storage::url('image/car/'.$car->car_img_front);
          if($numcar > 0){

          ?>
                  <tr>
                    <td width="15%" align="center" style="border-right:solid 0px;"  class="carDetail" data-id="<?php echo $car->car_id; ?>">
                      <img width="50%" src="{{$url_imgC}}"></td>
                    <td style="border-left:solid 0px;" class="text-black">
                      <h6><b class="text-black"><?php echo $car->car_number; ?></b></h6>
                      <?php echo $car->brand_name; ?>  &nbsp;&nbsp;<small ><?php echo $car->car_model; ?></small><br />
                      <small >รถแผนก :
                        <?php $sql_dep = DB::table('tb_job')->where('com_id','=',$com_id)->where('job_id','=',$car->dep_car)->select('job_name')->groupBy('job_name')->get();
                              foreach ($sql_dep as $dep) {
                                echo $dep->job_name;
                              }
                              if ($car->car_status == 4) {
                                $sql_dep = DB::table('tb_job')->where('com_id','=',$com_id)->where('job_id','=',$car->dep_id)->get();
                                     foreach ($sql_dep as $dep) {
                                       $dd = $dep->job_name;
                                     }
                                ?>
                                <br />
                                สถานะการใช้งาน : ยืมใช้งานโดยแผนก {{$dd}}
                                <?php
                              }
                         ?>
                      </small><br />
                      <?php if ($lv_user == 2 || $lv_user > 99 ) {
                          if ($car->car_status == 0 ){ ?>
                        <small class="">สถานะการใช้งาน : ใช้งาน</small><br>
                        <div class="row">
                          <button type="button" class='btn btn-warning btn-sm btn-waitcar' data-id="{{$car->car_id}}">รอซ่อม</button>
                          &nbsp;&nbsp;&nbsp;
                          <button type="button" class='btn btn-danger btn-sm btn-cancelcar' data-id="{{$car->car_id}}">ยกเลิกการใช้งาน</button>
                          &nbsp;&nbsp;&nbsp;
                          {{-- <button type="button" class='btn btn-info btn-sm btn-Loan' data-id="{{$car->car_id}}">ยืมรถแผนกอื่น</button> --}}
                        </div>
                      <?php }
                      elseif ($car->car_status == 2) {
                        ?>
                        <small class="">สถานะการใช้งาน : รอซ่อม</small><br>
                        <div class="row">
                          <button type="button" class='btn btn-success btn-sm btn-car' data-id="{{$car->car_id}}">ใช้งาน</button>
                          &nbsp;&nbsp;&nbsp;
                          <button type="button" class='btn btn-danger btn-sm btn-cancelcar' data-id="{{$car->car_id}}">ยกเลิกการใช้งาน</button>
                          &nbsp;&nbsp;&nbsp;
                          {{-- <button type="button" class='btn btn-info btn-sm btn-Loan' data-id="{{$car->car_id}}">ยืมรถแผนก</button> --}}
                        </div>
                        <?php }
                        elseif ($car->car_status == 4) {
                           $sql_dep = DB::table('tb_job')->where('com_id','=',$com_id)->where('job_id','=',$car->dep_id)->get();
                                foreach ($sql_dep as $dep) {
                                  $dd = $dep->job_name;
                                }

                        ?>
                        {{-- <small><small class=""></small>สถานะการใช้งาน : ยืมใช้งานโดยแผนก {{$dd}}<br></small> --}}
                        @if ($car->dep_car<>$dep_id)
                          <button type="button" class='btn btn-info btn-sm btn-Due' data-id="{{$car->car_id}}">คืนรถแผนก</button>
                        @endif
                        <?php
                        }
                        else{ ?>
                        <small><small class=""></small>สถานะการใช้งาน : ยกเลิกการใช้งาน<br></small>
                      <?php }
                        }?>
                    </td>
                  </tr>


    <?php }else{ ?>
            <tr>
              <td colspan="5" align="center" class="text-black"><h5>ไม่พบกิจกรรม</h5></td>
            </tr>
    <?php }endforeach;
   }else{ ?>
          <tr>
              <td colspan="5" align="center" class="text-black"><h5>ไม่พบกิจกรรม</h5></td>
          </tr>
  <?php } ?>

  <!-- <tr> <td> <?php //echo $qrycar->links(); ?> </td></tr> -->
</tbody>
</table>
