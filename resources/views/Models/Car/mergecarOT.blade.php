<?php
date_default_timezone_set("Asia/Bangkok");
         foreach ($users as $u):
          $emp_lv = $u->emp_level;
          $emp_id = $u->emp_id;
          $dep_emp= $u->dep_id;
          $job_id = $u->job_id;
          $com_id = $u->com_id;
         endforeach;

?>
 <div class="modal fade" id="modalBk" >
   <div class="modal-dialog modal-lg" role="document" id="dialogBk" >
     <div class="modal-content" >

       <div class="modal-header">
           <h6 class="modal-title "><span class="fa fa-edit text-black">  รายการจองสำเร็จ :</span>
       </div>

       <div class="modal-body">
         <input id="com_id" name="com_id" type="hidden" class="form-control" value="{{$com_id}}">
         <input id="emp_id" name="emp_id" type="hidden" class="form-control" value="{{$emp_id}}">
         <input id="bk_id" name="bk_id" type="hidden" class="form-control" value="{{$bk_id}}">
         <table class="table table-bordered tblBk">
           <thead>

             <th>เลขที่อ้างอิง</th>
             <th>ผู้จอง</th>
             <th>แผนก</th>
             <th>วันที่ขอใช้รถ</th>
             <th>ประเภทรถ</th>
             <th></th>
           </thead>
           <tbody class="">
             <?php
               $datenow = date("Y-m-d");
               $sql_bk = DB::table("tb_booking")->where("bk_id","=",$bk_id)->get();
               foreach ($sql_bk as $bk) {
                 $datestart = substr($bk->bk_start_start,0,10);
               }
               $sqlbooking = DB::table('tb_booking')
                             ->join("tb_car_type",function($join){
                                   $join->on("tb_booking.ctype_id","=","tb_car_type.ctype_id");
                               })
                             ->join('tb_employee', 'tb_employee.emp_id', '=' , 'tb_booking.emp_id')
                             ->where('tb_booking.bk_status', '=', 'success')
                             ->where('tb_booking.dep_car', '=', $job_id)
                             ->where('tb_booking.bk_ot', '=', 'on')
                             ->where('tb_booking.bk_start_start','LIKE',$datestart.'%')
                             ->orderBy('bk_start_start')->get();

               $numbooking = count($sqlbooking);
           if($numbooking > 0){
           ?>
                 @foreach ($sqlbooking as $bc)
           <?php
                 if($numbooking > 0){
                   // if ($bc->bk_date > date("Y-m-d 15:30:00")) {
                   //   echo "<tr bgcolor='#ffcdd2'>";
                   // }
                   // // if ($bc->bk_start_start < strtotime("+2 day", date("Y-m-d"))) {
                   // //     echo "<tr bgcolor='#c8e6c9'>";
                   // // }
                   //
               ?>



                   <td class="detailBk text-black" data-id="bk=<?php echo $bc->bk_id;?>">
                     <?php echo $bc->bk_id ?></td>
                   <td class="detailBk text-black" data-id="bk=<?php echo $bc->bk_id;?>">

                     <?php echo $bc->emp_fname." ".$bc->emp_lname; ?></td>
                     <td>
                       <?php
                       $sql_dep = DB::table('tb_department')->where('com_id','=',$com_id)->where('dep_id','=',$bc->dep_id)->get();
                       foreach ($sql_dep as $dep) {
                         echo  $dep->dep_name;
                       }
                       ?>
                     </td>
                   <td class="detailBk text-black" data-id="bk=<?php echo $bc->bk_id;?>">
                     <?php  if (!$bc->bk_use) {
                       echo datetime($bc->bk_start_start);echo " ถึง ";datetime($bc->bk_end_start);
                     } else {
                       echo datetimesome($bc->bk_start_start,$bc->bk_end_start);
                     }
                     $locate = DB::table('tb_booking_location')->where('bk_id','=',$bc->bk_id)->get();
                     foreach ($locate as $lo) {
                       $lo_id =$lo->location_id;
                         if ($lo->location_id == "1") {
                           echo "<br /><b>สถานที่เริ่มต้น</b> : ".$lo->location_name."<br>";
                         }
                         else {
                           echo "<b>สถานที่ ".($lo->location_id-1)."</b> : ".$lo->location_name;
                         }
                       }
                      ?>
                      <br />
                      <b>วัตถุประสงค์</b> :
                      <?php
                            if ($bc->bk_obj == "") {
                              echo "-";
                            }else {
                              echo $bc->bk_obj ;
                            }
                      ?>
                      <br>
                      <b>หมายเหตุ</b> :
                      <?php
                            if ($bc->bk_note == "") {
                              echo "-";
                            }else {
                              echo $bc->bk_note ;
                            }
                      ?>
                      <br>
                      <b>จำนวนผู้เดินทาง</b> :{{$bc->bk_percon}} คน
                   </td>
                   <td class="detailBk text-black" data-id="bk=<?php echo $bc->bk_id;?>">
                     {{$bc->ctype_name}}
                     <?php $job_car = DB::table('tb_job')->where('job_id','=',$bc->dep_car)->groupBy('job_name')->select('job_name')->get(); ?>
                     @foreach ($job_car as $job)
                       [{{$job->job_name}}]
                     @endforeach
                   </td>
                   <td align="center">
                     <button class="btn btn-sm btn-success MergeCar" data-id="bk_main=<?php echo $bc->bk_id; ?>">
                       <span class="fa fa-plus"> เลือก</span>
                     </button>

                     {{-- <button class="btn btn-sm btn-danger cantCar" data-id="data={{$bc->bk_id}}">
                       <span class="fa fa-minus-circle"></span> ยกเลิก
                     </button> --}}
                   </td>
                 </tr>
                 <?php }
                  elseif ($numbooking = 0){ ?>
                    <tr>
                        <td colspan="7" align="center"><h5>ไม่พบกิจกรรม</h5></td>
                    </tr>
                  <?php }?>
          @endforeach

         <?php }else{  ?>
                     <tr>
                         <td colspan="7" align="center"><h5>ไม่พบกิจกรรม</h5></td>
                     </tr>
         <?php } ?>
           </tbody>
         </table>

         <?php
         function datetime($datetime)
         {
           $y = substr($datetime,0,4);
           $m = substr($datetime,5,2);
           $d = substr($datetime,8,2);
           $h = substr($datetime,11,2);
           $i = substr($datetime,14,2);
           echo $d."/".$m."/".$y." เวลา ".$h.":".$i." น." ;
         }
         function datetimesome($datetime1,$datetime2)
         {
           $y1 = substr($datetime1,0,4);
           $m1 = substr($datetime1,5,2);
           $d1 = substr($datetime1,8,2);
           $h1 = substr($datetime1,11,2);
           $i1 = substr($datetime1,14,2);
           $y2 = substr($datetime2,0,4);
           $m2 = substr($datetime2,5,2);
           $d2 = substr($datetime2,8,2);
           $h2 = substr($datetime2,11,2);
           $i2 = substr($datetime2,14,2);
           return $d1."/".$m1."/".$y1." ถึง ".$d2."/".$m2."/".$y2." ช่วงเวลา ".$h1.":".$i1."น. ถึง ".$h2.":".$i2."น." ;
         }
          ?>

       </div>

     </div>
   </div>
</div>
<script src="http://127.0.0.1:8558/js/bootstrap.min.js"></script>
<script type="text/javascript">
  $('.MergeCar').click(function () {
    var detail = $(this).data("id");
    var merge = $("#bk_id").val();
    var emp_id = $("#emp_id").val()
    console.log(detail+"&bk_merge="+merge+"&emp_id="+emp_id);
    $.ajax({
      url:"/mergeDB",
      data:detail+"&bk_merge="+merge+"&emp_id="+emp_id,
      type:"GET",
      success:function(data){
        var obj = JSON.parse(data);
        if (obj['success']==true) {
          $.ajax({url:"/sendcar",data:"id="+obj['bk_id'],type:"POST",success:function(data){
            var obj = JSON.parse(data);
            console.log(obj);
              if (obj['success']==true) {
                  swal({
                    type:"success",
                    title:"สำเร็จ",
                    text:"คุณทำการบันทึกการจัดรถสำเร็จ",
                    confirmButtonText:"ตกลง",
                    confirmButtonColor:"#2ECC71",
                    closeOnConfirm:true,
                  },function(isConfirm){
                    location.reload();
                  });
                }
              }
            })
      }
      }
    })

  })
</script>
