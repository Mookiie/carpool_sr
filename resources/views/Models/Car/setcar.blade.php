<?php
$sqlbooking = DB::table('tb_booking')
->join("tb_car_type",function($join){
       $join->on('tb_booking.ctype_id', '=', 'tb_car_type.ctype_id');
       $join->on("tb_booking.com_id","=","tb_car_type.com_id");
  })
->join('tb_department','tb_booking.dep_id','=','tb_department.dep_id')
->join('tb_employee','tb_employee.emp_id','=' ,'tb_booking.emp_id')
->where('bk_id','=',"$bk_id")->get();

foreach ($sqlbooking as $bk):
 $bk_start = explode(" ",$bk->bk_start_start);
 $bk_end = explode(" ",$bk->bk_end_start);
 $status = $bk->bk_status;
 $ctype_id = $bk->ctype_id;
 $com_id = $bk->com_id;
 $dep_car = $bk->dep_car;
 $bk_use = $bk->bk_use;

 $Sh = substr($bk->bk_start_start,11,2);
 $End_h = substr($bk->bk_end_start,11,2);
endforeach;
if ($End_h == '00') {
  $Eh = 23;
}
else {
  $Eh = $End_h;
}
$date1 = $bk_start[0];
$date2 = $bk_end[0];
$d1= new DateTime($date1);
$d2 = new DateTime($date2);
$diff = $d1->diff($d2)->format("%a");


$arr = array();
      if($status == "wait"){
          array_push($arr,"รอการอนุมัติ","warning","#F39C12");
        }else if($status == "approve"){
          array_push($arr,"รอการจัดรถ","info","#3498DB");
        }else if($status == "success"){
          array_push($arr,"สำเร็จ","success","#2ECC71");
        }else if($status == "eject"){
          array_push($arr,"ยกเลิกการจอง","danger","#E74C3C");
        }else if($status == "ejectcar"){
          array_push($arr,"ยกเลิกการเดินทาง","danger","#E74C3C");
        }else if($status == "nonecar"){
          array_push($arr,"ไม่มีรถ","danger","#E74C3C");
        }
?>


<div class="modal fade" id="modalBk" >
  <div class="modal-dialog modal-lg" role="document" id="dialogBk" >
    <div class="modal-content" >

      <div class="modal-header">
        <h6 class="modal-title"><span class="fa fa-edit text-black">  รายการจอง :</span>
          <span class="badge badge-<?php echo $arr[1]; ?>"><?php echo $arr[0]; ?></span></h6>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
      </div>

      <div class="modal-body">

            <div class="row" id="c1">
              <div class="col-md-12">
                <div class="card card_bk">

                  <div class="row">
                    <div class="col-md-12">
                    <ul class="nav nav-tabs">

                      <li class="nav-item">
                        <a class="nav-link tap1 disabled active"><span class="badge badge-primary badge-tab" id="btap1">1</span> เลือกรถ</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link tap2 disabled"  href="#"><span class="badge badge-default badge-tab" id="btap2">2</span> เลือกคนขับรถ</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link tap3 disabled"  href="#"><span class="badge badge-default badge-tab" id="btap3">3</span> บันทึก</a>
                      </li>
                      {{-- <li class="nav-item" style="padding-left :40%">
                        <a class="nav-link tap3 disabled"></a>
                      </li> --}}
                    </ul>
                    </div>
                    <!--tab menu -->
                    <div class="col-md-12">
                      <br>
                      <div class="progress">
                        <div class="progress-bar" id="StepProgress" role="progressbar" style="width:0%;" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100">
                        </div>
                      </div>
                    </div>
                      <br>
                    <!-- step 1 -->
                    <div class="col-md-12" id="step1">

                      <br>
                      <div class="form-group row">
                          <label for="ctype" class="col-md-2 col-form-label">ประเภทรถ </label>
                          <div class="col-md-10">
                            <select class="form-control"  id="carType" name="carType">
                              <?php
                                $sqltypeCar = DB::table('tb_car_type')->where('com_id','=',$com_id)->get();
                                foreach ($sqltypeCar as $car):
                                  if ($car->ctype_id == $ctype_id) {
                                    echo "<option value='$car->ctype_id' selected> ".$car->ctype_name."</option>";
                                  }
                                  else {
                                    echo "<option value='$car->ctype_id'> ".$car->ctype_name."</option>";
                                  }
                                endforeach;
                              ?>
                            </select>
                          </div>
                      </div>

                      <input id="ctype_id" name="ctype_id" type="hidden" class="form-control" value="{{$ctype_id}}">
                      <div class="alert alert-warning alert-dismissible fade show" style="display:none;">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true" style="color:#000;">&times;</span>
                        </button>
                        คุณกำลังเลือกประเภทรถยนต์ไม่ตรงตามใบจอง
                      </div>

                      <div id='TbSetCar'></div>
                    </div>
                    <!-- step 1 -->
                    <!-- step 2 -->
                    <div class="col-md-12" id="step2" style="display:none;">
                      <br>
                      <button type="button" class="btn btn-sm btn-primary" id="btnprve1"><small><span class="fa fa-angle-left">  PREVIOS</span></small></button>
                      <br><br>
                        <div id='TbSetDriver'></div>
                      <table class="table table-bordered">
                        <thead>
                          <th colspan="3">

                          </th>
                        </thead>
                        <?php
                        $sqldrive = DB::table('tb_driver')->where('com_id','=',$com_id)
                        ->whereIn('dep_id', [$dep_car,'ALL'])
                        ->where('drive_status','=','0')
                        // ->where('drive_id','=','D1708220005')
                        ->orderBy('drive_id','ASC')->get();
                          foreach ($sqldrive as $driver):
                          $a = array();
                          $drive_id = $driver->drive_id;
                          $url_img = Storage::url('image/driver/'.$driver->drive_img);

                           if ($diff<=0) {
                             $sqllogdriver = DB::table('tb_log_driver')
                                       ->where('drive_id','=',$drive_id)
                                       ->where('dep_drive','=',$dep_car)
                                       ->where('log_date','=',$date1)
                                       ->get();
                             if (count($sqllogdriver)>0) {
                             for ($i=$Sh; $i <= $Eh ; $i++) {
                               $sqlloghourd = DB::table('tb_log_driver')
                                         ->where('drive_id','=',$drive_id)
                                         ->where('dep_drive','=',$dep_car)
                                         ->where('log_date','=',$date1)
                                         ->where('log_'.(int)$i,'=',null)
                                         ->get();
                                  if (count($sqlloghourd)>0) {
                                    array_push($a,0);
                                  }else {
                                    array_push($a,1);
                                  }//chk free
                               }//log hour
                             }//chk log day
                             else {
                               array_push($a,0);
                             }
                           }//วันเดียว
                           else {
                             if ($bk_use == null) {
                             //first
                               $log_day = DB::table('tb_log_driver')
                                         ->where('drive_id','=',$drive_id)
                                         ->where('dep_drive','=',$dep_car)
                                         ->where('log_date','=',$date1)
                                         ->get();
                               if (count($log_day)>0) {
                                 for ($i=$Sh; $i <= 23; $i++) {
                                   $log_hour = DB::table('tb_log_driver')
                                             ->where('drive_id','=',$drive_id)
                                             ->where('dep_drive','=',$dep_car)
                                             ->where('log_date','=',$date1)
                                             ->where('log_'.(int)$i,'=',null)
                                             ->get();

                                   if (count($log_hour)>0) {array_push($a,0);}
                                   else {array_push($a,1);}
                                   }
                                 }//have log
                               else {array_push($a,0);}//no log

                             //between
                              for ($i=1; $i < $diff ; $i++) {
                               $tomorrow = date('Y-m-d',strtotime(str_replace('-', '/', $date1) . "+".$i." days"));
                               $log_day = DB::table('tb_log_driver')
                                         ->where('drive_id','=',$drive_id)
                                         ->where('dep_drive','=',$dep_car)
                                         ->where('log_date','=',$tomorrow)
                                         ->get();
                               if (count($log_day)>0) {
                                 for ($j=0; $j <= 23; $j++) {
                                   $log_hour = DB::table('tb_log_driver')
                                             ->where('drive_id','=',$drive_id)
                                             ->where('dep_drive','=',$dep_car)
                                             ->where('log_date','=',$tomorrow)
                                             ->where('log_'.(int)$j,'=',null)
                                             ->get();

                                   if (count($log_hour)>0) {array_push($a,0);}
                                   else {array_push($a,1);}
                                  }//for hour
                                 }//have log
                               else {array_push($a,0);}//no log
                              }//for diffdate

                            //last
                              $log_day = DB::table('tb_log_driver')
                                        ->where('drive_id','=',$drive_id)
                                        ->where('dep_drive','=',$dep_car)
                                        ->where('log_date','=',$date2)
                                        ->get();
                              if (count($log_day)>0) {
                                for ($i=0; $i <= $Eh; $i++) {
                                     $log_hour = DB::table('tb_log_driver')
                                               ->where('drive_id','=',$drive_id)
                                               ->where('dep_drive','=',$dep_car)
                                               ->where('log_date','=',$date2)
                                               ->where('log_'.(int)$i,'=',null)
                                               ->get();

                                     if (count($log_hour)>0) {array_push($a,0);}
                                     else {array_push($a,1);}
                                  }
                               }//have log
                              else {array_push($a,0);}//no log

                            }//all day
                              else {
                                echo "some";
                              //first
                                $log_day =  DB::table('tb_log_driver')
                                          ->where('drive_id','=',$drive_id)
                                          ->where('dep_drive','=',$dep_car)
                                          ->where('log_date','=',$date1)
                                          ->get();

                                if (count($log_day)>0) {
                                  for ($i=$Sh; $i <= $Eh; $i++) {
                                    $log_hour = DB::table('tb_log_driver')
                                              ->where('drive_id','=',$drive_id)
                                              ->where('dep_drive','=',$dep_car)
                                              ->where('log_date','=',$date1)
                                              ->where('log_'.(int)$i,'=',null)
                                              ->get();

                                    if (count($log_hour)>0) {array_push($a,0);}
                                    else {array_push($a,1);}
                                    }
                                  }//have log
                                else {array_push($a,0);}//no log

                              //between
                               for ($i=1; $i <= $diff ; $i++) {
                                $tomorrow = date('Y-m-d',strtotime(str_replace('-', '/', $date1) . "+".$i." days"));
                                $log_day =   DB::table('tb_log_driver')
                                          ->where('drive_id','=',$drive_id)
                                          ->where('dep_drive','=',$dep_car)
                                          ->where('log_date','=',$tomorrow)
                                          ->get();;
                                if (count($log_day)>0) {
                                  for ($j=$Sh; $j <= $Eh; $j++) {
                                    $log_hour = DB::table('tb_log_driver')
                                              ->where('drive_id','=',$drive_id)
                                              ->where('dep_drive','=',$dep_car)
                                              ->where('log_date','=',$tomorrow)
                                              ->where('log_'.(int)$j,'=',null)
                                              ->get();

                                    if (count($log_hour)>0) {array_push($a,0);}
                                    else {array_push($a,1);}
                                    }
                                  }//have log
                                else {array_push($a,0);}//no log
                               }//for diffdate

                             }//someday

                           }//หลายวัน


                           for ($i=0; $i < count($a); $i++) {
                             if ($a[$i]==1) {
                              $chk = 1;break;
                             }
                             else {
                               $chk = 0;
                             }
                           }

                        if ($chk == 0) {
                          ?>
                          <tr>
                           <td width="10%" align="center" style="border-right:solid 0px;">
                             <img class="rounded-circle" width="100%"  src="{{$url_img}}"></td>
                           <td style="border-left:solid 0px;">
                             <?php echo $driver->drive_fname." ".$driver->drive_lname; ?>
                           </td>
                           <td width="10%">
                             <button class="btn btn-sm btn-primary setDrive" data-id="{{$driver->drive_id}}">
                               <span class="fa fa-sign-in"></span> เลือกคนขับ
                             </button>
                           </td>
                         </tr>
                     <?php
                   }
                     else {
                       ?>
                       <tr>
                      <td width="10%" align="center" style="border-right:solid 0px;">
                        <img class="rounded-circle" width="100%"  src="{{$url_img}}"></td>
                      <td style="border-left:solid 0px;">
                        <?php echo $driver->drive_fname." ".$driver->drive_lname; ?>
                      </td>
                      <td width="10%">
                        <button class="btn btn-sm" disabled>
                          <span class="fa fa-minus-circle"></span> ไม่ว่าง
                        </button>
                      </td>
                    </tr>
                       <?php
                     }

                      endforeach; ?>
                      </table>

                    </div>
                    <!-- step 2 -->
                    <!-- step 3 -->
                    <div class="col-md-12" id="step3" style="display:none;">
                      <br>
                      <button type="button" class="btn btn-sm btn-primary" id="btnprve2"><small><span class="fa fa-angle-left">  PREVIOS</span></small></button>
                      <br><br>
                      <input type="hidden" id="bk_id" value="<?php echo $bk_id; ?>">
                      <div id="Setfrm3"></div>
                    </div>
                    <!-- step 3 -->
                  </div>

                  <button class="btn btn-sm btn-danger cantCar" data-id="data={{$bk_id}}">
                    <span class="fa fa-car"></span> ไม่มีรถว่าง
                  </button>

                </div>
              </div>
            </div>
            <!-- cDetail -->
      </div>

    </div>
  </div>
</div>
<script>
var car , drive , bk_start , bk_end ;
var dataid = $("#bk_id").val();
$(".setCar").click(function(){
  car = $(this).data("id");
  showandhide(2,1,3);
  $("#StepProgress").css({"transition":"1s","width":"50%"});
});
$(".setDrive").click(function(){
  drive = $(this).data("id");
  showandhide(3,1,2);
  $("#StepProgress").css({"transition":"1s","width":"100%"});
  $("#StepProgress").addClass("bg-success");
  $('.cantCar').hide();
  $.ajax({
    url:"/setdrive",type:"GET",data:"car="+car+"&drive="+drive+"&dataid="+dataid,success:function(data){
         $("#Setfrm3").html(data);
    }
  });
});

$('#carType').change(function () {
  var type = $(this).val();
  var id = $('#ctype_id').val();
  if (type == id) {
      $('.alert').hide();
  }
  else {
      $('.alert').show();
  }
})
$(".cantCar").click(function(){
  var sData = $(this).data("id");
  var emp_id = $("#tblBk").data("id");
  // console.log(sData);
  swal({
    title:"คุณแน่ใจ ?",
    text:"คุณต้องการยกเลิกการจัดรถใช่หรือไม่ ",
    type:"warning",
    showCancelButton:true,
    confirmButtonColor:"#2ECC71",
    confirmButtonText:"ตกลง",
    cancelButtonText:"ยกเลิก",
    cancelButtonColor:"#E74C3C",
    closeOnConfirm:false,
  },function(isConfirm){
    if(isConfirm){
        swal({
          title: "ยกเลิก!",
          text: "กรุณากรอกเหตุผลที่ยกเลิก :",
          type: "input",
          showCancelButton: true,
          closeOnConfirm: false,
          inputPlaceholder: "เหตุผล"
        }, function (inputValue) {
          if (inputValue === false) return false;
          if (inputValue === "") {
            swal.showInputError("กรุณากรอกเหตุผลที่ยกเลิก");
            return false
          }
          $.ajax({url:"/approvestatus",data:sData+"&"+emp_id+"&type=nonecar&reasons="+inputValue,type:"POST",success:function(data){
              var obj = JSON.parse(data);
                if (obj['success']==true) {
                  swal({
                    type:"success",
                    title:"ยกเลิกการจัดรถสำเร็จ",
                    text:"คุณทำการยกเลิกการจัดรถสำเร็จ",
                    confirmButtonText:"ตกลง",
                    confirmButtonColor:"#2ECC71",
                    closeOnConfirm:true,
                  },function(isConfirm){
                    $.ajax({url:"/sendnonecar",data:sData,
                    type:"POST",success:function(data){
                      var obj = JSON.parse(data);
                        if (obj['success']==true) {
                          window.location = "/bookingcar";
                          }
                        }
                      })
                  });
              }
            }});
        });//Confirm
    }
  });
});

$("#btnprve1").click(function(){
  showandhide(1,2,3);
  $("#StepProgress").css({"transition":"1s","width":"0%"});
});
$("#btnprve2").click(function(){
  showandhide(2,1,3);
  $("#StepProgress").css({"transition":"1s","width":"50%"});
});
function showandhide(pshow,phide1,phide2){
    $("#step"+phide1).hide("slow");
    $("#step"+phide2).hide("slow");
    $("#step"+pshow).show("slow");
    $(".tap"+pshow).addClass("active");
    $(".tap"+phide1).removeClass("active");
    $(".tap"+phide2).removeClass("active");
    $("#btap"+pshow).removeClass("badge-default");
    $("#btap"+pshow).addClass("badge-primary");
    $("#btap"+phide1).removeClass("badge-primary");
    $("#btap"+phide1).addClass("badge-default");
    $("#btap"+phide2).removeClass("badge-primary");
    $("#btap"+phide2).addClass("badge-default");
    $("#StepProgress").removeClass("bg-success");
  }

</script>
