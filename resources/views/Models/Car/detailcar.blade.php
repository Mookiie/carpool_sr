<link rel="stylesheet" href="{{ asset('js/fullcalendar/fullcalendar.min.css') }}">
<script src="{{ asset('js/fullcalendar/fullcalendar.min.js') }}"></script>


<?php

          $sqlcar = DB::table('tb_car')
          // ->join('tb_department', 'tb_car.dep_id', '=' , 'tb_department.dep_id' )
          ->join('tb_brand', 'tb_car.brand_id', '=' , 'tb_brand.brand_id')
          ->join('tb_color', 'tb_car.color_id', '=' , 'tb_color.color_id')
          ->join('tb_car_type', 'tb_car.ctype_id', '=' , 'tb_car_type.ctype_id')
          ->where('car_id', '=', $car_id)->get();
          foreach ($sqlcar as $car):
           $imgf = Storage::url('image/car/'.$car->car_img_front);
           $imgr = Storage::url('image/car/'.$car->car_img_right);
           $imgl = Storage::url('image/car/'.$car->car_img_left);
           $imgb = Storage::url('image/car/'.$car->car_img_back);
           $tname = $car->ctype_name;
           $carnumber = $car->car_number;
           $carmodel = $car->car_model;
           $bname = $car->brand_name;
           $cname = $car->color_name;
           $machineid = $car->machine_id;
           $bodyid = $car->body_id;
           // $depname = $car->dep_name;
           $buydate=$car->buy_date;
           $indate = $car->in_date;
           $rstartdate = $car->rstart_date;
           $renddate = $car->rend_date;
           $actstart = $car->act_start;
           $actend = $car->act_end;
           $insurecop = $car->insure_cop;
           $insurestart = $car->insure_start;
           $insureend = $car->insure_end;
           $note = $car->car_note;
          endforeach;
          ?>
          <div class="card col-md-12 row">
          <div class="row">
              <div class="col-md-12">
              <div class="card">
                <div class="card-header">
                  <button class="close" id="detailClose">&times;</button>
                  <b>รายระเอียดรถยนต์ </b>
                  <input type="hidden" id="carid" value="data=<?php echo $car_id; ?>">
                </div>
                {{-- <ul class="nav nav-pills  bkItem" >
                  <li class="nav-item">
                  <a class="nav-link active" id="viewTbl">
                    <span class="fa fa-list"></span>
                  </a>
                  </li>
                   <li class="nav-item">
                  <a class="nav-link " id="viewCalendar">
                    <span class="fa fa-calendar-o"></span>
                  </a>
                  </li>

                </ul> --}}
                <div class="card-block" id="detailCar">
                  <div class="row">
                    <div class="col-md-4">
                      <ul class="nav flex-column car_detail_list">
                        <li class="nav-link">
                          <a class="image"><img class="img-thumbnail" width="100%" src="{{$imgf}}"></a>
                        </li>
                        <li class="nav-link">
                          <a class="image"><img class="img-thumbnail"  width="100%" src="{{$imgr}}"></a>
                        </li>
                        <li class="nav-link">
                          <a class="image"><img class="img-thumbnail"  width="100%" src="{{$imgl}}"></a>
                        </li>
                        <li class="nav-link">
                          <a class="image"><img class="img-thumbnail" width="100%" src="{{$imgb}}"></a>
                        </li>
                    </div>
                    <div class="col-md-8">
                      <table class="table table-bordered">
                        <tbody>
                          <tr>
                            <td width="25%"><b>ประเภทรถ</b></td>
                            <td><?php echo $tname; ?></td>
                          </tr>
                          <tr>
                            <td width="25%"><b>ทะเบียน</b></td>
                            <td><?php echo $carnumber; ?></td>
                          </tr>
                          <tr>
                            <td width="25%"><b>รุ่น </b></td>
                            <td><?php echo $carmodel; ?></td>
                          </tr>
                          <tr>
                            <td width="25%"><b>ยี่ห้อ </b></td>
                            <td><?php echo $bname; ?></td>
                          </tr>
                          <tr>
                            <td width="25%"><b>สี </b></td>
                            <td><?php echo $cname; ?></td>
                          </tr>
                          <tr>
                            <td width="25%"><b>หมายเลขเครื่อง </b></td>
                            <td><?php echo $machineid; ?></td>
                          </tr>
                          <tr>
                            <td width="25%"><b>หมายเลขตัวถัง </b></td>
                            <td><?php echo $bodyid; ?></td>
                          </tr>
                          {{-- <tr>
                            <td width="25%"><b>แผนก  </b></td>
                            <td> {{$depname}}</td>
                          </tr> --}}
                          <tr>
                            <td width="25%"><b>ซื้อมาวันที่  </b></td>
                            <td><?php echo $buydate; ?></td>
                          </tr><tr>
                            <td width="25%"><b>วันที่รับเข้า  </b></td>
                            <td><?php echo $indate; ?></td>
                          </tr>
                          <tr>
                            <td width="25%"><b>วันที่จดทะเบียน  </b></td>
                            <td><?php echo $rstartdate; ?></td>
                          </tr>
                          <tr>
                            <td width="25%"><b>ทะเบียนหมดอายุ  </b></td>
                            <td><?php echo $renddate; ?></td>
                          </tr>
                          <tr>
                            <td width="25%"><b> วันที่ทำ พ.ร.บ. </b></td>
                            <td><?php echo $actstart; ?></td>
                          </tr>
                          <tr>
                            <td width="25%"><b> พ.ร.บ. หมดอายุ </b></td>
                            <td><?php echo $actend; ?></td>
                          </tr>
                          <tr>
                            <td width="25%"><b> บริษัทประกัน </b></td>
                            <td><?php echo $insurecop; ?></td>
                          </tr>
                          <tr>
                            <td width="25%"><b> วันที่ทำประกัน </b></td>
                            <td><?php echo $insurestart; ?></td>
                          </tr>
                          <tr>
                            <td width="25%"><b> ประกันหมดอายุ </b></td>
                            <td><?php echo $insureend; ?></td>
                          </tr>
                          <tr>
                            <td width="25%"><b> หมายเหตุ </b></td>
                            <td><?php echo $note; ?></td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
                <div class="card-block" id="calendarCar" style="display:none;">

                </div>


              </div>
              </div>
          </div>

          </div>

          <script>
          //  console.log($("#carid").val());
          $("#viewTbl").click(function(){
            $("#detailCar").show();
            $(this).addClass("active");
            $("#viewCalendar").removeClass("active");
            $("#calendarCar").hide("slow");
          });



          $("#viewCalendar").click(function(){
            $("#calendarCar").show();
            $(this).addClass("active");
            $("#viewTbl").removeClass("active");
            $("#detailCar").hide("slow");
            $('#calendarCar').fullCalendar("today");
          });

          $(function(){
            $('#calendarCar').fullCalendar({
                header: {
                    left: 'prev,next today',  //  prevYear nextYea
                    center: 'title',
                    right: '',
                },
                buttonIcons:{
                    prev: 'left-single-arrow',
                    next: 'right-single-arrow',
                    prevYear: 'left-double-arrow',
                    nextYear: 'right-double-arrow'
                },
                events: {
                  url:"/allcarEvent?data="+$("#carid").val(),
                  error:function(data){
                    // console.log(data.responseText);
                  }
                },
                eventLimit:true,
                lang: 'th',
                eventClick: function(calEvent, jsEvent, view) {
                  $.ajax({
                    url:"/detail",
                    data:"bk="+calEvent.id,
                    type:"GET",success:function(data){
                        $(".modal-area").html(data);
                        $("#modalBk").modal("show");
                    }
                  });
                }

            });
          });
          function callDeatil(detail){
            $.ajax({
              url:"/detail",
              data:"bk="+calEvent.id,
              type:"GET",success:function(data){
                  $(".modal-area").html(data);
                  $("#modalBk").modal("show");
              }
            });
          }
          </script>
