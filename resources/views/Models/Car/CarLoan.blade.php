@extends('welcome')
@section('content')
@include('dashboard.SideNav')
<div class="container" style="margin-top: 0%;">

 <div class="card-block">

     <form id="frm_ChangeCar">

       <?php
       // $sql = DB::table("tb_department")->where("com_id",'=',$com_id)->where("dep_id",'<>',$dep_id)->get();
       $sql = DB::table("tb_job")->where("com_id",'=',$com_id)->where("dep_id",'<>',$dep_id)->select('job_id','job_name')->groupBy('job_id','job_name')->get();
        ?>
           <div class="card offset-sm-2 col-md-8">
             <div class="card-block">
               <div class="modal-header" style="line-height: 1.6;">
                   <h6 class="modal-title"><span class="fa fa-edit text-black">  ยืมรถยนต์ :</span>
               </div>
               <br />
               <input type="hidden"  id="car_id" name="car_id" value="{{$car_id}}">
               <input type="hidden"  id="com_id" name="com_id" value="{{$com_id}}">

               <div class="form-group row">
                 <label for="emp_sex" class="col-md-3 col-form-label">แผนกที่ให้ยืม </label>
                 <div class="col-8">
                 <select class="form-control mr-sm-2"  id="job_id" name="job_id">
                   <option value="">กรุณาเลือกแผนก</option>
                   @foreach ($sql as $dep)
                       {{-- <option value="{{$dep->dep_id}}">{{$dep->dep_name}}</option> --}}
                       <option value="{{$dep->job_id}}">{{$dep->job_name}}</option>
                   @endforeach

                 </select>
               </div>
               </div>

                <div class="col-12" align="center">
                  <button type="button" class="btn btn-success" id="btn_save">บันทึก</button>
                  <button type="reset" class="btn btn-danger" id="btn_prev">ยกเลิก</button>
                </div>
             </div>
            </div>
     </form>
   </div>

</div>
<script type="text/javascript">
  $("#btn_prev").click(function () {
    location.reload();
  })
  $("#btn_save").click(function () {
    var form_data = $("#frm_ChangeCar").serialize();
    $.ajax({
      url:"/loancar",
      data:form_data,
      type:"POST",
      success:function(data){
        var obj = JSON.parse(data);
          if (obj['success']==true) {
            swal({
              type:"success",
              title:"สำเร็จ",
              // text:"คุณทำการบันทึกสำเร็จ",
              confirmButtonText:"ตกลง",
              confirmButtonColor:"#2ECC71",
              closeOnConfirm:true,
            },function(isConfirm){
              window.location = "/car";
            });
          }
      }
    })
  })


</script>
@endsection
