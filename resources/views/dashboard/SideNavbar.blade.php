@section('side-nav')
@foreach ($users as $r)
  <?php
        $fullname = $r->emp_fname." ".$r->emp_lname;
        $lv_user = $r->emp_level;
        $img = $r->emp_img;
        $com_id = $r->com_id;
        $id = $r->emp_id;
        $dep_id=$r->dep_id;
        $job_id=$r->job_id;
   ?>
@endforeach

<nav class="nav flex-column sideNav" id="sideNav">
      {{-- <ul class="nav" style="height:15%;">
        <li class="nav-item">
        </li>
      </ul> --}}
        <li class="nav-link"><a href="/dashboard"><span class="fa fa-bar-chart" ></span> &nbsp;แผงควบคุม<?php //echo $lang["menu_dashboard"]; ?></a></li>
        <?php if($lv_user == 999){ ?>
          <li class="nav-link dropdown-toggle" id="Emp_link" data-parent ="#sideNav" data-toggle="collapse" data-target="#EmpCollapse">
            <a href="#"><span class="fa fa-users" ></span> &nbsp; ข้อมูลพนักงาน<?php //echo $lang["menu_booking"]; ?>
            {{-- <span class="caret_right fa fa-caret-down" ></span> --}}
            </a>
          </li>
          <div class="collapse collapseUl" id="EmpCollapse">
          <ul class="nav flex-column bg-inverse">
            <li><a href="/empcreate">&nbsp;&nbsp;&nbsp;&nbsp;<span class="fa fa-plus" ></span> &nbsp; เพิ่มข้อมูลพนักงาน <?php //echo $lang["menu_add_booking"]; ?></a></li>
            <!-- <li><a href="/emp"><span class="fa fa-list" ></span> &nbsp; รายการพนักงาน<?php //echo $lang["menu_list_booking"]; ?></a></li> -->
          </ul>
        </div>
        <?php } ?>
        <?php if($lv_user == 99||$lv_user == 999){
            //if(($lv_user == 99||$lv_user == 999)&&$com_id <> 'C0001'){
        ?>
          <li class="nav-link dropdown-toggle" id="Emp_invite" data-parent ="#sideNav" data-toggle="collapse" data-target="#InviteCollapse">
            <a href="#"><span class="fa fa-users" ></span> &nbsp; เชิญพนักงาน<?php //echo $lang["menu_booking"]; ?>
            {{-- <span class="caret_right fa fa-caret-down" ></span> --}}
            </a>
          </li>
          <div class="collapse collapseUl" id="InviteCollapse">
          <ul class="nav flex-column bg-inverse">
            <li><a href="/invite">&nbsp;&nbsp;&nbsp;&nbsp;<span class="fa fa-plus" ></span> &nbsp; เชิญพนักงาน <?php //echo $lang["menu_add_booking"]; ?></a></li>
            {{-- <li><a href="/emp">&nbsp;&nbsp;&nbsp;&nbsp;<span class="fa fa-list" ></span> &nbsp; รายการพนักงาน</a></li> --}}
          </ul>
        </div>
        <?php } ?>
        <?php if(($lv_user == 99||$lv_user == 999)&&$com_id == 'C0001'){ ?>
          <li class="nav-link dropdown-toggle" id="Emp_invite" data-parent ="#sideNav" data-toggle="collapse" data-target="#PermissionCollapse">
            <a href="#"><span class="fa fa-users" ></span> &nbsp; ข้อมูลพนักงาน<?php //echo $lang["menu_booking"]; ?>
            {{-- <span class="caret_right fa fa-caret-down" ></span> --}}
            </a>
          </li>
          <div class="collapse collapseUl" id="PermissionCollapse">
          <ul class="nav flex-column bg-inverse">
            {{-- <li><a href="/emp">&nbsp;&nbsp;&nbsp;&nbsp;<span class="fa fa-plus" ></span> &nbsp; รายการพนักงาน </a></li> --}}
            <li><a href="/SetPermission">&nbsp;&nbsp;&nbsp;&nbsp;<span class="fa fa-list" ></span> &nbsp; กำหนดสิทธิ์การใช้งาน<?php //echo $lang["menu_list_booking"]; ?></a></li>
            <li><a href="/SetApprove">&nbsp;&nbsp;&nbsp;&nbsp;<span class="fa fa-list" ></span> &nbsp; กำหนดสิทธิ์การอนุมัติ<?php //echo $lang["menu_list_booking"]; ?></a></li>
            <li><a href="/SetOrganize">&nbsp;&nbsp;&nbsp;&nbsp;<span class="fa fa-list" ></span> &nbsp; สิทธิ์การจัดรถ<?php //echo $lang["menu_list_booking"]; ?></a></li>

          </ul>
        </div>
        <?php } ?>
        <?php if($lv_user == 99||$lv_user == 999){ ?>
        <li class="nav-link dropdown-toggle" id="Dep_link" data-parent ="#sideNav" data-toggle="collapse" data-target="#DepCollapse">
          <a href="#"><span class="fa fa-university" ></span> &nbsp; ข้อมูลอื่นๆ<?php //echo $lang["menu_booking"]; ?>
          <!-- <span class="caret_right fa fa-caret-down"></span> -->
          </a>
        </li>
        <div class="collapse collapseUl" id="DepCollapse">
        <ul class="nav flex-column bg-inverse">
          <li><a href="/otheradd" >&nbsp;&nbsp;&nbsp;&nbsp;<span class="fa fa-plus" ></span> &nbsp; เพิ่มข้อมูลอื่นๆ <?php //echo $lang["menu_add_booking"]; ?></a></li>
          <li><a href="/otheredit" >&nbsp;&nbsp;&nbsp;&nbsp;<span class="fa fa-edit" ></span> &nbsp; แก้ไขข้อมูล<?php //echo $lang["menu_list_booking"]; ?></a></li>
        </ul>
      </div>
      <?php } ?>
        <li class="nav-link dropdown-toggle" id="booking_link" data-parent ="#sideNav" data-toggle="collapse" data-target="#bookingCollapse">
          <a href="#"><span class="fa fa-book" ></span> &nbsp; การจอง<?php //echo $lang["menu_booking"]; ?>
          <!-- <span class="caret_right fa fa-caret-down"></span> -->
          </a>
        </li>
        <div class="collapse collapseUl" id="bookingCollapse">
        <ul class="nav flex-column bg-inverse">
          <li><a href="/add">&nbsp;&nbsp;&nbsp;&nbsp;<span class="fa fa-user-plus" ></span> &nbsp; เพิ่มการจอง <?php //echo $lang["menu_add_booking"]; ?></a></li>
          <li><a href="/booking">&nbsp;&nbsp;&nbsp;&nbsp;<span class="fa fa-list" ></span> &nbsp; รายการจอง<?php //echo $lang["menu_list_booking"]; ?></a></li>
        </ul>
      </div>
      <?php if($lv_user == 1||$lv_user == 11||$lv_user == 2 || $lv_user == 999){ ?>
       <!-- <li class="nav-link">
       <a href="page.php?r=booking/wait"><span class="fa fa-edit">
       </span> &nbsp;<?php //echo $lang["menu_appv_booking"]; ?> -->
         <?php
         $wait = DB::table('tb_booking')->where('bk_status' ,'=', 'wait')->where('com_id','=',$com_id)->where('dep_id','=',$dep_id)->count();
         ?>
       <!-- </a>
       </li> -->
       <li class="nav-link dropdown-toggle" data-parent ="#sideNav" data-toggle="collapse" data-target="#approve">
         <a href="#"><span class="fa fa-check" ></span> &nbsp; อนุมัติ<?php //echo $lang["menu_appv"]; ?>
           <?php if($wait > 0){
           ?>
           <span class="caret_right_badge badge badge-danger">&nbsp;{{"NEW"}}&nbsp;</span>
           <?php } ?>
         </a>
       </li>
       <div class="collapse collapseUl" id="approve">
       <ul class="nav flex-column bg-inverse">
         <li>
           <a href="/waittoapprove">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;รออนุมัติ<?php //echo $lang["menu_appv_booking"]; ?>
             <?php if($wait > 0){
             ?>
             <span class="caret_right_badge badge badge-danger">&nbsp;{{ $wait }}&nbsp;</span>
             <?php } ?>
           </a>

         </li>
         {{-- <li><a href="/approved">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;อนุมัติแล้ว </a></li> --}}
       </ul>
      </div>
      <?php }?>
      <?php if($lv_user == 2||$lv_user == 999){ ?>
       <li class="nav-link"><a href="/bookingcar">
         <span class="fa fa-edit" ></span> &nbsp; รอจัดรถ<?php //echo $lang["menu_wait_booking"]; ?>
         <?php
         $approve = DB::table('tb_booking')->where('bk_status' ,'=', 'approve')->where('com_id','=',$com_id)->where('dep_car','=',$job_id)->whereNull('bk_ot')->count();
         if($approve > 0){
         ?>
         <span class="caret_right_badge badge badge-danger"> &nbsp;{{ $approve }}&nbsp;</span>
         <?php } ?>
       </a>
       </li>

       <li class="nav-link"><a href="/bookingOT">
         <span class="fa fa-clock-o" ></span> &nbsp; รอจัดรถ(OT)<?php //echo $lang["menu_wait_booking"]; ?>
         <?php
         $approve_ot = DB::table('tb_booking')->where('bk_status' ,'=', 'approve')->where('com_id','=',$com_id)->where('dep_car','=',$job_id)->whereNotNull('bk_ot')->count();
         if($approve_ot > 0){
         ?>
         <span class="caret_right_badge badge badge-danger">{{ $approve_ot }}&nbsp;</span>
         <?php } ?>
       </a>
       </li>

       <li class="nav-link"><a href="/bookingnonecar"><span class="fa fa-refresh" ></span> &nbsp; รายการไม่มีรถ</a></li>
       <li class="nav-link"><a href="/bus"><span class="fa fa-bus" ></span> &nbsp; รถรับ-ส่งพนักงาน</a></li>
       {{-- <li class="nav-link"><a href="#"><span class="fa fa-retweet" ></span> &nbsp; ขอยืมรถยนต์</a></li> --}}
       <?php if ($lv_user == 999) { ?>
       <li class="nav-link"><a href="/mile"><span class="fa fa-exchange" ></span> &nbsp; บันทึกเลขไมล์</a></li>
       <?php } } ?>
       <?php if($lv_user < 2){ ?>
       <li class="nav-link"><a href="/car"><span class="fa fa-car" ></span> &nbsp; ข้อมูลรถ<?php //echo $lang["menu_car"]; ?></a></li>
       <?php }else{ ?>
       <li class="nav-link dropdown-toggle" id="car_Link" data-parent ="#sideNav" data-toggle="collapse" data-target="#carCollapse">
         <a href="#"><span class="fa fa-car" ></span> &nbsp; รถยนต์<?php //echo $lang["menu_cars"]; ?>
         <!-- <span class="caret_right fa fa-caret-down"></span> -->
         </a>
       </li>
       <div class="collapse collapseUl" id="carCollapse">
       <ul class="nav flex-column bg-inverse">
         <li><a href="/car">&nbsp;&nbsp;&nbsp;&nbsp;<span class="fa fa-id-card" ></span> &nbsp; ข้อมูลรถ<?php //echo $lang["menu_car"]; ?></a></li>
         {{-- <li><a href="page.php?r=car">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;การต่อทะเบียน</a></li>
         <li><a href="page.php?r=car">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;การต่อ พ.ร.บ.</a></li>
         <li><a href="page.php?r=car">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;การต่อประกัน</a></li> --}}
       </ul>
      </div>
      <?php } ?>

       <li class="nav-link"><a href="/driver"><span class="fa fa-address-book" ></span> &nbsp; ข้อมูลคนขับรถ<?php //echo $lang["menu_driver"]; ?></a></li>

       <li class="nav-link"><a href="/calendardep"><span class="fa fa-calendar-o" ></span> &nbsp; ปฏิทินการใช้รถแผนก</a></li>

       <li class="nav-link"><a href="/calendar"><span class="fa fa-calendar-o" ></span> &nbsp; ปฏิทินการใช้รถส่วนกลาง</a></li>
       {{-- <li class="nav-link dropdown-toggle" id="Report_all" data-parent ="#sideNav" data-toggle="collapse" data-target="#ReportCollapse">
         <a href="#"><span class="fa fa-table" ></span> &nbsp; ข้อมูลการใช้งาน --}}
         {{-- <span class="caret_right fa fa-caret-down" ></span> --}}
         {{-- </a>
       </li> --}}
       {{-- <div class="collapse collapseUl" id="ReportCollapse">
       <ul class="nav flex-column bg-inverse">
         <li><a href="#">&nbsp;&nbsp;&nbsp;&nbsp;<span class="fa fa-car" ></span> &nbsp; รถยนต์ </a></li>
         <li><a href="#">&nbsp;&nbsp;&nbsp;&nbsp;<span class="fa fa-user" ></span> &nbsp; คนขับรถ</a></li>
       </ul>
     </div> --}}
       <?php
       if($lv_user == 1 || $lv_user == 11){ ?>
         <li class="nav-link dropdown-toggle" id="RBooking_Link" data-parent ="#sideNav" data-toggle="collapse" data-target="#ReportBookingCollapse">
           <a href="#"><span class="fa fa-file-text" ></span> &nbsp; รายงาน<?php //echo $lang["menu_cars"]; ?>
           <!-- <span class="caret_right fa fa-caret-down"></span> -->
           </a>
         </li>
         <div class="collapse collapseUl" id="ReportBookingCollapse">
         <ul class="nav flex-column bg-inverse">
           <li><a href="/reportbooking">&nbsp;&nbsp;&nbsp;&nbsp;<span class="fa fa-file-text" ></span> &nbsp; การจอง</a></li>
           {{-- <li><a href="page.php?r=car">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;การต่อทะเบียน</a></li>
           <li><a href="page.php?r=car">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;การต่อ พ.ร.บ.</a></li>
           <li><a href="page.php?r=car">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;การต่อประกัน</a></li> --}}
         </ul>
        </div>       <?php }
       elseif ($lv_user == 3) {
        ?>
        <li class="nav-link dropdown-toggle" id="RBooking_Link" data-parent ="#sideNav" data-toggle="collapse" data-target="#ReportCarCollapse">
          <a href="#"><span class="fa fa-file-o" ></span> &nbsp; รายงาน<?php //echo $lang["menu_cars"]; ?>
          <!-- <span class="caret_right fa fa-caret-down"></span> -->
          </a>
        </li>
       <div class="collapse collapseUl" id="ReportCarCollapse">
       <ul class="nav flex-column bg-inverse">
         {{-- <li><a href="/reportcar">&nbsp;&nbsp;&nbsp;&nbsp;<span class="fa fa-file-text" ></span> &nbsp; การใช้รถยนต์</a></li> --}}
         {{-- <li><a href="page.php?r=car">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;การต่อทะเบียน</a></li>
         <li><a href="page.php?r=car">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;การต่อ พ.ร.บ.</a></li>
         <li><a href="page.php?r=car">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;การต่อประกัน</a></li> --}}
       </ul>
      </div>
       <?php }elseif ( $lv_user == 99 ||$lv_user == 999) {
         ?>
         <li class="nav-link dropdown-toggle" id="RCar_Link" data-parent ="#sideNav" data-toggle="collapse" data-target="#ReportCarCollapse">
           <a href="#"><span class="fa fa-file-o" ></span> &nbsp; รายงาน<?php //echo $lang["menu_cars"]; ?>
           <!-- <span class="caret_right fa fa-caret-down"></span> -->
           </a>
         </li>
        <div class="collapse collapseUl" id="ReportCarCollapse">
        <ul class="nav flex-column bg-inverse">
          <li><a href="/reportbooking">&nbsp;&nbsp;&nbsp;&nbsp;<span class="fa fa-file-text" ></span> &nbsp; การจอง</a></li>
          {{-- <li><a href="/reportcar">&nbsp;&nbsp;&nbsp;&nbsp;<span class="fa fa-file-text" ></span> &nbsp; การใช้รถยนต์</a></li> --}}
        </ul>
       </div>
         <?php
       } ?>
</nav>

@endsection
