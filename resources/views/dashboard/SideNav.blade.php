
<nav class="navbar navbar-toggleable-md fixed-top navbar-inverse topNavbar" >
    <button type="button" class="btn sideNav-btn-open" onclick="openNav();">
      <span class="fa fa-reorder" style="color:#fff;"></span>
    </button>

    <ul class="navbar-nav mr-auto mt-2">
      <a class="navbar-brand" >
        <img class"logo-icon" src="{{$url_img_logo = Storage::url('image/logo/logo_siamraj.png')}}" width="40"><span class="headertext">CarPool Service</span>
      </a>
    </ul>
</nav>
{{-- <nav class="nav flex-column sideNav" id="sideNav" style="margin-top:15px;">

</nav> --}}
