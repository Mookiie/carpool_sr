@section('top-nav')
  <!-- Top NAVBAR -->
  @foreach ($users as $r)
    <?php
          $emp_id = $r->emp_id;
          $fullname = $r->emp_fname." ".$r->emp_lname;
          $lv_user = $r->emp_level;
          $img = $r->emp_img;
          $id = $r->emp_id;
          $com_id = $r->com_id;
     ?>
  @endforeach

  <nav class="navbar navbar-toggleable-md fixed-top navbar-inverse topNavbar" >
      <button type="button" class="btn sideNav-btn-open" onclick="openNav();">
        <span class="fa fa-reorder" ></span>
      </button>

      <ul class="navbar-nav navbar-brand margin-auto">
        <a class="brand" href="{{url('/dashboard')}}" >
          <img class"logo-icon" src="{{$url_img_logo = Storage::url('image/logo/Logo.png')}}" width="40"><span class="headertext">CarPool Service </span>
        </a>
      </ul>
      <ul class="navbar-nav my-2 my-lg-0 Nmenu-right">
         {{-- <li class="nav-item btn-group menu-lang">
          <a class="nav-link dropdown-toggle"  id="langDropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <img class="rounded-circle" width="32" src="image/icon/th.png">
          </a> --}}
          {{-- <div class="dropdown-menu dropdown-menu-right" aria-labelledby="langDropdown">--}}
            <?php
            // $l_0 = "<a class='dropdown-item clang' data-lang = 'en'><img class='rounded-circle' width='24' src='image/icon/en.png'> English</a>";
            // $l_1 ="<a class='dropdown-item clang' data-lang = 'th'><img class='rounded-circle' width='24' src='image/icon/th.png'> Thai</a>";
            // if($_COOKIE["lang"] == "en"){
            //   echo $l_0.$l_1;
            // }else{
            //   echo $l_1.$l_0;
            // }
            ?>
          {{--</div> --}}
        {{-- </li> --}}
        <li class="nav-item btn-group menu-profile">
          {{-- <a class="nav-link dropdown-toggle" id="profileDropdown" data-toggle="dropdown" > --}}
          <a class="nav-link" id="profileDropdown" data-toggle="dropdown" >
            <img class="rounded-circle" width="32" src="image/profile/user.png<?php //echo $rowuser_session->emp_img; ?>">
          </a>
          <div class="dropdown-menu dropdown-menu-right" aria-labelledby="profileDropdown">
            <a class="dropdown-item">
              <div class="row">
                <div class="col-12" align="center">
                  {{-- <img class="rounded-circle" width="100%" src="image/profile/user.png"> --}}
                </div>
                <div class="col-12" id="FullName">
                    <p>
                      <b>
                          {{ $fullname }}
                      </b>
                </div>
              </div>
            </a>
            <?php if ($com_id <> 'C0001') { ?>
            <a class="dropdown-item"  href="/change"><span class="fa fa-unlock-alt" style="color:#000;" ></span> เปลี่ยนรหัสผ่าน <?php //echo $lang["menu_profile_cpass"]; ?></a>
            <?php } ?>
            <a href="/logout" class="dropdown-item lastmenu" id="logout" data-id="{{$emp_id}}"><span class="fa fa-sign-out" style="color:#000;" ></span> ออกจากระบบ <?php //echo $lang["menu_profile_logout"]; ?></a>
          </div>
        </li>
     </ul>



     <div class="pos-f-t Nmenu-right-size">
     <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#sizeMenu" aria-controls="sizeMenu" aria-expanded="false" aria-label="Toggle navigation">
           <span class="fa fa-ellipsis-v" ></span>
     </button>
     <div class="collapse" id="sizeMenu">
     <ul class="nav flex-column" align="right">

       <li class="nav-item">
         <a class="nav-link" href="/change"><span class="fa fa-unlock-alt"></span> เปลี่ยนรหัสผ่าน</a>
       </li>
       <!-- <li class="nav-item">
         <a class="nav-link dropdown-toggle" data-toggle="collapse" data-target="#collapse_lang" >
           <img class="rounded-circle" width="24" src="image/icon/<?php //echo $lang["lang"]; ?>.png"> <?php //echo $lang["menu_profile_langsetting"]; ?></a>
         <div class="collapse bg-inverse" id="collapse_lang">
           <ul class="nav flex-column" align="right">
             <li class="nav-item">
               <?php
              //  $l_0 = "<a class='nav-link clang' data-lang = 'en'><img class='rounded-circle' width='24' src='image/icon/en.png'> EN</a>";
              //  $l_1 ="<a class='nav-link clang' data-lang = 'th'><img class='rounded-circle' width='24' src='image/icon/th.png'> TH</a>";
              //  if($_COOKIE["lang"] == "en"){
              //    echo $l_0.$l_1;
              //  }else{
              //    echo $l_1.$l_0;
              //  }
               ?>
             </li>
           </ul>
         </div>
       </li> -->
       <li class="nav-item">
         <a href="/logout" class="nav-link "><span class="fa fa-sign-out"></span> ออกจากระบบ<?php //echo $lang["menu_profile_logout"]; ?></a>
       </li>
     </ul>
     </div>

       </div>




    </div>
  </nav>
  <!-- Top NAVBAR -->
  <div class="bg-overlay" onclick="closeNav();"></div>
<script type="text/javascript">
  // $('#logout').click(function () {
  //     var emp_id = $(this).data('id');
  //     console.log(emp_id);
  //     $.ajax({
  //         url:"/logout",
  //         data:"id="+emp_id,
  //         type:"POST",
  //         success:function(data){
  //         }
  //       })
  // })
</script>
@endsection
