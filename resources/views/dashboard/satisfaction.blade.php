@extends('welcome')
@section('content')

<link rel="stylesheet" type="text/css" href="{{ asset('js/datetimepicker/jquery.datetimepicker.css')}}"/>
<script src="{{asset('js/datetimepicker/jquery.js')}}"></script>
<script src="{{asset('js/datetimepicker/build/jquery.datetimepicker.full.min.js')}}"></script>
<meta name="viewport" content="initial-scale=1.0, user-scalable=no">
<?php
    $url=  $_SERVER['REQUEST_URI'];
    $pieces = explode("?", $url);
    $data = base64_decode($pieces[1]);

 ?>
<input type="hidden" name="bk_id" id="bk_id" value="{{$data}}">
<?php
$sql = DB::table('tb_booking')->where('bk_id','=',$data)->where('satisfaction_car','!=',null)->get();
if (count($sql)>0) {
  ?>
  <div class="container-fluid">
    <div class="block block-login">
      <img src="{{$url_img_logo = Storage::url('image/logo/logo.png')}}">
      <h4>Siamraj Carpool Service</h4>
       <small>คุณได้ทำแบบสอบถามความพึงพอใจแล้ว</small>
      <div class="block-login block-login-form">
        <div  align="center">
          <button type="button" class="btn btn-success" id="btn-close">ปิด</button>
        </div>
      <script>
      $("#btn-close").click(function(){
        window.close();
      });
      </script>
  <?php
  }
else {
  ?>
@include('dashboard.SideNav')
<style>
.container {
    margin-top: 15%
}

@media screen and (min-width: 768px) {
    .container {
        margin-top: 8%
    }
@media screen and (min-width: 1024px) {
      .container {
            margin-top: 6%
      }
}
</style>

<div class="container">

  <div class="offset-sm-2 col-md-8">
    <div>
            <div class="card-block">

                <form id="test">

                  <div class="card">
                    <div class="card-header">
                      ความสะอาดของรถยนต์
                    </div>
                    <div class="card-body" align="center">
                      <br />
                        <div id="img_car" align="center"></div>
                      <br />
                      <div class="btn-group-toggle " data-toggle="buttons">
                        <label class="btn btn-primary car">
                          <input type="radio" name="r_car" id="option1" autocomplete="off" value="1" >แย่มาก
                        </label>
                        <label class="btn btn-primary car">
                          <input type="radio" name="r_car" id="option2" autocomplete="off" value="2" > แย่
                        </label>
                        <label class="btn btn-primary car">
                          <input type="radio" name="r_car" id="option3" autocomplete="off" value="3" > พอใช้
                        </label>
                        <label class="btn btn-primary car">
                          <input type="radio" name="r_car" id="option4" autocomplete="off" value="4" > ดี
                        </label>
                        <label class="btn btn-primary car active">
                          <input type="radio" name="r_car" id="option5" autocomplete="off" value="5" checked> ดีมาก
                        </label>
                      </div>
                    </div>
                  </div>
                <br />
                <div class="card">
                  <div class="card-header">
                      คนขับรถ
                  </div>
                  <div class="card-body" align="center">
                    <br />
                    <div id="img_driver" align="center" ></div>
                    <br />
                    <div align="center">
                      <div class="btn-group-toggle " data-toggle="buttons" align="center"  >
                        <label class="btn btn-primary driver">
                          <input type="radio" name="r_driver" id="option1" autocomplete="off" value="1" > แย่มาก
                        </label>
                        <label class="btn btn-primary driver">
                          <input type="radio" name="r_driver" id="option2" autocomplete="off" value="2" > แย่
                        </label>
                        <label class="btn btn-primary driver">
                          <input type="radio" name="r_driver" id="option3" autocomplete="off" value="3" > พอใช้
                        </label>
                        <label class="btn btn-primary driver">
                          <input type="radio" name="r_driver" id="option4" autocomplete="off" value="4" > ดี
                        </label>
                        <label class="btn btn-primary driver active">
                          <input type="radio" name="r_driver" id="option5" autocomplete="off" value="5" checked> ดีมาก
                        </label>
                      </div>
                    </div>
                  </div>
                </div>
                <br />
                <div class="card">
                  <div class="card-header">
                      ข้อเสนอแนะ(Suggestion)
                  </div>
                  <div class="card-body" align="center">
                    <textarea id="Suggestion" name="Suggestion" rows="4" style="width:100%;height:100%"></textarea>
                  </div>
                </div>


                  <div class="form-group form-group-row" align="center">
                      <button type="button" class="btn btn-success" id="btn_save">Send</button>
                  </div>
                </form>

                </div>
              </div>
    </div>
</div>

<script>

$(document).ready(function () {
  img("car",'5')
  img("driver",'5')
})

$("#btn_save").click(function() {
  var data_car = $('input[name=r_car]:checked');
  var data_driver = $('input[name=r_driver]:checked');
  var text = $("#Suggestion").val();
  var bk_id = $("#bk_id").val();
  console.log(data_car[0].value);
  console.log(data_driver[0].value);
  console.log(text);
  console.log(bk_id);
  $.ajax({type:"POST",
           data:"car="+data_car[0].value+"&driver="+data_driver[0].value+"&Suggestion="+text+"&bk_id="+bk_id,
           url:"/satisfactionDB",
           success:function(data){
             swal({
               title: "Send Success",
               type: "success",
               timer: 3000,
               showConfirmButton: false
             },function () {
                window.location = "/";
             });

           }
  })

})
$(".car").click(function(){
  var data = $(this)[0].children[0].value;

  img("car",data)
});

$(".driver").click(function(){
  var data = $(this)[0].children[0].value;
  img("driver",data)
});

function img(type,data) {
    $("#img_"+type).html("<img width='80' src='image/icon/satisfaction/satisfaction"+data+".png'>" );
}


</script>
<?php } ?>
@endsection
