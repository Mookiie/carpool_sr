@extends('welcome')
@extends('dashboard.topNavbar')
@extends('dashboard.SideNavbar')

@section('content')
@foreach ($users as $r)
  <?php
        $emp_id=$r->emp_id;
        $lv_user = $r->emp_level;
        $com_id = $r->com_id;
   ?>
@endforeach

<style>
  div.card.inCard{
    margin-bottom: 15px;
    border:solid 1px #ccc;
    border-radius: 0px;
    padding: 0;
  }
  .inCard .card-warning ,.card-info{
    color:#fff;
  }
  .dash-headder{
    padding: 5px 25px;
    margin-top:15px;
    border-bottom: solid 0px #ccc;
  }
  .table-dash tr td{
    border:none;
    border-bottom: solid 1px #ccc;
  }
  .tdhead{
    padding: 10px;
    margin-right: -5px;
    font-size: 10px;
    text-align: right;
    color: #fff;
    font-weight: bold;
    border-radius: 10px 0px 0px 10px;
  }
</style>

<?php
        date_default_timezone_set("Asia/Bangkok");
        $date =date("Y-m-d H:i:s");
         // $DBBW=DB::table('tb_booking')->where('bk_status','=','wait')->get();
         // foreach ($DBBW as $bk) {
         //   $id = $bk->bk_id;
         //   $bkdate= $bk->bk_start_start;
         //   if($date>$bkdate){
         //     $status = "eject";
         //   }else{
         //     $status = " ";
         //   }
         //   if ($status == "eject") {
         //     $sqlUPDATE = DB::table('tb_booking')
         //                  ->where('bk_id', '=' ,$id)
         //                  ->update(['bk_status' => $status,
         //                            'approve_by' => 'SYS000000000',
         //                            'approve_date' => $bkdate,
         //                            'bk_reasons' => 'ไม่ได้รับการอนุมัติภายในกำหนด',
         //                          ]);
         //   }
         // }

         // $DBBA=DB::table('tb_booking')->where('bk_status','=','approve')->get();
         // foreach ($DBBA as $bk) {
         //    $id = $bk->bk_id;
         //   $bkdate= $bk->bk_start_start;
         //   if($date>$bkdate){
         //     $status = "nonecar";
         //   }else{
         //     $status = " ";
         //   }
         //   if ($status == "nonecar") {
         //     $sqlUPDATE = DB::table('tb_booking')
         //                  ->where('bk_id', '=' ,$id)
         //                  ->update(['bk_status' => $status,
         //                            'setcar_by' => 'SYS000000000',
         //                            'setcar_date' => $bkdate,
         //                            'bk_reasons' => 'ไม่ได้รับการจัดรถภายในกำหนด',
         //                          ]);
         //  }
         // }

 ?>

<?php
$strwait = "";
$strapp = "";
$buttom = "ไปยังรายการจอง";
$pathwait ="";
if($lv_user == "0"){
   $path = "booking/query";
   $pathwait = "/wait";
   $pathcar = "booking/query/approve";
}else if($lv_user == "1"){
  $strwait = " ";
  $path = "booking/wait";
  $pathcar = "booking/query/approve";
}else if($lv_user == "2"){
  $pathcar = "booking/car";
  $buttom = "ไปยังรายการจัดรถ";
}
?>
<div class="container-dashboard">

    <div class="col-md-12 row" >

        <div class="col-md-4">
          <div class="card mr-b-15">
            <div class="dash-headder">
              <span class="fa fa-tasks">&nbsp;&nbsp;<label>รายการที่รออยู่ของฉัน</label></span>
            </div>
              <div class="card-block">
          <?php  // if($lv_user == "0"||$lv_user == "1" ||$lv_user == "11" ||$lv_user >= "99"){  ?>
            <div class="card inCard">
                <div class="card-warning card-block">
                  <h5><small style="color:#fff;">รออนุมัติ</small></h5>
                  <?php
                    $wait = DB::table('tb_booking')->where('bk_status' ,'=', 'wait')->where('emp_id','=',$emp_id)->count();
                  ?>
                  <h2>{{ $wait }}<small><small><small style="color:#fff;"> รายการ</small></small></small></h2>

                </div>
                <div class="card-block text-right">
                    <a href="/wait" class="btn btn-sm btn-warning">ไปยังการจอง <span class="fa fa-angle-right" style="color:#fff;"></span></a>
                </div>
            </div>
            <?php // }  ?>
            <div class="card inCard">
                  <input type="hidden" id="lv" value="<?php echo $lv_user; ?>">
                <div class="card-info card-block">
                  <h5><small style="color:#fff;">รอการจัดรถ</small></h5>
                  <?php

                    $approve = DB::table('tb_booking')->where('bk_status' ,'=', 'approve')->where('emp_id','=',$emp_id)->count();

                  ?>
                  <h2>{{ $approve }}<small><small><small style="color:#fff;"> รายการ</small></small></small></h2>

                </div>

                <div class="card-block text-right">
                    <a href="/approve" class="btn btn-sm btn-info"> {{ $buttom }} <span class="fa fa-angle-right" style="color:#fff;"></span></span></a>
                </div>
                </div>
              </div>
            </div>
        </div>

        <?php

          $sqlbooking = DB::table('tb_booking')->where('tb_booking.emp_id', '=', $emp_id)->orderBy('bk_id', 'desc')->limit(5)->get();

        ?>
          <div class="col-md-8" >
                  <div class="card ">
                    <div class="dash-headder">

                        <span class="fa fa-list" style="color:#000;">&nbsp;&nbsp;<label> กิจกรรมของฉัน </label></span>


                    </div>
                    <br>


                  @foreach ($sqlbooking as $b)
                    <?php  if(count($sqlbooking) > 0){ ?>

                    <?php  }
                     ?>

                      <table class="table table-sm table-dash">

                        <tbody>
                          <?php

                            if(count($sqlbooking) > 0){

                              if($b->bk_status == "approve"){
                                    $bg = "bg-info";
                                    $width ="60%";
                              }else if($b->bk_status == "wait"){
                                    $bg = "bg-warning";
                                    $width ="30%";
                              }else if($b->bk_status == "success"){
                                    $bg = "bg-success";
                                    $width ="100%";
                              }else if($b->bk_status == "merge"){
                                    $bg = "bg-success";
                                    $width ="100%";
                              }else if($b->bk_status == "complete"){
                                    $bg = "bg-success";
                                    $width ="100%";
                              }else{
                                    $bg = "bg-danger";
                                    $width ="100%";
                              }
                            ?>
                              <tr class="detailBk" data-id="bk={{$b->bk_id}}">
                                <td style="border-right:solid 1px #ccc;" width="20%;">
                                  <div class="tdhead {{$bg}}">
                                    <?php
                                    $status = "";
                                    if($b->bk_status == "wait"){
                                            $status = "รอการอนุมัติ";
                                    }elseif ($b->bk_status == "approve"){
                                            $status = "รอการจัดรถ";
                                    }elseif ($b->bk_status =="success"){
                                            $status = "สำเร็จ";
                                    }elseif ($b->bk_status =="merge"){
                                            $status = "สำเร็จ(ร่วมเดินทาง)";
                                    }elseif ($b->bk_status =="complete"){
                                            $status = "ดำเนินการเสร็จสิ้น";
                                    }elseif ($b->bk_status =="eject"){
                                            $status = "ยกเลิกการจอง";
                                    }else if($status == "ejectcar"){
                                            $status = "ยกเลิกการเดินทาง";
                                    }elseif ($b->bk_status =="nonecar"){
                                            $status = "ไม่มีรถ";
                                    }
                                     ?>
                                     {{$status}}
                                  </div>
                                </td>
                                <td>
                                  <h5 style="color:#000;">{{ $b->bk_id }}</h5>
                                  <small style="color:#000;">
                                    <?php
                                        $y = substr($b->bk_date,0,4);
                                        $m = substr($b->bk_date,5,2);
                                        $d = substr($b->bk_date,8,2);
                                        $h = substr($b->bk_date,11,2);
                                        $i = substr($b->bk_date,14,2);
                                     ?>
                                      {{ "วันที่จอง ".$d."/".$m."/".$y." เวลา ".$h.":".$i." น." }}
                                  </small>
                                </td>
                                <td width="15%">
                                  <div class="progress">
                                    <div class="progress-bar {{$bg}}" role="progressbar" style="width:{{$width}}">
                                    </div>
                                  </div>
                                </td>
                              </tr>

                          <?php
                          }else{ ?>
                              <tr>
                                  <td colspan="5" align="center"><h5>ไม่พบกิจกรรม</h5></td>
                              </tr>
                          <?php }?>
                                  @endforeach

                        </tbody>

                      </table>
                      <div  class="offset-md-9 col-md-3" align="right">
                          <a href="/booking" class="btn btn-sm btn-primary">
                            <small style="color:#fff;">ดูทั้งหมด</small> <span class="fa fa-angle-right" style="color:#fff;"></span></a>
                      </div>
                    <p>


                  </div>

              </div>

    </div>
</div>
    <div class="modal-area"></div>

    <script type="text/javascript">
    $(".detailBk").click(function(){
     var id = $(this).data("id");
    $.ajax({
        url:"/detail",
        data:id,
        type:"GET",
        success:function(data){
            $(".modal-area").html(data);
            $("#modalBk").modal("show");
        }
      })
    });
    </script>

@endsection
